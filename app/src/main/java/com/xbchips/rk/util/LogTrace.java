package com.xbchips.rk.util;

import android.util.Log;

import com.xbchips.rk.BuildConfig;


public class LogTrace {
    private static final boolean IS_DEBUG = BuildConfig.LOG_DEBUG;

    public static void i(String TAG, String msg) {
        if (IS_DEBUG)
            Log.i(TAG + "-->", msg);
    }

    public static void d(String TAG, String msg) {
        if (IS_DEBUG)
            Log.d(TAG + "-->", msg);
    }

    public static void w(String TAG, String msg) {
        if (IS_DEBUG)
            Log.w(TAG + "-->", msg);
    }

    public static void e(String TAG, String msg) {
        if (IS_DEBUG)
            Log.e(TAG + "-->", msg);
    }

    public static void v(String TAG, String msg) {
        if (IS_DEBUG)
            Log.v(TAG + "-->", msg);
    }
}
