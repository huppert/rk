package com.xbchips.rk.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.text.TextUtils;

import com.xbchips.rk.XvideoApplication;

import java.io.File;
import java.util.UUID;

/**
 * Created by york on 2017/8/4.
 */

public class UuidUtil {
    private static String uuid;
    private static final String PREFS_FILE = "device_id.xml";
    private static final String PREFS_DEVICE_ID = "device_id";

    public static String getUUid() {
        if (uuid == null) {
            String path = Environment.getExternalStorageDirectory() + "/Xvideo";
            File file = new File(path, "ZGV2aWNlX2lk");
            if (!file.exists()) {
                final SharedPreferences prefs = XvideoApplication.getApplication()
                        .getSharedPreferences(PREFS_FILE, 0);
                uuid = prefs.getString(PREFS_DEVICE_ID, null);
                if (TextUtils.isEmpty(uuid)) {
                    uuid = UUID.randomUUID().toString();
                }
                FileIOUtils.writeFileFromString(file.getPath(), uuid);
            } else {
                uuid = FileIOUtils.readFile2String(file.getPath());
            }
        }
        return uuid;
    }

    public static String getVersionName(Context context) throws Exception {
        // 获取packagemanager的实例
        PackageManager packageManager = context.getPackageManager();
        // getPackageName()是你当前类的包名，0代表是获取版本信息
        PackageInfo packInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
        String version = packInfo.versionName;
        return version;
    }
}
