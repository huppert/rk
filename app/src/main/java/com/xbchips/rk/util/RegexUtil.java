package com.xbchips.rk.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by york on 2017/8/4.
 * 正则表达式工具类
 */

public class RegexUtil {
    /**
     * 电话号码
     * 精确匹配，note:可能新出的号码没加上
     */
    public static final String MOBILE_REGEXP = "^((13[0-9])|(14[5|7])|(15[0|1|2|3|5|6|7|8|9])|(17[6|7|8])|18[0-9])\\d{8}|(170[0|5|9]\\d{7})$";
    /**
     * 电话号码
     */
    public static final String PHONE_NUMBER_REGEXP = "^[\\d]{11}$";

    /**
     * 大陆手机号码11位数，匹配格式：前三位固定格式+后8位任意数
     * 此方法中前三位格式有：
     * 13+任意数
     * 15+除4的任意数
     * 18+除1和4的任意数
     * 17+除9的任意数
     * 147
     */
    public static final String CHINA_NUMBER = "^((13[0-9])|(15[^4])|(18[0,2,3,5-9])|(17[0-8])|(147))\\d{8}$";

    /**
     * 香港手机号码8位数，5|6|8|9开头+7位任意数
     */
    public static final String HK_NUMBER = "^(5|6|8|9)\\d{7}$";
    /**
     * 密码
     */
    public static final String PWD_REGEXP = "^[0-9A-Za-z]{6,12}$";
    /**
     * 验证码
     */
    public static final String VERIFY_CODE = "^[\\d]{6}$";
    /**
     * 两位小数
     */
    public static final String DECIMAL = "^\\d+(?:\\.\\d{1,2})?$";

    /**
     * username
     */
    //		String regExp = "^(?![0-9]+$)[0-9A-Za-z]{6,20}$";
    public static final String USERNAME_REGEXP = "^[0-9A-Za-z]{4,12}$";

    /**
     * nickname
     */
    public static final String NICKNAME_REGEXP = "^[0-9A-Za-z\\u4e00-\\u9fa5]{2,12}$";

    /**
     * name
     */
    public static final String NAME_REGEXP = "^[A-Za-z\\u4e00-\\u9fa5]{2,20}$";
    /**
     * email
     */
    public static final String EMAIL_REGEXP = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";


    public static final String NUMBER = "\\d+";

    /**
     * @param pattern 正则表达式
     * @param input   要匹配的字符串
     * @return 匹配为true，否则false
     */
    public static boolean match(String pattern, String input) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(input);
        return m.find();
    }

    /**
     * 取出符合规则的字符串
     *
     * @param pattern 正则表达式
     * @param input   输入字符串
     * @return 过虑的字符串
     */
    public static String group(String pattern, String input) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(input);
        if (m.find()) {
            return m.group();
        }
        return null;
    }


}