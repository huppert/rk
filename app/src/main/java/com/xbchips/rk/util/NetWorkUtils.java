package com.xbchips.rk.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import com.xbchips.rk.XvideoApplication;

/**
 * 网络工具类
 */
public class NetWorkUtils {
    public static final int NET_TYPE_NONE = 0;
    public static final int NET_TYPE_WIFI = 1;
    public static final int NET_TYPE_2G = 2;
    public static final int NET_TYPE_3G = 3;
    public static final int NET_TYPE_4G = 4;

    /**
     * 获取当前的网络状态
     * 没有网络=0
     * WIFI网络=1
     * 4G网络=4
     * 3G网络=3
     * 2G网络=2
     */
    public static int getAPNType(Context context) {
        //结果返回值
        int netType = NET_TYPE_NONE;
        //获取手机所有连接管理对象
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        //获取NetworkInfo对象
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        //NetworkInfo对象为空 则代表没有网络
        if (networkInfo == null) {
            return netType;
        }
        //否则 NetworkInfo对象不为空 则获取该networkInfo的类型
        int nType = networkInfo.getType();
        if (nType == ConnectivityManager.TYPE_WIFI) {
            //WIFI
            netType = NET_TYPE_WIFI;
        } else if (nType == ConnectivityManager.TYPE_MOBILE) {
            int nSubType = networkInfo.getSubtype();
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            //3G   联通的3G为UMTS或HSDPA 电信的3G为EVDO
            if (nSubType == TelephonyManager.NETWORK_TYPE_LTE
                    && !telephonyManager.isNetworkRoaming()) {
                netType = NET_TYPE_4G;
            } else if (nSubType == TelephonyManager.NETWORK_TYPE_UMTS
                    || nSubType == TelephonyManager.NETWORK_TYPE_HSDPA
                    || nSubType == TelephonyManager.NETWORK_TYPE_EVDO_0
                    && !telephonyManager.isNetworkRoaming()) {
                netType = NET_TYPE_3G;
                //2G 移动和联通的2G为GPRS或EGDE，电信的2G为CDMA
            } else if (nSubType == TelephonyManager.NETWORK_TYPE_GPRS
                    || nSubType == TelephonyManager.NETWORK_TYPE_EDGE
                    || nSubType == TelephonyManager.NETWORK_TYPE_CDMA
                    && !telephonyManager.isNetworkRoaming()) {
                netType = NET_TYPE_2G;
            } else {
                netType = NET_TYPE_2G;
            }
        }
        return netType;
    }


    public static String getNetWorkTypeString() {
        int type = getAPNType(XvideoApplication.getApplication());
        switch (type) {
            case NET_TYPE_NONE:
                return "none";
            case NET_TYPE_WIFI:
                return "wifi";
            case NET_TYPE_2G:
                return "2g";
            case NET_TYPE_3G:
                return "3g";
            case NET_TYPE_4G:
                return "4g";
            default:
                return "none";
        }

    }

}