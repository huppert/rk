package com.xbchips.rk.util;

import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;

import com.xbchips.rk.XvideoApplication;
import com.xbchips.rk.common.Constants;

import java.io.File;

/**
 * 文件下载
 */

public class DownloadUtil {
    private Context mContext;
    private DownloadManager downloadManager;

    public DownloadUtil() {
        this.mContext = XvideoApplication.getApplication();
        downloadManager = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);

    }

    /**
     * 下载
     *
     * @param uri 文件路径
     * @return taskId
     */
    public long downLoad(String uri, String filename) {

        filename = filename + ".apk";

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(uri));
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setTitle(filename);
        request.setMimeType(Constants.APK_MIME_TYPE);
        request.setDescription("正在下载...");
        request.allowScanningByMediaScanner();
        File file = new File(mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), filename);
        if (file.exists()) {
            file.delete();
        }
        //路径 名称
        request.setDestinationInExternalFilesDir(mContext, Environment.DIRECTORY_DOWNLOADS, filename);

        return downloadManager.enqueue(request);

    }

/** --------------------------------------------------------------------------------------------------------------------------------------*/


    /**
     * 删除下载任务
     *
     * @param taskIds downloadIds
     */
    public void remove(long... taskIds) {
        downloadManager.remove(taskIds);
    }

    /**
     * 下载进度状态查询
     *
     * @param downloadId 下载ID
     * @return index(0 当前进度 1 总长度 2 下载状态)
     */
    public int[] getBytesAndStatus(long downloadId) {
        int[] bytesAndStatus = new int[]{-1, -1, 0};
        DownloadManager.Query query = new DownloadManager.Query().setFilterById(downloadId);
        Cursor c = null;
        try {
            c = downloadManager.query(query);
            if (c != null && c.moveToFirst()) {
                bytesAndStatus[0] = c.getInt(c.getColumnIndexOrThrow(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                bytesAndStatus[1] = c.getInt(c.getColumnIndexOrThrow(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                bytesAndStatus[2] = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bytesAndStatus;
    }

    public Uri getUriForDownloadedFile(long id) {
        return downloadManager.getUriForDownloadedFile(id);
    }
}

