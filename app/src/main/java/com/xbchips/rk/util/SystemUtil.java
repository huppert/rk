package com.xbchips.rk.util;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.xbchips.rk.XvideoApplication;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.system.entity.Configs;

import io.reactivex.Observable;

/**
 * Created by york on 2017/11/3.
 */

public class SystemUtil {


    /**
     * 过滤敏感字符
     *
     * @param text 目标字符串
     * @return 匹配的敏感字符，否则null
     */
    public static String checkSensitiveWords(String text) {
        String json = PreferenceUtils.getString(XvideoApplication.getApplication(), Constants.PREF_CONFIG);
        return Observable.just(json)
                .filter(s -> !TextUtils.isEmpty(s))
                .flatMapIterable(json1 -> new Gson().fromJson(json1, Configs.class).getKeyword_masking().getContent())
                .filter(text::contains)
                .blockingFirst(null);
    }
}
