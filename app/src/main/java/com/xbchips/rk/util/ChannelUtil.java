package com.xbchips.rk.util;

import android.text.TextUtils;

import com.leon.channel.helper.ChannelReaderUtil;
import com.xbchips.rk.XvideoApplication;

public class ChannelUtil {

    public static String getChannel() {
        String channel = ChannelReaderUtil.getChannel(XvideoApplication.getApplication());
        if (TextUtils.isEmpty(channel)) {
            return "server";
        }
        return channel;
    }
}
