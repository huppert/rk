package com.xbchips.rk.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by york on 2017/8/7.
 */

public class DateUtil {
    public static final String PATTERN_YYYYMMDD = "yyyy-MM-dd";
    private static final long ONE_DAY = 24 * 60 * 60 * 1000;

    public static String format(long mills) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(PATTERN_YYYYMMDD, Locale.getDefault());
        return dateFormat.format(new Date(mills));
    }


    public static Date parseDate(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(PATTERN_YYYYMMDD, Locale.getDefault());
        try {
            return dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int calcDays(long mills) {
        long difference = mills - System.currentTimeMillis();
        if (difference < 0) {
            return 0;
        } else {
            return (int) Math.floor(difference / ONE_DAY);
        }
    }


    public static String transformTime(long seconds) {
        String result;
        if (seconds < 60) {
            result = seconds + "秒";
        } else if (seconds < 60 * 60) {
            result = seconds / 60 + "分钟";
        } else if (seconds < 24 * 60 * 60) {
            result = seconds / (60 * 60) + "小时";
        } else {
            result = seconds / (24 * 60 * 60) + "天";
        }
        return result;
    }
}
