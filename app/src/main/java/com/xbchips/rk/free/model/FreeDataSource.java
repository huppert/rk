package com.xbchips.rk.free.model;

import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.free.entity.FreeResponse;
import com.xbchips.rk.free.entity.Message;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by york on 2017/8/1.
 */

public class FreeDataSource {
    private FreeService mService;

    public FreeDataSource() {
        mService = HttpRequest.createService(FreeService.class);
    }

    public Observable<ObjectResponse<FreeResponse>> loadList(int page, int pageSize) {
        return mService.loadList(page, pageSize);
    }

    public Observable<ObjectResponse<List<Message>>> lodMsgs() {
        return mService.getMsgs();
    }
}
