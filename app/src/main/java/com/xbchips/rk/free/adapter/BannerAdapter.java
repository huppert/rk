package com.xbchips.rk.free.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.asksira.loopingviewpager.LoopingPagerAdapter;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.xbchips.rk.R;
import com.xbchips.rk.activities.HtmlActivity;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.free.entity.Banner;

import java.util.List;

import static com.xbchips.rk.activities.HtmlActivity.ARG_TITLE;
import static com.xbchips.rk.activities.HtmlActivity._URL;

public class BannerAdapter extends LoopingPagerAdapter<Banner> {
    private final RequestOptions mOptions;
    private Context mContext;
    private ViewGroup.LayoutParams mLayoutParams;
    private List<Banner> mList;

    public BannerAdapter(Context context, List<Banner> itemList, boolean isInfinite) {
        super(context, itemList, isInfinite);
        mContext = context;
        mList = itemList;
        mLayoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .placeholder(R.drawable.place_holder2)
                .dontAnimate();
    }


    @Override
    protected View inflateView(int viewType, ViewGroup container, int listPosition) {
        ImageView imageView = new ImageView(mContext);
        imageView.setId(R.id.free_banner_id);
        imageView.setLayoutParams(mLayoutParams);
        return imageView;
    }

    @Override
    protected void bindView(View convertView, int position, int viewType) {
        Banner banner = mList.get(position);
        ImageView imageView = convertView.findViewById(R.id.free_banner_id);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        ImageLoader.load(mContext, banner.getThumb_img_url(), imageView, mOptions);
        imageView.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(banner.getUrl())) {
                Intent intent = new Intent(mContext, HtmlActivity.class);
                intent.putExtra(ARG_TITLE, banner.getTitle());
                intent.putExtra(_URL, banner.getUrl());
                mContext.startActivity(intent);
            }
        });
    }


}
