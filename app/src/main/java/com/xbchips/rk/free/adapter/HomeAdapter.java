package com.xbchips.rk.free.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.util.DisplayUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by york on 2017/7/27.
 */

public class HomeAdapter extends BaseAdapter<VideoItem> {

    private Context mContext;
    private RequestOptions mOptions;
    private int bottomMargin;

    public HomeAdapter(List<VideoItem> list, Context context) {
        super(list, context);
        mContext = context;
        mOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .placeholder(R.drawable.place_holder2)
                .dontAnimate();
        bottomMargin = DisplayUtils.dip2px(context, 10);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_FOOTER:
                return getFooterViewHolder(parent);
            case TYPE_HEADER:
                return new HeaderViewHolder(mHeaderView);
            case TYPE_ITEM:
                return new FreeViewHolder(mInflater.inflate(R.layout.item_home, parent, false));
        }
        return null;
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        FreeViewHolder viewHolder = (FreeViewHolder) holder;
        VideoItem videoItem = mList.get(getRealPosition(position));
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) viewHolder.imageLayout.getLayoutParams();

        if (videoItem.getIs_advert() == 1) {
            layoutParams.dimensionRatio = "25:7";
            viewHolder.imageLayout.setScaleType(ImageView.ScaleType.FIT_XY);
            ImageLoader.load(mContext, videoItem.getImg_url(), viewHolder.imageLayout, mOptions);
            viewHolder.tvMovieName.setVisibility(View.GONE);
            viewHolder.llBgMask.setVisibility(View.GONE);
            viewHolder.tvWatchCount.setVisibility(View.GONE);
            viewHolder.tvLikeCount.setVisibility(View.GONE);
        } else {
            layoutParams.dimensionRatio = "25:14";
            viewHolder.tvMovieName.setText(videoItem.getTitle());
            ImageLoader.load(mContext, videoItem.getThumb_img_url(), viewHolder.imageLayout, R.drawable.place_holder2);
            viewHolder.tvWatchCount.setText(mContext.getString(R.string.watch_times, videoItem.getViews()));
            viewHolder.tvLikeCount.setText(String.valueOf(videoItem.getLike()));
            viewHolder.tvMovieTime.setText(videoItem.getUpdated_at());//日期
            viewHolder.tvMovieName.setVisibility(View.VISIBLE);
            viewHolder.llBgMask.setVisibility(View.VISIBLE);
            viewHolder.tvWatchCount.setVisibility(View.VISIBLE);
            viewHolder.tvLikeCount.setVisibility(View.VISIBLE);
            viewHolder.llBgMask.setBackgroundColor(mContext.getResources().getColor(R.color.color_1A));
        }
        viewHolder.imageLayout.setOnClickListener(mOnClickListener);
        viewHolder.imageLayout.setLayoutParams(layoutParams);
        viewHolder.imageLayout.setTag(R.id.glideId, videoItem);

    }


    static class FreeViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_layout)
        ImageView imageLayout;
        @BindView(R.id.tv_movie_time)
        TextView tvMovieTime;
        @BindView(R.id.ll_bg_mask)
        FrameLayout llBgMask;
        @BindView(R.id.tv_movie_name)
        TextView tvMovieName;
        @BindView(R.id.tv_watch_count)
        TextView tvWatchCount;
        @BindView(R.id.tv_like_count)
        TextView tvLikeCount;

        public FreeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
