package com.xbchips.rk.free.entity;

/**
 * Created by york on 2017/8/1.
 */

public class VideoItem {
    protected String id;
    protected String title;
    protected String thumb_img_url;
    protected int like;
    protected int views;
    protected String time;
    protected String url;
    protected String desc;
    protected String rank;
    protected String img_url;
    protected int is_advert;
    protected String updated_at;
    protected String username;
    protected int price;
    protected boolean is_liuxiang;

    public VideoItem() {
    }

    public VideoItem(String id, String title, String thumb_img_url, int like, int views) {
        this.id = id;
        this.title = title;
        this.thumb_img_url = thumb_img_url;
        this.like = like;
        this.views = views;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public int getIs_advert() {
        return is_advert;
    }

    public void setIs_advert(int is_advert) {
        this.is_advert = is_advert;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumb_img_url() {
        return thumb_img_url;
    }

    public void setThumb_img_url(String thumb_img_url) {
        this.thumb_img_url = thumb_img_url;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public String getUsername() {
        return username;
    }

    public int getPrice() {
        return price;
    }

    public String getTime() {
        return time;
    }

    public boolean isIs_liuxiang() {
        return is_liuxiang;
    }
}
