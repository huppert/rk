package com.xbchips.rk.free.widget;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.xbchips.rk.R;
import com.xbchips.rk.activities.HtmlActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.xbchips.rk.activities.HtmlActivity._URL;

/**
 * Created by york on 2017/7/27.
 * 免费-头部轮播图
 */

public class FreeHeaderLayout extends LinearLayout {
    /**
     * 当前轮播消息对应url
     */
    private String mCurrentMsgUrl;
    @BindView(R.id.text_switcher)
    public TextSwitcher mTextSwitcher;

    public FreeHeaderLayout(Context context) {
        super(context);
        setOrientation(VERTICAL);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_free_header, this);
        ButterKnife.bind(this, view);

    }


    /**
     * 消息通知
     */
    public void initTextSwitcher() {
        mTextSwitcher.setFactory(() -> {
            TextView t = new TextView(getContext());
            t.setId(R.id.switcher_text_id);
            t.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
            t.setTextColor(ContextCompat.getColor(getContext(), R.color.marquee_text_color));
            t.setTextSize(12);
            t.setMaxLines(1);
            t.setEllipsize(TextUtils.TruncateAt.END);
            t.setClickable(true);
            t.setOnClickListener(mOnClickListener);
            return t;
        });
        Animation in = AnimationUtils.loadAnimation(getContext(), R.anim.bottom_in);
        Animation out = AnimationUtils.loadAnimation(getContext(), R.anim.top_out);
        mTextSwitcher.setInAnimation(in);
        mTextSwitcher.setOutAnimation(out);
    }


    private OnClickListener mOnClickListener = v -> {
        if (!TextUtils.isEmpty(mCurrentMsgUrl)) {
            Intent in = new Intent(getContext(), HtmlActivity.class);
            in.putExtra(_URL, mCurrentMsgUrl);
            getContext().startActivity(in);
        }
    };

    public void setCurrentMsgUrl(String currentMsgUrl) {
        mCurrentMsgUrl = currentMsgUrl;
    }
}
