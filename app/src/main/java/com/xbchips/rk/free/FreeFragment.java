package com.xbchips.rk.free;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.asksira.loopingviewpager.LoopingViewPager;
import com.xbchips.rk.R;
import com.xbchips.rk.activities.HtmlActivity;
import com.xbchips.rk.base.BaseFragment;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.free.adapter.BannerAdapter;
import com.xbchips.rk.free.adapter.HomeAdapter;
import com.xbchips.rk.free.entity.Banner;
import com.xbchips.rk.free.entity.FreeResponse;
import com.xbchips.rk.free.entity.Message;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.free.widget.FreeHeaderLayout;
import com.xbchips.rk.video.VideoDetailActivity;
import com.xbchips.rk.widgets.CustomSwipeRefreshLayout;
import com.xbchips.rk.widgets.EndlessRecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

import static com.xbchips.rk.activities.HtmlActivity.ARG_TITLE;
import static com.xbchips.rk.activities.HtmlActivity._URL;

/**
 * Created by york on 2017/7/26.
 */

public class FreeFragment extends BaseFragment implements FreeContract.IFreeView {
    @BindView(R.id.recycler_view)
    EndlessRecyclerView mRecyclerView;
    @BindView(R.id.refresh_layout)
    CustomSwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.loop_view_pager)
    LoopingViewPager loopViewPager;

    private FreeHeaderLayout mHeaderLayout;
    private List<VideoItem> mList = new ArrayList<>();
    /**
     * 轮播消息
     */
    private List<Message> mMessages = new ArrayList<>();
    private HomeAdapter mAdapter;
    private FreeContract.IFreePresenter mPresenter = new FreePresenter(this);
    private boolean mInitialized;
    private boolean mPaused;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_free, container, false);
        mUnBinder = ButterKnife.bind(this, rootView);
        init();
        return rootView;
    }


    @Override
    public void onPause() {
        super.onPause();
        mPaused = true;
        loopViewPager.pauseAutoScroll();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPaused = false;
        loopViewPager.resumeAutoScroll();
    }

    @Override
    protected void init() {
        isInit = true;
        initRecyclerView();
        mHeaderLayout.initTextSwitcher();
        mPresenter.loadData(mRecyclerView.getCurPage(), Constants.DEFAULT_PAGESIZE);
        mPresenter.loadMsgs();
    }


    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mHeaderLayout = new FreeHeaderLayout(getContext());
        mHeaderLayout.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mAdapter = new HomeAdapter(mList, getContext());
        mAdapter.setHeaderView(mHeaderLayout);
        mAdapter.setOnClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setOnLoadMoreListener(() -> {
            mAdapter.addFooter();
            mPresenter.loadData(mRecyclerView.getCurPage(), Constants.DEFAULT_PAGESIZE);
        });
        mRefreshLayout.setOnRefreshListener(() -> {
            mRecyclerView.setCurPage(1);
            mRecyclerView.setLoading(true);
            mPresenter.refresh(Constants.DEFAULT_PAGESIZE);
        });
    }

    private void initBanners(Banner[] banners) {
        loopViewPager.setAdapter(new BannerAdapter(mContext, Arrays.asList(banners), true));
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.image_layout:
                VideoItem videoItem = (VideoItem) v.getTag(R.id.glideId);
                if (videoItem.getIs_advert() == 1) {
                    Intent intent = new Intent(getContext(), HtmlActivity.class);
                    intent.putExtra(ARG_TITLE, videoItem.getTitle());
                    intent.putExtra(_URL, videoItem.getUrl());
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getContext(), VideoDetailActivity.class);
                    intent.putExtra(VideoDetailActivity.ARG_VIDEO_ID, videoItem.getId());
                    intent.putExtra(VideoDetailActivity.ARG_VIDEO_TITLE, videoItem.getTitle());
                    intent.putExtra("isFromFree", true);
                    startActivity(intent);
                }

                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.unSubscribe();
    }

    @Override
    public void setPresenter(FreeContract.IFreePresenter presenter) {
    }


    @Override
    public void setData(FreeResponse data) {
        int size = data.getList().size();
        mList.addAll(data.getList());
        mRecyclerView.setPageCount(data.getPage().getTotal());
        mAdapter.notifyItemRangeInserted(mList.size() - size + mAdapter.getHeaderSize(), size);
        if (mRecyclerView.isLastPage()) {
            mAdapter.addNoMoreView();
        }
        if (mRecyclerView.getCurPage() == 1) {
            initBanners(data.getBanner());
        }
        if (!mInitialized) {
            mInitialized = true;
        }
    }

    @Override
    public void pullRefreshCompleted(boolean success, FreeResponse data) {
        mList.clear();
        mList.addAll(data.getList());
        mRecyclerView.setPageCount(data.getPage().getTotal());
        mAdapter.notifyDataSetChanged();
        mAdapter.removeFooter();
        if (mRecyclerView.isLastPage()) {
            mAdapter.addNoMoreView();
        }
        if (mRecyclerView.getCurPage() == 1) {
            initBanners(data.getBanner());
        }
        mRecyclerView.setLoading(false);
        mRefreshLayout.setRefreshing(false);
    }

    @Override
    public void loadMore(FreeResponse data) {
        mAdapter.removeFooter();
        setData(data);
        mRecyclerView.setLoading(false);
    }

    @Override
    public void processError(Throwable e) {
        handleFailure(e);
        mRecyclerView.setLoading(false);
        mRefreshLayout.setRefreshing(false);
        if (mRecyclerView.getCurPage() > 1) {
            mRecyclerView.pageDecrement();
            mAdapter.removeFooter();
        }
    }

    @Override
    public void updateTextSwitcher(List<Message> data) {
        mMessages.addAll(data);
        Disposable disposable = Flowable.interval(0, 3, TimeUnit.SECONDS)
                .filter(aLong -> !mPaused && isVisible())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> {
                    Message message = mMessages.get(aLong.intValue() % mMessages.size());
                    mHeaderLayout.mTextSwitcher.setText(message.getTitle());
                    mHeaderLayout.setCurrentMsgUrl(message.getUrl());
                });
        mCompositeDisposable.add(disposable);
    }

}
