package com.xbchips.rk.free;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.util.DisplayUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by york on 2017/7/27.
 */

public class NewFreeAdapter extends BaseAdapter<VideoItem> {


    private boolean markEnable;
    private int mSpace;
    private Context mContext;
    private RequestOptions mOptions;

    public NewFreeAdapter(List<VideoItem> list, Context context) {
        super(list, context);
        mSpace = DisplayUtils.dip2px(context, 5);
        mContext = context;
        mOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .placeholder(R.drawable.place_holder2)
                .dontAnimate();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_FOOTER:
                return getFooterViewHolder(parent);
            case TYPE_HEADER:
                return new HeaderViewHolder(mHeaderView);
            case TYPE_ITEM:
                return new FreeViewHolder(mInflater.inflate(R.layout.item_new_free, parent, false));
        }
        return null;
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        FreeViewHolder viewHolder = (FreeViewHolder) holder;
        VideoItem videoItem = mList.get(getRealPosition(position));
        viewHolder.mImageView.setOnClickListener(mOnClickListener);
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) viewHolder.mImageView.getLayoutParams();
        layoutParams.leftMargin = layoutParams.rightMargin = mSpace;
        //广告
        if (videoItem.getIs_advert() == 1) {
            layoutParams.dimensionRatio = "11:3";
            viewHolder.mImageView.setScaleType(ImageView.ScaleType.FIT_XY);
            ImageLoader.load(mContext, videoItem.getImg_url(), viewHolder.mImageView, mOptions);
            viewHolder.mTvPlayCount.setVisibility(View.GONE);
            viewHolder.mTvMovieName.setVisibility(View.GONE);
        } else {
            layoutParams.dimensionRatio = "25:14";
            viewHolder.mTvMovieName.setVisibility(View.VISIBLE);
            viewHolder.mTvMovieName.setText(videoItem.getTitle());
            viewHolder.mTvPlayCount.setVisibility(View.VISIBLE);
            ImageLoader.load(mContext, videoItem.getThumb_img_url(), viewHolder.mImageView, R.drawable.place_holder2);
            viewHolder.mTvPlayCount.setText(mContext.getString(R.string.watch_times, videoItem.getViews()));
            viewHolder.tvMovieTime.setText(videoItem.getUpdated_at());//日期
        }
        viewHolder.mImageView.setLayoutParams(layoutParams);
        viewHolder.mImageView.setTag(R.id.glideId, videoItem);

    }


    static class FreeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_layout)
        ImageView mImageView;
        @BindView(R.id.tv_movie_name)
        TextView mTvMovieName;
        @BindView(R.id.tv_play_count)
        TextView mTvPlayCount;
        @BindView(R.id.tv_movie_time)
        TextView tvMovieTime;

        public FreeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
