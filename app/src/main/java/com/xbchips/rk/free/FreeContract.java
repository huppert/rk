package com.xbchips.rk.free;

import com.xbchips.rk.base.BasePresenter;
import com.xbchips.rk.base.BaseView;
import com.xbchips.rk.free.entity.FreeResponse;
import com.xbchips.rk.free.entity.Message;

import java.util.List;

/**
 * Created by york on 2017/8/1.
 */

public class FreeContract {
    interface IFreeView extends BaseView<IFreePresenter> {
        void setData(FreeResponse data);

        void pullRefreshCompleted(boolean success, FreeResponse data);

        void loadMore(FreeResponse data);

        void processError(Throwable throwable);

        void updateTextSwitcher(List<Message> data);
    }

    interface IFreePresenter extends BasePresenter {
        void loadData(int page, int pageSize);

        void refresh(int pageSize);

        void loadMsgs();
    }
}
