package com.xbchips.rk.free;

import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.free.entity.FreeResponse;
import com.xbchips.rk.free.entity.Message;
import com.xbchips.rk.free.model.FreeDataSource;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by york on 2017/8/1.
 */

public class FreePresenter implements FreeContract.IFreePresenter {
    private FreeContract.IFreeView mFreeView;
    private FreeDataSource mDataSource;
    private CompositeDisposable mCompositeDisposable;

    public FreePresenter(FreeContract.IFreeView freeView) {
        mFreeView = freeView;
        mDataSource = new FreeDataSource();
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void loadData(final int page, int pageSize) {
        Consumer<ObjectResponse<FreeResponse>> onNext = response -> {
            if (!mFreeView.handleResponse(response)) {
                FreeResponse data = response.getData();
                if (page == 1)
                    mFreeView.setData(data);
                else
                    mFreeView.loadMore(data);
            }
        };
        Consumer<Throwable> onError = new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable throwable) throws Exception {
                mFreeView.processError(throwable);
            }
        };
        mCompositeDisposable.add(mDataSource.loadList(page, pageSize)
                .compose(RxUtils.applySchedulers())
                .subscribe(onNext, onError)
        );
    }

    @Override
    public void refresh(int pageSize) {
        Disposable disposable = mDataSource.loadList(1, pageSize)
                .compose(RxUtils.<ObjectResponse<FreeResponse>>applySchedulers())
                .subscribe(new Consumer<ObjectResponse<FreeResponse>>() {
                    @Override
                    public void accept(@NonNull ObjectResponse<FreeResponse> response) throws Exception {
                        if (!mFreeView.handleResponse(response)) {
                            mFreeView.pullRefreshCompleted(true, response.getData());
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        mFreeView.processError(throwable);
                    }
                });
        mCompositeDisposable.add(disposable);
    }


    @Override
    public void loadMsgs() {
        Disposable disposable = mDataSource.lodMsgs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(response -> !mFreeView.handleResponse(response))
                .subscribe(response -> {
                    List<Message> data = response.getData();
                    if (data != null && data.size() > 0) {
                        mFreeView.updateTextSwitcher(data);
                    }
                }, throwable -> mFreeView.processError(throwable));
        mCompositeDisposable.add(disposable);
    }


    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }
}
