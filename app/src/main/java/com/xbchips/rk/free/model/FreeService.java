package com.xbchips.rk.free.model;

import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.free.entity.FreeResponse;
import com.xbchips.rk.free.entity.Message;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by york on 2017/8/1.
 */
public interface FreeService {

    /**
     * 免费视频列表
     */
    @GET("video/list/free")
    Observable<ObjectResponse<FreeResponse>> loadList(@Query("page") int page, @Query("page_size") int pageSize);


    /**
     * 消息通知列表
     */
    @GET("news")
    Observable<ObjectResponse<List<Message>>> getMsgs();
}