package com.xbchips.rk.free;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.util.DisplayUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by york on 2017/7/27.
 */

public class FavorAdapter extends BaseAdapter<VideoItem> {
    private int mSpace;
    private Context mContext;
    private RequestOptions mOptions;

    public FavorAdapter(List<VideoItem> list, Context context) {
        super(list, context);
        mSpace = DisplayUtils.dip2px(context, 5);
        mContext = context;
        mOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .placeholder(R.drawable.place_holder2)
                .dontAnimate();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_FOOTER:
                return getFooterViewHolder(parent);
            case TYPE_HEADER:
                return new HeaderViewHolder(mHeaderView);
            case TYPE_ITEM:
                return new FreeViewHolder(mInflater.inflate(R.layout.item_favor, parent, false));
        }
        return null;
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        FreeViewHolder viewHolder = (FreeViewHolder) holder;
        VideoItem videoItem = mList.get(getRealPosition(position));
        viewHolder.ivCover.setTag(R.id.glideId, videoItem);
        viewHolder.ivCover.setOnClickListener(mOnClickListener);

        viewHolder.tvTitle.setText(videoItem.getTitle());
        ImageLoader.load(mContext, videoItem.getThumb_img_url(), viewHolder.ivCover);
        viewHolder.tvPlayCount.setText(mContext.getString(R.string.watch_times, videoItem.getViews()));
        viewHolder.tvMovieTime.setText(videoItem.getTime());//日期


    }


    static class FreeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_cover)
        ImageView ivCover;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_movie_time)
        TextView tvMovieTime;
        @BindView(R.id.tv_play_count)
        TextView tvPlayCount;

        public FreeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
