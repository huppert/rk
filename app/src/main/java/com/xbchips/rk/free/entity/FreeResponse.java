package com.xbchips.rk.free.entity;

import com.xbchips.rk.base.response.PageResponse;

/**
 * Created by york on 2017/8/3.
 */

public class FreeResponse extends PageResponse<VideoItem> {

    private Banner banner[];

    public Banner[] getBanner() {
        return banner;
    }

    public void setBanner(Banner[] banner) {
        this.banner = banner;
    }
}
