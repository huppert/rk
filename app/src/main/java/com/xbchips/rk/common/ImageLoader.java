package com.xbchips.rk.common;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.xbchips.rk.R;

/**
 * Created by york on 2017/6/22.
 */

public class ImageLoader {

    private static final RequestOptions DEFAULT_REQUEST_OPTIONS = new RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .centerCrop()
            .placeholder(R.drawable.bg_video_holder)
            .error(R.drawable.bg_video_holder)
            .dontAnimate();

    public static void load(Context context, Object model, ImageView view) {
        view.setScaleType(ImageView.ScaleType.CENTER);
        Glide.with(context)
                .load(model)
                .apply(DEFAULT_REQUEST_OPTIONS)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        view.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        return false;
                    }
                })
                .into(view);
    }

    public static void load(Context context, Object model, ImageView view, RequestOptions options) {
        Glide.with(context)
                .load(model)
                .apply(options)
                .into(view);
    }

    public static void fitCenter(Context context, Object model, ImageView view) {
        load(context, model, view, DEFAULT_REQUEST_OPTIONS.clone()
                .fitCenter()
        );
    }

    public static void loadAvatar(Context context, Object model, ImageView view) {
        load(context, model, view, new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .transform(new CenterCrop(), new CircleCrop())
                .placeholder(R.drawable.ic_avatar_gray)
                .dontAnimate()
        );
    }

    public static void load(Context context, Object model, ImageView view, @DrawableRes int placeholder) {
        load(context, model, view, DEFAULT_REQUEST_OPTIONS.clone()
                .error(placeholder)
                .placeholder(placeholder));
    }


}
