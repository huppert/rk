package com.xbchips.rk.common;

import android.os.Build;

import com.xbchips.rk.BuildConfig;
import com.xbchips.rk.util.NetWorkUtils;


public class ServiceUrlAppend {

    public String appendParameters(String originalUrl, String userId) {
        StringBuilder sb = new StringBuilder(originalUrl);
        sb.append("&enterurl=")
                .append("userid=").append(userId).append("_")
                .append("version=").append(BuildConfig.VERSION_NAME).append("_")
                .append("platform=").append("android").append("_")
                .append("brand=").append(Build.BRAND).append("_")
                .append("model=").append(Build.MODEL).append("_")
                .append("network=").append(NetWorkUtils.getNetWorkTypeString()).append("_")
                .append("channel=").append(Constants.DEFAULT_CHANNEL);
        return sb.toString();
    }
}
