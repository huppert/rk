package com.xbchips.rk.common;

import com.xbchips.rk.BuildConfig;
import com.xbchips.rk.util.ChannelUtil;

/**
 * Created by york on 2017/7/26.
 */

public class Constants {
    public static final String PREF_USER = "PREF_USER";
    public static final String PREF_CONFIG = "pref_config";

    public static final String VIDEO_DOMAINS = "video_domains";
    //    public static final String public static final String isUpdata = "updata"; = "video_domains";
    public static final String isUpdata = "updata";


    public static final String DEFAULT_CHANNEL = ChannelUtil.getChannel();
    public static final String DEFAULT_PLATFORM = "android";
    public static final int DEFAULT_PAGESIZE = 10;
    public static final String SORT_NEW = "new";
    public static final String SORT_DEFAULT = "";
    public static final String SORT_HOTTEST = "views";
    public static final String SORT_COLLECT = "collect";

    public static final String PAY_WAY_WX = "wechat";
    public static final String PAY_WAY_ALIPAY = "alipay";
    public static final String PAY_WAY_WAP = "wap";
    /**
     * 视频详情data
     */
    public static final String VIDEO_DATA = "video_data";
    /**
     * 短信验证码间隔时间
     */
    public static final int CODE_COUNTDOWN_TIME = 300;

    /**
     * 刷新登录状态的action
     */
    public static final String ACTION_REFRESH_STATE =
            BuildConfig.APPLICATION_ID + ".action_refresh_state";
    /**
     * 点击底部导航栏tab
     */
    public static final String ACTION_PERFORM_TAB =
            BuildConfig.APPLICATION_ID + ".action_perform_tab";


    /**
     * loginFragment是否为主Fragment
     */
    public static final String ARG_IS_MAIN = "arg_is_main";
    /**
     * 启动页轮播广告
     */
    public static final String APP_START = "AppStart";
    /**
     * APP底部全站广告
     */
    public static final String APP_FOOTER = "AppFooter";
    /**
     * 个人中心顶部硬广
     */
    public static final String APP_USER_TOP = "AppUserTop";
    /**
     * APP播放页顶部硬广
     */
    public static final String APP_PLAY_TOP = "AppPlayTop";
    public static final String APP_PLAY_FOOTER = "AppPlayFooter";
    public static final String APP_PLAY_CENTER = "AppPlayCenter";

    public static final String IS_ADULT = "is_adult";

    public static final String SEARCH_HISTORY = "search_history";
    public static final String APK_MIME_TYPE = "application/vnd.android.package-archive";
}
