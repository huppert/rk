package com.xbchips.rk.common;


import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.base.BaseView2;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.base.response.PageResponse;
import com.xbchips.rk.widgets.EndlessRecyclerView;

import java.util.List;

import io.reactivex.annotations.NonNull;

/**
 * Created by york on 2017/11/20.
 */

public class DataHandler<T> {

    private final List<T> mList;
    private EndlessRecyclerView mRecyclerView;
    private BaseView2 mView;
    private BaseAdapter mAdapter;

    public DataHandler(List<T> list, EndlessRecyclerView recyclerView, BaseView2 view) {
        mList = list;
        mRecyclerView = recyclerView;
        mView = view;
        mAdapter = (BaseAdapter) mRecyclerView.getAdapter();
    }

    public void handResp(ObjectResponse<? extends PageResponse<T>> response) {
        if (mRecyclerView.isFirstPage()) {
            mList.clear();
        }
        mAdapter.removeFooter();
        if (!mView.handleResponse(response)) {
            PageResponse<T> data = response.getData();
            List<T> list = data.getList();
            mRecyclerView.setPageCount(data.getPage().getTotal());
            mList.addAll(list);
            int size = list.size();
            if (mRecyclerView.isFirstPage()) {
                mAdapter.notifyDataSetChanged();
            } else {
                mAdapter.notifyItemRangeInserted(mList.size() - size, size);
            }
            if (mRecyclerView.isLastPage()) {
                mAdapter.addNoMoreView();
            }
        }
        mRecyclerView.setLoading(false);
    }

    public void processError(@NonNull Throwable throwable) {
        mView.handleFailure(throwable);
        mRecyclerView.setLoading(false);
        if (!mRecyclerView.isFirstPage()) {
            mRecyclerView.pageDecrement();
            mAdapter.removeFooter();
        }
    }
}
