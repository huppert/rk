package com.xbchips.rk.common;

import com.xbchips.rk.BuildConfig;
import com.xbchips.rk.lx.util.CountingRequestBody;
import com.xbchips.rk.util.EncryptUtils;
import com.xbchips.rk.util.NetWorkUtils;
import com.xbchips.rk.util.UuidUtil;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class UploadRequest {

    private OkHttpClient mOkHttpClient;
    private Retrofit mRetrofit;
    private CountingRequestBody.Listener mListener;

    public UploadRequest(CountingRequestBody.Listener listener) {
        mListener = listener;
    }

    private OkHttpClient getInstance() {
        if (mOkHttpClient == null) {
            mOkHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(1, TimeUnit.MINUTES)
                    .writeTimeout(1, TimeUnit.MINUTES)
                    .addNetworkInterceptor(new ProgressInterceptor(mListener))
                    .build();
        }
        return mOkHttpClient;
    }


    /**
     * 点播域名
     */
    public <T> T createService(Class<T> clazz, String url) {
        if (mRetrofit == null) {
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(getInstance())
                    .build();
        }
        return mRetrofit.create(clazz);
    }


    private static class ProgressInterceptor implements Interceptor {
        private CountingRequestBody.Listener mListener;

        public ProgressInterceptor(CountingRequestBody.Listener listener) {
            mListener = listener;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();
            String apiToken = UserHelper.getApiToken();
            HttpUrl.Builder newBuilder = original.url().newBuilder();
            addParams(newBuilder);
            HttpUrl httpUrl = newBuilder
                    .addQueryParameter("vc", BuildConfig.VERSION_NAME)
                    .addQueryParameter("ch", Constants.DEFAULT_CHANNEL)
                    .addQueryParameter("os", "android")
                    .addQueryParameter("api_token", apiToken)
                    .addQueryParameter("app_code", BuildConfig.APPCODE)
                    .build();
            Request request = original.newBuilder()
                    .url(httpUrl)
                    .method(original.method(), new CountingRequestBody(original.body(), mListener))
                    .addHeader("api_token", apiToken)
                    .build();
            return chain.proceed(request);
        }

        private void addParams(HttpUrl.Builder newBuilder) {
            LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<>();
            linkedHashMap.put("deviceid", UuidUtil.getUUid());
            linkedHashMap.put("network", NetWorkUtils.getNetWorkTypeString());
            linkedHashMap.put("platform", Constants.DEFAULT_PLATFORM);
            linkedHashMap.put("rand", String.valueOf(ThreadLocalRandom.current().nextInt(100000000)));
            linkedHashMap.put("time", String.valueOf(System.currentTimeMillis() / 1000));
            linkedHashMap.put("userid", UserHelper.getLocalUser() == null ? "0" : UserHelper.getLocalUser().getId());
            linkedHashMap.put("version", BuildConfig.VERSION_NAME);
            Set<Map.Entry<String, String>> entrySet = linkedHashMap.entrySet();
            StringBuilder sb = new StringBuilder();
            for (Map.Entry<String, String> entry : entrySet) {
                sb.append(entry.getKey()).append("=").append(entry.getValue());
                newBuilder.addQueryParameter(entry.getKey(), entry.getValue());
            }
            String s = sb.toString() + "L9gjDXXNOp3Hd977";
            newBuilder.addQueryParameter("guid", EncryptUtils.encryptMD5ToString(s).toLowerCase());
        }
    }
}
