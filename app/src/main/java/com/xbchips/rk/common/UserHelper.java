package com.xbchips.rk.common;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.xbchips.rk.XvideoApplication;
import com.xbchips.rk.user.entity.User;
import com.xbchips.rk.util.PreferenceUtils;

/**
 * Created by york on 2017/4/6.
 * cache user info
 */

public class UserHelper {
    private static User sLocalUser;

    public static void setLocalUser(User user) {
        sLocalUser = user;
        if (sLocalUser == null) {
            PreferenceUtils.delString(XvideoApplication.getApplication(), Constants.PREF_USER);
        } else {
            PreferenceUtils.setString(XvideoApplication.getApplication(), Constants.PREF_USER, new Gson().toJson(user));
        }
    }

    @Nullable
    public static User getLocalUser() {
        if (sLocalUser == null) {
            String userString = PreferenceUtils.getString(XvideoApplication.getApplication(), Constants.PREF_USER);
            if (TextUtils.isEmpty(userString)) return null;
            sLocalUser = new Gson().fromJson(userString, User.class);
        }
        return sLocalUser;
    }

    public static boolean isLoginState() {
        return sLocalUser != null && !TextUtils.isEmpty(sLocalUser.getApi_token());
    }


    @NonNull
    public static String getApiToken() {
        User localUser = getLocalUser();
        if (localUser == null) {
            return "";
        } else {
            return localUser.getApi_token();
        }

    }

    public static void clearToken() {
        sLocalUser.setApi_token("");
        setLocalUser(sLocalUser);
    }
}
