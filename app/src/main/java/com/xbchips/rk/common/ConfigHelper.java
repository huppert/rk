package com.xbchips.rk.common;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.xbchips.rk.XvideoApplication;
import com.xbchips.rk.system.entity.Configs;
import com.xbchips.rk.system.entity.DynamicConfig;
import com.xbchips.rk.util.PreferenceUtils;

import java.lang.reflect.Field;

import io.reactivex.Observable;

/**
 * Created by york on 2018/1/5.
 * 读取配置文件
 */

public class ConfigHelper {
    private static final String TAG = "ConfigHelper";

    public static Observable<String> getValue(String configKey) {
        return Observable.create(e -> {
            String json = PreferenceUtils.getString(XvideoApplication.getApplication(), Constants.PREF_CONFIG);
            if (!TextUtils.isEmpty(json)) {
                Configs configs = new Gson().fromJson(json, Configs.class);
                Field field = configs.getClass().getDeclaredField(configKey);
                field.setAccessible(true);
                DynamicConfig dynamicConfig = (DynamicConfig) field.get(configs);
                if (dynamicConfig != null && dynamicConfig.getStatus() == 8) {
                    String url = dynamicConfig.getContent().get(0);
                    e.onNext(url == null ? "" : url);
                } else {
                    e.onNext("");
                }
            } else {
                e.onNext("");
            }
        });
    }
}
