package com.xbchips.rk.common;

import android.util.Log;

import com.xbchips.rk.BuildConfig;
import com.xbchips.rk.XvideoApplication;
import com.xbchips.rk.util.EncryptUtils;
import com.xbchips.rk.util.LogTrace;
import com.xbchips.rk.util.NetWorkUtils;
import com.xbchips.rk.util.UuidUtil;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static androidx.constraintlayout.widget.Constraints.TAG;

/**
 * 网络请求类
 */
public class HttpRequest {

    private volatile static OkHttpClient sOkHttpClient;
    private volatile static Retrofit sRetrofit;
    private static final long HTTP_DISK_CACHE_MAX_SIZE = 10 * 1024 * 1024;

    private static OkHttpClient getInstance() {
        if (sOkHttpClient == null) {
            synchronized (HttpRequest.class) {
                if (sOkHttpClient == null) {
                    sOkHttpClient = new OkHttpClient.Builder()
                            .cache(new Cache(new File(XvideoApplication.getApplication().getCacheDir(), "httpCache"), HTTP_DISK_CACHE_MAX_SIZE))
                            .addNetworkInterceptor(new RequestInterceptor())
                            .addInterceptor(new LoggingInterceptor())
                            .build();
                }
            }
        }
        return sOkHttpClient;
    }


    /**
     * 点播域名
     */
    public synchronized static <T> T createService(Class<T> clazz) {
        if (sRetrofit == null) {
            sRetrofit = new Retrofit.Builder()
                    .baseUrl(SystemDomain.server_domain + "/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(getInstance())
                    .build();
        }
        return sRetrofit.create(clazz);
    }


    private static class LoggingInterceptor implements Interceptor {

        private static final String TAG = "LoggingInterceptor.class";

        @Override
        public Response intercept(Chain chain) throws IOException {

            //release版本不打印log
            if (!BuildConfig.DEBUG) {
                return chain.proceed(chain.request());
            }
            Request original = chain.request();
            long t1 = System.nanoTime();
            LogTrace.i(TAG, String.format(Locale.CHINA, "Sending request %s on %s%n%s",
                    original.url(), chain.connection(), original.headers()));

            Response response = chain.proceed(original);
            String body = response.body().string();
            long t2 = System.nanoTime();
            LogTrace.i(TAG, String.format(Locale.CHINA, "Received response for %s in %.1fms%n%s",
                    response.request().url(), (t2 - t1) / 1e6d, body));


            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), body))
                    .build();
        }
    }

    private static class RequestInterceptor implements Interceptor {
        private static final String PROPERTY = System.getProperty("http.agent");

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();
            HttpUrl.Builder newBuilder = original.url().newBuilder();
            newBuilder
                    .addQueryParameter("vc", BuildConfig.VERSION_NAME)
                    .addQueryParameter("ch", Constants.DEFAULT_CHANNEL)
                    .addQueryParameter("os", "android")
                    .addQueryParameter("app_code", BuildConfig.APPCODE);
            addParams(newBuilder);
            HttpUrl httpUrl = newBuilder.build();
            Request.Builder builder = original.newBuilder();
            Request request = builder
                    .url(httpUrl)
                    .addHeader("User-Agent", PROPERTY)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("api_token", UserHelper.getApiToken())
                    .build();
            return chain.proceed(request);
        }


        private void addParams(HttpUrl.Builder newBuilder) {
            //按首字母 排序
            LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<>();
            linkedHashMap.put("deviceid", UuidUtil.getUUid());
            linkedHashMap.put("network", NetWorkUtils.getNetWorkTypeString());
            linkedHashMap.put("platform", Constants.DEFAULT_PLATFORM);
            linkedHashMap.put("rand", String.valueOf(ThreadLocalRandom.current().nextInt(100000000)));
            linkedHashMap.put("time", String.valueOf(System.currentTimeMillis() / 1000));
            linkedHashMap.put("userid", UserHelper.isLoginState() ? UserHelper.getLocalUser().getId() : "0");
            linkedHashMap.put("version", BuildConfig.VERSION_NAME);
            Set<Map.Entry<String, String>> entrySet = linkedHashMap.entrySet();
            StringBuilder sb = new StringBuilder();
            for (Map.Entry<String, String> entry : entrySet) {
                sb.append(entry.getKey()).append("=").append(entry.getValue());
                newBuilder.addQueryParameter(entry.getKey(), entry.getValue());
            }
            String s = sb.toString() + "L9gjDXXNOp3Hd977";
            Log.d(TAG, "addParams: " + s);
            newBuilder.addQueryParameter("guid", EncryptUtils.encryptMD5ToString(s).toLowerCase());

        }
    }


}
