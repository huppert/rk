package com.xbchips.rk.video.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;

import java.util.List;

public class ListPopwindowAdapter extends BaseAdapter<String> {

    private String mCurrentLine;

    public ListPopwindowAdapter(List<String> list, Context context, String currentLine) {
        super(list, context);
        mCurrentLine = currentLine;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyHolder(mInflater.inflate(R.layout.item_list_popwindows, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyHolder myHolder = (MyHolder) holder;
        String line = mList.get(position);
        myHolder.mTextView.setTextColor(line.equals(mCurrentLine) ? 0xffF36B39 : Color.WHITE);
        myHolder.mTextView.setText(line);
        myHolder.mTextView.setTag(position);
        myHolder.itemView.setOnClickListener(mOnClickListener);
    }


    static class MyHolder extends RecyclerView.ViewHolder {

        TextView mTextView;

        private MyHolder(View itemView) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.tv_line);
        }
    }


}
