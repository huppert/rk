package com.xbchips.rk.video.fragments;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.user.activities.UserAuthActivity;
import com.xbchips.rk.util.DisplayUtils;
import com.xbchips.rk.video.adapter.FastCommentAdapter;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CommentLayout extends LinearLayout {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.tv_comment)
    TextView tvComment;
    @BindView(R.id.iv_send)
    ImageView ivSend;
    private boolean showing;
    private boolean inited;
    private Unbinder mUnbinder;
    private int maxHeight;
    private OnStateChangeListener mStateChnageListener;

    public CommentLayout(Context context) {
        this(context, null);
    }

    public CommentLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_comment, this);
        mUnbinder = ButterKnife.bind(this, view);

        int statusBarHeight = DisplayUtils.getStatusBarHeight(getContext());
        int videoHeight = DisplayUtils.getScreenW(getContext()) / 25 * 14;
        int margin = DisplayUtils.dip2px(getContext(), 20);
        maxHeight = DisplayUtils.getScreenH(getContext()) - statusBarHeight - videoHeight - margin;
        init();
    }


    protected void init() {
        tvComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setBtnState();
                Log.d("commentlayout", "afterTextChanged: ");
            }
        });

        tvComment.setOnClickListener(v -> {
            if (!UserHelper.isLoginState()) {
                alertLoginDialog();
                return;
            }
            Log.d(getClass().getName(), "tvcomment: ");
            if (!isShowing()) {
                popup();
            }
        });

        ivSend.setOnClickListener(v -> {
            if (!UserHelper.isLoginState()) {
                alertLoginDialog();
                return;
            }
            if (!TextUtils.isEmpty(tvComment.getText())) {
                if (mStateChnageListener != null) {
                    mStateChnageListener.sendComment(tvComment.getText().toString());
                }
                dismiss();
                ivSend.setImageResource(R.drawable.ic_comment);
                tvComment.setText(null);
            } else {
                if (!isShowing()) {
                    popup();
                }
            }

        });
    }


    private void alertLoginDialog() {
        new AlertDialog.Builder(getContext())
                .setMessage(getContext().getString(R.string.login_first_need))
                .setPositiveButton(getContext().getString(R.string.ensure), (dialog, which) -> {
                            Intent intent = new Intent(getContext(), UserAuthActivity.class);
                            intent.putExtra(UserAuthActivity.ARG_IS_LOGIN, true);
                            getContext().startActivity(intent);
                        }
                )
                .show();
    }


    private void initRecyclerView() {
        inited = true;
        String[] array = getResources().getStringArray(R.array.fast_comments);
        mRecyclerView.setVisibility(View.VISIBLE);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        DividerItemDecoration decoration = new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL);
        decoration.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider_comment_list));
        mRecyclerView.addItemDecoration(decoration);
        FastCommentAdapter adapter = new FastCommentAdapter(Arrays.asList(array), getContext());
        adapter.setOnClickListener(v -> {
            tvComment.setText(v.getTag().toString());

        });
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mRecyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                ViewGroup.LayoutParams layoutParams = mRecyclerView.getLayoutParams();
                if (layoutParams.height > maxHeight) {
                    layoutParams.height = maxHeight;
                    mRecyclerView.setLayoutParams(layoutParams);
                }


            }
        });
    }


    private void setBtnState() {
        boolean enable = !TextUtils.isEmpty(tvComment.getText());
        if (enable) {
            ivSend.setImageResource(R.drawable.icon_send);
            ivSend.setEnabled(true);
        } else {
            ivSend.setImageResource(R.drawable.ic_comment);
            ivSend.setEnabled(false);
        }
    }


    public void popup() {
        if (!inited) {
            initRecyclerView();
        }
        mRecyclerView.setVisibility(VISIBLE);
        showing = true;
        if (mStateChnageListener != null) {
            mStateChnageListener.onChanged(true);
        }
    }

    public void dismiss() {
        mRecyclerView.setVisibility(GONE);
        showing = false;
        if (mStateChnageListener != null) {
            mStateChnageListener.onChanged(false);
        }
    }

    public boolean isShowing() {
        return showing;
    }


    public interface OnStateChangeListener {
        void onChanged(boolean showing);

        void sendComment(String text);
    }

    public void setStateChnageListener(OnStateChangeListener stateChnageListener) {
        mStateChnageListener = stateChnageListener;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }
}
