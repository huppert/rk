package com.xbchips.rk.video;

import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.common.HttpRequest;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by york on 2017/8/4.
 */

public class VideoPresenter implements VideoContract.Presenter {
    private static final String TAG = "VideoPresenter";
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private VideoContract.View mView;
    private VideoService mService;

    public VideoPresenter(VideoContract.View view) {
        mView = view;
        mService = HttpRequest.createService(VideoService.class);
    }


    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }

    @Override
    public void loadData(String videoId, final boolean isRefresh) {
        executeTask(mService.load(videoId), response -> {
                    if (isRefresh) {
                        mView.refreshAllState(response.getData());
                    } else {
                        mView.setData(response.getData());
                    }
                }
        );
    }


    @Override
    public void getVideoUrl(String videoId, String isSearch) {
        mCompositeDisposable.add(mService.playVideo(videoId, isSearch)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                            if (response.isSuccess()) {
                                mView.getVideoUrlSuccess(response.getData().getPlay_url());
                            } else {
                                boolean isConsume = mView.handleResponse(response);
                                mView.getVideoUrlFailed(isConsume);
                            }

                        },
                        throwable -> {
                            mView.handleFailure(throwable);
                            mView.getVideoUrlFailed(false);
                        })
        );

    }


    @Override
    public void like(String videoId) {
        executeTask(mService.like(videoId),
                response -> mView.setLikeState(1, response.getData().getLike()));
    }

    @Override
    public void dislike(String videoId) {
        executeTask(mService.dislike(videoId),
                response -> mView.setLikeState(2, response.getData().getLike()));
    }


    @Override
    public void mark(String videoId) {
        executeTask(mService.mark(videoId), response -> mView.setMarkState(true));
    }

    @Override
    public void unMark(String videoId) {
        executeTask(mService.unmark(videoId), response -> mView.setMarkState(false));
    }

    @Override
    public void getComments(String videoId, String sort, int page, int pageCount) {
        Disposable disposable = mService.getComments(videoId, sort, page, pageCount)
                .compose(RxUtils.applySchedulers())
                .filter(t -> !mView.handleResponse(t))
                .subscribe(response -> mView.getCommentsSuccess(response),
                        throwable -> mView.getCommentsFailed(throwable));
        mCompositeDisposable.add(disposable);
    }


    @Override
    public void sendComment(String comment) {

    }


    private <T> void executeTask(Observable<ObjectResponse<T>> observable, Consumer<ObjectResponse<T>> onNext) {
        mCompositeDisposable.add(observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(t -> !mView.handleResponse(t))
                .subscribe(onNext, throwable -> mView.handleFailure(throwable))
        );
    }

}
