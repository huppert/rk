package com.xbchips.rk.video.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by york on 2017/8/1.
 */

public class VideoDetail implements Parcelable {
    private String id;
    private String title;
    private String thumb_img_url;
    private int like;
    @SerializedName("is_collect")
    private boolean collect;
    private int views;
    private String description;
    private List<String> tags;
    private List<String> content;
    private String play_url;
    private int assess;

    protected VideoDetail(Parcel in) {
        id = in.readString();
        title = in.readString();
        thumb_img_url = in.readString();
        like = in.readInt();
        collect = in.readByte() != 0;
        views = in.readInt();
        description = in.readString();
        tags = in.createStringArrayList();
        content = in.createStringArrayList();
        play_url = in.readString();
        assess = in.readInt();
    }

    public static final Creator<VideoDetail> CREATOR = new Creator<VideoDetail>() {
        @Override
        public VideoDetail createFromParcel(Parcel in) {
            return new VideoDetail(in);
        }

        @Override
        public VideoDetail[] newArray(int size) {
            return new VideoDetail[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumb_img_url() {
        return thumb_img_url;
    }

    public void setThumb_img_url(String thumb_img_url) {
        this.thumb_img_url = thumb_img_url;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public boolean isCollect() {
        return collect;
    }

    public void setCollect(boolean collect) {
        this.collect = collect;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public String getDescription() {
        if (description == null) {
            description = "";
        }
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getContent() {
        return content;
    }

    public void setContent(List<String> content) {
        this.content = content;
    }

    public String getPlay_url() {
        return play_url;
    }

    public void setPlay_url(String play_url) {
        this.play_url = play_url;
    }

    public int getAssess() {
        return assess;
    }

    public void setAssess(int assess) {
        this.assess = assess;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(thumb_img_url);
        dest.writeInt(like);
        dest.writeByte((byte) (collect ? 1 : 0));
        dest.writeInt(views);
        dest.writeString(description);
        dest.writeStringList(tags);
        dest.writeStringList(content);
        dest.writeString(play_url);
        dest.writeInt(assess);
    }
}
