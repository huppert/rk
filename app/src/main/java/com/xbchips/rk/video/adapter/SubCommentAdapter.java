package com.xbchips.rk.video.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.video.entity.Comment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubCommentAdapter extends BaseAdapter<Comment> {
    public SubCommentAdapter(List<Comment> list, Context context) {
        super(list, context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return viewType == TYPE_ITEM ?
                new SubCommentViewHolder(mInflater.inflate(R.layout.item_sub_comment, parent, false)) :
                getFooterViewHolder(parent);
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        SubCommentViewHolder viewHolder = (SubCommentViewHolder) holder;
        Comment comment = mList.get(position);
        ImageLoader.loadAvatar(mContext, comment.getAvatar(), viewHolder.ivAvatar);
        viewHolder.tvComment.setText(comment.getContent());
        viewHolder.tvNickname.setText(comment.getNickname());
    }

    static class SubCommentViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_avatar)
        ImageView ivAvatar;
        @BindView(R.id.tv_nickname)
        TextView tvNickname;
        @BindView(R.id.tv_comment)
        TextView tvComment;

        public SubCommentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
