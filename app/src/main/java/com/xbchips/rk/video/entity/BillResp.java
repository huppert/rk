package com.xbchips.rk.video.entity;

import com.xbchips.rk.base.response.PageResponse;

public class BillResp extends PageResponse<Bill> {
    private int totalBill;
    private double totalIncome;
    private double totalConsume;

    public int getTotalBill() {
        return totalBill;
    }

    public double getTotalIncome() {
        return totalIncome;
    }

    public double getTotalConsume() {
        return totalConsume;
    }
}
