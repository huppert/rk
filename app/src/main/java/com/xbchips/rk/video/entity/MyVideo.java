package com.xbchips.rk.video.entity;

public class MyVideo {
    private String id;
    private String title;
    private int count;
    private String thumb_img_url;
    private String created_at;
    private String video_url;
    private String intro;
    private String price;
    private int status;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getCount() {
        return count;
    }

    public String getThumb_img_url() {
        return thumb_img_url;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getVideo_url() {
        return video_url;
    }

    public String getPrice() {
        return price;
    }

    public int getStatus() {
        return status;
    }

    public String getIntro() {
        return intro;
    }
}
