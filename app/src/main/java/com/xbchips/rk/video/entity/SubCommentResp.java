package com.xbchips.rk.video.entity;

import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.base.response.PageResponse;

import java.util.List;

public class SubCommentResp extends ObjectResponse {

    protected List<Comment> list;

    protected PageResponse.Page page;

    public List<Comment> getList() {
        return list;
    }

    public PageResponse.Page getPage() {
        return page;
    }
}
