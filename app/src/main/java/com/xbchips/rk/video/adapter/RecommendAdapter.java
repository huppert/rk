package com.xbchips.rk.video.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.free.entity.VideoItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by york on 2017/9/25.
 */

public class RecommendAdapter extends BaseAdapter<VideoItem> {


    public RecommendAdapter(List<VideoItem> list, Context context) {
        super(list, context);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = mInflater.inflate(R.layout.item_recommend, parent, false);
        return new RecommendViewHolder(itemView);
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final RecommendViewHolder holder = (RecommendViewHolder) viewHolder;
        final VideoItem videoItem = mList.get(position);
        holder.tvTitle.setText(videoItem.getTitle());
        ImageLoader.load(mContext, videoItem.getIs_advert() == 1 ? videoItem.getImg_url() : videoItem.getThumb_img_url(), holder.ivCover);
        holder.ivCover.setOnClickListener(mOnClickListener);
        holder.ivCover.setTag(holder.ivCover.getId(), videoItem);
    }

    static class RecommendViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_cover)
        ImageView ivCover;
        @BindView(R.id.tv_title)
        TextView tvTitle;

        public RecommendViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
