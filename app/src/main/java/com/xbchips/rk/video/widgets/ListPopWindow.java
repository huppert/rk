package com.xbchips.rk.video.widgets;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.video.adapter.ListPopwindowAdapter;

import java.util.ArrayList;
import java.util.List;

public class ListPopWindow extends PopupWindow {
    private Context mContext;

    private OnItemClickListener mOnItemClickListener;

    public ListPopWindow(Context context) {
        mContext = context;
    }

    public void init(int size, String currentLine) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.view_list_popwindow, null);
        RecyclerView recyclerView = view.findViewById(R.id.recycler_menu);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        List<String> lines = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            lines.add(mContext.getString(R.string.hd_line, i + 1));
        }
        ListPopwindowAdapter adapter = new ListPopwindowAdapter(lines, mContext, currentLine);
        adapter.setOnClickListener(v -> {
            mOnItemClickListener.OnItemClick(((int) v.getTag()));
            dismiss();
        });
        recyclerView.setAdapter(adapter);
        setContentView(view);
        setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setOutsideTouchable(true);
    }


    public interface OnItemClickListener {
        void OnItemClick(int itemPosition);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }
}
