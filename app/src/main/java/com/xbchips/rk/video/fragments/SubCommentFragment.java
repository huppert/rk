package com.xbchips.rk.video.fragments;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseFragment;
import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.user.entity.User;
import com.xbchips.rk.util.BackHandlerHelper;
import com.xbchips.rk.util.DisplayUtils;
import com.xbchips.rk.video.VideoService;
import com.xbchips.rk.video.adapter.SubCommentAdapter;
import com.xbchips.rk.video.entity.Comment;
import com.xbchips.rk.video.entity.SubCommentResp;
import com.xbchips.rk.widgets.EndlessRecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;

public class SubCommentFragment extends BaseFragment implements BackHandlerHelper.FragmentBackHandler {
    private static final String ARG_CID = "arg_cid";
    private static final String ARG_VID = "arg_vid";
    private static final String ARG_COUNT = "arg_count";
    @BindView(R.id.tv_comment_counts)
    TextView tvCommentCounts;
    @BindView(R.id.recycler_view)
    EndlessRecyclerView recyclerView;
    @BindView(R.id.comment_layout)
    CommentLayout commentLayout;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    private final VideoService service = HttpRequest.createService(VideoService.class);
    private OnCountChangeListener mListener;
    private List<Comment> mList = new ArrayList<>();
    private SubCommentAdapter mAdapter;
    /**
     * 回复条数
     */
    private int relpyCount;

    public static SubCommentFragment getInstance(int cid, String vid, int count) {
        final SubCommentFragment fragment = new SubCommentFragment();
        final Bundle bundle = new Bundle();
        bundle.putInt(ARG_CID, cid);
        bundle.putString(ARG_VID, vid);
        bundle.putInt(ARG_COUNT, count);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int videoHeight = DisplayUtils.getScreenW(getContext()) / 25 * 14;
        int maxHeight = DisplayUtils.getScreenH(getContext()) - videoHeight;
        View view = inflater.inflate(R.layout.fragment_sub_comment, container, false);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = maxHeight;
        view.setLayoutParams(layoutParams);

        mUnBinder = ButterKnife.bind(this, view);
        init();
        return view;
    }

    @Override
    protected void init() {
        isInit = true;
        initViews();
        loadData();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        view.setOnTouchListener((v, ev) -> dispatch(ev));
    }

    private boolean dispatch(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN && commentLayout.isShowing()) {
            int[] outLocation = new int[2];
            commentLayout.getLocationInWindow(outLocation);
            Rect rect = new Rect(outLocation[0], outLocation[1], outLocation[0] + commentLayout.getWidth(), outLocation[1] + commentLayout.getHeight());

            if (!rect.contains(((int) ev.getRawX()), ((int) ev.getRawY()))) {
                commentLayout.dismiss();
                return true;
            }
        }
        return false;
    }


    private void initViews() {
        relpyCount = getArguments().getInt(ARG_COUNT);
        setTvCommentCounts();
        ivBack.setOnClickListener(v -> getActivity().onBackPressed());
        commentLayout.setStateChnageListener(new CommentLayout.OnStateChangeListener() {
            @Override
            public void onChanged(boolean showing) {

            }

            @Override
            public void sendComment(String text) {
                comment(text);
            }
        });
        initRecyclerView();
    }

    private void setTvCommentCounts() {
        tvCommentCounts.setText(getString(R.string.reply_format, relpyCount));
    }

    private void loadData() {
        final int commentId = getArguments().getInt(ARG_CID);
        final Disposable disposable = service.getReplies(commentId, recyclerView.getCurPage(), 20)
                .compose(RxUtils.applySchedulers())
                .filter(resp -> !handleResponse(resp))
                .subscribe(this::handResp, this::processError);
        mCompositeDisposable.add(disposable);
    }


    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new SubCommentAdapter(mList, getContext());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setOnTouchListener((v, event) -> dispatch(event));
    }


    private void comment(String content) {
        final HashMap<String, Object> map = new HashMap<>();
        final int commentId = getArguments().getInt(ARG_CID);
        map.put("content", content);
        map.put("parent_id", commentId);
        final Disposable disposable = service.comment(getArguments().getString(ARG_VID), map)
                .compose(RxUtils.applySchedulers())
                .filter(response -> !handleResponse(response))
                .subscribe(response -> {
                    final String creatTime = response.getData().getCreat_time();
                    int id = response.getData().getId();
                    final User user = UserHelper.getLocalUser();
                    mList.add(0, new Comment(id, user.getNickname(), user.getAvatar(), content, creatTime));
                    mAdapter.notifyItemInserted(0);
                    relpyCount++;
                    setTvCommentCounts();
                    if (mListener != null) {
                        mListener.onChanged(commentId, relpyCount);
                    }
                    showToast("发送成功");
                }, this::handleFailure);
        mCompositeDisposable.add(disposable);
    }

    public void handResp(SubCommentResp response) {
        if (recyclerView.isFirstPage()) {
            mList.clear();
        }
        mAdapter.removeFooter();
        if (!handleResponse(response)) {
            List<Comment> list = response.getList();
            recyclerView.setPageCount(response.getPage().getTotal());
            mList.addAll(list);
            int size = list.size();
            if (recyclerView.isFirstPage()) {
                mAdapter.notifyDataSetChanged();
            } else {
                mAdapter.notifyItemRangeInserted(mList.size() - size, size);
            }
            if (recyclerView.isLastPage()) {
                mAdapter.addNoMoreView();
            }
        }
        recyclerView.setLoading(false);
    }

    public void processError(@NonNull Throwable throwable) {
        handleFailure(throwable);
        recyclerView.setLoading(false);
        if (!recyclerView.isFirstPage()) {
            recyclerView.pageDecrement();
            mAdapter.removeFooter();
        }
    }

    @Override
    public boolean onBackPressed() {
        if (commentLayout.isShowing()) {
            commentLayout.dismiss();
            return true;
        }
        return BackHandlerHelper.handleBackPress(this);
    }

    public interface OnCountChangeListener {
        void onChanged(int id, int count);
    }

    public void setListener(OnCountChangeListener listener) {
        mListener = listener;
    }
}
