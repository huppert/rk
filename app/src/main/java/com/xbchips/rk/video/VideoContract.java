package com.xbchips.rk.video;

import com.xbchips.rk.base.BasePresenter;
import com.xbchips.rk.base.BaseView;
import com.xbchips.rk.video.entity.CommentsResp;
import com.xbchips.rk.video.entity.VideoDetail;

/**
 * Created by york on 2017/8/4.
 */

public interface VideoContract {

    interface View extends BaseView<Presenter> {
        void setData(VideoDetail data);

        void setLikeState(int state, int like_percent);

        void setMarkState(boolean state);

        void refreshAllState(VideoDetail data);

        void getVideoUrlSuccess(String url);

        void getVideoUrlFailed(boolean isErrorConsumed);

        /**
         * 获取评论列表
         */
        void getCommentsSuccess(CommentsResp response);

        void getCommentsFailed(Throwable throwable);

    }

    interface Presenter extends BasePresenter {
        void loadData(String videoId, boolean isRefresh);

        void getVideoUrl(String videoId, String isSearch);

        void like(String videoId);

        void dislike(String videoId);

        void mark(String videoId);

        void unMark(String videoId);

        void getComments(String videoId, String sort, int page, int pageCount);

        void sendComment(String comment);

    }
}
