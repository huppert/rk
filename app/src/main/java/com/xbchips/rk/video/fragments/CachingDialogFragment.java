package com.xbchips.rk.video.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.offline.Download;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.xbchips.rk.R;
import com.xbchips.rk.XvideoApplication;
import com.xbchips.rk.profile.activity.CachingActivity;
import com.xbchips.rk.util.ObjectUtil;
import com.xbchips.rk.video.adapter.CacheListAdapter;
import com.xbchips.rk.video.entity.DownloadData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CachingDialogFragment extends BottomSheetDialogFragment {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.tv_look_cache)
    TextView tvLookCache;
    private Unbinder mUnbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_caching, container, false);
        mUnbinder = ButterKnife.bind(this, view);


        List<Download> downloads = XvideoApplication.getApplication().getDownloadManager().getCurrentDownloads();
        List<String> list = new ArrayList<>();
        for (Download download : downloads) {
            DownloadData data = ObjectUtil.unmarshall(download.request.data, DownloadData.CREATOR);
            list.add(data.getTitle());
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new CacheListAdapter(list, getContext()));

        tvLookCache.setOnClickListener(v -> {
            startActivity(new Intent(getContext(), CachingActivity.class));
            dismiss();
        });
        return view;
    }


    @Override
    public void dismiss() {
        super.dismiss();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }
}
