package com.xbchips.rk.video.widgets;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.xbchips.rk.R;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.util.DateUtil;
import com.xbchips.rk.util.DisplayUtils;
import com.xbchips.rk.video.entity.VideoParam;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 上传确认框
 */
public class ConfirmWindow extends PopupWindow {

    private final Context mContext;
    @BindView(R.id.tv_desc)
    TextView tvDesc;
    @BindView(R.id.iv_cover)
    ImageView ivCover;
    @BindView(R.id.tv_author)
    TextView tvAuthor;
    @BindView(R.id.tv_price)
    TextView tvPrice;
    @BindView(R.id.tv_duration)
    TextView tvDuration;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.btn_cancel)
    FrameLayout btnCancel;
    @BindView(R.id.btn_submit)
    FrameLayout btnSubmit;
    @BindView(R.id.title_bar)
    TextView titleBar;
    private View.OnClickListener mListener;


    public ConfirmWindow(Context context) {
        super(context);
        mContext = context;
    }

    public void show(VideoParam video) {
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_confirm, null);
        ButterKnife.bind(this, contentView);
        setContentView(contentView);

        int radius = DisplayUtils.dip2px(mContext, 3);

        int btnConner = radius * 2;
        btnCancel.setBackground(createGradientDrawable(R.color.cf_cancel, new float[]{btnConner, btnConner, btnConner, btnConner, btnConner, btnConner, btnConner, btnConner}));
        btnSubmit.setBackground(createGradientDrawable(R.color.cf_confirm, new float[]{btnConner, btnConner, btnConner, btnConner, btnConner, btnConner, btnConner, btnConner}));
        titleBar.setBackground(createGradientDrawable(R.color.cf_title_bg, new float[]{radius * 4, radius * 4, radius * 4, radius * 4, 0, 0, 0, 0}));
        tvDesc.setText(video.getDesc());
        tvAuthor.setText(mContext.getString(R.string.author_f, UserHelper.getLocalUser().getUsername()));
        tvPrice.setText(mContext.getString(R.string.price_f, video.getPrice()));
        tvDuration.setText(mContext.getString(R.string.duration_f, getDuration(video.getImgUrl())));
        tvTitle.setText(mContext.getString(R.string.title_f, video.getTitle()));
        ImageLoader.load(mContext, video.getImgUrl(), ivCover);

        btnSubmit.setOnClickListener(mListener);
        btnCancel.setOnClickListener(mListener);

        setBackgroundDrawable(new ColorDrawable(0x90000000));
        setOutsideTouchable(false);
        setFocusable(true);
        setAnimationStyle(R.style.PopupWindowAnimationStyle);
        showAtLocation(contentView, Gravity.CENTER, 0, 0);
    }


//    private String getDuration(Uri uri) {
//        Cursor cursor = null;
//        String duration = null;
//        try {
//            cursor = MediaStore.Video.query(mContext.getContentResolver(), uri,
//                    new String[]{MediaStore.Video.Media.DURATION});
//            if (cursor.moveToFirst()) {
//                duration = cursor.getString(cursor.getColumnIndex(MediaStore.Video
//                        .Media.DURATION));
//            }
//        } finally {
//            if (cursor != null) {
//                cursor.close();
//            }
//        }
//
//        Log.d(getClass().getName(), "getDuration: " + duration);
//        return duration == null ? "未知" : DateUtil.transformTime(Long.parseLong(duration) / 1000);
//    }


    private String getDuration(Uri uri) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(mContext, uri);
        String duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        retriever.release();
        return duration == null ? "未知" : DateUtil.transformTime(Long.parseLong(duration) / 1000);
    }

    @NonNull
    private GradientDrawable createGradientDrawable(@ColorRes int color, float[] radii) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setShape(GradientDrawable.RECTANGLE);
        drawable.setCornerRadii(radii);
        drawable.setColor(ContextCompat.getColor(mContext, color));
        return drawable;
    }

    public void setListener(View.OnClickListener listener) {
        mListener = listener;
    }


}
