package com.xbchips.rk.video.entity;

import com.xbchips.rk.base.response.PageResponse;

public class MyVideosResp extends PageResponse<MyVideo> {

    private int totalVideo;
    private String surpass;
    private int totalPurchase;

    public int getTotalVideo() {
        return totalVideo;
    }

    public String getPercent() {
        return surpass;
    }

    public int getTotalPurchase() {
        return totalPurchase;
    }
}
