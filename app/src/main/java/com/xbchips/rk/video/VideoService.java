package com.xbchips.rk.video;

import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.base.response.PageResponse;
import com.xbchips.rk.base.response.PageResponse2;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.video.entity.Assess;
import com.xbchips.rk.video.entity.BillResp;
import com.xbchips.rk.video.entity.CommentsResp;
import com.xbchips.rk.video.entity.MyVideosResp;
import com.xbchips.rk.video.entity.SendCommentResp;
import com.xbchips.rk.video.entity.SubCommentResp;
import com.xbchips.rk.video.entity.VideoDetail;

import java.util.HashMap;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by york on 2017/8/4.
 */

public interface VideoService {
    /**
     * 视频详情
     */
    @GET("video/detail/{video_id}")
    Observable<ObjectResponse<VideoDetail>> load(@Path("video_id") String videoId);

    /**
     * 获取视频地址
     */
    @GET("video/play/{video_id}")
    Observable<ObjectResponse<VideoDetail>> playVideo(@Path("video_id") String videoId, @Query("is_search") String isSearch);

    /**
     * 好评
     */
    @GET("video/like/{video_id}")
    Observable<ObjectResponse<Assess>> like(@Path("video_id") String videoId);

    /**
     * 差评
     */
    @GET("video/dislike/{video_id}")
    Observable<ObjectResponse<Assess>> dislike(@Path("video_id") String videoId);

    /**
     * ic_pf_favor
     */
    @GET("video/collect/{video_id}")
    Observable<ObjectResponse<String>> mark(@Path("video_id") String videoId);

    /**
     * 取消收藏
     */
    @GET("video/cancelCollect/{video_id}")
    Observable<ObjectResponse<String>> unmark(@Path("video_id") String videoId);


    /**
     * 推荐列表
     */
    @GET("video/recommends/{video_id}")
    Observable<ObjectResponse<PageResponse<VideoItem>>> getRecommends(@Path("video_id") String videoId,
                                                                      @Query("page") int page,
                                                                      @Query("page_size") int pageSize);


    /**
     * 视频评论列表
     */
    @GET("video/review/{video_id}")
    Observable<CommentsResp> getComments(@Path("video_id") String videoId,
                                         @Query("sort") String sort,
                                         @Query("page") int page,
                                         @Query("page_size") int pageSize);

    /**
     * 评论回复列表
     */
    @GET("video/getSubComment/{review_id}")
    Observable<SubCommentResp> getReplies(@Path("review_id") int commentId,
                                          @Query("page") int page,
                                          @Query("page_size") int pageSize);

    /**
     * 评论
     */
    @POST("video/publishReview/{video_id}")
    Observable<ObjectResponse<SendCommentResp>> comment(@Path("video_id") String videoId,
                                                        @Body HashMap<String, Object> map);

    /**
     * 点赞评论
     */

    @POST("video/likeReview/{review_id}")
    Observable<ObjectResponse<String>> likeComment(@Path("review_id") int id);

    /**
     * 上传视频
     */
    @Multipart
    @POST("video/upload")
    Observable<ObjectResponse<HashMap<String, String>>> uploadVideo(@Part MultipartBody.Part video);

    /**
     * 提交视频信息
     */
    @POST("video/addMyVideo")
    Observable<ObjectResponse<String>> addVideo(@Body HashMap<String, String> map);

    /**
     * 购买视频
     */
    @POST("video/buyVideo")
    Observable<ObjectResponse<String>> buyVideo(@Body HashMap<String, String> map);

    /**
     * 我的视频
     *
     * @param status 0:待审核 1:审核通过 2:审核未通过
     */
    @GET("video/getMyVideo")
    Observable<ObjectResponse<MyVideosResp>> getMyVideos(@Query("status") Integer status, @Query("page") int page, @Query("page_size") int pageSize);

    /**
     * 购买的视频
     */
    @GET("video/getPurchase")
    Observable<ObjectResponse<MyVideosResp>> getPurChase(@Query("page") int page, @Query("page_size") int pageSize);

    /**
     * 账单
     */
    @GET("video/videoBill")
    Observable<ObjectResponse<BillResp>> getBills(@Query("page") int page,
                                                  @Query("page_size") int pageSize,
                                                  @Query("start_time") String startDate,
                                                  @Query("end_time") String endDate);

    /**
     * 视频收藏总数
     */
    @GET("video/collectCount")
    Observable<ObjectResponse<HashMap<String, String>>> getFavoriteCount();

    /**
     * 是否购买过
     */
    @GET("video/isBought")
    Observable<ObjectResponse<String>> judge(@Query("video_id") String videoId);


    /**
     * 下载的视频
     */
    @GET("download/getDownloaded")
    Observable<PageResponse2> getDownloadList(@Query("page") int page,
                                              @Query("page_size") int pageSize);

    /**
     * 剩余下载次数
     */
    @GET("download/getDownloadTimes")
    Observable<ObjectResponse<Integer>> getTimes();

    /**
     * 下载视频(添加下载记录)
     */
    @GET("download/DownloadVideos")
    Observable<ObjectResponse> downloadVideo(@Query("videoId") String videoId);

    /**
     * 删除下载记录
     */
    @POST("download/delDownloaded")
    Observable<ObjectResponse> deleteDownloadedVideo(@Body HashMap<String, String> map);

}
