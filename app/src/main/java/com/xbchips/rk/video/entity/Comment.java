package com.xbchips.rk.video.entity;

/**
 * Created by york on 2017/9/25.
 */

public class Comment {

    private int id;
    private String nickname;
    private String avatar;
    private String content;
    private String creat_time;
    private int likes;
    private int Sub_comment_count;
    private boolean selected;

    public Comment() {
    }

    public Comment(int id, String nickname, String avatar, String content, String creatTime) {
        this.id = id;
        this.nickname = nickname;
        this.avatar = avatar;
        this.content = content;
        this.creat_time = creatTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreat_time() {
        return creat_time;
    }

    public void setCreat_time(String creat_time) {
        this.creat_time = creat_time;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public void setSub_comment_count(int sub_comment_count) {
        Sub_comment_count = sub_comment_count;
    }

    public int getSub_comment_count() {
        return Sub_comment_count;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
