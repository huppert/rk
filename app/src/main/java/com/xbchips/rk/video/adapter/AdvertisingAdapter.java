package com.xbchips.rk.video.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.xbchips.rk.R;
import com.xbchips.rk.activities.HtmlActivity;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.system.entity.AdvertList;

import java.util.List;

public class AdvertisingAdapter extends BaseAdapter<AdvertList> {
    public AdvertisingAdapter(List<AdvertList> list, Context context) {
        super(list, context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AdvertiseViewHolder(mInflater.inflate(R.layout.adater_item_advert, parent, false));
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        AdvertiseViewHolder holder = (AdvertiseViewHolder) viewHolder;
        Glide.with(mContext)
                .load(mList.get(position).getImg_url())
                .error(R.drawable.place_holder2)
                .placeholder(R.drawable.place_holder2)
                .into(holder.img);
        holder.img.setOnClickListener(v -> {
            Intent mIntent = new Intent(mContext, HtmlActivity.class);
            AdvertList advertList = mList.get(position);
            mIntent.putExtra(HtmlActivity._URL, advertList.getUrl());
            mIntent.putExtra(HtmlActivity.ARG_TITLE, advertList.getTitle());
            mContext.startActivity(mIntent);
        });
    }


    static class AdvertiseViewHolder extends RecyclerView.ViewHolder {
        public ImageView img;

        public AdvertiseViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.iv_advertising);
        }
    }

}
