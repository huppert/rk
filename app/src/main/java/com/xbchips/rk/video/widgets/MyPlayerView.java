package com.xbchips.rk.video.widgets;

import android.content.Context;
import android.util.AttributeSet;

import com.google.android.exoplayer2.ui.PlayerView;

public class MyPlayerView extends PlayerView {
    public MyPlayerView(Context context) {
        this(context, null);
    }

    public MyPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public boolean performClick() {
        return true;
    }
}
