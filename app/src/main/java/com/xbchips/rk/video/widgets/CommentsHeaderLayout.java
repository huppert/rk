package com.xbchips.rk.video.widgets;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.none.pagerlayoutmanager.PagerGridLayoutManager;
import com.none.pagerlayoutmanager.PagerGridSnapHelper;
import com.xbchips.rk.R;
import com.xbchips.rk.XvideoApplication;
import com.xbchips.rk.activities.HtmlActivity;
import com.xbchips.rk.base.BaseView2;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.lx.JudgeLxPresenter;
import com.xbchips.rk.system.entity.AdvertList;
import com.xbchips.rk.system.entity.Configs;
import com.xbchips.rk.system.entity.DynamicConfig;
import com.xbchips.rk.util.DisplayUtils;
import com.xbchips.rk.util.PreferenceUtils;
import com.xbchips.rk.video.VideoDetailActivity;
import com.xbchips.rk.video.adapter.AdvertisingAdapter;
import com.xbchips.rk.video.adapter.RecommendAdapter;
import com.xbchips.rk.widgets.ItemDecoration;
import com.xbchips.rk.widgets.SpaceItemDecoration;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.xbchips.rk.activities.HtmlActivity.ARG_TITLE;
import static com.xbchips.rk.activities.HtmlActivity._URL;

public class CommentsHeaderLayout extends LinearLayout {
    /**
     * 最热
     */
    private static final String SORT_HOTTEST = "Hottest";
    /**
     * 最新
     */
    private static final String SORT_NEWEST = "Newest";
    @BindView(R.id.tv_video_title)
    public TextView tvVideoTitle;
    @BindView(R.id.tv_watch_count)
    public TextView tvWatchCount;
    @BindView(R.id.tv_like_count)
    public TextView tvLikeCount;
    @BindView(R.id.tv_favor)
    public TextView tvFavor;
    @BindView(R.id.tv_sort)
    public TextView tvSort;
    @BindView(R.id.tv_download)
    public TextView tvDownload;
    @BindView(R.id.tv_switch_line)
    public TextView tvSwitchLine;
    @BindView(R.id.tv_all_comments)
    public TextView tvAllComments;
    @BindView(R.id.center_advertise)
    public RecyclerView AdvertiseRecyclerView;
    @BindView(R.id.recommend_recycler_view)
    public RecyclerView recommendRecyclerView;
    @BindView(R.id.ll_indicators)
    public LinearLayout indicators;
    private JudgeLxPresenter mLxPresenter;
    /**
     * 排序方式
     */
    private String mSort;

    public CommentsHeaderLayout(Context context, BaseView2 baseView) {
        super(context);
        setOrientation(VERTICAL);
        final View view = LayoutInflater.from(context).inflate(R.layout.comments_header, this);
        ButterKnife.bind(this, view);
        mLxPresenter = new JudgeLxPresenter(getContext(), baseView);
    }


    /**
     * 推荐列表
     */
    public void initRecommendRecyclerView(List<VideoItem> list) {
        final PagerGridLayoutManager pagerGridLayoutManager = new PagerGridLayoutManager(3, 2, PagerGridLayoutManager.HORIZONTAL);
        pagerGridLayoutManager.setPageListener(new PagerGridLayoutManager.PageListener() {
            @Override
            public void onPageSizeChanged(int pageSize) {

            }

            @Override
            public void onPageSelect(int pageIndex) {
                for (int i = 0; i < indicators.getChildCount(); i++) {
                    indicators.getChildAt(i).setSelected(i == pageIndex);
                }
            }
        });
        recommendRecyclerView.setLayoutManager(pagerGridLayoutManager);
        PagerGridSnapHelper pageSnapHelper = new PagerGridSnapHelper();
        pageSnapHelper.attachToRecyclerView(recommendRecyclerView);
        recommendRecyclerView.addItemDecoration(new ItemDecoration(DisplayUtils.dip2px(getContext(), 5), 2));
        final RecommendAdapter adapter = new RecommendAdapter(list, getContext());
        adapter.setOnClickListener(v -> {
            final VideoItem item = (VideoItem) v.getTag(v.getId());
            if (item.getIs_advert() == 1) {
                Intent intent = new Intent(getContext(), HtmlActivity.class);
                intent.putExtra(_URL, item.getUrl());
                intent.putExtra(ARG_TITLE, item.getTitle());
                getContext().startActivity(intent);
            } else {
                if (item.isIs_liuxiang()) {
                    mLxPresenter.judge(item);
                } else {
                    Intent intent = new Intent(getContext(), VideoDetailActivity.class);
                    intent.putExtra(VideoDetailActivity.ARG_VIDEO_ID, item.getId());
                    intent.putExtra(VideoDetailActivity.ARG_VIDEO_TITLE, item.getTitle());
                    getContext().startActivity(intent);
                }

            }


        });
        recommendRecyclerView.setAdapter(adapter);
    }


    /**
     * 推荐indicators
     */
    public void initIndicators(int count) {
        int pageCount = (int) Math.ceil(count / 6.0);
        final int size = DisplayUtils.dip2px(getContext(), 6f);
        LayoutParams params = new LayoutParams(size, size);
        params.rightMargin = size;
        for (int i = 0; i < pageCount; i++) {
            final ImageView imageView = new ImageView(getContext());
            imageView.setImageResource(R.drawable.selector_rc_indicator);
            imageView.setSelected(i == 0);
            indicators.addView(imageView, params);
        }
    }


    /**
     * 设置广告banner
     */
    public void setAdvert(List<AdvertList> data) {
        AdvertisingAdapter centerAdapter = new AdvertisingAdapter(data, getContext());
        AdvertiseRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        AdvertiseRecyclerView.addItemDecoration(new SpaceItemDecoration(DisplayUtils.dip2px(getContext(), 5)));
        AdvertiseRecyclerView.setAdapter(centerAdapter);
    }


    /**
     * 评论总数
     */
    public void setCommentCount(int count) {
        SpannableString sb = new SpannableString(getContext().getString(R.string.all_trending_comments, count));
        sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(),R.color.vv_all_rm_count)), 5, sb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        sb.setSpan(new AbsoluteSizeSpan(12, true), 5, sb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvAllComments.setText(sb);
    }


    @OnClick({R.id.tv_like_count, R.id.tv_favor, R.id.tv_switch_line, R.id.tv_sort,
            R.id.tv_download, R.id.tv_share})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_sort://评论排序
                CommentSortWindow sortWindow = new CommentSortWindow(getContext());
                sortWindow.setOnItemClickListener(position -> {
                    //sort
                    if (position == 0) {
                        mSort = SORT_NEWEST;
                        tvSort.setText(R.string.recent);
                    } else {
                        mSort = SORT_HOTTEST;
                        tvSort.setText(R.string.hottest);
                    }
                    sortWindow.dismiss();
                    if (mOnMyClickListener != null) {
                        mOnMyClickListener.onSorted();
                    }

                });
                final int[] anchorLoc = new int[2];
                tvSort.getLocationOnScreen(anchorLoc);
                int xOffset = DisplayUtils.dip2px(getContext(), 50);
                int yOffset = DisplayUtils.dip2px(getContext(), 110);
                sortWindow.showAtLocation(tvSort, Gravity.TOP | Gravity.START, anchorLoc[0] - xOffset, anchorLoc[1] - yOffset);
                break;
            case R.id.tv_like_count://点赞
                if (mOnMyClickListener != null) {
                    mOnMyClickListener.onLiked();
                }
                break;
            case R.id.tv_favor://ic_pf_favor
                if (mOnMyClickListener != null) {
                    mOnMyClickListener.onFavored();
                }
                break;
            case R.id.tv_switch_line://切换线路
                if (mOnMyClickListener != null) {
                    mOnMyClickListener.onSwitch();
                }
                break;
            case R.id.tv_download:
                if (mOnMyClickListener != null) {
                    mOnMyClickListener.onDownload();
                }
                break;
            case R.id.tv_share:
                shareLink();
                break;
        }
    }

    public String getSort() {
        return mSort;
    }

    /**
     * 复制分享链接
     */
    private void shareLink() {
        String json = PreferenceUtils.getString(XvideoApplication.getApplication(), Constants.PREF_CONFIG);
        if (!TextUtils.isEmpty(json)) {
            Configs configs = new Gson().fromJson(json, Configs.class);
            DynamicConfig dynamicConfig = configs.getShare_link();
            if (dynamicConfig != null && dynamicConfig.getStatus() == 8) {
                String url = dynamicConfig.getDescription();
                copy(url);
                Toast.makeText(getContext(), "复制成功", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void copy(String text) {
        ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("text", text);
        clipboard.setPrimaryClip(clip);
    }


    private OnMyClickListener mOnMyClickListener;

    public interface OnMyClickListener {
        void onSorted();

        void onLiked();

        void onFavored();

        void onSwitch();

        void onDownload();
    }

    public void setOnMyClickListener(OnMyClickListener onMyClickListener) {
        mOnMyClickListener = onMyClickListener;
    }
}
