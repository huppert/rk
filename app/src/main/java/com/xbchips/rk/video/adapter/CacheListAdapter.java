package com.xbchips.rk.video.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;

import java.util.List;

public class CacheListAdapter extends BaseAdapter<String> {
    public CacheListAdapter(List<String> list, Context context) {
        super(list, context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CacheListViewHolder(mInflater.inflate(R.layout.item_cache_list, parent, false));
    }


    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        TextView itemView = (TextView) viewHolder.itemView;
        itemView.setText(mList.get(position));
    }

    static class CacheListViewHolder extends RecyclerView.ViewHolder {

        public CacheListViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
