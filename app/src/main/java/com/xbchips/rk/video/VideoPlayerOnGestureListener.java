package com.xbchips.rk.video;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import androidx.core.view.GestureDetectorCompat;

import com.google.android.exoplayer2.Player;
import com.xbchips.rk.util.LogTrace;

/**
 * Created by york on 2017/10/6.
 */

public class VideoPlayerOnGestureListener extends GestureDetector.SimpleOnGestureListener implements View.OnTouchListener {
    private static final String TAG = "VideoPlayerOnGestureLis";
    private final GestureDetectorCompat mGestureDetectorCompat;
    private VideoGestureListener mVideoGestureListener;
    private static final int NONE = 0, VOLUME = 1, BRIGHTNESS = 2, FF_REW = 3;
    private int mViewWidth;
    /**
     * 横向偏移阈值，让快进快退不那么敏感
     */
    private static final int THRESHOLD = 5;
    private boolean hasFF_REW = false;
    private int mScrollMode = NONE;

    public VideoPlayerOnGestureListener(Context context, VideoGestureListener videoGestureListener) {
        LogTrace.d(TAG, "VideoPlayerOnGestureListener: " + context);
        mVideoGestureListener = videoGestureListener;
        mGestureDetectorCompat = new GestureDetectorCompat(context, this);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        LogTrace.d(TAG, "onDown: ");
        //每次按下都重置为NONE
        mScrollMode = NONE;
        if (mVideoGestureListener != null) {
            mVideoGestureListener.onDown(e);
        }
        return true;
    }


    @Override
    public boolean onDoubleTap(MotionEvent e) {
        if (mVideoGestureListener != null) {
            mVideoGestureListener.onDoubleTapGesture(e);
            return true;
        }
        return super.onDoubleTap(e);
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        if (mVideoGestureListener != null) {
            mVideoGestureListener.onSingleTapGesture(e);
            return true;
        }
        return super.onSingleTapUp(e);
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        LogTrace.d(TAG, "onScroll: e1:" + e1.getX() + "," + e1.getY());
        LogTrace.d(TAG, "onScroll: e2:" + e2.getX() + "," + e2.getY());
        LogTrace.d(TAG, "onScroll: X:" + distanceX + "  Y:" + distanceY);
        switch (mScrollMode) {
            case NONE:
                LogTrace.d(TAG, "NONE: ");
                if (Math.abs(distanceX) - Math.abs(distanceY) > THRESHOLD) {
                    mScrollMode = FF_REW;
                } else {
                    mScrollMode = e1.getX() < mViewWidth / 2 ? BRIGHTNESS : VOLUME;
                }
                break;
            case VOLUME:
                if (mVideoGestureListener != null) {
                    mVideoGestureListener.onVolumeGesture(e1, e2, distanceX, distanceY);
                }
                LogTrace.d(TAG, "VOLUME: ");
                break;
            case BRIGHTNESS:
                if (mVideoGestureListener != null) {
                    mVideoGestureListener.onBrightnessGesture(e1, e2, distanceX, distanceY);
                }
                LogTrace.d(TAG, "BRIGHTNESS: ");
                break;
            case FF_REW:
                if (mVideoGestureListener != null) {
                    mVideoGestureListener.onFF_REWGesture(e1, e2, distanceX, distanceY);
                }
                hasFF_REW = true;
                LogTrace.d(TAG, "FF_REW: ");
                break;
        }
        return true;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        LogTrace.d(TAG, "onTouch: " + v.getTag());
        //非结束状态才响应
        int state = (int) v.getTag(v.getId());
        if (state == Player.STATE_ENDED)
            return false;
        mViewWidth = v.getWidth();
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (hasFF_REW) {
                if (mVideoGestureListener != null) {
                    mVideoGestureListener.onEndFF_REW(event);
                }
                hasFF_REW = false;
            } else if (mScrollMode == BRIGHTNESS || mScrollMode == VOLUME) {
                if (mVideoGestureListener != null) {
                    mVideoGestureListener.onUpVoiceOrBrightness();
                }
            }
        }
        return mGestureDetectorCompat.onTouchEvent(event);
    }


    interface VideoGestureListener {
        /**
         * 亮度手势，左半部上下滑动时候调用
         */
        void onBrightnessGesture(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY);

        /**
         * 音量手势， 右半部上下滑动时候调用
         */
        void onVolumeGesture(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY);

        /**
         * 快进快退手势， 左右滑动的时候调用
         */

        void onFF_REWGesture(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY);

        /**
         * 单击手势，确认是单击的时候调用
         */
        void onSingleTapGesture(MotionEvent e);

        /**
         * 双击手势，确认是双击的时候调用
         */
        void onDoubleTapGesture(MotionEvent e);

        /**
         * 按下手势，第一根手指按下时候调用
         */
        void onDown(MotionEvent e);

        /**
         * 快进快退执行后的松开时候调用
         */
        void onEndFF_REW(MotionEvent e);

        /**
         * 亮度或声执行后音松开时调用
         */
        void onUpVoiceOrBrightness();
    }

}


