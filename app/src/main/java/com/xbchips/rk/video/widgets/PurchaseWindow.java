package com.xbchips.rk.video.widgets;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.xbchips.rk.R;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.widgets.RoundButton;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PurchaseWindow extends PopupWindow {
    @BindView(R.id.tv_tutorials)
    TextView tvTutorials;
    @BindView(R.id.rb_confirm)
    RoundButton rbConfirm;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_author)
    TextView tvAuthor;
    @BindView(R.id.tv_price)
    TextView tvPrice;
    @BindView(R.id.cl_content)
    ConstraintLayout clContent;
    @BindView(R.id.root)
    FrameLayout root;
    private View.OnClickListener mListener;
    private Context mContext;

    public PurchaseWindow(Context context) {
        super(context);
        mContext = context;
    }


    public void show(VideoItem video) {
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_purchase, null);
        ButterKnife.bind(this, contentView);
        setContentView(contentView);
        setBackgroundDrawable(new ColorDrawable(0x33000000));
        setOutsideTouchable(false);
        setFocusable(true);
        setAnimationStyle(R.style.PopupWindowAnimationStyle);
        showAtLocation(contentView, Gravity.CENTER, 0, 0);
        tvTutorials.setPaintFlags(tvTutorials.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        rbConfirm.setOnClickListener(mListener);
        tvTutorials.setOnClickListener(mListener);
        clContent.setOnClickListener(mListener);
        root.setOnClickListener(mListener);
        tvTitle.setText(mContext.getString(R.string.video_title_f, video.getTitle()));
        tvAuthor.setText(mContext.getString(R.string.video_author_f, video.getUsername()));
        tvPrice.setText(mContext.getString(R.string.video_price_f, video.getPrice()));

    }

    public void setListener(View.OnClickListener listener) {
        mListener = listener;
    }


}
