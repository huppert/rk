package com.xbchips.rk.video.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.util.DisplayUtils;

import java.util.List;

public class FastCommentAdapter extends BaseAdapter<String> {
    public FastCommentAdapter(List<String> list, Context context) {
        super(list, context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TextView textView = new TextView(mContext);
        textView.setTextColor(ContextCompat.getColor(mContext, R.color.fc_item_text));
        textView.setTextSize(15);
        int base = DisplayUtils.dip2px(mContext, 5);
        int height = base * 10;
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setPadding(base * 3, 0, base * 2, 0);
        textView.setMaxLines(2);
        textView.setEllipsize(TextUtils.TruncateAt.END);
        textView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));
        return new FastCommentViewHolder(textView);
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        FastCommentViewHolder holder = (FastCommentViewHolder) viewHolder;
        TextView itemView = (TextView) holder.itemView;
        String text = mList.get(position);
        itemView.setText(text);
        itemView.setTag(text);
        itemView.setOnClickListener(mOnClickListener);

    }

    static class FastCommentViewHolder extends RecyclerView.ViewHolder {

        public FastCommentViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
