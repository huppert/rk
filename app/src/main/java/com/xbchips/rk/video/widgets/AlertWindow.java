package com.xbchips.rk.video.widgets;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.xbchips.rk.R;

/**
 * Created by york on 2017/11/1.
 * 收藏成功提示框
 */

public class AlertWindow extends PopupWindow {
    private View.OnClickListener mListener;
    private Context mContext;

    public AlertWindow(Context context) {
        super(context);
        mContext = context;
    }

    public void show() {
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_bookmark, null);
        setContentView(contentView);
        setBackgroundDrawable(new ColorDrawable(0x20000000));
        setOutsideTouchable(false);
        setFocusable(true);
        setAnimationStyle(R.style.PopupWindowAnimationStyle);
        showAtLocation(contentView, Gravity.CENTER, 0, 0);
        ImageView iv_close = contentView.findViewById(R.id.iv_close);
        TextView to_bookmark = contentView.findViewById(R.id.to_bookmark);
        iv_close.setOnClickListener(v -> dismiss());
        to_bookmark.setOnClickListener(mListener);
    }

    public void setListener(View.OnClickListener listener) {
        mListener = listener;
    }
}
