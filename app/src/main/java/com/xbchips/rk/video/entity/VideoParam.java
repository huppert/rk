package com.xbchips.rk.video.entity;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public class VideoParam implements Parcelable {
    private String author;
    private Uri imgUrl;
    private String price;
    private String duration;
    private String title;
    private String desc;

    public VideoParam() {
    }

    protected VideoParam(Parcel in) {
        author = in.readString();
        imgUrl = in.readParcelable(Uri.class.getClassLoader());
        price = in.readString();
        duration = in.readString();
        title = in.readString();
        desc = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(author);
        dest.writeParcelable(imgUrl, flags);
        dest.writeString(price);
        dest.writeString(duration);
        dest.writeString(title);
        dest.writeString(desc);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VideoParam> CREATOR = new Creator<VideoParam>() {
        @Override
        public VideoParam createFromParcel(Parcel in) {
            return new VideoParam(in);
        }

        @Override
        public VideoParam[] newArray(int size) {
            return new VideoParam[size];
        }
    };

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Uri getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(Uri imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
