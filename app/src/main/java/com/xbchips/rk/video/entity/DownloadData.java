package com.xbchips.rk.video.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class DownloadData implements Parcelable {
    private String videoId;
    private String title;
    private String coverUrl;
    private String videoUrl;
    private long videoDuration;
    private boolean opened;

    public DownloadData(String videoId, String title, String coverUrl, String videoUrl, long videoDuration) {
        this.videoId = videoId;
        this.title = title;
        this.coverUrl = coverUrl;
        this.videoUrl = videoUrl;
        this.videoDuration = videoDuration;
    }


    protected DownloadData(Parcel in) {
        videoId = in.readString();
        title = in.readString();
        coverUrl = in.readString();
        videoUrl = in.readString();
        videoDuration = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(videoId);
        dest.writeString(title);
        dest.writeString(coverUrl);
        dest.writeString(videoUrl);
        dest.writeLong(videoDuration);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DownloadData> CREATOR = new Creator<DownloadData>() {
        @Override
        public DownloadData createFromParcel(Parcel in) {
            return new DownloadData(in);
        }

        @Override
        public DownloadData[] newArray(int size) {
            return new DownloadData[size];
        }
    };

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public long getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(long videoDuration) {
        this.videoDuration = videoDuration;
    }

    public boolean isOpened() {
        return opened;
    }

    public void setOpened(boolean opened) {
        this.opened = opened;
    }
}
