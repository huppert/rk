package com.xbchips.rk.video.widgets;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.xbchips.rk.R;

public class CommentSortWindow extends PopupWindow {
    private Context mContext;

    private OnItemClickListener mOnItemClickListener;

    public CommentSortWindow(Context context) {
        mContext = context;
        init();
    }

    public void init() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_window_sort, null);
        setContentView(view);
        setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setOutsideTouchable(true);

        View recent = view.findViewById(R.id.tv_by_recent);
        View popular = view.findViewById(R.id.tv_by_popular);
        recent.setOnClickListener(v -> mOnItemClickListener.OnItemClick(0));
        popular.setOnClickListener(v -> mOnItemClickListener.OnItemClick(1));
    }


    public interface OnItemClickListener {
        void OnItemClick(int itemPosition);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }
}
