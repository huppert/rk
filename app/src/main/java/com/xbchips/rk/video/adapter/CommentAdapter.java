package com.xbchips.rk.video.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.video.entity.Comment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by york on 2017/9/25.
 */

public class CommentAdapter extends BaseAdapter<Comment> {


    public CommentAdapter(List<Comment> list, Context context) {
        super(list, context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_ITEM:
                return new CommentViewHolder(mInflater.inflate(R.layout.item_comment, parent, false));
            case TYPE_FOOTER:
                return getFooterViewHolder(parent);
            case TYPE_HEADER:
                return new HeaderViewHolder(mHeaderView);

        }
        throw new IllegalArgumentException("wrong item type");
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        CommentViewHolder viewHolder = (CommentViewHolder) holder;
        Comment comment = mList.get(getRealPosition(position));
        ImageLoader.loadAvatar(mContext, comment.getAvatar(), viewHolder.ivAvatar);
        viewHolder.tvComment.setText(comment.getContent());
        viewHolder.tvNickname.setText(comment.getNickname());
        viewHolder.tvTime.setText(comment.getCreat_time());
        viewHolder.tvLikeCount.setText(String.valueOf(comment.getLikes()));
        viewHolder.tvLikeCount.setOnClickListener(mOnClickListener);
        viewHolder.tvLikeCount.setTag(comment);
        viewHolder.tvReplyCount.setText(String.valueOf(comment.getSub_comment_count()));
        viewHolder.tvReplyCount.setTag(comment);
        viewHolder.tvReplyCount.setOnClickListener(mOnClickListener);


    }

    static class CommentViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_avatar)
        ImageView ivAvatar;
        @BindView(R.id.tv_nickname)
        TextView tvNickname;
        @BindView(R.id.tv_comment)
        TextView tvComment;
        @BindView(R.id.tv_like_count)
        TextView tvLikeCount;
        @BindView(R.id.tv_reply_count)
        TextView tvReplyCount;
        @BindView(R.id.tv_time)
        TextView tvTime;

        public CommentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
