package com.xbchips.rk.video.entity;

/**
 * Created by york on 2017/8/14.
 * 评价百分比
 */

public class Assess {
    /**
     * 好评百分比
     */
    private int like;

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }
}
