package com.xbchips.rk.video.widgets;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.xbchips.rk.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FullControlLayout extends FrameLayout {

    @BindView(R.id.tv_like_count)
    public TextView tvLikeCount;
    @BindView(R.id.tv_download)
    public TextView tvDownload;
    @BindView(R.id.tv_switch_line)
    public TextView tvSwitchLine;
    @BindView(R.id.tv_favor)
    public TextView tvFavor;
    @BindView(R.id.tv_share)
    TextView tvShare;
    private List<String> lines;
    private Unbinder mUnbinder;
    private OnViewClickedListener mOnViewClickedListener;
    private ListPopWindow popWindows;

    public FullControlLayout(Context context) {
        this(context, null);
    }

    public FullControlLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_full_controller, this);
        mUnbinder = ButterKnife.bind(this, view);

        view.setOnClickListener(v -> ((Activity) getContext()).onBackPressed());
    }


    public void setTvLikeCount(TextView textView) {
        tvLikeCount.setSelected(textView.isSelected());
        tvLikeCount.setText(textView.getText());
    }

    public void setTvFavor(TextView textView) {
        tvFavor.setSelected(textView.isSelected());
        tvFavor.setText(textView.getText());
    }

    public void setTvSwitchLine(TextView textView) {
        tvSwitchLine.setSelected(textView.isSelected());
        tvSwitchLine.setText(textView.getText());
    }


    public void setLines(List<String> lines) {
        this.lines = lines;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

    }

    @OnClick({R.id.tv_like_count, R.id.tv_download, R.id.tv_switch_line, R.id.tv_favor, R.id.tv_share})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_like_count:
                if (mOnViewClickedListener != null) {
                    mOnViewClickedListener.onLikeCountClicked();
                }
                break;
            case R.id.tv_download:
                if (mOnViewClickedListener != null) {
                    mOnViewClickedListener.onDownloadClicked();
                }
                break;
            case R.id.tv_switch_line:
                if (popWindows != null && popWindows.isShowing()) {
                    popWindows.dismiss();
                }
                popWindows = new ListPopWindow(getContext());
                popWindows.setOnItemClickListener(position -> {
                    tvSwitchLine.setSelected(true);
                    if (mOnViewClickedListener != null) {
                        mOnViewClickedListener.onLineSelected(position);
                    }

                });

                String currentLine = tvSwitchLine.getText().toString();
                popWindows.init(lines.size(), currentLine);
                popWindows.showAsDropDown(tvSwitchLine);
                break;
            case R.id.tv_favor:
                if (mOnViewClickedListener != null) {
                    mOnViewClickedListener.onFavorClicked();
                }
                break;
            case R.id.tv_share:
                break;
        }
    }

    public interface OnViewClickedListener {

        void onLikeCountClicked();

        void onDownloadClicked();

        void onLineSelected(int position);

        void onFavorClicked();
    }

    public void setOnViewClickedListener(OnViewClickedListener onViewClickedListener) {
        mOnViewClickedListener = onViewClickedListener;
    }
}
