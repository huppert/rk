package com.xbchips.rk.video.entity;

import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.base.response.PageResponse;

import java.util.List;

public class CommentsResp extends ObjectResponse<List<Comment>> {


    protected Page page;


    public Page getPage() {
        return page;
    }

    public static class Page extends PageResponse.Page {
        private int amount;

        public int getAmount() {
            return amount;
        }
    }

}
