package com.xbchips.rk.video;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.offline.Download;
import com.google.android.exoplayer2.offline.DownloadHelper;
import com.google.android.exoplayer2.offline.DownloadManager;
import com.google.android.exoplayer2.offline.DownloadRequest;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.xbchips.rk.R;
import com.xbchips.rk.XvideoApplication;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.profile.activity.BookMarkActivity;
import com.xbchips.rk.system.SystemService;
import com.xbchips.rk.system.entity.CloudDomain;
import com.xbchips.rk.user.activities.UserAuthActivity;
import com.xbchips.rk.user.activities.WapPayActivity;
import com.xbchips.rk.user.entity.User;
import com.xbchips.rk.util.BackHandlerHelper;
import com.xbchips.rk.util.LogTrace;
import com.xbchips.rk.util.NetWorkUtils;
import com.xbchips.rk.util.PreferenceUtils;
import com.xbchips.rk.video.adapter.CommentAdapter;
import com.xbchips.rk.video.entity.Comment;
import com.xbchips.rk.video.entity.CommentsResp;
import com.xbchips.rk.video.entity.DownloadData;
import com.xbchips.rk.video.entity.VideoDetail;
import com.xbchips.rk.video.fragments.CachingDialogFragment;
import com.xbchips.rk.video.fragments.CommentLayout;
import com.xbchips.rk.video.fragments.SubCommentFragment;
import com.xbchips.rk.video.widgets.AlertWindow;
import com.xbchips.rk.video.widgets.CommentsHeaderLayout;
import com.xbchips.rk.video.widgets.FullControlLayout;
import com.xbchips.rk.video.widgets.ListPopWindow;
import com.xbchips.rk.video.widgets.MyPlayerView;
import com.xbchips.rk.widgets.EndlessRecyclerView;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import okhttp3.HttpUrl;

public class VideoDetailActivity extends BaseActivity implements VideoContract.View,
        Player.EventListener {
    private static final String TAG = "VideoDetailActivity";
    private static final int CODE_REQUEST = 201;
    public static final String ARG_VIDEO_ID = "video_id";
    public static final String ARG_VIDEO_TITLE = "video_tile";
    public static final String ARG_VIDEO_ISXIANGYOU = "video_isxiangyou";

    @BindView(R.id.comment_recycler_view)
    EndlessRecyclerView commentRecyclerView;
    @BindView(R.id.iv_cover)
    ImageView ivCover;
    @BindView(R.id.iv_pre_play)
    ImageView ivPrePlay;
    @BindView(R.id.player_view)
    MyPlayerView mPlayerView;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_menu)
    ImageView ivMenu;
    @BindView(R.id.iv_full_screen)
    ImageView ivFullScreen;
    @BindView(R.id.tv_ff)
    TextView tvFf;
    @BindView(R.id.tv_rewind)
    TextView tvRewind;
    @BindView(R.id.buffer_bar)
    ProgressBar bufferBar;
    @BindView(R.id.btn_replay)
    TextView btnReplay;
    @BindView(R.id.tv_percent)
    TextView mTvPercent;
    @BindView(R.id.cl_video)
    ConstraintLayout clVideo;
    @BindView(R.id.comment_layout)
    CommentLayout commentLayout;
    private CommentsHeaderLayout mHeaderLayout;

    private SimpleExoPlayer mPlayer;

    private final StringBuilder builder = new StringBuilder();
    private final Formatter formatter = new Formatter(builder, Locale.getDefault());
    private final VideoPresenter mPresenter = new VideoPresenter(this);
    private final VideoService service = HttpRequest.createService(VideoService.class);
    private String mVideoId;
    private int mAssess;
    private DataSource.Factory dataSourceFactory;
    private long resumePosition;
    private int resumeWindow;
    private boolean shouldAutoPlay;
    private boolean isEndedState;

    private boolean mIsFullMode;
    private float mBrightnessValue;

    /**
     * 当前视频的url
     */
    private String videoUrl;
    /**
     * 当前ms
     */
    private long mPositionMs;
    /**
     * 快进快退之前的ms
     */
    private long mOriginPositionMs;
    /**
     * 线路选择window
     */
    private ListPopWindow popWindows;

    /**
     * 当前线路下标 默认-1 使用接口返回的域名,只有播放错误时才会改变
     */
    private int mCurrentLineIndex = -1;


    private FullControlLayout mFullControlLayout;
    /**
     * 评论列表
     */
    private final List<Comment> mComments = new ArrayList<>();

    private boolean isFromFree;
    private String isSearch;
    private Disposable mFfRewindDisposable;
    /**
     * 封面url
     */
    private String mCoverUrl;
    /**
     * 评论数量
     */
    private int mCommentCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        recreate();
    }

    private void init() {
        initView();
        mVideoId = getIntent().getStringExtra(ARG_VIDEO_ID);
        isFromFree = getIntent().getBooleanExtra("isFromFree", false);
        isSearch = getIntent().getStringExtra("isFromSearch");
        if (isSearch == null) {
            isSearch = "";
        }
        shouldAutoPlay = true;
        clearResumePosition();
        dataSourceFactory = buildDataSourceFactory();
        mPlayerView.requestFocus();
        setScreenMode(mIsFullMode);
        mPresenter.loadData(mVideoId, false);
        initVideoLines();
        getAdvert();
        getRecommends();
        loadComments();


    }

    private void initView() {
        mHeaderLayout = new CommentsHeaderLayout(this, this);
        mHeaderLayout.setOnMyClickListener(new CommentsHeaderLayout.OnMyClickListener() {
            @Override
            public void onSorted() {
                loadComments();
            }

            @Override
            public void onLiked() {
                thumbsup();
            }

            @Override
            public void onFavored() {
                favor();
            }

            @Override
            public void onSwitch() {
                if (popWindows != null && popWindows.isShowing()) {
                    popWindows.dismiss();
                }
                popWindows = new ListPopWindow(VideoDetailActivity.this);
                popWindows.setOnItemClickListener(itemPosition -> selectLine(itemPosition));
                String currentLine = mHeaderLayout.tvSwitchLine.getText().toString();
                popWindows.init(lines.size(), currentLine);
                popWindows.showAsDropDown(mHeaderLayout.tvSwitchLine);
            }

            @Override
            public void onDownload() {
                checkDownload();
            }
        });
        mHeaderLayout.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        //视频地址加载完成之前 切换和下载不能点击
        mHeaderLayout.tvSwitchLine.setEnabled(false);
        mHeaderLayout.tvDownload.setEnabled(false);
        String title = getIntent().getStringExtra(ARG_VIDEO_TITLE);
        tvTitle.setText(title);
        mHeaderLayout.tvVideoTitle.setText(title);
        mHeaderLayout.tvLikeCount.setText("0%");
        mHeaderLayout.tvFavor.setText(R.string.favor);
        mHeaderLayout.setCommentCount(mCommentCount);
        commentLayout.setStateChnageListener(new CommentLayout.OnStateChangeListener() {
            @Override
            public void onChanged(boolean showing) {

            }

            @Override
            public void sendComment(String text) {
                comment(text);
            }
        });
        initCommentRecyclerView();
    }


    /**
     * 评论列表
     */
    private void initCommentRecyclerView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        commentRecyclerView.setLayoutManager(layoutManager);
        CommentAdapter adapter = new CommentAdapter(mComments, this);
        adapter.setHeaderView(mHeaderLayout);
        adapter.setOnClickListener(v -> {
            final Comment comment = (Comment) v.getTag();
            switch (v.getId()) {
                case R.id.tv_like_count:
                    likeComment(comment);
                    break;
                case R.id.tv_reply_count:
                    final int subCommentCount = comment.getSub_comment_count();
                    SubCommentFragment subCommentFragment = SubCommentFragment.getInstance(comment.getId(), mVideoId, subCommentCount);
                    subCommentFragment.setListener((id, count) -> {
                        for (int i = 0; i < mComments.size(); i++) {
                            final Comment comment1 = mComments.get(i);
                            if (comment1.getId() == id) {
                                comment1.setSub_comment_count(count);
                                commentRecyclerView.getAdapter().notifyItemChanged(i);
                                break;
                            }
                        }
                    });
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fl_fragment, subCommentFragment)
                            .addToBackStack(null)
                            .commit();
                    break;
            }
        });
        commentRecyclerView.setAdapter(adapter);
        commentRecyclerView.setOnLoadMoreListener(() -> {
            adapter.addFooter();
            loadComments();
        });
        commentRecyclerView.post(() -> layoutManager.scrollToPosition(0));
    }


    private void loadComments() {
        mPresenter.getComments(mVideoId, mHeaderLayout.getSort(), commentRecyclerView.getCurPage(), 20);
    }

    private void getRecommends() {
        Disposable disposable = service.getRecommends(mVideoId, 1, 20)
                .compose(RxUtils.applySchedulers())
                .filter(response -> !handleResponse(response))
                .subscribe(response -> {
                    final List<VideoItem> list = response.getData().getList();
                    mHeaderLayout.initIndicators(list.size());
                    mHeaderLayout.initRecommendRecyclerView(list);
                }, throwable -> {
                });
        mCompositeDisposable.add(disposable);
    }

    private void comment(String content) {
        final HashMap<String, Object> map = new HashMap<>();
        map.put("content", content);
        final Disposable disposable = service.comment(mVideoId, map)
                .compose(RxUtils.applySchedulers())
                .filter(response -> !handleResponse(response))
                .subscribe(response -> {
                    showToast("发送成功");
                    final User localUser = UserHelper.getLocalUser();
                    final String creatTime = response.getData().getCreat_time();
                    int id = response.getData().getId();
                    final Comment comment = new Comment(id, localUser.getNickname(), localUser.getAvatar(), content, creatTime);
                    mComments.add(0, comment);
                    commentRecyclerView.getAdapter().notifyItemInserted(1);
                    mCommentCount++;
                    mHeaderLayout.setCommentCount(mCommentCount);
                }, this::handleFailure);
        mCompositeDisposable.add(disposable);
    }


    private void likeComment(Comment comment) {
        final Disposable disposable = service.likeComment(comment.getId())
                .compose(RxUtils.applySchedulers())
                .filter(response -> !handleResponse(response))
                .subscribe(response -> {
                    comment.setSelected(true);
                    comment.setLikes(comment.getLikes() + 1);
                    for (int i = 0; i < mComments.size(); i++) {
                        if (mComments.get(i).getId() == comment.getId()) {
                            commentRecyclerView.getAdapter().notifyItemChanged(i + 1);
                            break;
                        }
                    }
                }, this::handleFailure);
        mCompositeDisposable.add(disposable);
    }


    private void checkDownload() {
        if (XvideoApplication.getApplication().getDownloadTracker().isDownloaded(Uri.parse(videoUrl))) {
            showToast("已经在下载列表中");
            return;
        }
        showProgress();
        final Disposable disposable = service.downloadVideo(mVideoId)
                .compose(RxUtils.applySchedulers())
                .filter(resp -> !handleResponse(resp))
                .subscribe(response -> downloadVideo(), this::handleFailure);
        mCompositeDisposable.add(disposable);
    }

    /**
     * 广告
     */
    private void getAdvert() {
        Disposable disposable = HttpRequest.createService(SystemService.class)
                .getPlayAdvertList(Constants.APP_PLAY_CENTER)
                .compose(RxUtils.applySchedulers())
                .subscribe(response -> {
                    if (response.getStatus() == 0) {
                        mHeaderLayout.setAdvert(response.getData());
                        wifiPlayVideo();
                    } else {
                        wifiPlayVideo();
                    }
                }, throwable -> wifiPlayVideo());
        mCompositeDisposable.add(disposable);
    }


    /**
     * 线路列表
     */
    private final List<String> lines = new ArrayList<>();

    /**
     * 初始化线路列表
     */
    private void initVideoLines() {
        String pref = PreferenceUtils.getString(this, Constants.VIDEO_DOMAINS);
        CloudDomain cloudDomain = new Gson().fromJson(pref, CloudDomain.class);
        lines.addAll(cloudDomain.getVideo());
        LogTrace.d(TAG, "initVideoLines: " + lines.toString());
    }


    /**
     * 线路选择
     *
     * @param domain 要切换的域名
     */
    private void switchLine(String domain) {
        //如果传的是完整域名则切除原有域名，保留视频地址
        if (videoUrl.contains("http://")) {
            videoUrl = domain + HttpUrl.parse(videoUrl).encodedPath();
        } else {
            String domain1 = lines.get(0);
            videoUrl = domain1 + videoUrl;
        }
        initPlayer();

    }


    private void wifiPlayVideo() {
        if (NetWorkUtils.getAPNType(this) == 1) { //wifi环境下直接播放视频
            //可能视频已经开始播放，而广告才加载完，这里做判断
            boolean isPlaying = mPlayerView.getPlayer() != null &&
                    (mPlayerView.getPlayer().getPlaybackState() == Player.STATE_READY || mPlayerView.getPlayer().getPlayWhenReady());
            if (!isPlaying) {
                loadVideoUrl();
            }


        }
    }


    VideoPlayerOnGestureListener.VideoGestureListener mVideoGestureListener = new VideoPlayerOnGestureListener.VideoGestureListener() {
        @Override
        public void onBrightnessGesture(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            mPlayerView.hideController();
            int height = mPlayerView.getHeight();
            LogTrace.i(TAG, "onBrightnessGesture: SimpleExoPlayerView height=" + height);
            mBrightnessValue = (e1.getY() - e2.getY()) / 4 / height + mBrightnessValue;
            if (mBrightnessValue < 0)
                mBrightnessValue = 0;
            if (mBrightnessValue > 1)
                mBrightnessValue = 1;
            LogTrace.d(TAG, "onBrightnessGesture: screenBrightness=" + mBrightnessValue);
            WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
            layoutParams.screenBrightness = mBrightnessValue;
            getWindow().setAttributes(layoutParams);
            updateBrightnessPercent((int) (mBrightnessValue * 100));
        }

        @Override
        public void onVolumeGesture(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            mPlayerView.hideController();
            int height = mPlayerView.getHeight();
            LogTrace.d(TAG, "onVolumeGesture: SimpleExoPlayerView height=" + height);
            float volume = (e1.getY() - e2.getY()) / 4 / height + mPlayer.getVolume();
            if (volume < 0)
                volume = 0;
            if (volume > 1)
                volume = 1;
            LogTrace.d(TAG, "onVolumeGesture: volume=" + volume);
            mPlayer.setVolume(volume);
            updateVolumePercent((int) (volume * 100));
        }

        @Override
        public void onFF_REWGesture(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            mPlayerView.hideController();
            long duration = mPlayer.getDuration();
            if (duration == C.TIME_UNSET)
                return;
            int per = (int) (duration / mPlayerView.getWidth());
            float offset = e2.getX() - e1.getX();
            mPositionMs = (long) (offset / 2 * per) + mPlayer.getCurrentPosition();
            if (mPositionMs < 0)
                mPositionMs = 0;
            if (mPositionMs > duration)
                mPositionMs = duration;
            LogTrace.d(TAG, "onFF_REWGesture: mPositionMs=" + mPositionMs);
            updateTime(offset);
        }

        @Override
        public void onSingleTapGesture(MotionEvent e) {
            if (mPlayerView.isControllerVisible()) {
                mPlayerView.hideController();
            } else {
                mPlayerView.showController();
            }
        }

        @Override
        public void onDoubleTapGesture(MotionEvent e) {
            if (mFfRewindDisposable != null && !mFfRewindDisposable.isDisposed()) {
                mFfRewindDisposable.dispose();
            }
            //快退10秒
            if (e.getX() < mPlayerView.getWidth() / 2.0f) {
                tvFf.setVisibility(View.INVISIBLE);
                mPlayer.seekTo(Math.max(0, mPlayer.getCurrentPosition() - 10_000));
                tvRewind.setVisibility(View.VISIBLE);
            } else {//快进10秒
                mPlayer.seekTo(Math.min(mPlayer.getCurrentPosition() + 10_000, mPlayer.getDuration()));
                tvFf.setVisibility(View.VISIBLE);
                tvRewind.setVisibility(View.INVISIBLE);
            }
            //2秒后隐藏 快进或者快退
            mFfRewindDisposable = Observable.timer(2, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                    .subscribe(aLong -> {
                        if (tvRewind.getVisibility() == View.VISIBLE) {
                            tvRewind.setVisibility(View.INVISIBLE);
                        } else {
                            tvFf.setVisibility(View.INVISIBLE);
                        }

                    }, throwable -> {
                    });
            mCompositeDisposable.add(mFfRewindDisposable);

        }

        @Override
        public void onDown(MotionEvent e) {
            mOriginPositionMs = mPlayer.getCurrentPosition();
        }

        @Override
        public void onEndFF_REW(MotionEvent e) {
            mPlayer.seekTo(mPositionMs);
            Disposable disposable = Observable.timer(2, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                    .subscribe(aLong -> mTvPercent.setVisibility(View.GONE), throwable -> {

                    });
            mCompositeDisposable.add(disposable);
        }

        @Override
        public void onUpVoiceOrBrightness() {
            Disposable disposable = Observable.timer(2, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                    .subscribe(aLong -> mTvPercent.setVisibility(View.GONE), throwable -> {

                    });
            mCompositeDisposable.add(disposable);
        }
    };


    /**
     * 更新声音百分比
     */
    private void updateVolumePercent(int percent) {
        mTvPercent.setVisibility(View.VISIBLE);
        Drawable drawable = ContextCompat.getDrawable(this, percent == 0 ?
                R.drawable.playctrl_mute : R.drawable.playctrl_volume);
        mTvPercent.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
        mTvPercent.setText(getString(R.string.percent, percent));
    }

    /**
     * 更新亮度百分比
     */
    private void updateBrightnessPercent(int percent) {
        mTvPercent.setVisibility(View.VISIBLE);
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.playctrl_brightness);
        mTvPercent.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
        mTvPercent.setText(getString(R.string.percent, percent));
    }

    /**
     * 更新时间
     */
    private void updateTime(float deltaX) {
        mTvPercent.setVisibility(View.VISIBLE);
        Drawable drawable = ContextCompat.getDrawable(this, deltaX > 0 ? R.drawable.ic_ff : R.drawable.ic_rewind);
        mTvPercent.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
        long deltaMs = (mPositionMs - mOriginPositionMs) / 1000;
        String format = deltaMs > 0 ? "%s  +%ds" : "%s  %ds";
        mTvPercent.setText(String.format(Locale.CHINA, format, Util.getStringForTime(builder, formatter, mPositionMs), deltaMs));
    }


    @Override
    public void setData(VideoDetail data) {
        tvTitle.setText(data.getTitle());
        mHeaderLayout.tvVideoTitle.setText(data.getTitle());
        mHeaderLayout.tvWatchCount.setText(getString(R.string.watch_times, data.getViews()));
        mHeaderLayout.tvFavor.setText(R.string.favor);
        mCoverUrl = data.getThumb_img_url();
        ImageLoader.load(this, data.getThumb_img_url(), ivCover, R.drawable.bg_video_holder);

        refreshAllState(data);
    }

    @Override
    public void setLikeState(int state, int percent) {
        switch (state) {
            case 0://未评价
                break;
            case 1://点赞
                mHeaderLayout.tvLikeCount.setSelected(true);
                break;
            case 2://不喜欢
                break;
        }
        mAssess = state;
        mHeaderLayout.tvLikeCount.setText(String.format(Locale.getDefault(), "%d%%", percent));
        if (mFullControlLayout != null && mFullControlLayout.isAttachedToWindow()) {
            mFullControlLayout.setTvLikeCount(mHeaderLayout.tvLikeCount);
        }
    }

    @Override
    public void setMarkState(boolean state) {
        mHeaderLayout.tvFavor.setSelected(state);
        if (mFullControlLayout != null && mFullControlLayout.isAttachedToWindow()) {
            mFullControlLayout.setTvFavor(mHeaderLayout.tvFavor);
        }
        if (state) {
            showWindow();
        } else {
            showToast(R.string.canceled);
        }
    }

    @Override
    public void refreshAllState(VideoDetail data) {
        mHeaderLayout.tvFavor.setSelected(data.isCollect());
        setLikeState(data.getAssess(), data.getLike());
    }

    @Override
    public void getVideoUrlSuccess(String url) {
        clVideo.removeView(ivPrePlay);
        mHeaderLayout.tvSwitchLine.setEnabled(true);
        videoUrl = url;
        initPlayer();
        mPlayerView.setOnTouchListener(new VideoPlayerOnGestureListener(this, mVideoGestureListener));
    }

    @Override
    public void getVideoUrlFailed(boolean isConsumed) {
        ivPrePlay.setVisibility(View.VISIBLE);
        bufferBar.setVisibility(View.INVISIBLE);
        if (!isConsumed) {
            showToast("获取视频地址失败，请稍后重试");
        }

    }

    @Override
    public void getCommentsSuccess(CommentsResp response) {
        BaseAdapter adapter = (BaseAdapter) commentRecyclerView.getAdapter();
        if (commentRecyclerView.getCurPage() == 1) {
            mCommentCount = response.getPage().getAmount();
            mHeaderLayout.setCommentCount(mCommentCount);
            mComments.clear();
        }
        adapter.removeFooter();
        commentRecyclerView.setPageCount(response.getPage().getTotal());
        List<Comment> list = response.getData();
        mComments.addAll(list);
        int size = list.size();
        if (commentRecyclerView.isFirstPage()) {
            adapter.notifyDataSetChanged();
        } else {
            adapter.notifyItemRangeInserted(mComments.size() - size + adapter.getHeaderSize(), size);
        }
        if (commentRecyclerView.isLastPage()) {
            adapter.addNoMoreView();
        }

        commentRecyclerView.setLoading(false);
    }


    @Override
    public void getCommentsFailed(Throwable throwable) {
        handleFailure(throwable);
        commentRecyclerView.setLoading(false);
        if (!commentRecyclerView.isFirstPage()) {
            commentRecyclerView.pageDecrement();
            BaseAdapter adapter = (BaseAdapter) commentRecyclerView.getAdapter();
            adapter.removeFooter();
        }

    }

    /**
     * 显示收藏成功提示框
     */
    private void showWindow() {
        AlertWindow alertWindow = new AlertWindow(this);
        alertWindow.setListener(v -> {
            Intent intent = new Intent(VideoDetailActivity.this, BookMarkActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, CODE_REQUEST);
            alertWindow.dismiss();
        });
        alertWindow.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == CODE_REQUEST) {
            mPresenter.loadData(mVideoId, true);
        }
    }


    @OnClick({R.id.btn_replay, R.id.iv_full_screen, R.id.iv_pre_play, R.id.iv_back, R.id.iv_menu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_replay:
                view.setVisibility(View.INVISIBLE);
                replay();
                break;
            case R.id.iv_full_screen:
                setScreenMode(!mIsFullMode);
                break;
            case R.id.iv_back:
                if (mIsFullMode) {
                    setScreenMode(false);
                } else {
                    finish();
                }
                break;
            case R.id.iv_pre_play:
                if (NetWorkUtils.getAPNType(this) != 1) {//非wifi网络提示
                    new AlertDialog.Builder(this)
                            .setCancelable(false)
                            .setMessage(R.string.not_wifi_network)
                            .setPositiveButton(R.string.continued, (dialog, which) -> loadVideoUrl())
                            .setNegativeButton(R.string.cancel, null)
                            .show();
                } else {
                    loadVideoUrl();
                }
                break;

            case R.id.iv_menu:
                mFullControlLayout = new FullControlLayout(this);
                mFullControlLayout.setOnViewClickedListener(new FullControlLayout.OnViewClickedListener() {
                    @Override
                    public void onLikeCountClicked() {
                        thumbsup();
                    }

                    @Override
                    public void onDownloadClicked() {
                        checkDownload();
                    }

                    @Override
                    public void onLineSelected(int position) {
                        //关闭mFullControlLayout
                        onBackPressed();
                        selectLine(position);
                    }


                    @Override
                    public void onFavorClicked() {
                        favor();

                    }
                });
                mFullControlLayout.setLines(lines);
                mFullControlLayout.setTvFavor(mHeaderLayout.tvFavor);
                mFullControlLayout.setTvLikeCount(mHeaderLayout.tvLikeCount);
                mFullControlLayout.setTvSwitchLine(mHeaderLayout.tvSwitchLine);
                ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                layoutParams.topToTop = 0;
                layoutParams.startToStart = 0;
                clVideo.addView(mFullControlLayout, layoutParams);
                break;
        }
    }


    private void downloadVideo() {
        XvideoApplication application = (XvideoApplication) getApplication();
        RenderersFactory renderersFactory = application.buildRenderersFactory(true);
        application.getDownloadTracker().toggleDownload(
                getSupportFragmentManager(),
                new DownloadData(mVideoId, tvTitle.getText().toString(), mCoverUrl, videoUrl, mPlayer.getDuration()),
                Uri.parse(videoUrl),
                null,
                renderersFactory);

        application.getDownloadManager().addListener(new DownloadManager.Listener() {
            @Override
            public void onDownloadChanged(DownloadManager downloadManager, Download download) {
                dismissDialog();
                application.getDownloadManager().removeListener(this);
                Log.d(TAG, "onDownloadChanged: " + download.state);
                if (download.state == Download.STATE_QUEUED && getLifecycle().getCurrentState() == Lifecycle.State.RESUMED) {
                    CachingDialogFragment fragment = new CachingDialogFragment();
                    fragment.show(getSupportFragmentManager(), "");
                }
            }
        });

    }

    /**
     * 收藏和取消收藏
     */
    private void favor() {
        if (mHeaderLayout.tvFavor.isSelected()) {
            mPresenter.unMark(mVideoId);
        } else {
            mPresenter.mark(mVideoId);
        }
    }

    /**
     * 点赞
     */
    private void thumbsup() {
        if (mAssess == 0) {
            mPresenter.like(mVideoId);
        } else {
            showToast(R.string.already_assessed);
        }
    }

    private void selectLine(int position) {
        mHeaderLayout.tvSwitchLine.setSelected(true);
        String domain = lines.get(position);
        switchLine(domain);
        mHeaderLayout.tvSwitchLine.setText(getString(R.string.hd_line, position + 1));
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN && commentLayout.isShowing()) {
            int[] outLocation = new int[2];
            commentLayout.getLocationInWindow(outLocation);
            Rect rect = new Rect(outLocation[0], outLocation[1], outLocation[0] + commentLayout.getWidth(), outLocation[1] + commentLayout.getHeight());
            if (!rect.contains(((int) ev.getRawX()), ((int) ev.getRawY()))) {
                commentLayout.dismiss();
                return true;
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 加载video url
     */
    private void loadVideoUrl() {
        if (isFromFree || UserHelper.isLoginState()) {
            bufferBar.setVisibility(View.VISIBLE);
            clVideo.setEnabled(false);
            ivPrePlay.setVisibility(View.INVISIBLE);
            mPresenter.getVideoUrl(mVideoId, isSearch);
        } else {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.login_first_need)
                    .setPositiveButton(R.string.ensure, (dialog, which) -> {
                        Intent intent = new Intent(VideoDetailActivity.this, UserAuthActivity.class);
                        intent.putExtra(UserAuthActivity.ARG_IS_LOGIN, true);
                        startActivity(intent);
                    }).show();
        }
    }


    @Override
    public void setPresenter(VideoContract.Presenter presenter) {

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        LogTrace.d(TAG, "onWindowFocusChanged: " + hasFocus);
        if (hasFocus
                && (Build.VERSION.SDK_INT < Build.VERSION_CODES.N
                || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && !isInMultiWindowMode()))) {
            setSystemUI();
        }
    }


    private void setSystemUI() {
        if (mIsFullMode) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.color_343434));
        }

    }

    private boolean inited;
    /**
     * 首次加载完成后清除 封面
     */
    private boolean isPreparing;

    /**
     * 初始化播放器
     */
    private void initPlayer() {
        if (inited) {
            mPlayer.removeListener(this);
            mPlayer.release();
        }

        if (isEndedState) {//处于播放完毕状态时，手动初始化
            return;
        }
        DefaultTrackSelector trackSelector = new DefaultTrackSelector();
        mPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        mPlayer.addListener(this);
        mPlayerView.setPlayer(mPlayer);
        mPlayer.setPlayWhenReady(shouldAutoPlay);
        inited = true;
        MediaSource videoSource = buildMediaSource(Uri.parse(videoUrl), null);
        boolean haveResumePosition = resumeWindow != C.INDEX_UNSET;
        if (haveResumePosition) {
            mPlayer.seekTo(resumeWindow, resumePosition);
        }
        isPreparing = true;
        mPlayer.prepare(videoSource, !haveResumePosition, false);
        clVideo.setEnabled(true);
    }


    /**
     * Returns a new DataSource factory.
     */
    private DataSource.Factory buildDataSourceFactory() {
        return ((XvideoApplication) getApplication()).buildDataSourceFactory();
    }

    private MediaSource buildMediaSource(Uri uri, @Nullable String overrideExtension) {
        DownloadRequest downloadRequest =
                ((XvideoApplication) getApplication()).getDownloadTracker().getDownloadRequest(uri);
        if (downloadRequest != null) {
            return DownloadHelper.createMediaSource(downloadRequest, dataSourceFactory);
        }
        @C.ContentType int type = Util.inferContentType(uri, overrideExtension);
        switch (type) {
            case C.TYPE_HLS:
                return new HlsMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            case C.TYPE_OTHER:
                return new ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            default:
                throw new IllegalStateException("Unsupported type: " + type);
        }
    }


    private void releasePlayer() {
        if (mPlayer != null) {
            LogTrace.d(TAG, "player release");
            shouldAutoPlay = mPlayer.getPlayWhenReady();
            updateResumePosition();
            mPlayer.release();
            mPlayer = null;
            inited = false;
        }
    }


    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        LogTrace.d(TAG, "onPlayerStateChanged: ready=" + playWhenReady + "  playbackState=" + playbackState);

        mPlayerView.setKeepScreenOn(playWhenReady);
        mPlayerView.setTag(mPlayerView.getId(), playbackState);

        //播放结束
        if (playbackState == Player.STATE_ENDED) {
            mTvPercent.setVisibility(View.GONE);
            User localUser = UserHelper.getLocalUser();
            //非VIP
            if (UserHelper.isLoginState() && !localUser.getVip().is_vip()) {
                if (mIsFullMode) {
                    setScreenMode(false);
                }

                new AlertDialog.Builder(this)
                        .setTitle("充值VIP")
                        .setMessage("充值VIP，享受更多免费福利")
                        .setCancelable(false)
                        .setOnKeyListener((dialog, keyCode, event) -> {
                            if (keyCode == KeyEvent.KEYCODE_SEARCH) {
                            }
                            return false;
                        })
                        .setPositiveButton("确定", (dialog, which) ->
                                startActivity(new Intent(this, WapPayActivity.class)))
                        .show();

            }
            isEndedState = true;
            btnReplay.setText(R.string.replay);
            btnReplay.setVisibility(View.VISIBLE);

        }

        //首次加载开始 去掉封面图
        if (playbackState == Player.STATE_READY && isPreparing) {
            ivCover.setVisibility(View.INVISIBLE);
            btnReplay.setVisibility(View.INVISIBLE);
            ivCover.setImageBitmap(null);
            mHeaderLayout.tvDownload.setEnabled(true);
            isPreparing = false;
        }

        //加载缓冲中
        if (playbackState == Player.STATE_BUFFERING) {
            mPlayerView.hideController();
            bufferBar.setVisibility(View.VISIBLE);

            if (isEndedState) {
                isEndedState = false;
                btnReplay.setVisibility(View.INVISIBLE);
            }

        } else {
            bufferBar.setVisibility(View.INVISIBLE);
        }
    }


    @Override
    public void onPlayerError(ExoPlaybackException error) {
        //activity已经销毁？crash日志偶发此错误
        if (isDestroyed())
            return;
        //资源错误
        if (error.type == ExoPlaybackException.TYPE_SOURCE) {
            if (NetWorkUtils.getAPNType(this) == 0) {
                clVideo.setEnabled(false);
                btnReplay.setVisibility(View.VISIBLE);
                btnReplay.setText(R.string.unavailable_network);
                return;
            }
            if (mCurrentLineIndex < lines.size() - 1) {
                showToast(getString(R.string.auto_switch_line, mCurrentLineIndex + 2));
                mHeaderLayout.tvSwitchLine.setText(getString(R.string.hd_line, mCurrentLineIndex + 2));
                mCurrentLineIndex++;
                switchLine(lines.get(mCurrentLineIndex));
            } else {
                clVideo.setEnabled(false);
                btnReplay.setVisibility(View.VISIBLE);
                btnReplay.setText(getPlayerErrorMsg(error));
            }
        }


    }


    private void updateResumePosition() {
        resumeWindow = mPlayer.getCurrentWindowIndex();
        resumePosition = mPlayer.isCurrentWindowSeekable() ? Math.max(0, mPlayer.getCurrentPosition())
                : C.TIME_UNSET;
        LogTrace.d(TAG, "updateResumePosition: " + resumePosition + "   CurrentWindowIndex:" + resumeWindow);
    }

    private void clearResumePosition() {
        resumeWindow = C.INDEX_UNSET;
        resumePosition = C.TIME_UNSET;
    }


    /**
     * 播放器具体错误信息
     */
    private String getPlayerErrorMsg(ExoPlaybackException e) {
        switch (e.type) {
            case ExoPlaybackException.TYPE_SOURCE:
                return getString(R.string.player_error);
            case ExoPlaybackException.TYPE_RENDERER:
                return getString(R.string.player_renderer_failed);
            case ExoPlaybackException.TYPE_UNEXPECTED:
                return getString(R.string.player_unexpected_failed);
        }
        return getString(R.string.player_error);
    }

    /**
     * 设置屏幕模式-全屏and竖屏
     */
    private void setScreenMode(boolean isFullMode) {
        mIsFullMode = isFullMode;
        mPlayerView.setTag(mIsFullMode);
        ivMenu.setVisibility(isFullMode ? View.VISIBLE : View.INVISIBLE);
        tvTitle.setVisibility(isFullMode ? View.VISIBLE : View.INVISIBLE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N
                || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && !isInMultiWindowMode())) {
            setRequestedOrientation(mIsFullMode ? ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE :
                    ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) clVideo.getLayoutParams();
        params.width = mIsFullMode ? ConstraintLayout.LayoutParams.MATCH_PARENT : 0;
        params.height = mIsFullMode ? ConstraintLayout.LayoutParams.MATCH_PARENT : 0;
        params.dimensionRatio = mIsFullMode ? null : "25:14";
        clVideo.setLayoutParams(params);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N
                || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && !isInMultiWindowMode())) {
            setSystemUI();
        }

    }

    private void replay() {
        isEndedState = false;
        clearResumePosition();
        initPlayer();
    }

    @Override
    public void onBackPressed() {
        if (BackHandlerHelper.handleBackPress(this)) {
            return;
        }

        if (mFullControlLayout != null && mFullControlLayout.isAttachedToWindow()) {
            clVideo.removeView(mFullControlLayout);
            mFullControlLayout = null;
            return;
        }
        if (commentLayout.isShowing()) {
            commentLayout.dismiss();
            return;
        }

        if (mIsFullMode) {
            setScreenMode(false);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23 && videoUrl != null) {
            initPlayer();
        }
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        super.onPause();
        if (Util.SDK_INT <= 23 && videoUrl != null) {
            releasePlayer();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Util.SDK_INT <= 23 && videoUrl != null) {
            initPlayer();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23 && videoUrl != null) {
            releasePlayer();
        }
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy");
        super.onDestroy();
        mPresenter.unSubscribe();
        mPlayerView.removeAllViews();
    }


}
