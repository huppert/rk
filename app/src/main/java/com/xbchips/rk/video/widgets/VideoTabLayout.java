package com.xbchips.rk.video.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.xbchips.rk.R;

/**
 * Created by york on 2017/9/27.
 */

public class VideoTabLayout extends LinearLayout implements View.OnClickListener {

    private OnBtnSelectedListener mOnBtnSelectedListener;
    private View btns[];
    private int mSelectedPostion;
    private LinearLayout mLlSlice;
    private LinearLayout mLlRecommend;

    public VideoTabLayout(Context context) {
        this(context, null);
    }

    public VideoTabLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_tab, this);
        mLlSlice = (LinearLayout) view.findViewById(R.id.ll_slice);
        mLlRecommend = (LinearLayout) view.findViewById(R.id.ll_recommend);
        mLlSlice.setOnClickListener(this);
        mLlSlice.setSelected(true);
        mLlRecommend.setOnClickListener(this);
        btns = new View[]{mLlSlice, mLlRecommend};
    }


    @Override
    public void onClick(View view) {
        select(view);
    }

    private void select(View view) {
        for (int i = 0; i < btns.length; i++) {
            if (view.getId() == btns[i].getId()) {
                view.setSelected(true);
                mSelectedPostion = i;
                if (mOnBtnSelectedListener != null) {
                    mOnBtnSelectedListener.onSelected(i);
                }
            } else {
                btns[i].setSelected(false);
            }
        }
    }

    public int getSelectedPosition() {
        return mSelectedPostion;
    }


    public void setOnBtnSelectedListener(OnBtnSelectedListener listener) {
        mOnBtnSelectedListener = listener;
    }

    public interface OnBtnSelectedListener {
        void onSelected(int position);
    }
}
