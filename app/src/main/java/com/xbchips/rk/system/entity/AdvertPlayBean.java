package com.xbchips.rk.system.entity;

import java.util.List;

public class AdvertPlayBean {

    private List<AdvertList> AppPlayTop;
    private List<AdvertList> AppPlayCenter;
    private List<AdvertList> AppPlayFooter;

    public List<AdvertList> getAppPlayTop() {
        return AppPlayTop;
    }

    public void setAppPlayTop(List<AdvertList> appPlayTop) {
        AppPlayTop = appPlayTop;
    }

    public List<AdvertList> getAppPlayCenter() {
        return AppPlayCenter;
    }

    public void setAppPlayCenter(List<AdvertList> appPlayCenter) {
        AppPlayCenter = appPlayCenter;
    }

    public List<AdvertList> getAppPlayFooter() {
        return AppPlayFooter;
    }

    public void setAppPlayFooter(List<AdvertList> appPlayFooter) {
        AppPlayFooter = appPlayFooter;
    }
}
