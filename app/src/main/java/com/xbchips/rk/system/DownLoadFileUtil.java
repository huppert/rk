package com.xbchips.rk.system;


import com.xbchips.rk.common.DownLoadFile;
import com.xbchips.rk.common.SystemDomain;
import com.xbchips.rk.util.LogTrace;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;


public class DownLoadFileUtil {
    private static final String TAG = "DownLoadFileUtil";

    private File apkFile;
    private DownLoadFile mDownLoadFile;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    public DownLoadFileUtil(File apkFile, DownLoadFile file) {
        this.apkFile = apkFile;
        mDownLoadFile = file;
    }

    private SystemService getService() {
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(SystemDomain.server_domain + "/api/")
                .client(new OkHttpClient())
                .build();
        return retrofit.create(SystemService.class);
    }

    public void download(String url) {
        Disposable disposable = getService().downloadFile(url)
                .subscribeOn(Schedulers.io())
                .filter(Response::isSuccessful)
                .subscribe(response -> writeFile(response.body()), throwable -> {
                });
        mCompositeDisposable.add(disposable);
    }

    private boolean writeFile(ResponseBody body) {
        try {
            InputStream inputStream = null;
            OutputStream outputStream = null;
            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                mDownLoadFile.setTotalFileSize(fileSize);
                long fileSizeDownloaded = 0;
                inputStream = body.byteStream();
                outputStream = new FileOutputStream(apkFile);
                while (true) {
                    int read = inputStream.read(fileReader);
                    if (read == -1) {
                        destroy();
                        break;
                    }
                    outputStream.write(fileReader, 0, read);
                    fileSizeDownloaded += read;
                    mDownLoadFile.setCurrentFileSize(fileSizeDownloaded);
                    LogTrace.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();
                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    public void destroy() {
        mCompositeDisposable.clear();
    }
}
