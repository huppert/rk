package com.xbchips.rk.system.entity;

/**
 * Created by york on 2017/8/26.
 */

public class Configs {
    public static final String CONF_BG_IAMGE = "login_background_img";
    public static final String CONF_CUSTOMER_SERVICE = "customer_service";
    public static final String CONF_PROMOTE_LIST = "promote_list";
    public static final String CONF_KEYWORD_MASKING = "keyword_masking";
    public static final String CONF_SPLASH_URL = "splash_url";
    public static final String CONF_SHARE_LINK = "share_link";
    public static final String CONF_UPLOAD_URL = "upload_url";


    private DynamicConfig login_background_img;//获取登录和注册背景图片
    private DynamicConfig customer_service; //800客服地址
    private DynamicConfig promote_list;//推广简介
    private DynamicConfig keyword_masking;//关键字屏蔽
    private DynamicConfig splash_url;//开屏页跳转url
    private DynamicConfig share_link;//分享url

    public DynamicConfig getUpload_url() {
        return upload_url;
    }

    public void setUpload_url(DynamicConfig upload_url) {
        this.upload_url = upload_url;
    }

    private DynamicConfig upload_url;//上传我的视频上传地址

    public DynamicConfig getLogin_background_img() {
        return login_background_img;
    }

    public void setLogin_background_img(DynamicConfig login_background_img) {
        this.login_background_img = login_background_img;
    }

    public DynamicConfig getCustomer_service() {
        return customer_service;
    }

    public void setCustomer_service(DynamicConfig customer_service) {
        this.customer_service = customer_service;
    }

    public DynamicConfig getPromote_list() {
        return promote_list;
    }

    public void setPromote_list(DynamicConfig promote_list) {
        this.promote_list = promote_list;
    }

    public DynamicConfig getKeyword_masking() {
        return keyword_masking;
    }

    public void setKeyword_masking(DynamicConfig keyword_masking) {
        this.keyword_masking = keyword_masking;
    }

    public DynamicConfig getSplash_url() {
        return splash_url;
    }

    public void setSplash_url(DynamicConfig splash_url) {
        this.splash_url = splash_url;
    }

    public DynamicConfig getShare_link() {
        return share_link;
    }
}
