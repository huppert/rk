package com.xbchips.rk.system.entity;

public class AnnounNotice {
    private String jump_link;
    private String content;
    private String img_url;

    public String getJump_link() {
        return jump_link;
    }

    public void setJump_link(String jump_link) {
        this.jump_link = jump_link;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }
}
