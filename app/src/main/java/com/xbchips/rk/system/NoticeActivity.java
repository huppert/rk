package com.xbchips.rk.system;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.common.ActivityStackManager;
import com.xbchips.rk.util.LogTrace;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NoticeActivity extends BaseActivity {
    private static final String TAG = "NoticeActivity";
    public static final String _URL = "url";
    public static final String _CLOSEABLE = "closeable";
    @BindView(R.id.webView)
    WebView mWebView;
    /**
     * activity 是否能手动关闭
     */
    private boolean mCloseable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);
        ButterKnife.bind(this);
        init();
    }

    protected void init() {
        initTopBar();
        mCloseable = getIntent().getBooleanExtra(_CLOSEABLE, true);
        if (!mCloseable) {
            mBtnBack.setVisibility(View.GONE);
        }
        mTvTitle.setText(R.string.announcement);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                LogTrace.d(TAG, url);
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
        mWebView.loadUrl(getIntent().getStringExtra(_URL));
    }

    @Override
    public void onBackPressed() {
        if (mCloseable) {
            super.onBackPressed();
        } else {
            ActivityStackManager.exit();
        }
    }
}
