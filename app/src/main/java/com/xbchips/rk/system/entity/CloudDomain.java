package com.xbchips.rk.system.entity;

import java.util.List;

/**
 * Created by york on 2018/7/13.
 */
public class CloudDomain {
    private int v2;
    private List<String> service;
    private List<String> video;

    public int getV2() {
        return v2;
    }

    public void setV2(int v2) {
        this.v2 = v2;
    }

    public List<String> getService() {
        return service;
    }

    public void setService(List<String> service) {
        this.service = service;
    }

    public List<String> getVideo() {
        return video;
    }

    public void setVideo(List<String> video) {
        this.video = video;
    }
}
