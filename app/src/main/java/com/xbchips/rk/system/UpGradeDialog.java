package com.xbchips.rk.system;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;

import com.xbchips.rk.BuildConfig;
import com.xbchips.rk.R;
import com.xbchips.rk.common.DownLoadFile;
import com.xbchips.rk.system.entity.VersionInfo;
import com.xbchips.rk.util.LogTrace;

import java.io.File;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Created by york on 2017/4/17.
 * 升级进度框
 */

public class UpGradeDialog {
    private static final String TAG = "UpGradeDialog";
    private DownLoadFileUtil mDownLoadFileUtil;
    private ProgressBar progress;
    private File apkFile;
    private Activity mContext;
    private AlertDialog mAlertDialog;
    private final DownLoadFile mDownLoadFile = new DownLoadFile();
    private Disposable mDisposable;

    public UpGradeDialog(Activity context) {
        mContext = context;
        apkFile = new File(mContext.getExternalFilesDir(null), "xvideo.apk");
        mDownLoadFileUtil = new DownLoadFileUtil(apkFile, mDownLoadFile);
    }

    private void show(String title, String message) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_download, null);
        mAlertDialog = new AlertDialog.Builder(mContext)
                .setView(view)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .show();
        progress = view.findViewById(R.id.progress);
        progress.getProgressDrawable().setColorFilter(
                0xFFC6104E, android.graphics.PorterDuff.Mode.SRC_IN);

    }

    public void update(VersionInfo.Info info) {
        String title = mContext.getString(R.string.check_new_version, info.getVersion_code());
        String message = mContext.getString(R.string.upgrading);
        show(title, message);
        mDownLoadFileUtil.download(info.getDownload_url());
        checkProgress();
    }


    private void checkProgress() {
        mDisposable = Flowable.interval(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> updateProgress(mDownLoadFile.getCurrentFileSize(), mDownLoadFile.getTotalFileSize()));

    }

    private void updateProgress(long fileSizeDownloaded, long totalLength) {
        if (totalLength > 0) {
            LogTrace.d(TAG, "APK curLength = " + fileSizeDownloaded + "total = " + totalLength);
            double percent = fileSizeDownloaded / (double) totalLength;
            LogTrace.d(TAG, "accept: percent=" + percent);
            progress.setProgress((int) (percent * 100));
            if (progress.getProgress() == 100) {
                mDisposable.dispose();
                mDownLoadFileUtil.destroy();
                installApk();
            }
        }
    }

    private void installApk() {
        Intent install = new Intent(Intent.ACTION_VIEW);
        install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            uri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".fileprovider", apkFile);
            install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            uri = Uri.fromFile(apkFile);
        }
        install.setDataAndType(uri, "application/vnd.android.package-archive");
        mContext.startActivity(install);

        mAlertDialog.dismiss();
        mContext.finish();

    }
}
