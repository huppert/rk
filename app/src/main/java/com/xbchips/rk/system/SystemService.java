package com.xbchips.rk.system;


import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.free.entity.Banner;
import com.xbchips.rk.system.entity.AdvertList;
import com.xbchips.rk.system.entity.AnnounNotice;
import com.xbchips.rk.system.entity.Configs;
import com.xbchips.rk.system.entity.Notice;
import com.xbchips.rk.system.entity.VersionInfo;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by york on 2017/8/8.
 */

public interface SystemService {
    @GET
    @Streaming
    Observable<Response<ResponseBody>> downloadFile(@Url String url);

    /**
     * 检查更新
     */
    @GET("version/update")
    Observable<ObjectResponse<VersionInfo>> checkVersion();


    /**
     * 获取广告列表
     */

    @GET("advert/new_list")
    Observable<ObjectResponse<List<AdvertList>>> getAdvertList(@Query("cate_code") String cateCode);

    /**
     * 获取播放页面聚合广告列表
     */

    @GET("advert/new_list")
    Observable<ObjectResponse<List<AdvertList>>> getPlayAdvertList(@Query("cate_code") String cateCode);

    /**
     * 通知
     */
    @GET("sys/notice")
    Observable<ObjectResponse<Notice>> notice();

    /**
     * 公告通知
     */
    @GET("notice")
    Observable<ObjectResponse<AnnounNotice>> Annnotice();

    /**
     * 获取配置
     */
    @GET("config/list")
    Observable<ObjectResponse<Configs>> getConfigs();

    /**
     * 广告列表
     *
     * @param position 1 会员中心 2 播放页
     */
    @GET("advert/list")
    Observable<ObjectResponse<List<Banner>>> getAds(@Query("positions") int position);

}
