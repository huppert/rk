package com.xbchips.rk.profile;

import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.xbchips.rk.R;
import com.xbchips.rk.activities.HtmlActivity;
import com.xbchips.rk.base.BaseFragment;
import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.common.ConfigHelper;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.common.ServiceUrlAppend;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.free.entity.Banner;
import com.xbchips.rk.profile.activity.BookMarkActivity;
import com.xbchips.rk.profile.activity.MyDownloadActivity;
import com.xbchips.rk.profile.activity.ProfileActivity;
import com.xbchips.rk.profile.activity.PurchaseActivity;
import com.xbchips.rk.profile.activity.SystemSettingActivity;
import com.xbchips.rk.profile.activity.UploadedActivity;
import com.xbchips.rk.profile.widgets.ProfileHeaderLayout;
import com.xbchips.rk.system.SystemService;
import com.xbchips.rk.system.entity.Configs;
import com.xbchips.rk.user.UserPresenter;
import com.xbchips.rk.user.UserView;
import com.xbchips.rk.user.activities.UserAuthActivity;
import com.xbchips.rk.user.activities.WapPayActivity;
import com.xbchips.rk.util.DisplayUtils;
import com.xbchips.rk.util.LogTrace;
import com.xbchips.rk.video.VideoService;
import com.xbchips.rk.widgets.AdRecyclerView;
import com.xbchips.rk.widgets.CustomSwipeRefreshLayout;
import com.xbchips.rk.widgets.VerticalImageSwitcher;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;
import static com.xbchips.rk.activities.HtmlActivity.ARG_TITLE;

/**
 * Created by york on 2017/7/26.
 */

public class ProfileFragment extends BaseFragment implements UserView {
    private static final int CODE_PROFILE = 200;
    private final UserPresenter mPresenter = new UserPresenter(this);
    @BindView(R.id.cl_top)
    ProfileHeaderLayout clTop;
    @BindView(R.id.tv_link)
    TextView tvLink;
    @BindView(R.id.tv_spread)
    TextView tvSpread;
    @BindView(R.id.iv_vip)
    ImageView ivVip;
    @BindView(R.id.top_advertise)
    AdRecyclerView topAdvertise;
    @BindView(R.id.bottom_advertise)
    VerticalImageSwitcher mBottomAd;
    @BindView(R.id.tv_item)
    TextView mTvItem;
    @BindView(R.id.refresh_layout)
    CustomSwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.tv_left_download_times)
    TextView tvLeftDownloadTimes;
    private final VideoService service = HttpRequest.createService(VideoService.class);


    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final boolean logined = UserHelper.isLoginState();
            clTop.inflate(logined);
            mRefreshLayout.setEnabled(logined);
        }
    };

    private SystemService mSystemService = HttpRequest.createService(SystemService.class);


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        mUnBinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mBottomAd != null) {
            mBottomAd.pause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mBottomAd != null) {
            mBottomAd.resume();
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (mBottomAd != null) {
            if (hidden) {
                mBottomAd.pause();
            } else {
                mBottomAd.resume();
            }
        }

    }

    @Override
    protected void init() {
        mRefreshLayout.setOnRefreshListener(() -> {
            mPresenter.checkUser();
            getTopAdvert();
            getTimesForDownload();
        });
        final boolean logined = UserHelper.isLoginState();
        int leftDays = 0;
        if (UserHelper.getLocalUser() != null) {
            leftDays = UserHelper.getLocalUser().getVip().getLeftDays();
        }
        ImageLoader.load(getContext(), leftDays > 0 ? R.drawable.banner_update_vip : R.drawable.become_vip_banner, ivVip, new RequestOptions()
                .transform(new MultiTransformation<>(new CenterCrop(), new RoundedCorners(DisplayUtils.dip2px(getContext(), 6))))
        );
        clTop.inflate(logined);
        mRefreshLayout.setEnabled(logined);
        setDl();
        setShareLink();

        IntentFilter intentFilter = new IntentFilter(Constants.ACTION_REFRESH_STATE);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver, intentFilter);
        getTopAdvert();
        getBottomAD();
        getTimesForDownload();
    }

    private void getBottomAD() {
        Disposable disposable = mSystemService.getAds(1)
                .compose(RxUtils.applySchedulers())
                .filter(response -> !handleResponse(response))
                .subscribe(response -> {
                    final List<Banner> data = response.getData();
                    if (data != null && data.size() > 0) {
                        mBottomAd.setVisibility(View.VISIBLE);
                        mBottomAd.start(data);
                    }
                }, this::handleFailure);
        mCompositeDisposable.add(disposable);
    }


    /**
     * APP顶部广告
     */
    private void getTopAdvert() {
        Disposable disposable = mSystemService.getAdvertList(Constants.APP_USER_TOP)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if (response.getStatus() == 0) {
                        topAdvertise.initData(response.getData());
                    }
                }, throwable -> LogTrace.i(TAG, "获取个人中心顶部硬广失败=" + throwable.getMessage()));
        mCompositeDisposable.add(disposable);
    }


    @OnClick({R.id.tv_my_favorite, R.id.my_videos, R.id.purchase_record, R.id.download_record,
            R.id.btn_copy_link, R.id.btn_copy, R.id.systemsettings, R.id.cl_top,
            R.id.tv_service, R.id.iv_vip})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cl_top:
                if (!UserHelper.isLoginState()) {
                    toAuthActivity(true);
                } else {
                    startActivityForResult(new Intent(getContext(), ProfileActivity.class), CODE_PROFILE);
                }
                break;
            case R.id.tv_my_favorite:
                if (UserHelper.isLoginState()) {
                    startActivity(new Intent(getContext(), BookMarkActivity.class));
                } else {
                    toAuthActivity(true);
                }
                break;
            case R.id.my_videos:
                startActivity(new Intent(getContext(), UploadedActivity.class));
                break;
            case R.id.iv_vip:
                startActivity(new Intent(getContext(), WapPayActivity.class));
                break;
            case R.id.purchase_record:
                startActivity(new Intent(getContext(), PurchaseActivity.class));
                break;
            case R.id.download_record:
                if (UserHelper.isLoginState()) {
                    startActivity(new Intent(getContext(), MyDownloadActivity.class));
                } else {
                    toAuthActivity(true);
                }
                break;
            case R.id.btn_copy_link:
                copy(tvLink.getText().toString());
                showToast(R.string.copied_to_clipboard);
                break;
            case R.id.btn_copy:
                copy(getEmail());
                showToast(R.string.copied_to_clipboard);
                break;
            case R.id.systemsettings:
                startActivity(new Intent(getContext(), SystemSettingActivity.class));
                break;
            case R.id.tv_service:
                toService();
                break;
        }
    }


    /**
     * 客服
     */
    private void toService() {
        Disposable disposable = ConfigHelper.getValue(Configs.CONF_CUSTOMER_SERVICE)
                .compose(RxUtils.applySchedulers())
                .map(url -> new ServiceUrlAppend().appendParameters(url, UserHelper.getLocalUser() == null ? "" : UserHelper.getLocalUser().getId()))
                .subscribe(url -> ProfileFragment.this.toHtmlActivity(url, getString(R.string.service_center)));
        mCompositeDisposable.add(disposable);

    }


    private void copy(String text) {
        ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("text", text);
        clipboard.setPrimaryClip(clip);
    }

    private void toHtmlActivity(String url, String title) {
        Intent intent = new Intent(getActivity(), HtmlActivity.class);
        intent.putExtra(ARG_TITLE, title);
        intent.putExtra(HtmlActivity._URL, url);
        startActivity(intent);
    }


    private void toAuthActivity(boolean isLogin) {
        Intent intent = new Intent(getActivity(), UserAuthActivity.class);
        intent.putExtra(UserAuthActivity.ARG_IS_LOGIN, isLogin);
        startActivity(intent);
    }

    private String getEmail() {
        String s = mTvItem.getText().toString();
        String[] arr = s.split("：");
        if (arr.length > 1) {
            return arr[1];
        }
        return s;
    }

    private void refreshState() {
        clTop.refreshData();
        mRefreshLayout.setEnabled(UserHelper.isLoginState());
    }

    private void getTimesForDownload() {
        final Disposable disposable = service.getTimes()
                .compose(RxUtils.applySchedulers())
                .filter(ObjectResponse::isSuccess)
                .subscribe(response -> {
                    tvLeftDownloadTimes.setVisibility(View.VISIBLE);
                    tvLeftDownloadTimes.setText(getString(R.string.left_times_format, response.getData()));
                }, throwable -> {
                });
        mCompositeDisposable.add(disposable);
    }

    /**
     * 合作代理
     */
    private void setDl() {
        Disposable disposable = ConfigHelper.getValue(Configs.CONF_PROMOTE_LIST)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> mTvItem.setText(s));
        mCompositeDisposable.add(disposable);
    }

    /**
     * 分享链接
     */
    private void setShareLink() {
        Disposable disposable = ConfigHelper.getValue(Configs.CONF_SHARE_LINK)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> tvLink.setText(s));
        mCompositeDisposable.add(disposable);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE_PROFILE && resultCode == RESULT_OK) {
            mRefreshLayout.setRefreshing(true);
            mPresenter.checkUser();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
        if (mBottomAd != null) {
            mBottomAd.stop();
        }
    }

    @Override
    public void checkSuccess() {
        refreshState();
        mRefreshLayout.setRefreshing(false);
    }

    @Override
    public void failed(Throwable throwable) {
        mRefreshLayout.setRefreshing(false);
        handleFailure(throwable);
    }

}
