package com.xbchips.rk.profile.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.profile.adapter.SpreadAdapter;
import com.xbchips.rk.profile.entity.Spread;
import com.xbchips.rk.user.service.UserService;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SpreadActivity extends BaseActivity {
    @BindView(R.id.tv_my_link)
    TextView mTvMyLink;
    @BindView(R.id.tv_spread_count)
    TextView mTvSpreadCount;
    @BindView(R.id.tv_spread_vip_count)
    TextView mTvSpreadVipCount;
    @BindView(R.id.tv_spread_vip_money)
    TextView mTvSpreadVipMoney;
    @BindView(R.id.tv_point_count)
    TextView mTvPointCount;
    @BindView(R.id.tv_copy)
    TextView mTvCopy;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    private UserService service;
    private long respData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spread);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        initTopBar();
        mTvTitle.setText(R.string.spread);
        service = HttpRequest.createService(UserService.class);
        getData();
    }

    private void getData() {
        Disposable disposable = service.getPromote()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(response -> !handleResponse(response))
                .subscribe(response -> setData(response.getData()), this::handleFailure);
        mCompositeDisposable.add(disposable);
    }

    private void setData(Spread data) {
        mTvMyLink.setText(data.getPromoteUrl());
        mTvSpreadCount.setText(String.valueOf(data.getUserRegMembers()));
        mTvSpreadVipCount.setText(String.valueOf(data.getPromoteRechargeMembers()));
        mTvSpreadVipMoney.setText(String.valueOf(data.getPromoteRechargeTotal()));
        mTvPointCount.setText(String.valueOf(data.getUserResPointTotal()));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        SpreadAdapter adapter = new SpreadAdapter(data.getExchange(), this);
        adapter.setOnClickListener(mOnClickListener);
        mRecyclerView.setAdapter(adapter);
        mTvCopy.setOnClickListener(mOnClickListener);
    }


    private void copyLink() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newUri(getContentResolver(), "URI", Uri.parse(mTvMyLink.getText().toString()));
        clipboard.setPrimaryClip(clip);
        showToast(R.string.copied_to_clipboard);
    }

    private void showWindow() {
        final PopupWindow popupWindow = new PopupWindow(this);
        popupWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        View contentView = LayoutInflater.from(this).inflate(R.layout.layout_congratulations, null);
        popupWindow.setContentView(contentView);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0x20000000));
        popupWindow.setOutsideTouchable(false);
        popupWindow.setFocusable(true);
        popupWindow.setAnimationStyle(R.style.PopupWindowAnimationStyle);
        popupWindow.showAtLocation(contentView, Gravity.CENTER, 0, 0);
        ImageView iv_close = (ImageView) contentView.findViewById(R.id.iv_close);
        iv_close.setOnClickListener(v -> popupWindow.dismiss());
    }


    private View.OnClickListener mOnClickListener = view -> {
        switch (view.getId()) {
            case R.id.tv_copy:
                copyLink();
                break;
            case R.id.btn_exchange:
                Spread.ExchangeBean exchangeBean = (Spread.ExchangeBean) view.getTag();
                if (Integer.parseInt(mTvPointCount.getText().toString()) < exchangeBean.getUse()) {
                    showToast("红利点不足");
                    return;
                }
                new AlertDialog.Builder(SpreadActivity.this)
                        .setMessage(exchangeBean.getTitle())
                        .setPositiveButton(getString(R.string.exchange), (dialog, which) -> {
                            showProgress();
                            exchange(exchangeBean.getUse());
                        })
                        .setNegativeButton(getString(R.string.cancel), null)
                        .show();

                break;
        }
    };


    private void exchange(final int point) {
        Disposable disposable = service.exchange(point)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(response -> !handleResponse(response))
                .subscribe(response -> exchangeSuccess(response, point), this::handleFailure);
        mCompositeDisposable.add(disposable);
    }

    private void exchangeSuccess(@NonNull ObjectResponse<HashMap<String, Long>> response, int point) {
        respData = response.getData().get("expire_at");
        int curPoints = Integer.parseInt(mTvPointCount.getText().toString());
        mTvPointCount.setText(String.valueOf(curPoints - point));
        dismissDialog();
        showWindow();
    }

    @Override
    public void finish() {
        if (respData != 0) {
            Intent intent = new Intent();
            intent.putExtra("data", respData);
            setResult(RESULT_OK, intent);
        }
        super.finish();
    }
}
