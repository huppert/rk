package com.xbchips.rk.profile.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.xbchips.rk.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class GenderSelectFragment extends BottomSheetDialogFragment {
    private static final String ARG_GENDER = "arg_gender";
    @BindView(R.id.tv_male)
    TextView tvMale;
    @BindView(R.id.tv_female)
    TextView tvFemale;
    private OnGenderSelectedListener mListener;
    private Unbinder mUnbinder;


    public static GenderSelectFragment getInstance(String gender) {
        GenderSelectFragment genderSelectFragment = new GenderSelectFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_GENDER, gender);
        genderSelectFragment.setArguments(bundle);
        return genderSelectFragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.layout_gender_select, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        String gender = getArguments().getString(ARG_GENDER);
        select(gender);
        return view;
    }


    @OnClick({R.id.tv_male, R.id.tv_female})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_male:
                select(getString(R.string.male));
                if (mListener != null) {
                    mListener.onSelected(getString(R.string.male));
                    dismiss();
                }
                break;
            case R.id.tv_female:
                select(getString(R.string.female));
                if (mListener != null) {
                    mListener.onSelected(getString(R.string.female));
                    dismiss();
                }
                break;
        }
    }


    private void select(String gender) {
        if (getString(R.string.unknown).equals(gender)) return;
        //男
        if (getString(R.string.male).equals(gender)) {
            tvMale.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_tick, 0);
            tvFemale.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        } else {
            tvMale.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            tvFemale.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_tick, 0);
        }

    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    public interface OnGenderSelectedListener {
        void onSelected(String gender);
    }


    public void setListener(OnGenderSelectedListener listener) {
        mListener = listener;
    }

}
