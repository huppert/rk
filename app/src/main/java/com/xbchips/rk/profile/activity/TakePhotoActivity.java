package com.xbchips.rk.profile.activity;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;

import com.xbchips.rk.BuildConfig;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.util.LogTrace;
import com.xbchips.rk.widgets.AppSettingsDialog;

import java.io.File;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;

/**
 * 照相或选择图库
 */
@RuntimePermissions
public class TakePhotoActivity extends BaseActivity {
    private static final String TAG = "TakePhotoActivity";
    public static final int CAMERA_CAPTRUE = 10;
    public static final int PICK_PHOTO = 11;
    public static final int CROP_PHOTO = 12;
    private static final int RC_CAMERA_PERM = 101;
    /**
     * 选择或照相后图片的uri
     */
    private Uri mImageUri;
    private File mTempFile;


    /**
     * 打开相机
     */
    @NeedsPermission(Manifest.permission.CAMERA)
    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        setTempFile(new File(getExternalCacheDir(), System.currentTimeMillis() + ".jpg"));
        LogTrace.d(TAG, getTempFile().getAbsolutePath());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Uri uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".fileprovider", getTempFile());
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            setImageUri(uri);
        } else {
            setImageUri(Uri.fromFile(getTempFile()));
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, getImageUri());
        startActivityForResult(intent, CAMERA_CAPTRUE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        TakePhotoActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }


    protected void cropFromCamera() {
        Intent intent = new Intent();
        Uri uri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".fileprovider", getTempFile());
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            uri = getImageUri();
        }
        cropPhoto(intent, uri);
    }

    protected void cropFromGallery() {
        cropPhoto(new Intent(), getImageUri());
    }

    /**
     * 裁剪图片
     */
    private void cropPhoto(Intent intent, Uri uri) {
        intent.setAction("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        //设置华为裁剪为方形
        if (Build.MANUFACTURER.equals("HUAWEI") || Build.BRAND.contains("HUAWEI") || Build.MODEL.contains("HUAWEI")) {
            intent.putExtra("aspectX", 9998);
            intent.putExtra("aspectY", 9999);
        } else {
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
        }
        intent.putExtra("outputX", 200);
        intent.putExtra("outputY", 200);

        File externalFilesDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (externalFilesDir == null) {
            showToast(R.string.no_sdcard);
            return;
        }
        setTempFile(new File(getExternalCacheDir(), System.currentTimeMillis() + ".jpg"));
        setImageUri(Uri.fromFile(getTempFile()));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, getImageUri());
        Intent chooserIntent = Intent.createChooser(intent, "选择照片裁剪应用");
        startActivityForResult(chooserIntent, CROP_PHOTO);
    }

    @OnPermissionDenied(Manifest.permission.CAMERA)
    void showDeniedForCamera() {
        showToast(R.string.denied_camera);
    }

    @OnNeverAskAgain(Manifest.permission.CAMERA)
    void showNeverAskForCamera() {
        new AppSettingsDialog.Builder(this)
                .setRationale(R.string.camera_permission)
                .build()
                .show();
    }

    public Uri getImageUri() {
        return mImageUri;
    }

    public void setImageUri(Uri imageUri) {
        this.mImageUri = imageUri;
    }

    public File getTempFile() {
        return mTempFile;
    }

    public void setTempFile(File tempFile) {
        this.mTempFile = tempFile;
    }

}
