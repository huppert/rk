package com.xbchips.rk.profile.widgets;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.xbchips.rk.R;
import com.xbchips.rk.util.DisplayUtils;

/**
 * Created by york on 2017/11/7.
 */

public class RefreshButton extends LinearLayout {
    private ImageView mIconRefresh;
    private ObjectAnimator rotation;

    public RefreshButton(Context context) {
        this(context, null);
    }

    public RefreshButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_refresh_button, this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        int left = DisplayUtils.dip2px(context, 10);
        int top = DisplayUtils.dip2px(context, 3);
        view.setPadding(left, top, left, top);
        view.setLayoutParams(params);
        mIconRefresh = (ImageView) view.findViewById(R.id.icon_refresh);
        setBackground(createGradientDrawable(left / 5));
    }


    @NonNull
    private GradientDrawable createGradientDrawable(int radius) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setShape(GradientDrawable.RECTANGLE);
        drawable.setCornerRadius(radius);
        drawable.setColor(0xffe20055);
        return drawable;
    }

    public void loading() {
        setEnabled(false);
        rotation = ObjectAnimator.ofFloat(mIconRefresh, "rotation", 0f, 360f);
        rotation.setDuration(350);
        rotation.setRepeatMode(ObjectAnimator.RESTART);
        rotation.setRepeatCount(-1);
        rotation.start();
    }

    public void complete() {
        setEnabled(true);
        rotation.end();
    }
}
