package com.xbchips.rk.profile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.base.response.PageResponse;
import com.xbchips.rk.common.ActivityStackManager;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.free.FavorAdapter;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.user.service.UserService;
import com.xbchips.rk.util.DisplayUtils;
import com.xbchips.rk.util.LogTrace;
import com.xbchips.rk.video.VideoDetailActivity;
import com.xbchips.rk.video.VideoService;
import com.xbchips.rk.widgets.CustomSwipeRefreshLayout;
import com.xbchips.rk.widgets.EndlessRecyclerView;
import com.xbchips.rk.widgets.ItemDecoration;
import com.xbchips.rk.widgets.RoundButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class BookMarkActivity extends BaseActivity {
    private static final String TAG = "BookMarkActivity";
    @BindView(R.id.recycler_view)
    EndlessRecyclerView mRecyclerView;
    @BindView(R.id.refresh_layout)
    CustomSwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.rl_empty)
    RelativeLayout mRlEmpty;
    @BindView(R.id.btn_watch)
    RoundButton mBtnWatch;
    private List<VideoItem> mList = new ArrayList<>();
    private FavorAdapter mAdapter;
    private UserService service;
    private VideoService mVideoService;
    private boolean isBack = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_mark);
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LogTrace.d(TAG, "onNewIntent: ");
        refresh();
    }

    private void init() {
        initTopBar();
        mTvTitle.setText(R.string.my_bookmark);
        service = HttpRequest.createService(UserService.class);
        mVideoService = HttpRequest.createService(VideoService.class);
        mBtnWatch.setOnClickListener(mOnClickListener);
        initRecyclerView();
        loadData();
    }

    private void initRecyclerView() {
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int i) {
                if (i == mList.size()) {
                    return 2;
                }
                return 1;
            }
        });
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(new ItemDecoration(DisplayUtils.dip2px(this, 5), 2));
        mRecyclerView.setEmptyView(mRlEmpty);
        mAdapter = new FavorAdapter(mList, this);
        mAdapter.setOnClickListener(mOnClickListener);
        mRecyclerView.swapAdapter(mAdapter, false);
        mRecyclerView.setOnLoadMoreListener(this::loadMore);
        mRefreshLayout.setOnRefreshListener(this::refresh);
    }

    private void loadMore() {
        mAdapter.addFooter();
        loadData();
    }

    private void refresh() {
        mRecyclerView.setCurPage(1);
        mRecyclerView.setLoading(true);
        loadData();
    }

    private void loadData() {
        Disposable disposable = service.getMarks(mRecyclerView.getCurPage(), Constants.DEFAULT_PAGESIZE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setData, this::processError);
        mCompositeDisposable.add(disposable);
    }

    private void unMark(final VideoItem videoItem) {
        Disposable disposable = mVideoService.unmark(videoItem.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(response -> !handleResponse(response))
                .subscribe(response -> removeItem(videoItem), this::handleFailure);
        mCompositeDisposable.add(disposable);
    }

    private void removeItem(VideoItem videoItem) {
        int index = mList.indexOf(videoItem);
        mList.remove(index);
        mAdapter.notifyItemRemoved(index);
    }


    private void setData(ObjectResponse<PageResponse<VideoItem>> response) {
        if (mRecyclerView.getCurPage() == 1) {
            mList.clear();
        } else {
            mAdapter.removeFooter();
        }
        if (!handleResponse(response)) {
            PageResponse<VideoItem> data = response.getData();
            List<VideoItem> list = data.getList();
            mRecyclerView.setPageCount(data.getPage().getTotal());
            mList.addAll(list);
            if (mRecyclerView.getCurPage() == 1) {
                mAdapter.notifyDataSetChanged();
            } else {
                int size = list.size();
                mAdapter.notifyItemRangeInserted(mList.size() - size, size);
            }
        }
        mRecyclerView.setLoading(false);
        mRefreshLayout.setRefreshing(false);
    }


    private void processError(@NonNull Throwable throwable) {
        handleFailure(throwable);
        mRecyclerView.setLoading(false);
        mRefreshLayout.setRefreshing(false);
        if (mRecyclerView.getCurPage() > 1) {
            mRecyclerView.pageDecrement();
            mAdapter.removeFooter();
        }
    }

    private View.OnClickListener mOnClickListener = v -> {
        switch (v.getId()) {
            case R.id.btn_watch:
                ActivityStackManager.remove(BookMarkActivity.this);
                isBack = false;
                finish();
                if (ActivityStackManager.getLast() instanceof VideoDetailActivity) {
                    ActivityStackManager.getLast().finish();
                }
                LocalBroadcastManager.getInstance(BookMarkActivity.this).sendBroadcast(new Intent(Constants.ACTION_PERFORM_TAB));
                break;
//            case R.id.tv_cancel_mark:
//                unMark((VideoItem) v.getTag());
//                break;
            case R.id.iv_cover:
                Intent intent = new Intent(BookMarkActivity.this, VideoDetailActivity.class);
                VideoItem tag = (VideoItem) v.getTag(R.id.glideId);
                intent.putExtra(VideoDetailActivity.ARG_VIDEO_ID, tag.getId());
                intent.putExtra(VideoDetailActivity.ARG_VIDEO_TITLE, tag.getTitle());
                startActivity(intent);
                break;
        }
    };

    @Override
    public void finish() {
        if (isBack) {
            setResult(RESULT_OK);
        }
        super.finish();
    }
}
