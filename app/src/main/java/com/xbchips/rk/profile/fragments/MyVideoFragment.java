package com.xbchips.rk.profile.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseFragment;
import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.base.response.PageResponse;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.profile.adapter.MyVideosAdapter;
import com.xbchips.rk.profile.listener.OnLoadCountListener;
import com.xbchips.rk.util.DisplayUtils;
import com.xbchips.rk.video.VideoDetailActivity;
import com.xbchips.rk.video.VideoService;
import com.xbchips.rk.video.entity.MyVideo;
import com.xbchips.rk.video.entity.MyVideosResp;
import com.xbchips.rk.widgets.EndlessRecyclerView;
import com.xbchips.rk.widgets.ItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class MyVideoFragment extends BaseFragment {
    private static final String ARG_INDEX = "index";
    @BindView(R.id.recycler_view)
    EndlessRecyclerView mRecyclerView;
    private VideoService mService = HttpRequest.createService(VideoService.class);
    private MyVideosAdapter mAdapter;
    private final ArrayList<MyVideo> mList = new ArrayList<>();
    private OnLoadCountListener mListener;

    public static MyVideoFragment getInstance(int index) {
        MyVideoFragment fragment = new MyVideoFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_INDEX, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_video, container, false);
        mUnBinder = ButterKnife.bind(this, rootView);
        init();
        return rootView;
    }

    @Override
    protected void init() {
        isInit = true;
        initRecyclerView();
    }

    public void setListener(OnLoadCountListener listener) {
        mListener = listener;
    }

    private void initRecyclerView() {
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == mList.size()) {
                    return 2;
                }
                return 1;
            }
        });
        mRecyclerView.setLayoutManager(layoutManager);
        int px = DisplayUtils.dip2px(getContext(), 6);
        mRecyclerView.addItemDecoration(new ItemDecoration(px, 2));
        mAdapter = new MyVideosAdapter(mList, getContext(), this, ARG_INDEX);
        mAdapter.setOnClickListener(v -> {
            int position = ((int) v.getTag());
            MyVideo myVideo = mList.get(position);
            int index = getArguments().getInt(ARG_INDEX);
            if (index == 0) {
                //审核通过
                if (myVideo.getStatus() == 1) {
                    Intent intent = new Intent(getContext(), VideoDetailActivity.class);
                    intent.putExtra(VideoDetailActivity.ARG_VIDEO_ID, myVideo.getId());
                    intent.putExtra(VideoDetailActivity.ARG_VIDEO_TITLE, myVideo.getTitle());
                    intent.putExtra(VideoDetailActivity.ARG_VIDEO_ISXIANGYOU, "xiangyou");
                    startActivity(intent);
                } else {
                    showToast(R.string.reviewing_wait);
                }
            } else if (index == 1) {
                Intent intent = new Intent(getContext(), VideoDetailActivity.class);
                intent.putExtra(VideoDetailActivity.ARG_VIDEO_ID, myVideo.getId());
                intent.putExtra(VideoDetailActivity.ARG_VIDEO_TITLE, myVideo.getTitle());
                intent.putExtra(VideoDetailActivity.ARG_VIDEO_ISXIANGYOU, "xiangyou");
                startActivity(intent);
            }


        });
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setOnLoadMoreListener(() -> {
            mAdapter.addFooter();
            loadData();
        });
        loadData();
    }


    private void loadData() {
        Observable<ObjectResponse<MyVideosResp>> myVideos = getArguments().getInt(ARG_INDEX) == 0 ?
                mService.getMyVideos(null, mRecyclerView.getCurPage(), 20) :
                mService.getPurChase(mRecyclerView.getCurPage(), 20);
        Disposable disposable = myVideos
                .compose(RxUtils.applySchedulers())
                .filter(resp -> !handleResponse(resp))
                .subscribe(this::setData, this::processError);
        mCompositeDisposable.add(disposable);

    }

    private void setData(ObjectResponse<MyVideosResp> response) {
        if (mRecyclerView.getCurPage() == 1) {
            int index = getArguments().getInt(ARG_INDEX);
            MyVideosResp data = response.getData();
            if (mListener != null) {
                mListener.onLoadCountSuccess(index, index == 0 ? data.getTotalVideo() : data.getTotalPurchase());
            }
            mList.clear();
        } else {
            mAdapter.removeFooter();
        }
        if (!handleResponse(response)) {
            PageResponse<MyVideo> data = response.getData();
            List<MyVideo> list = data.getList();
            mRecyclerView.setPageCount(data.getPage().getTotal());
            mList.addAll(list);
            if (mRecyclerView.getCurPage() == 1) {
                mAdapter.notifyDataSetChanged();
            } else {
                int size = list.size();
                mAdapter.notifyItemRangeInserted(mList.size() - size, size);
            }
        }
        mRecyclerView.setLoading(false);
    }

    private void processError(@NonNull Throwable throwable) {
        handleFailure(throwable);
        mRecyclerView.setLoading(false);
        if (mRecyclerView.getCurPage() > 1) {
            mRecyclerView.pageDecrement();
            mAdapter.removeFooter();
        }
    }

}
