package com.xbchips.rk.profile.activity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.exoplayer2.offline.Download;
import com.xbchips.rk.R;
import com.xbchips.rk.XvideoApplication;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.profile.adapter.CachingAdapter;
import com.xbchips.rk.util.LogTrace;
import com.xbchips.rk.widgets.EndlessRecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class CachingActivity extends BaseActivity {
    private static final String TAG = "CachingActivity";
    @BindView(R.id.recycler_view)
    EndlessRecyclerView recyclerView;
    private final List<Download> list = new ArrayList<>();
    private boolean intervalFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloading);
        ButterKnife.bind(this);
        initTopBar();
        initRecyclerView();
        schedule();
    }

    @Override
    protected void onResume() {
        super.onResume();
        intervalFlag = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        intervalFlag = false;
    }

    @Override
    protected void initTopBar() {
        super.initTopBar();
        mTvTitle.setText(R.string.caching);

    }

    private void initRecyclerView() {
        list.addAll(XvideoApplication.getApplication().getDownloadManager().getCurrentDownloads());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        CachingAdapter adapter = new CachingAdapter(list, this);
        adapter.setOnClickListener(v -> {
            Download download = (Download) v.getTag();
            switch (v.getId()) {
                case R.id.tv_pause:
                    LogTrace.d(TAG, "current download state: " + download.state);
                    final String id = download.request.id;
                    if (download.state == Download.STATE_STOPPED) {
                        XvideoApplication.getApplication().getDownloadManager().setStopReason(id, Download.STOP_REASON_NONE);
                    } else {
                        XvideoApplication.getApplication().getDownloadManager().setStopReason(id, -1);
                    }
                    int index = getIndex(id);
                    if (index != -1) {
                        adapter.notifyItemChanged(index);
                    }
                    break;
                case R.id.tv_cancel:
                    XvideoApplication.getApplication().getDownloadManager().removeDownload(download.request.id);
                    adapter.notifyItemRemoved(getIndex(download.request.id));
                    break;
            }
        });
        recyclerView.setAdapter(adapter);
    }


    private int getIndex(String downloadId) {
        int index = -1;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).request.id.equals(downloadId)) {
                index = i;
            }
        }
        return index;
    }

    private void schedule() {
        Disposable disposable = Observable.interval(2, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .filter(t -> intervalFlag)
                .subscribe(aLong -> {
                    list.clear();
                    list.addAll(XvideoApplication.getApplication().getDownloadManager().getCurrentDownloads());
                    recyclerView.getAdapter().notifyDataSetChanged();
                }, throwable -> {

                });
        mCompositeDisposable.add(disposable);
    }
}
