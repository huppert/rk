package com.xbchips.rk.profile.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.offline.Download;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.util.ObjectUtil;
import com.xbchips.rk.video.entity.DownloadData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CachingAdapter extends BaseAdapter<Download> {


    public CachingAdapter(List<Download> list, Context context) {
        super(list, context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return viewType == TYPE_FOOTER ? getFooterViewHolder(parent) :
                new CachingViewHolder(mInflater.inflate(R.layout.item_caching, parent, false));
    }


    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        CachingViewHolder holder = (CachingViewHolder) viewHolder;
        Download download = mList.get(position);
        DownloadData downloadData = ObjectUtil.unmarshall(download.request.data, DownloadData.CREATOR);
        holder.tvTitle.setText(downloadData.getTitle());
        ImageLoader.load(mContext, downloadData.getCoverUrl(), holder.ivCover);
        holder.progressBar.setProgress((int) (download.getPercentDownloaded()));
        holder.tvPause.setText(download.state == Download.STATE_STOPPED ? R.string.resume : R.string.pause);
        holder.tvPause.setTag(download);
        holder.tvPause.setOnClickListener(mOnClickListener);
        holder.tvCancel.setTag(download);
        holder.tvCancel.setOnClickListener(mOnClickListener);

    }

    static class CachingViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_cover)
        ImageView ivCover;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.progress_bar)
        ProgressBar progressBar;
        @BindView(R.id.tv_pause)
        TextView tvPause;
        @BindView(R.id.tv_cancel)
        TextView tvCancel;

        public CachingViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
