package com.xbchips.rk.profile.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.video.entity.Bill;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BillAdapter extends BaseAdapter<Bill> {

    public BillAdapter(List<Bill> list, Context context) {
        super(list, context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return i == TYPE_ITEM ? new BillViewHolder(mInflater.inflate(R.layout.item_bill, viewGroup, false))
                : getFooterViewHolder(viewGroup);
    }


    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        BillViewHolder viewHolder = (BillViewHolder) holder;
        Bill bill = mList.get(position);
        boolean isInCome = "收入".equals(bill.getTransaction());
        viewHolder.tvAmount.setText((isInCome ? "+" : "-") + bill.getPrice());
        viewHolder.tvTime.setText(bill.getTime());
        viewHolder.tvTitle.setText(mContext.getString(R.string.bill_content, bill.getUsername(), bill.getTransaction(), bill.getPrice()));
    }

    static class BillViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.tv_amount)
        TextView tvAmount;


        public BillViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
