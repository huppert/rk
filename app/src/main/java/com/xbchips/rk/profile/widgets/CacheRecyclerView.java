package com.xbchips.rk.profile.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.xbchips.rk.widgets.EndlessRecyclerView;

public class CacheRecyclerView extends EndlessRecyclerView {
    private View header;

    public CacheRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean isEmpty() {
        return super.isEmpty() && header.getVisibility() == GONE;
    }

    public void setHeader(View header) {
        this.header = header;
    }
}
