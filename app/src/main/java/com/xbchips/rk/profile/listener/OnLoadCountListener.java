package com.xbchips.rk.profile.listener;

public interface OnLoadCountListener {
    void onLoadCountSuccess(int index, int count);
}
