package com.xbchips.rk.profile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.base.response.PageResponse;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.profile.adapter.PurchaseAdapter;
import com.xbchips.rk.video.VideoDetailActivity;
import com.xbchips.rk.video.VideoService;
import com.xbchips.rk.video.entity.MyVideo;
import com.xbchips.rk.video.entity.MyVideosResp;
import com.xbchips.rk.widgets.EndlessRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;

public class PurchaseActivity extends BaseActivity {

    @BindView(R.id.recycler_view)
    EndlessRecyclerView mRecyclerView;
    @BindView(R.id.tv_bill)
    TextView tvBill;
    private VideoService mService = HttpRequest.createService(VideoService.class);
    private PurchaseAdapter mAdapter;
    private final ArrayList<MyVideo> mList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);
        ButterKnife.bind(this);
        initTopBar();
        initRecyclerView();
    }

    @Override
    protected void initTopBar() {
        super.initTopBar();
        mTvTitle.setText(R.string.purchase_record);
        tvBill.setOnClickListener(v -> startActivity(new Intent(this, BillActivity.class)));
    }


    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new PurchaseAdapter(mList, this);
        mAdapter.setOnClickListener(v -> {
            int position = ((int) v.getTag());
            MyVideo myVideo = mList.get(position);
            Intent intent = new Intent(this, VideoDetailActivity.class);
            intent.putExtra(VideoDetailActivity.ARG_VIDEO_ID, myVideo.getId());
            intent.putExtra(VideoDetailActivity.ARG_VIDEO_TITLE, myVideo.getTitle());
            intent.putExtra(VideoDetailActivity.ARG_VIDEO_ISXIANGYOU, "xiangyou");
            startActivity(intent);


        });
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setOnLoadMoreListener(() -> {
            mAdapter.addFooter();
            loadData();
        });
        loadData();
    }


    private void loadData() {
        Disposable disposable = mService.getPurChase(mRecyclerView.getCurPage(), 20)
                .compose(RxUtils.applySchedulers())
                .filter(resp -> !handleResponse(resp))
                .subscribe(this::setData, this::processError);
        mCompositeDisposable.add(disposable);

    }

    private void setData(ObjectResponse<MyVideosResp> response) {
        if (mRecyclerView.getCurPage() == 1) {
            mList.clear();
        } else {
            mAdapter.removeFooter();
        }
        if (!handleResponse(response)) {
            PageResponse<MyVideo> data = response.getData();
            List<MyVideo> list = data.getList();
            mRecyclerView.setPageCount(data.getPage().getTotal());
            mList.addAll(list);
            if (mRecyclerView.getCurPage() == 1) {
                mAdapter.notifyDataSetChanged();
            } else {
                int size = list.size();
                mAdapter.notifyItemRangeInserted(mList.size() - size, size);
            }
        }
        mRecyclerView.setLoading(false);
    }

    private void processError(@NonNull Throwable throwable) {
        handleFailure(throwable);
        mRecyclerView.setLoading(false);
        if (mRecyclerView.getCurPage() > 1) {
            mRecyclerView.pageDecrement();
            mAdapter.removeFooter();
        }
    }
}
