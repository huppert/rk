package com.xbchips.rk.profile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.profile.AccountService;
import com.xbchips.rk.user.activities.UserAuthActivity;
import com.xbchips.rk.user.entity.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Harry on 2017/9/19.
 */

public class AccountSecurityActivity extends BaseActivity {
    private static final String TAG = "AccountSecurityActivity";
    private static final int CODE = 201;
    @BindView(R.id.bind_phone)
    LinearLayout mBindPhone;
    @BindView(R.id.modify_password)
    LinearLayout mModifyPassword;
    @BindView(R.id.log_out)
    LinearLayout mLogOut;
    @BindView(R.id.tv_bind_phone)
    TextView mTvBindPhone;
    @BindView(R.id.tv_phone_number)
    TextView mTvPhoneNumber;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security_account);
        ButterKnife.bind(this);
        init();


    }

    private void init() {
        initTopBar();
        mTvTitle.setText(R.string.security_account);
        mTvBindPhone.setText(TextUtils.isEmpty(getPhoneNumber()) ? R.string.bind_phone : R.string.unbind_phone);
        String phone = getPhoneNumber();
        if (!TextUtils.isEmpty(phone)) {
            phone = phone.substring(0, 3) + "****" + phone.substring(7, 11);
        }
        mTvPhoneNumber.setText(phone);
    }

    private String getPhoneNumber() {
        User localUser = UserHelper.getLocalUser();
        if (localUser == null) return null;
        return localUser.getPhone();
    }

    @OnClick({R.id.bind_phone, R.id.modify_password, R.id.log_out})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bind_phone:
                toBindActivity();
                break;
            case R.id.modify_password:
                startActivity(new Intent(this, ModifyPasswordActivity.class));
                break;
            case R.id.log_out:
                if (TextUtils.isEmpty(getPhoneNumber())) {
                    new AlertDialog.Builder(this)
                            .setTitle("提示")
                            .setMessage("您还没有完成手机绑定，现在登出账号可能导致之后无法正常登录，是否继续？")
                            .setPositiveButton(R.string.bind_phone, (dialog, which) -> toBindActivity())
                            .setNegativeButton(R.string.keep_exit, (dialog, which) -> {
                                logout();
                            })
                            .show();
                } else {
                    logout();
                }
                break;
        }
    }

    private void toBindActivity() {
        Intent intent = new Intent(this, TextUtils.isEmpty(getPhoneNumber()) ?
                BindPhoneActivity.class : BindPhoneAlreadyActivity.class);
        startActivityForResult(intent, CODE);
    }

    private void logout() {
        showProgress("正在进行登出账号操作，请稍后");
        AccountService service = HttpRequest.createService(AccountService.class);
        Disposable disposable = service.logout()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::logoutSuccess, this::handleFailure);
        mCompositeDisposable.add(disposable);

    }

    private void logoutSuccess(@NonNull ObjectResponse<String> response) {
        if (!handleResponse(response)) {
            dismissDialog();
            UserHelper.clearToken();
            LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(Constants.ACTION_REFRESH_STATE));
            finish();
            Intent intent = new Intent(this, UserAuthActivity.class);
            intent.putExtra(UserAuthActivity.ARG_IS_LOGIN, true);
            startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == CODE) {
            init();
        }
    }
}
