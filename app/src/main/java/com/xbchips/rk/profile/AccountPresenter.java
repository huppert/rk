package com.xbchips.rk.profile;

import com.xbchips.rk.base.BasePresenter;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.common.HttpRequest;

import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by york on 2017/10/12.
 */

public class AccountPresenter implements BasePresenter {
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private AccountService mUserService;
    private AccountView mView;

    public AccountPresenter(AccountView accountView) {
        mView = accountView;
        mUserService = HttpRequest.createService(AccountService.class);
    }

    public void bindPhone(HashMap<String, String> map) {
        executeTask(mUserService.bindPhone(map), response -> mView.onSuccess());
    }


    public void unbindPhone(HashMap<String, String> map) {
        executeTask(mUserService.unbindPhone(map), response -> mView.onSuccess());
    }


    public void getCode(int type, String phone) {
        executeTask(mUserService.getVerifyCode(type, phone), response -> {
            mView.getCodeSuccess();
        });

    }


    public void resetPassword(HashMap<String, String> map) {
        executeTask(mUserService.resetPassword(map), response -> mView.onSuccess());
    }


    private <T> void executeTask(Observable<ObjectResponse<T>> observable, Consumer<ObjectResponse<T>> onNext) {
        mCompositeDisposable.add(observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .filter(response -> !mView.handleResponse(response))
                .subscribe(onNext, throwable -> mView.handleFailure(throwable))
        );
    }


    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }

}
