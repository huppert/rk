package com.xbchips.rk.profile;

import com.xbchips.rk.base.response.ObjectResponse;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by york on 2017/10/12.
 */

public interface AccountService {
    /**
     * 获取验证码
     *
     * @param type  1 绑定 2 解绑 3修改密码
     * @param phone 手机号
     */
    @GET("user/verifyCode/{type}/{phone}")
    Observable<ObjectResponse<String>> getVerifyCode(@Path("type") int type, @Path("phone") String phone);

    /**
     * 绑定手机号
     *
     * @param map {
     *            password  登录密码
     *            mobile    手机号
     *            code      验证码
     *            }
     */
    @POST("user/bind")
    Observable<ObjectResponse<String>> bindPhone(@Body HashMap<String, String> map);

    /**
     * 解绑手机号
     *
     * @param map {
     *            password  登录密码
     *            mobile    手机号
     *            code      验证码
     *            }
     */
    @POST("user/unbind")
    Observable<ObjectResponse<String>> unbindPhone(@Body HashMap<String, String> map);


    /**
     * 重置密码
     *
     * @param map {
     *            password          密码
     *            verifyPassword    校验密码
     *            mobile            手机号
     *            code              验证码
     *            }
     */
    @POST("user/reset")
    Observable<ObjectResponse<String>> resetPassword(@Body HashMap<String, String> map);


    /**
     * 注销
     */
    @GET("user/loginout")
    Observable<ObjectResponse<String>> logout();
}
