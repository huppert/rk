package com.xbchips.rk.profile.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.exoplayer2.offline.Download;
import com.google.android.exoplayer2.offline.DownloadCursor;
import com.google.android.exoplayer2.offline.DownloadManager;
import com.google.android.exoplayer2.offline.DownloadRequest;
import com.xbchips.rk.R;
import com.xbchips.rk.XvideoApplication;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.base.response.PageResponse;
import com.xbchips.rk.profile.adapter.DownloadAdapter;
import com.xbchips.rk.profile.widgets.CacheHeaderLayout;
import com.xbchips.rk.profile.widgets.CacheRecyclerView;
import com.xbchips.rk.util.LogTrace;
import com.xbchips.rk.util.ObjectUtil;
import com.xbchips.rk.video.VideoDetailActivity;
import com.xbchips.rk.video.entity.DownloadData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class MyDownloadActivity extends BaseActivity {
    private static final String TAG = "MyDownloadActivity";
    @BindView(R.id.recycler_view)
    CacheRecyclerView recyclerView;
    @BindView(R.id.ll_empty)
    LinearLayout llEmpty;
    @BindView(R.id.cache_header)
    CacheHeaderLayout mHeaderLayout;

    private List<DownloadData> mList = new ArrayList<>();
    private DownloadAdapter adapter;

    private final DownloadManager.Listener listener = new DownloadManager.Listener() {
        @Override
        public void onDownloadChanged(DownloadManager downloadManager, Download download) {
            if (download.state == Download.STATE_COMPLETED) {
                if (mList.isEmpty()) {
                    loadData();
                } else {
                    DownloadData downloadData = ObjectUtil.unmarshall(download.request.data, DownloadData.CREATOR);
                    mList.add(0, downloadData);
                    recyclerView.getAdapter().notifyItemInserted(0);
                }
                //主动刷新正在缓存的列表
                mHeaderLayout.updateTotalProgress();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_download);
        ButterKnife.bind(this);
        initTopBar();
        initRecyclerView();
        loadData();
        XvideoApplication.getApplication().getDownloadManager().addListener(listener);
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (mHeaderLayout != null) {
            mHeaderLayout.setIntervalFlag(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mHeaderLayout != null) {
            mHeaderLayout.setIntervalFlag(true);
        }
    }


    @Override
    protected void initTopBar() {
        super.initTopBar();
        mTvTitle.setText(R.string.my_download);
    }


    private void loadData() {
        final Observable<PageResponse<DownloadData>> observable = Observable.create(e ->
                e.onNext(getDownloads(recyclerView.getCurPage(), 20)));

        Disposable disposable = observable
                .compose(RxUtils.applySchedulers())
                .subscribe(this::handResp, this::processError);
        mCompositeDisposable.add(disposable);
    }


    public void handResp(PageResponse<DownloadData> response) {
        if (recyclerView.isFirstPage()) {
            mList.clear();
        }
        adapter.removeFooter();
        List<DownloadData> list = response.getList();
        recyclerView.setPageCount(response.getPage().getTotal());
        mList.addAll(list);
        int size = list.size();
        if (recyclerView.isFirstPage()) {
            adapter.notifyDataSetChanged();
        } else {
            adapter.notifyItemRangeInserted(mList.size() - size, size);
        }
        if (recyclerView.isLastPage()) {
            adapter.addNoMoreView();
        }
        recyclerView.setLoading(false);
    }

    public void processError(@NonNull Throwable throwable) {
        handleFailure(throwable);
        recyclerView.setLoading(false);
        if (!recyclerView.isFirstPage()) {
            recyclerView.pageDecrement();
            adapter.removeFooter();
        }
    }


    private PageResponse<DownloadData> getDownloads(int curPage, int pageSize) {
        final DownloadManager downloadManager = XvideoApplication.getApplication().getDownloadManager();
        PageResponse<DownloadData> pageResponse = new PageResponse<>();
        if (downloadManager == null) return pageResponse;
        PageResponse.Page page = new PageResponse.Page();
        page.setCurrent(curPage);
        page.setSize(pageSize);
        List<Download> downloads = new ArrayList<>();

        try (DownloadCursor cursor = downloadManager.getDownloadIndex().getDownloads(Download.STATE_COMPLETED)) {
            final int count = cursor.getCount();
            page.setTotal((int) Math.ceil(count / (float) pageSize));

            int start = (curPage - 1) * pageSize;
            int end = Math.min(start + pageSize, count);
            cursor.moveToPosition(start);

            while (cursor.getPosition() < end) {
                Download download = cursor.getDownload();
                downloads.add(download);
                LogTrace.d(TAG, "total =  " + download.contentLength + "     download = " + download.getBytesDownloaded());
                cursor.moveToNext();
            }
            pageResponse.setPage(page);
            pageResponse.setList(transform(downloads));
        } catch (IOException e) {
            Log.w(TAG, "Failed to query downloads", e);
        }
        return pageResponse;
    }


    private List<DownloadData> transform(List<Download> downloads) {
        final List<DownloadData> downloadDatas = new ArrayList<>();
        for (Download download : downloads) {
            DownloadData downloadData = ObjectUtil.unmarshall(download.request.data, DownloadData.CREATOR);
            downloadDatas.add(downloadData);
        }

        return downloadDatas;
    }


    private void initRecyclerView() {
        recyclerView.setHeader(mHeaderLayout);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new DownloadAdapter(mList, this);
        adapter.setOnClickListener(v -> {
            DownloadData videoItem = ((DownloadData) v.getTag());
            switch (v.getId()) {
                case R.id.tv_delete:
                    delete(videoItem);
                    break;
                case R.id.cl_content:
                    Intent intent = new Intent(this, VideoDetailActivity.class);
                    intent.putExtra(VideoDetailActivity.ARG_VIDEO_ID, videoItem.getVideoId());
                    intent.putExtra(VideoDetailActivity.ARG_VIDEO_TITLE, videoItem.getTitle());
                    startActivity(intent);
                    break;
            }
        });
        recyclerView.swapAdapter(adapter, false);
        recyclerView.setEmptyView(llEmpty);
        recyclerView.setOnLoadMoreListener(() -> {
            adapter.addFooter();
            loadData();
        });
        mHeaderLayout.initCaching();
    }

    private void delete(DownloadData videoItem) {
        Uri uri = Uri.parse(videoItem.getVideoUrl());
        XvideoApplication application = XvideoApplication.getApplication();
        if (application.getDownloadTracker().isDownloaded(uri)) {
            DownloadRequest request = application.getDownloadTracker().getDownloadRequest(uri);
            if (request != null) {
                application.getDownloadManager().removeDownload(request.id);
            }
        }
        int index = getIndexById(videoItem.getVideoId());
        if (index != -1) {
            mList.remove(index);
            recyclerView.getAdapter().notifyItemRemoved(index);
        }
    }


    private int getIndexById(String videoId) {
        int index = -1;
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).getVideoId().equals(videoId)) {
                index = i;
                break;
            }
        }
        return index;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        XvideoApplication.getApplication().getDownloadManager().removeListener(listener);
    }
}
