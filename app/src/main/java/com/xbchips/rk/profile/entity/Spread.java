package com.xbchips.rk.profile.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by york on 2017/10/19.
 * 推广
 */

public class Spread {
    private int userRegMembers;
    private int userResPointTotal;
    private int promoteRechargeMembers;
    private int promoteRechargeTotal;
    private String promoteUrl;
    private List<ExchangeBean> exchange;

    public int getUserRegMembers() {
        return userRegMembers;
    }

    public void setUserRegMembers(int userRegMembers) {
        this.userRegMembers = userRegMembers;
    }


    public int getPromoteRechargeMembers() {
        return promoteRechargeMembers;
    }

    public void setPromoteRechargeMembers(int promoteRechargeMembers) {
        this.promoteRechargeMembers = promoteRechargeMembers;
    }

    public int getUserResPointTotal() {
        return userResPointTotal;
    }

    public void setUserResPointTotal(int userResPointTotal) {
        this.userResPointTotal = userResPointTotal;
    }

    public int getPromoteRechargeTotal() {
        return promoteRechargeTotal;
    }

    public void setPromoteRechargeTotal(int promoteRechargeTotal) {
        this.promoteRechargeTotal = promoteRechargeTotal;
    }

    public String getPromoteUrl() {
        return promoteUrl;
    }

    public void setPromoteUrl(String promoteUrl) {
        this.promoteUrl = promoteUrl;
    }

    public List<ExchangeBean> getExchange() {
        if (exchange == null) {
            exchange = new ArrayList<>();
        }
        return exchange;
    }

    public void setExchange(List<ExchangeBean> exchange) {
        this.exchange = exchange;
    }

    public static class ExchangeBean {
        private String title;
        private int use;
        private int days;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getUse() {
            return use;
        }

        public void setUse(int use) {
            this.use = use;
        }

        public int getDays() {
            return days;
        }

        public void setDays(int days) {
            this.days = days;
        }
    }
}
