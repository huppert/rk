package com.xbchips.rk.profile.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.xbchips.rk.R;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.user.entity.User;
import com.xbchips.rk.util.DisplayUtils;

public class ProfileHeaderLayout extends FrameLayout {
    public ProfileHeaderLayout(@NonNull Context context) {
        this(context, null);
    }

    public ProfileHeaderLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }


    public void inflate(boolean logined) {
        removeAllViews();
        if (logined) {
            LayoutInflater.from(getContext()).inflate(R.layout.layout_profile_header_user, this);
            setPadding(0, 0, 0, DisplayUtils.dip2px(getContext(), 30));
            refreshData();
        } else {
            LayoutInflater.from(getContext()).inflate(R.layout.layout_profile_header_visitor, this);
            setPadding(0, 0, 0, DisplayUtils.dip2px(getContext(), 50));
        }
    }


    public void refreshData() {
        ImageView avatar = findViewById(R.id.iv_avatar_login);
        TextView username = findViewById(R.id.tv_username);
        TextView signature = findViewById(R.id.tv_signature);
        TextView nickname = findViewById(R.id.tv_nickname);
        TextView leftDays = findViewById(R.id.tv_left_days);
        User localUser = UserHelper.getLocalUser();
        ImageLoader.loadAvatar(getContext(), localUser.getAvatar(), avatar);
        username.setText(getContext().getString(R.string.username_format, localUser.getUsername()));
        signature.setText(localUser.getSignature());
        nickname.setText(localUser.getNickname());
        leftDays.setText(localUser.getVip().getLeftDays() + "天");
    }

}
