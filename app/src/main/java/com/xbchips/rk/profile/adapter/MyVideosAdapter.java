package com.xbchips.rk.profile.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.profile.fragments.MyVideoFragment;
import com.xbchips.rk.util.DisplayUtils;
import com.xbchips.rk.video.entity.MyVideo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyVideosAdapter extends BaseAdapter<MyVideo> {

    private int mRadius;
    private MyVideoFragment myVideoFragment;
    private String mInde;

    public MyVideosAdapter(List<MyVideo> list, Context context, MyVideoFragment fragment, String inde) {
        super(list, context);
        mRadius = DisplayUtils.dip2px(context, 6);
        myVideoFragment = fragment;
        mInde = inde;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return viewType == TYPE_ITEM ? new MyVideoViewHolder(mInflater.inflate(R.layout.item_myvideo, viewGroup, false)) :
                getFooterViewHolder(viewGroup);
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        MyVideo myVideo = mList.get(position);
        MyVideoViewHolder viewHolder = (MyVideoViewHolder) holder;
        int status = myVideo.getStatus();
        int anInt = myVideoFragment.getArguments().getInt(mInde);
        if (anInt == 0) {
            //待审核
            if (status == 0) {
                viewHolder.tvTimes.setVisibility(View.VISIBLE);
                viewHolder.tvTimes.setText(String.valueOf(myVideo.getCount()));
                viewHolder.tvTitle.setText("等待审核");

            } else if (status == 1) {//审核通过
                viewHolder.tvTimes.setVisibility(View.VISIBLE);
                viewHolder.tvTimes.setText(String.valueOf(myVideo.getCount()));
                viewHolder.tvTitle.setText(myVideo.getTitle());
            } else {
                viewHolder.tvTimes.setVisibility(View.INVISIBLE);
                viewHolder.tvTitle.setText("审核未通过");
            }

            ImageLoader.load(mContext, status == 1 ? myVideo.getThumb_img_url() : R.drawable.reviewing, viewHolder.ivCover, new RequestOptions()
                    .transform(new MultiTransformation<>(new CenterCrop(), new RoundedCorners(mRadius)))
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .placeholder(R.drawable.place_holder)
                    .dontAnimate());
        } else if (anInt == 1) {
            viewHolder.tvTimes.setVisibility(View.VISIBLE);
            viewHolder.tvTimes.setText(String.valueOf(myVideo.getCount()));
            viewHolder.tvTitle.setText(myVideo.getTitle());
            ImageLoader.load(mContext, status == 0 ? myVideo.getThumb_img_url() : R.drawable.reviewing, viewHolder.ivCover, new RequestOptions()
                    .transform(new MultiTransformation<>(new CenterCrop(), new RoundedCorners(mRadius)))
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .placeholder(R.drawable.place_holder)
                    .dontAnimate());
        }

        viewHolder.itemView.setTag(position);
        viewHolder.itemView.setOnClickListener(mOnClickListener);
    }

    static class MyVideoViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_cover)
        ImageView ivCover;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_times)
        TextView tvTimes;

        public MyVideoViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
