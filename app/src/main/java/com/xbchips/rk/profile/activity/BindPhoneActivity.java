package com.xbchips.rk.profile.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.profile.AccountPresenter;
import com.xbchips.rk.profile.AccountView;
import com.xbchips.rk.user.entity.User;
import com.xbchips.rk.util.RegexUtil;

import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Harry on 2017/9/19.
 */

public class BindPhoneActivity extends BaseActivity implements AccountView {
    private final AccountPresenter mPresenter = new AccountPresenter(this);
    @BindView(R.id.edit_text)
    EditText mEditText;
    @BindView(R.id.btn_code)
    Button mBtnCode;
    @BindView(R.id.edit_text_code)
    EditText mEditTextCode;
    @BindView(R.id.edit_text_pw)
    EditText mEditTextPw;
    @BindView(R.id.btn_bind)
    Button mBtnBind;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_bind);
        ButterKnife.bind(this);
        initTopBar();
        mTvTitle.setText(R.string.bind_phone);
    }


    @OnClick({R.id.btn_code, R.id.btn_bind})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_code:
                if (checkPhoneNumber()) {
                    showProgress();
                    mPresenter.getCode(1, getPhoneNumber());
                }
                break;
            case R.id.btn_bind:
                if (checkForm()) {
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("mobile", getPhoneNumber());
                    hashMap.put("code", getCode());
                    hashMap.put("password", getPassword());
                    showProgress();
                    mPresenter.bindPhone(hashMap);
                }
                break;
        }
    }


    private boolean checkForm() {
        if (!checkPhoneNumber()) return false;
        if (TextUtils.isEmpty(getCode())) {
            showToast("验证码不能为空");
            return false;
        }
        if (TextUtils.isEmpty(getPassword())) {
            showToast("登录密码不能为空");
            return false;
        }
        return true;
    }

    private boolean checkPhoneNumber() {
        if (!RegexUtil.match(RegexUtil.PHONE_NUMBER_REGEXP, getPhoneNumber())) {
            showToast(getString(R.string.wrong_number));
            return false;
        }
        return true;
    }

    @androidx.annotation.NonNull
    private String getPassword() {
        return mEditTextPw.getText().toString();
    }

    @androidx.annotation.NonNull
    private String getCode() {
        return mEditTextCode.getText().toString();
    }

    @androidx.annotation.NonNull
    private String getPhoneNumber() {
        return mEditText.getText().toString();
    }

    private void countDownTime(final int time) {
        Observable.interval(0, 1, TimeUnit.SECONDS)
                .take(time)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        mBtnCode.setEnabled(false);
                        mBtnCode.setBackgroundColor(Color.GRAY);
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Long>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        mCompositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(@NonNull Long aLong) {
                        mBtnCode.setText(String.format(Locale.getDefault(), "%s秒后重试", time - 1 - aLong));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        mBtnCode.setEnabled(true);
                        mBtnCode.setBackgroundResource(R.drawable.bg_code);
                        mBtnCode.setText(getString(R.string.get_code));
                    }
                });
    }

    @Override
    public void setPresenter(Object presenter) {

    }

    @Override
    public void getCodeSuccess() {
        showToast("发送成功");
        dismissDialog();
        countDownTime(Constants.CODE_COUNTDOWN_TIME);
    }

    @Override
    public void onSuccess() {
        showToast("绑定成功");
        User localUser = UserHelper.getLocalUser();
        localUser.setPhone(getPhoneNumber());
        dismissDialog();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.unSubscribe();
    }
}
