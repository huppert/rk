package com.xbchips.rk.profile.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.profile.entity.Spread;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by york on 2017/10/24.
 */

public class SpreadAdapter extends BaseAdapter<Spread.ExchangeBean> {


    public SpreadAdapter(List<Spread.ExchangeBean> list, Context context) {
        super(list, context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SpreadViewHolder(mInflater.inflate(R.layout.item_spread, parent, false));
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        SpreadViewHolder holder = (SpreadViewHolder) viewHolder;
        Spread.ExchangeBean exchange = mList.get(position);
        holder.mTvExchange.setText(exchange.getTitle());
        holder.mBtnExchange.setTag(exchange);
        holder.mBtnExchange.setOnClickListener(mOnClickListener);
    }

    static class SpreadViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_exchange)
        TextView mTvExchange;
        @BindView(R.id.btn_exchange)
        Button mBtnExchange;

        public SpreadViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
