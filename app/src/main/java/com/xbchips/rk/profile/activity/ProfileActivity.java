package com.xbchips.rk.profile.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.view.TimePickerView;
import com.xbchips.rk.R;
import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.profile.fragments.GenderSelectFragment;
import com.xbchips.rk.user.entity.User;
import com.xbchips.rk.user.service.UserService;
import com.xbchips.rk.util.DateUtil;

import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ProfileActivity extends TakePhotoActivity {

    @BindView(R.id.iv_avatar)
    ImageView ivAvatar;
    @BindView(R.id.et_nickname)
    EditText etNickname;
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.tv_gender)
    TextView tvGender;
    @BindView(R.id.tv_birthday)
    TextView tvBirthday;
    @BindView(R.id.tv_signature)
    EditText tvSignature;
    private boolean withAvatar;
    private final UserService service = HttpRequest.createService(UserService.class);
    private TimePickerView mBirthPickerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        final User localUser = UserHelper.getLocalUser();
        tvUsername.setText(localUser.getUsername());
        etNickname.setText(localUser.getNickname());
        ImageLoader.loadAvatar(this, localUser.getAvatar(), ivAvatar);
        tvGender.setText(localUser.getGender());
        tvBirthday.setText(localUser.getBirthday());
        tvSignature.setText(localUser.getSignature());
    }

    @OnClick({R.id.iv_back, R.id.tv_save, R.id.fl_avatar,
            R.id.ll_gender, R.id.ll_birthday,})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_save:
                showProgress();
                if (withAvatar) {
                    upload();
                } else {
                    edit(UserHelper.getLocalUser().getAvatar());
                }
                break;
            case R.id.fl_avatar:
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                Intent chooserIntent = Intent.createChooser(intent, "选择相册应用");
                startActivityForResult(chooserIntent, PICK_PHOTO);
                break;
            case R.id.ll_gender:
                GenderSelectFragment fragment = GenderSelectFragment.getInstance(tvGender.getText().toString());
                fragment.setListener(gender -> tvGender.setText(gender));
                fragment.show(getSupportFragmentManager(), null);
                break;
            case R.id.ll_birthday:
                pickDate();
                break;
        }
    }


    /**
     * 选择生日
     */
    private void pickDate() {
        final Calendar c = Calendar.getInstance();
        String birthday = getBirth();
        if (TextUtils.isEmpty(birthday)) {
            c.setTimeInMillis(System.currentTimeMillis());
        } else {
            String[] split = birthday.split("-");
            c.set(Calendar.YEAR, Integer.parseInt(split[0]));
            c.set(Calendar.MONTH, Integer.parseInt(split[1]) - 1);
            c.set(Calendar.DAY_OF_MONTH, Integer.parseInt(split[2]));
        }

        mBirthPickerView = new TimePickerBuilder(this, (date, v) -> {
            tvBirthday.setText(DateUtil.format(date.getTime()));

        })
                .setLayoutRes(R.layout.layout_birth_picker, v -> {
                    View cancel = v.findViewById(R.id.tv_cancel);
                    View complete = v.findViewById(R.id.tv_complete);
                    cancel.setOnClickListener(v1 -> mBirthPickerView.dismiss());
                    complete.setOnClickListener(v1 -> {
                        mBirthPickerView.returnData();
                        mBirthPickerView.dismiss();

                    });
                })
                .setType(new boolean[]{true, true, true, false, false, false})
                .setContentTextSize(20)
                .setTextColorCenter(ContextCompat.getColor(this, R.color.dp_wheel_center_text))
                .setTextColorOut(ContextCompat.getColor(this, R.color.dp_wheel_out))
                .setDividerColor(ContextCompat.getColor(this, R.color.dp_wheel_divider))
                .setOutSideCancelable(false)
                .setLabel("年", "月", "日", "时", "分", "秒")
                .setDate(c)
                .isDialog(false)
                .setDecorView(getWindow().getDecorView().findViewById(android.R.id.content))
                .build();
        mBirthPickerView.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) return;
        switch (requestCode) {
            case CAMERA_CAPTRUE:
                cropFromCamera();
                break;
            case PICK_PHOTO:
                setImageUri(data.getData());
                cropFromGallery();
                break;
            case CROP_PHOTO:
                withAvatar = true;
                ImageLoader.loadAvatar(this, getImageUri(), ivAvatar);
                break;
        }
    }


    private void upload() {
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpeg"), getTempFile());
        String filename = System.currentTimeMillis() + ".jpg";
        MultipartBody.Part part = MultipartBody.Part.createFormData("file", filename, requestBody);
        final Disposable disposable = service.uploadAvatar(part)
                .compose(RxUtils.applySchedulers())
                .filter(resp -> !handleResponse(resp))
                .subscribe(response -> edit(response.getData()), this::handleFailure);
        mCompositeDisposable.add(disposable);
    }


    private void edit(String avatarUrl) {
        final HashMap<String, Object> map = new HashMap<>();
        map.put("avatar", avatarUrl);
        map.put("sex", String.valueOf(getGender()));
        map.put("nickname", etNickname.getText().toString());
        map.put("birthday", tvBirthday.getText().toString());
        map.put("signature", tvSignature.getText().toString());


        final Disposable disposable = service.editProfile(map)
                .compose(RxUtils.applySchedulers())
                .filter(resp -> !handleResponse(resp))
                .subscribe(response -> {
                    showToast("修改成功");
                    dismissDialog();
                    setResult(Activity.RESULT_OK);
                    finish();
                }, this::handleFailure);
        mCompositeDisposable.add(disposable);
    }


    private String getBirth() {
        return tvBirthday.getText().toString();
    }

    private int getGender() {
        final String text = tvGender.getText().toString();
        if ("男".equals(text)) {
            return 1;
        } else if ("女".equals(text)) {
            return 2;
        } else {
            return 0;
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            final View view = getCurrentFocus();
            if (isShouldHideInput(view, ev)) {
                hideSoftInput(view.getWindowToken());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    private boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] ints = new int[2];
            v.getLocationInWindow(ints);
            Rect rect = new Rect(ints[0], ints[1], ints[0] + v.getWidth(), ints[1] + v.getHeight());
            final boolean b = !rect.contains(((int) event.getRawX()), ((int) event.getRawY()));
            if (b) {
                v.setFocusable(false);
                v.setFocusableInTouchMode(true);
            }
            return b;
        }

        return false;
    }


    private void hideSoftInput(IBinder token) {
        if (token != null) {
            InputMethodManager im = ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE));
            im.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
