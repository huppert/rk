package com.xbchips.rk.profile.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.video.entity.MyVideo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PurchaseAdapter extends BaseAdapter<MyVideo> {


    public PurchaseAdapter(List<MyVideo> list, Context context) {
        super(list, context);

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return viewType == TYPE_ITEM ? new PurchaseViewHolder(mInflater.inflate(R.layout.item_purchase, viewGroup, false)) :
                getFooterViewHolder(viewGroup);
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        MyVideo myVideo = mList.get(position);
        PurchaseViewHolder viewHolder = (PurchaseViewHolder) holder;
        int status = myVideo.getStatus();
        viewHolder.tvCount.setText(String.valueOf(myVideo.getCount()));
        viewHolder.tvTitle.setText(myVideo.getTitle());
        ImageLoader.load(mContext, status == 0 ? myVideo.getThumb_img_url() : R.drawable.reviewing, viewHolder.ivCover);

        viewHolder.itemView.setTag(position);
        viewHolder.itemView.setOnClickListener(mOnClickListener);
    }

    static class PurchaseViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_cover)
        ImageView ivCover;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_count)
        TextView tvCount;

        public PurchaseViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
