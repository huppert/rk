package com.xbchips.rk.profile.widgets;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.exoplayer2.offline.Download;
import com.xbchips.rk.R;
import com.xbchips.rk.XvideoApplication;
import com.xbchips.rk.profile.activity.CachingActivity;
import com.xbchips.rk.util.ObjectUtil;
import com.xbchips.rk.video.entity.DownloadData;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class CacheHeaderLayout extends FrameLayout {
    @BindView(R.id.cl_caching)
    ConstraintLayout clCaching;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_progress)
    TextView tvProgress;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.tv_count)
    TextView tvCount;
    private boolean intervalFlag = true;
    private Unbinder mUnbinder;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    public CacheHeaderLayout(@NonNull Context context) {
        this(context, null);
    }

    public CacheHeaderLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        final View view = LayoutInflater.from(context).inflate(R.layout.layout_cache_header, this);
        mUnbinder = ButterKnife.bind(this, view);
    }


    /**
     * ic_caching
     */
    public void initCaching() {
        setOnClickListener(v -> getContext().startActivity(new Intent(getContext(), CachingActivity.class)));
        List<Download> downloads = XvideoApplication.getApplication().getDownloadManager().getCurrentDownloads();
        if (!downloads.isEmpty()) {
            setVisibility(VISIBLE);
            tvCount.setText(String.valueOf(downloads.size()));
            Disposable disposable = Observable.interval(0, 1, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                    .filter(t -> intervalFlag)
                    .subscribe(aLong -> updateTotalProgress(),
                            throwable -> {
                            });
            mCompositeDisposable.add(disposable);

        }
    }

    public void updateTotalProgress() {
        List<Download> downloads = XvideoApplication.getApplication().getDownloadManager().getCurrentDownloads();
        if (downloads.isEmpty()) {
            setVisibility(GONE);
            return;
        } else {
            setVisibility(VISIBLE);
            tvCount.setText(String.valueOf(downloads.size()));
        }

        long totalDownloadedBytes = 0;
        long totalBytes = 0;

        for (int i = 0; i < downloads.size(); i++) {
            Download download = downloads.get(i);
            if (i == 0) {
                DownloadData downloadData = ObjectUtil.unmarshall(download.request.data, DownloadData.CREATOR);
                tvTitle.setText(downloadData.getTitle());
            }
            //下载的字节
            final long bytesDownloaded = download.getBytesDownloaded();
            totalDownloadedBytes += bytesDownloaded;
            //下载的百分比
            final float percentDownloaded = download.getPercentDownloaded();
            if (bytesDownloaded != 0 && percentDownloaded != 0) {
                totalBytes += (bytesDownloaded / (percentDownloaded / 100));
            }
        }
        tvProgress.setText(getHumanSize(totalDownloadedBytes) + "/" + getHumanSize(totalBytes));
        float progress = totalDownloadedBytes * 100f / totalBytes;
        progressBar.setProgress((int) progress);

    }


    private String getHumanSize(long bytes) {
        //kb
        long size = bytes / 8 / 1024;

        double m = size / 1024.0;
        double g = size / 1048576.0;
        if (g > 1) {
            return String.format(Locale.CHINA, "%.2fGB", g);
        } else if (m > 1) {
            return String.format(Locale.CHINA, "%.2fMB", m);
        } else {
            return String.format(Locale.CHINA, "%dKB", size);
        }
    }

    public void setIntervalFlag(boolean intervalFlag) {
        this.intervalFlag = intervalFlag;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mCompositeDisposable.clear();
    }
}
