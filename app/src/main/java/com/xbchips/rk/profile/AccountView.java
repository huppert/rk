package com.xbchips.rk.profile;

import com.xbchips.rk.base.BaseView;

/**
 * Created by york on 2017/10/12.
 */

public interface AccountView extends BaseView {
    void getCodeSuccess();

    void onSuccess();
}
