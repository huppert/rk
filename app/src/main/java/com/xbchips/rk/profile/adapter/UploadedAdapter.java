package com.xbchips.rk.profile.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.video.entity.MyVideo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UploadedAdapter extends BaseAdapter<MyVideo> {


    public UploadedAdapter(List<MyVideo> list, Context context) {
        super(list, context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return viewType == TYPE_ITEM ? new UploadedViewHolder(mInflater.inflate(R.layout.item_uploaded, viewGroup, false)) :
                getFooterViewHolder(viewGroup);
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        MyVideo myVideo = mList.get(position);
        UploadedViewHolder viewHolder = (UploadedViewHolder) holder;
        viewHolder.tvTitle.setText(myVideo.getTitle());
        int status = myVideo.getStatus();
        viewHolder.tvCount.setText(String.valueOf(myVideo.getCount()));
        //待审核
        if (status == 0) {
            viewHolder.tvState.setText("等待审核");
        } else if (status == 1) {//审核通过
            viewHolder.tvState.setText("上传成功");
        } else {
            viewHolder.tvState.setText("审核未通过");
        }

        ImageLoader.load(mContext, status == 1 ? myVideo.getThumb_img_url() : R.drawable.reviewing, viewHolder.ivCover);
        viewHolder.itemView.setTag(position);
        viewHolder.itemView.setOnClickListener(mOnClickListener);
    }

    static class UploadedViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_cover)
        ImageView ivCover;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_count)
        TextView tvCount;
        @BindView(R.id.tv_state)
        TextView tvState;

        public UploadedViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
