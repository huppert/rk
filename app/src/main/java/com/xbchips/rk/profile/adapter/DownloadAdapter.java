package com.xbchips.rk.profile.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.google.android.exoplayer2.util.Util;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.video.entity.DownloadData;

import java.util.Formatter;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DownloadAdapter extends BaseAdapter<DownloadData> {
    private final StringBuilder formatBuilder;
    private final Formatter formatter;

    public DownloadAdapter(List<DownloadData> list, Context context) {
        super(list, context);
        formatBuilder = new StringBuilder();
        formatter = new Formatter(formatBuilder, Locale.getDefault());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_FOOTER:
                return getFooterViewHolder(parent);
            case TYPE_HEADER:
                return new HeaderViewHolder(mHeaderView);
            case TYPE_ITEM:
                return new DownloadViewHolder(mInflater.inflate(R.layout.item_download, parent, false));
        }
        return null;

    }


    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        DownloadViewHolder holder = (DownloadViewHolder) viewHolder;
        DownloadData videoItem = mList.get(getRealPosition(position));
        holder.tvTitle.setText(videoItem.getTitle());
        ImageLoader.load(mContext, videoItem.getCoverUrl(), holder.ivCover);
        holder.tvDuration.setText(Util.getStringForTime(formatBuilder, formatter, videoItem.getVideoDuration()));
        if (videoItem.isOpened()) {
            holder.swipeLayout.open();
        } else {
            holder.swipeLayout.close();
        }
        holder.tvCancel.setOnClickListener(v -> {
            holder.swipeLayout.close();
            videoItem.setOpened(false);
        });
        holder.swipeLayout.removeAllSwipeListener();
        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
                videoItem.setOpened(true);
            }

            @Override
            public void onClose(SwipeLayout layout) {
                videoItem.setOpened(false);
            }
        });
        holder.tvDelete.setTag(videoItem);
        holder.tvDelete.setOnClickListener(mOnClickListener);
        holder.clContent.setTag(videoItem);
        holder.clContent.setOnClickListener(mOnClickListener);

    }

    static class DownloadViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_delete)
        TextView tvDelete;
        @BindView(R.id.tv_cancel)
        TextView tvCancel;
        @BindView(R.id.iv_cover)
        ImageView ivCover;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_duration)
        TextView tvDuration;
        @BindView(R.id.swipe_layout)
        SwipeLayout swipeLayout;
        @BindView(R.id.cl_content)
        ConstraintLayout clContent;

        public DownloadViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
