package com.xbchips.rk.profile.activity;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.profile.adapter.BillAdapter;
import com.xbchips.rk.profile.fragments.DatePickerDialogFragment;
import com.xbchips.rk.video.VideoService;
import com.xbchips.rk.video.entity.Bill;
import com.xbchips.rk.video.entity.BillResp;
import com.xbchips.rk.widgets.EndlessRecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.Disposable;

public class BillActivity extends BaseActivity {
    @BindView(R.id.recycler_view)
    EndlessRecyclerView mRecyclerView;
    private final List<Bill> mList = new ArrayList<>();
    @BindView(R.id.tv_total_amount)
    TextView tvTotalAmount;
    @BindView(R.id.tv_date_range)
    TextView tvDateRange;
    private VideoService mService = HttpRequest.createService(VideoService.class);
    private BillAdapter mAdapter;
    private String[] mDates = new String[2];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill);
        ButterKnife.bind(this);
        initTopBar();
        initRecyclerView();
        setCurMonthDate();
        loadData();
    }

    @Override
    protected void initTopBar() {
        super.initTopBar();
        mTvTitle.setText(R.string.bills);
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new BillAdapter(mList, this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setOnLoadMoreListener(() -> {
            mAdapter.addFooter();
            loadData();
        });
    }

    private void loadData() {
        Disposable disposable = mService.getBills(mRecyclerView.getCurPage(), 20, mDates[0], mDates[1])
                .compose(RxUtils.applySchedulers())
                .filter(response -> !handleResponse(response))
                .subscribe(this::setData, this::processError);
        mCompositeDisposable.add(disposable);
    }


    private void setData(ObjectResponse<BillResp> response) {
        if (mRecyclerView.getCurPage() == 1) {
            mList.clear();
        } else {
            mAdapter.removeFooter();
        }
        if (!handleResponse(response)) {
            BillResp data = response.getData();
            List<Bill> list = data.getList();
            mRecyclerView.setPageCount(data.getPage().getTotal());
            mList.addAll(list);
            if (mRecyclerView.getCurPage() == 1) {
                mAdapter.notifyDataSetChanged();
                tvTotalAmount.setText(getString(R.string.total_amount, data.getTotalConsume(), data.getTotalIncome()));
            } else {
                int size = list.size();
                mAdapter.notifyItemRangeInserted(mList.size() - size, size);
            }
        }
        mRecyclerView.setLoading(false);
    }

    private void processError(@NonNull Throwable throwable) {
        handleFailure(throwable);
        mRecyclerView.setLoading(false);
        if (mRecyclerView.getCurPage() > 1) {
            mRecyclerView.pageDecrement();
            mAdapter.removeFooter();
        }
    }


    private void setCurMonthDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        String startDate = String.format(Locale.CHINA, "%d-%02d-%02d", year, month + 1, 1);
        String endDate = String.format(Locale.CHINA, "%d-%02d-%02d", year, month + 1, day);
        mDates[0] = startDate;
        mDates[1] = endDate;
    }

    @OnClick(R.id.tv_date_range)
    public void onViewClicked() {
        DatePickerDialogFragment dialogFragment = DatePickerDialogFragment.getInstance(mDates);
        dialogFragment.setOnDateSelectedListener((startDate, endDate) -> {
            tvDateRange.setText(startDate + "\n" + endDate);
            mDates[0] = startDate;
            mDates[1] = endDate;
            mRecyclerView.setCurPage(1);
            loadData();
        });
        dialogFragment.show(getSupportFragmentManager(), "dialogFragment");
    }
}
