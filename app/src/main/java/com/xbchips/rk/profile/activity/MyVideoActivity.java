package com.xbchips.rk.profile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.lx.activities.UploadActivity;
import com.xbchips.rk.profile.fragments.MyVideoFragment;
import com.xbchips.rk.profile.listener.OnLoadCountListener;
import com.xbchips.rk.vip.adapter.ViewPagerAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyVideoActivity extends BaseActivity {

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    private String[] mTitles = new String[]{"上传", "购买"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_video);
        ButterKnife.bind(this);
        initTopBar();
        initTabLayout();
    }

    private void initTabLayout() {
        List<Fragment> fragments = new ArrayList<>();
        for (int i = 0; i < mTitles.length; i++) {
            MyVideoFragment fragment = MyVideoFragment.getInstance(i);
            fragment.setListener(mListener);
            fragments.add(fragment);
        }
        viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), fragments, Arrays.asList(mTitles)));
        tabLayout.setupWithViewPager(viewPager);
    }

    private OnLoadCountListener mListener = new OnLoadCountListener() {
        @Override
        public void onLoadCountSuccess(int index, int count) {
            tabLayout.getTabAt(index).setText(mTitles[index] + count);
        }
    };


    @OnClick({R.id.tv_bills, R.id.fab_add})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_bills:
                startActivity(new Intent(this, BillActivity.class));
                break;
            case R.id.fab_add:
                startActivity(new Intent(this, UploadActivity.class));
                break;
        }

    }
}
