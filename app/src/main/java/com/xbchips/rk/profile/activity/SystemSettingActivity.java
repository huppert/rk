package com.xbchips.rk.profile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.xbchips.rk.BuildConfig;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.base.RespHandler;
import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.profile.AccountService;
import com.xbchips.rk.system.SystemService;
import com.xbchips.rk.system.entity.VersionInfo;
import com.xbchips.rk.user.activities.UserAuthActivity;
import com.xbchips.rk.user.entity.User;
import com.xbchips.rk.widgets.NewVersionDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SystemSettingActivity extends BaseActivity {

    @BindView(R.id.tv_version)
    TextView tvVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_system_setting);
        ButterKnife.bind(this);
        initTopBar();
        tvVersion.setText(getString(R.string.current_version_format, BuildConfig.VERSION_NAME));
    }


    @Override
    protected void initTopBar() {
        super.initTopBar();
        mTvTitle.setText(R.string.systemsettings);
    }

    @OnClick({R.id.tv_bind_phone, R.id.tv_modify_pwd, R.id.tv_logout, R.id.fl_update_version})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_bind_phone:
                toBindActivity();
                break;
            case R.id.tv_modify_pwd:
                startActivity(new Intent(this, ModifyPasswordActivity.class));
                break;
            case R.id.tv_logout:
                if (TextUtils.isEmpty(getPhoneNumber())) {
                    new AlertDialog.Builder(this)
                            .setTitle("提示")
                            .setMessage("您还没有完成手机绑定，现在登出账号可能导致之后无法正常登录，是否继续？")
                            .setPositiveButton(R.string.bind_phone, (dialog, which) -> toBindActivity())
                            .setNegativeButton(R.string.keep_exit, (dialog, which) -> {
                                logout();
                            })
                            .show();
                } else {
                    logout();
                }
                break;
            case R.id.fl_update_version:
                checkVersion();
                break;
        }
    }


    /**
     * APP升级
     */
    private void checkVersion() {
        Disposable disposable = HttpRequest.createService(SystemService.class).checkVersion()
                .compose(RxUtils.applySchedulers())
                .subscribe(response -> {
                    VersionInfo mVersion = response.getData();
                    if (response.getStatus() == RespHandler.SUCCESS_ONE && mVersion.isUpdate()) {
                        showToast("请求失败");
                    } else if (response.getStatus() == RespHandler.SUCCESS_CODE && mVersion.isUpdate()) {
                        //非强制升级
                        showUpdateDialog(mVersion);

                    } else {
                        showToast("已是最新版本");
                    }
                }, throwable -> handleFailure(throwable));
        mCompositeDisposable.add(disposable);

    }


    private void showUpdateDialog(VersionInfo mVersion) {
        NewVersionDialogFragment fragment = new NewVersionDialogFragment();
        fragment.setVersionInfo(mVersion);
        fragment.show(getSupportFragmentManager(), "");
    }

    private void toBindActivity() {
        Intent intent = new Intent(this, TextUtils.isEmpty(getPhoneNumber()) ?
                BindPhoneActivity.class : BindPhoneAlreadyActivity.class);
        startActivity(intent);
    }

    private String getPhoneNumber() {
        User localUser = UserHelper.getLocalUser();
        if (localUser == null) return null;
        return localUser.getPhone();
    }

    private void logout() {
        showProgress("正在进行登出账号操作，请稍后");
        AccountService service = HttpRequest.createService(AccountService.class);
        Disposable disposable = service.logout()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::logoutSuccess, this::handleFailure);
        mCompositeDisposable.add(disposable);

    }

    private void logoutSuccess(@NonNull ObjectResponse<String> response) {
        if (!handleResponse(response)) {
            dismissDialog();
            UserHelper.clearToken();
            LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(Constants.ACTION_REFRESH_STATE));
            finish();
            Intent intent = new Intent(this, UserAuthActivity.class);
            intent.putExtra(UserAuthActivity.ARG_IS_LOGIN, true);
            startActivity(intent);
        }
    }

}
