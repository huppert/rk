package com.xbchips.rk.profile.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.view.TimePickerView;
import com.xbchips.rk.R;
import com.xbchips.rk.util.DateUtil;
import com.xbchips.rk.util.LogTrace;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class DatePickerDialogFragment extends DialogFragment {
    private static final String TAG = "DatePickerDialogFragmen";
    private static final String ARG_DATE = "arg_date";
    @BindView(R.id.tv_start_date)
    TextView tvStartDate;
    @BindView(R.id.fl_start)
    FrameLayout flStart;
    @BindView(R.id.tv_end_date)
    TextView tvEndDate;
    @BindView(R.id.fl_end)
    FrameLayout flEnd;
    @BindView(R.id.fl_picker)
    FrameLayout flPicker;
    Unbinder unbinder;
    private View mView;
    private OnDateSelectedListener mOnDateSelectedListener;
    private TimePickerView mPickerView;


    public static DatePickerDialogFragment getInstance(String[] array) {
        DatePickerDialogFragment dialogFragment = new DatePickerDialogFragment();
        Bundle args = new Bundle();
        args.putStringArray(ARG_DATE, array);
        dialogFragment.setArguments(args);
        return dialogFragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        // 设置宽度为屏宽, 靠近屏幕底部。
        Dialog dialog = getDialog();
        Window win = dialog.getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

        WindowManager.LayoutParams params = win.getAttributes();
        params.dimAmount = 0.0f;
        params.gravity = Gravity.BOTTOM;
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        win.setAttributes(params);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.layout_date_picker, container, false);
        unbinder = ButterKnife.bind(this, mView);
        String[] dates = getArguments().getStringArray(ARG_DATE);

        tvStartDate.setText(dates[0]);
        tvEndDate.setText(dates[1]);
        flStart.setSelected(true);

        setPicker(dates);
        return mView;
    }


    private void setPicker(String[] dates) {
        String[] selectedDateStr = dates[0].split("-");
        Calendar selectedDate = Calendar.getInstance();
        selectedDate.set((Integer.parseInt(selectedDateStr[0])), Integer.parseInt(selectedDateStr[1]) - 1, Integer.parseInt(selectedDateStr[2]));


        Calendar startDate = Calendar.getInstance();
        startDate.set(2000, 0, 1);

        Calendar endDate = Calendar.getInstance();
        endDate.set(endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH), endDate.get(Calendar.DAY_OF_MONTH));

        mPickerView = new TimePickerBuilder(getContext(), null)
                .setLayoutRes(R.layout.custom_time_picker, v -> {
                })
                .setTimeSelectChangeListener(date -> {
                    LogTrace.d(TAG, date.getTime() + "");
                    if (flStart.isSelected()) {
                        tvStartDate.setText(getTime(date));
                    } else {
                        tvEndDate.setText(getTime(date));
                    }
                })
                .setType(new boolean[]{true, true, true, false, false, false})
                .setLabel("年", "月", "日", "", "", "")
                .setDividerColor(Color.DKGRAY)
                .setContentTextSize(15)
                .setDate(selectedDate)
                .setTextColorCenter(ContextCompat.getColor(getContext(), R.color.bdp_wheel_center))
                .setRangDate(startDate, endDate)
                .setDecorView(flPicker)
                .setBgColor(ContextCompat.getColor(getContext(), R.color.bdp_wheel_bg))
                .setOutSideCancelable(false)
                .build();


        mPickerView.show(flPicker, false);


    }

    private String getTime(Date date) {//可根据需要自行截取数据显示
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
        return format.format(date);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.fl_start, R.id.fl_end, R.id.tv_cancel, R.id.tv_complete})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.fl_start:
                flStart.setSelected(true);
                flEnd.setSelected(false);
                Date date = DateUtil.parseDate(getStartDate());
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                mPickerView.setDate(calendar);
                break;
            case R.id.fl_end:
                flEnd.setSelected(true);
                flStart.setSelected(false);
                Calendar end = Calendar.getInstance();
                end.setTime(DateUtil.parseDate(getEndDate()));
                mPickerView.setDate(end);
                break;
            case R.id.tv_cancel:
                dismiss();
                break;
            case R.id.tv_complete:
                dismiss();
                mOnDateSelectedListener.onDateSelected(getStartDate(), getEndDate());
                break;

        }
    }


    private String getStartDate() {
        return tvStartDate.getText().toString();
    }

    private String getEndDate() {
        return tvEndDate.getText().toString();
    }

    public interface OnDateSelectedListener {

        void onDateSelected(String startDate, String endDate);

    }

    public void setOnDateSelectedListener(OnDateSelectedListener onDateSelectedListener) {
        mOnDateSelectedListener = onDateSelectedListener;
    }
}
