package com.xbchips.rk.user;

/**
 * Created by york on 2017/8/23.
 */

public interface UserView {

    void checkSuccess();

    void failed(Throwable throwable);

}
