package com.xbchips.rk.user.service;

import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.base.response.PageResponse;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.profile.entity.Spread;
import com.xbchips.rk.user.entity.User;

import java.util.HashMap;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by york on 2017/8/3.
 */

public interface UserService {
    /**
     * 首次启动APP时，调用此接口，检测用户是否已注册
     *
     * @param identifier 唯一标识
     */
    @GET("user/check/{identifier}")
    Observable<ObjectResponse<User>> check(@Path("identifier") String identifier);


    /**
     * 登录
     *
     * @param map{ username sting //用户名称
     *             password sting //用户密码
     *             channel string //客户端渠道号}
     */
    @POST("user/login/{identifier}")
    Observable<ObjectResponse<User>> login(@Path("identifier") String identifier, @Body HashMap<String, String> map);

    /**
     * 注册
     *
     * @param map{ username sting //用户名称
     *             password sting //用户密码
     *             nickname sting  //用户昵称
     *             channel string //客户端渠道号
     *             }
     */
    @POST("user/register/{identifier}")
    Observable<ObjectResponse<User>> register(@Path("identifier") String identifier, @Body HashMap<String, String> map);

    /**
     * 获取用户收藏
     */
    @GET("user/list/collect")
    Observable<ObjectResponse<PageResponse<VideoItem>>> getMarks(@Query("page") int page, @Query("page_size") int pageSize);


    /**
     * 推广数据
     */
    @GET("user/promote")
    Observable<ObjectResponse<Spread>> getPromote();


    /**
     * 积分兑换
     */
    @GET("user/exchange/{point}")
    Observable<ObjectResponse<HashMap<String, Long>>> exchange(@Path("point") int point);


    /**
     * 上传头像
     */
    @Multipart
    @POST("web/uploadImage")
    Observable<ObjectResponse<String>> uploadAvatar(@Part MultipartBody.Part file);

    /**
     * 修改用户信息
     */
    @POST("user/edit")
    Observable<ObjectResponse<String>> editProfile(@Body HashMap<String, Object> map);


}
