package com.xbchips.rk.user.activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.xbchips.rk.BuildConfig;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.user.UserPresenter;
import com.xbchips.rk.user.UserView;
import com.xbchips.rk.user.service.VipTypeService;
import com.xbchips.rk.util.LogTrace;

import java.util.HashMap;
import java.util.List;

import io.reactivex.disposables.Disposable;
import okhttp3.HttpUrl;

import static android.content.Intent.URI_INTENT_SCHEME;

/**
 * Created by User on 2017/6/12.
 */

public class WapPayActivity extends BaseActivity implements UserView {
    private static final String TAG = "WapPayActivity";
    /**
     * 支付宝支付成功状态码
     */
    private static final String PAY_SUCCESS_CODE = "9000";
    private static final int PAY_REQUEST_CODE = 201;
    private UserPresenter userPresenter = new UserPresenter(this);
    private ValueCallback<Uri> mUploadMessage;
    private ValueCallback<Uri[]> mUploadCallbackAboveL;
    private final static int FILECHOOSER_RESULTCODE = 202;
    private WebView mWebView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wap_pay);
        init();
    }

    protected void init() {
        initTopBar();
        mTvTitle.setText(R.string.recharge_center);
        mWebView = findViewById(R.id.wv);
        VipTypeService service = HttpRequest.createService(VipTypeService.class);
        Disposable disposable = service.pay()
                .compose(RxUtils.applySchedulers())
                .filter(response -> !handleResponse(response))
                .subscribe(response -> webViewLoadUrl(response.getData()), this::handleFailure);
        mCompositeDisposable.add(disposable);

    }


    private void webViewLoadUrl(String originUrl) {
        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        mWebView.addJavascriptInterface(new JavaInterface(this), "javaObject");
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setDomStorageEnabled(true);
        String appCachePath = getApplicationContext().getCacheDir().getAbsolutePath();
        settings.setAppCachePath(appCachePath);
        settings.setAllowFileAccess(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        mWebView.setVerticalScrollBarEnabled(true);
        mWebView.setHorizontalScrollBarEnabled(true);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("alipays://") || url.startsWith("intent://")
                        || url.startsWith("weixin://") || url.startsWith("mqqapi://")) {
                    handleUrl(url);
                    return true;
                }
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
        });

        mWebView.setWebChromeClient(new WebChromeClient() {
            // For Android 4.1
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                LogTrace.d(TAG, "openFileChoose(ValueCallback<Uri> uploadMsg, String acceptType, String capture)");
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("*/*");
                startActivityForResult(Intent.createChooser(i, "File Browser"),
                        FILECHOOSER_RESULTCODE);
            }

            // For Android 5.0+
            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback,
                                             FileChooserParams fileChooserParams) {
                LogTrace.d(TAG, "onShowFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture)");
                mUploadCallbackAboveL = filePathCallback;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("*/*");
                startActivityForResult(Intent.createChooser(i, "File Browser"),
                        FILECHOOSER_RESULTCODE);
                return true;
            }
        });

        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("api_token", UserHelper.getApiToken());
        String dstUrl = HttpUrl.parse(originUrl).newBuilder()
                .addQueryParameter("vc", BuildConfig.VERSION_NAME)
                .addQueryParameter("ch", Constants.DEFAULT_CHANNEL)
                .addQueryParameter("os", Constants.DEFAULT_PLATFORM)
                .addQueryParameter("app_code", BuildConfig.APPCODE)
                .build()
                .toString();
        Log.d(TAG, "zuizhong   url: " + dstUrl);
        mWebView.loadUrl(dstUrl, requestHeaders);
    }


    private void handleUrl(String url) {
        try {
            Intent intent = Intent.parseUri(url, URI_INTENT_SCHEME);
            intent.setAction(Intent.ACTION_VIEW);
            List<ResolveInfo> resolves = getPackageManager().queryIntentActivities(intent, 0);
            int size = resolves.size();
            if (size > 0) {
                startActivityIfNeeded(intent, PAY_REQUEST_CODE);
                //微信没有回调界面
            } else {
                if (url.startsWith("weixin://")) {
                    showToast(R.string.please_install_wechat);
                    finish();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            showToast(getString(R.string.pay_error));
        }
    }

//    public void invokeNativeAlipay() {
//        String orderInfo = "alipay_sdk=lokielse%2Fomnipay-alipay&app_id=2018012302037119&biz_content=%7B%22subject%22%3A%22%5Cu6d4b%5Cu8bd5%22%2C%22out_trade_no%22%3A%222018020305452651724031%22%2C%22total_amount%22%3A%2224.98%22%2C%22product_code%22%3A%22QUICK_MSECURITY_PAY%22%7D&charset=UTF-8&format=JSON&method=alipay.trade.app.pay&notify_url=http%3A%2F%2Fwww.dajin0571.com%2Fapi%2Falipay%2Fnotify&sign_type=RSA&timestamp=2018-02-03+05%3A45%3A26&version=1.0&sign=hDlW8Ck4yUe634MpQUbG0f24nj8qWyzLhpMJWQyvrI3GczXzmERdC4o7j5S%2Bzwih9T5lo%2BdLl3HRRbtjZpydZNnwcf2yPYudlaT4bXr49Xi5FI3XDqKEckYOx5r1Oq5C3z0Nafn80qamPWaJBGwsQ1iX0q3GdA34iHWPVvhevUdUDYasXNWyb2lMSvFA9h%2Bt%2FZTYzZ8CSlbQNbqeFoB5auPZzrhmjTttoxPRx3cMqXRY%2FWj4LzZHhLuTMiZri2njFFa3UNtVtr48A6mNoIA4mclA3FaBrcpaw%2F5fj%2FMxtX1%2BtVUQHPvwRhjWbwZwCWTz0i6xb0hPWEIrkGbcK%2F58Cg%3D%3D";
//        Observable.create((ObservableOnSubscribe<Map<String, String>>) e -> {
//            PayTask alipay = new PayTask(this);
//            Map<String, String> map = alipay.payV2(orderInfo, true);
//            e.onNext(map);
//        })
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(map -> {
//                    String resultStatus = map.get("resultStatus");
//                    LogTrace.d(TAG, "invokeNativeAlipay: " + resultStatus);
//                    LogTrace.d(TAG, "invokeNativeAlipay: " + map.get("result"));
//                    LogTrace.d(TAG, "invokeNativeAlipay: " + map.get("memo"));
//                    if (TextUtils.equals(resultStatus, PAY_SUCCESS_CODE)) {
//                        // 该笔订单是否真实支付成功，需要依赖服务端的异步通知。
//                        showToast("支付成功");
//                    } else {
//                        // 该笔订单真实的支付结果，需要依赖服务端的异步通知。
//                        showToast("支付失败");
//                    }
//                }, throwable -> showToast("支付失败"));
//    }


    static class JavaInterface {
        private BaseActivity mContext;

        public JavaInterface(BaseActivity context) {
            mContext = context;
        }

        /**
         * 调起本地支付宝
         */
        @JavascriptInterface
        public void invokeNativeAlipay(String orderInfo) {

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogTrace.d(TAG, "onActivityResult: " + resultCode);
        if (requestCode == PAY_REQUEST_CODE) {
            checkUser();
        }
        if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage && null == mUploadCallbackAboveL)
                return;
            Uri result = data == null || resultCode != RESULT_OK ? null : data.getData();
            if (mUploadCallbackAboveL != null) {
                onActivityResultAboveL(requestCode, resultCode, data);
            } else if (mUploadMessage != null) {
                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void onActivityResultAboveL(int requestCode, int resultCode, Intent data) {
        if (requestCode != FILECHOOSER_RESULTCODE || mUploadCallbackAboveL == null) {
            return;
        }

        Uri[] results = null;
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                String dataString = data.getDataString();
                ClipData clipData = data.getClipData();
                if (clipData != null) {
                    results = new Uri[clipData.getItemCount()];
                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        ClipData.Item item = clipData.getItemAt(i);
                        results[i] = item.getUri();
                    }
                }
                if (dataString != null)
                    results = new Uri[]{Uri.parse(dataString)};
            }
        }
        mUploadCallbackAboveL.onReceiveValue(results);
        mUploadCallbackAboveL = null;
    }


    private void checkUser() {
        showProgress(getString(R.string.getting_pay_result));
        userPresenter.checkUser();
    }

    @Override
    public void checkSuccess() {
        dismissDialog();
        Intent intent = new Intent(Constants.ACTION_REFRESH_STATE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        finish();
    }

    @Override
    public void failed(Throwable throwable) {
        handleFailure(throwable);
        finish();
    }

}
