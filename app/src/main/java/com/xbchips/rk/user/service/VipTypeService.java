package com.xbchips.rk.user.service;

import com.xbchips.rk.base.response.ObjectResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by york on 2017/8/5.
 */

public interface VipTypeService {

    @GET("pay")
    Observable<ObjectResponse<String>> pay();
}
