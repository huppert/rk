package com.xbchips.rk.user;

import com.xbchips.rk.base.BasePresenter;
import com.xbchips.rk.base.RespHandler;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.user.service.UserService;
import com.xbchips.rk.util.UuidUtil;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by york on 2017/8/23.
 */

public class UserPresenter implements BasePresenter {
    private static final String TAG = "UserPresenter";
    private UserService service;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private UserView mView;

    public UserPresenter(UserView View) {
        mView = View;
        service = HttpRequest.createService(UserService.class);
    }


    public void checkUser() {
        Disposable disposable = service.check(UuidUtil.getUUid())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                            if (response.getStatus() == RespHandler.SUCCESS_CODE
                                    || response.getStatus() == RespHandler.NOT_FOUND) {
                                UserHelper.setLocalUser(response.getData());
                                mView.checkSuccess();
                            }
                        }, throwable -> mView.failed(throwable)
                );
        mCompositeDisposable.add(disposable);
    }


    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }

}
