package com.xbchips.rk.user.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseFragment;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.system.entity.Configs;
import com.xbchips.rk.system.entity.DynamicConfig;
import com.xbchips.rk.user.UserAuthCallBack;
import com.xbchips.rk.user.entity.User;
import com.xbchips.rk.user.service.UserService;
import com.xbchips.rk.util.PreferenceUtils;
import com.xbchips.rk.util.RegexUtil;
import com.xbchips.rk.util.SoftInputUtils;
import com.xbchips.rk.util.UuidUtil;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RegisterFragment extends BaseFragment {
    @BindView(R.id.et_username)
    EditText mEtUsername;
    @BindView(R.id.et_password)
    EditText mEtPassword;
    @BindView(R.id.tv_login)
    TextView mTvLogin;
    @BindView(R.id.iv_bg)
    ImageView mIvBg;
    private UserAuthCallBack mCallBack;

    public void setCallBack(UserAuthCallBack callBack) {
        mCallBack = callBack;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_register, container, false);
        mUnBinder = ButterKnife.bind(this, rootView);
        init();
        return rootView;
    }

    public void init() {
        setBackGround();
        mEtUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mEtUsername.setCompoundDrawablesWithIntrinsicBounds(
                        TextUtils.isEmpty(s) ? R.drawable.ic_username_unfoucsed :
                                R.drawable.ic_username_foucsed, 0, 0, 0);
            }
        });
        mEtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mEtPassword.setCompoundDrawablesWithIntrinsicBounds(
                        TextUtils.isEmpty(s) ? R.drawable.ic_lock_unfoucsed :
                                R.drawable.ic_lock_foucsed, 0, 0, 0);
            }
        });
    }

    private void setBackGround() {
        String json = PreferenceUtils.getString(getContext(), Constants.PREF_CONFIG);
        if (TextUtils.isEmpty(json)) {
            return;
        }
        Configs configs = new Gson().fromJson(json, Configs.class);
        DynamicConfig dynamicConfig = configs.getLogin_background_img();
        if (dynamicConfig.getStatus() == 8) {
            ImageLoader.load(getContext(), dynamicConfig.getContent().get(0),
                    mIvBg, R.drawable.drawable_login);
        }
    }


    private void register() {
        UserService service = HttpRequest.createService(UserService.class);
        HashMap<String, String> map = new HashMap<>();
        map.put("username", getUserName());
        map.put("password", getPassWord());
        map.put("channel", Constants.DEFAULT_CHANNEL);
        Disposable disposable = service.register(UuidUtil.getUUid(), map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(response -> !handleResponse(response))
                .subscribe(response -> {
                    dismissDialog();
                    User data = response.getData();
                    data.setIs_register(true);
                    UserHelper.setLocalUser(data);
                    Intent intent = new Intent(Constants.ACTION_REFRESH_STATE);
                    LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
                    getActivity().finish();
                }, this::handleFailure);
        mCompositeDisposable.add(disposable);
    }

    @NonNull
    private String getPassWord() {
        return mEtPassword.getText().toString();
    }

    @NonNull
    private String getUserName() {
        return mEtUsername.getText().toString();
    }

    private boolean validate() {
        if (!RegexUtil.match(RegexUtil.USERNAME_REGEXP, getUserName())) {
            showToast("用户名为4-12位的字母/数字");
            return false;
        }
        if (!RegexUtil.match(RegexUtil.PWD_REGEXP, getPassWord())) {
            showToast("密码为6-12位的英文字母/数字");
            return false;
        }
        return true;
    }

    @OnClick({R.id.btn_register, R.id.tv_login})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_register:
                if (validate()) {
                    SoftInputUtils.hideSoft(rootView);
                    showProgress(null);
                    register();
                }
                break;
            case R.id.tv_login:
                if (getArguments() != null && getArguments().getBoolean(Constants.ARG_IS_MAIN, false)) {
                    mCallBack.action(this);
                } else {
                    getActivity().onBackPressed();
                }
                break;
        }
    }

}
