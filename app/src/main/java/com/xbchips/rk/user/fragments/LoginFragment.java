package com.xbchips.rk.user.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseFragment;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.common.ConfigHelper;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.profile.activity.ModifyPasswordActivity;
import com.xbchips.rk.system.entity.Configs;
import com.xbchips.rk.user.UserAuthCallBack;
import com.xbchips.rk.user.entity.User;
import com.xbchips.rk.user.service.UserService;
import com.xbchips.rk.util.SoftInputUtils;
import com.xbchips.rk.util.UuidUtil;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LoginFragment extends BaseFragment {
    @BindView(R.id.et_username)
    EditText mEtUsername;
    @BindView(R.id.et_password)
    EditText mEtPassword;
    @BindView(R.id.tv_register)
    TextView mTvRegister;
    @BindView(R.id.iv_bg)
    ImageView mIvBg;
    private UserAuthCallBack mCallBack;

    public void setCallBack(UserAuthCallBack callBack) {
        mCallBack = callBack;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_login, container, false);
        mUnBinder = ButterKnife.bind(this, rootView);
        init();
        return rootView;
    }

    public void init() {
        setBackGround();

        mEtUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mEtUsername.setCompoundDrawablesWithIntrinsicBounds(
                        TextUtils.isEmpty(s) ? R.drawable.ic_username_unfoucsed :
                                R.drawable.ic_username_foucsed, 0, 0, 0);
            }
        });
        mEtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mEtPassword.setCompoundDrawablesWithIntrinsicBounds(
                        TextUtils.isEmpty(s) ? R.drawable.ic_lock_unfoucsed :
                                R.drawable.ic_lock_foucsed, 0, 0, 0);
            }
        });
        setLastUserName();

    }

    private void setLastUserName() {
        User localUser = UserHelper.getLocalUser();
        if (localUser != null && localUser.getUsername() != null) {
            mEtUsername.setText(localUser.getUsername());
        }
    }

    private void setBackGround() {
        Disposable disposable = ConfigHelper.getValue(Configs.CONF_BG_IAMGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> ImageLoader.load(getContext(), s, mIvBg, R.drawable.drawable_login));
        mCompositeDisposable.add(disposable);
    }

    private boolean validate() {
        if (TextUtils.isEmpty(getUserName())) {
            showToast(R.string.username_not_be_null);
            return false;
        }
        if (TextUtils.isEmpty(getPassWord())) {
            showToast(R.string.password_not_be_null);
            return false;
        }
        return true;
    }

    @NonNull
    private String getPassWord() {
        return mEtPassword.getText().toString();
    }

    @NonNull
    private String getUserName() {
        return mEtUsername.getText().toString();
    }

    @OnClick({R.id.btn_login, R.id.tv_register, R.id.forget_password})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                if (validate()) {
                    SoftInputUtils.hideSoft(rootView);
                    showProgress(getString(R.string.logining));
                    login();
                }
                break;
            case R.id.tv_register:
                if (getArguments() != null && getArguments().getBoolean(Constants.ARG_IS_MAIN, false)) {
                    mCallBack.action(this);
                } else {
                    getActivity().onBackPressed();
                }
                break;
            case R.id.forget_password:
                Intent intent = new Intent(getActivity(), ModifyPasswordActivity.class);
                intent.putExtra(ModifyPasswordActivity.ARG_FLAG, true);
                startActivity(intent);
                break;
        }
    }

    private void login() {
        UserService service = HttpRequest.createService(UserService.class);
        HashMap<String, String> map = new HashMap<>();
        map.put("username", getUserName());
        map.put("password", getPassWord());
        map.put("channel", Constants.DEFAULT_CHANNEL);
        Disposable disposable = service.login(UuidUtil.getUUid(), map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(response -> !handleResponse(response))
                .subscribe(this::loginSuccess, this::handleFailure);
        mCompositeDisposable.add(disposable);
    }

    private void loginSuccess(@io.reactivex.annotations.NonNull ObjectResponse<User> response) {
        dismissDialog();
        User data = response.getData();
        UserHelper.setLocalUser(data);
        Intent intent = new Intent(Constants.ACTION_REFRESH_STATE);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
        getActivity().finish();
    }

}
