package com.xbchips.rk.user.activities;

import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.xbchips.rk.BuildConfig;
import com.xbchips.rk.R;
import com.xbchips.rk.activities.MainActivity;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.user.UserAuthCallBack;
import com.xbchips.rk.user.fragments.LoginFragment;
import com.xbchips.rk.user.fragments.RegisterFragment;

public class UserAuthActivity extends BaseActivity implements UserAuthCallBack {

    public static final String ARG_IS_LOGIN = "arg_is_login";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_auth);
        if (BuildConfig.SKIN_MODE){
            getWindow().setStatusBarColor(ContextCompat.getColor( this, R.color.login_default));
        }
        init();
    }

    private void init() {
        if (getIntent().getBooleanExtra(ARG_IS_LOGIN, false)) {
            LoginFragment loginFragment = new LoginFragment();
            loginFragment.setCallBack(this);
            initFragment(loginFragment);
        } else {
            RegisterFragment registerFragment = new RegisterFragment();
            registerFragment.setCallBack(this);
            initFragment(registerFragment);
        }

    }

    private void initFragment(Fragment fragment) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.ARG_IS_MAIN, true);
        fragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fl_content, fragment).commit();
    }

    private void showFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fl_content, fragment)
                .show(fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void action(Fragment fragment) {
        if (fragment instanceof RegisterFragment) {
            LoginFragment loginFragment = new LoginFragment();
            loginFragment.setCallBack(this);
            showFragment(loginFragment);
        } else {
            RegisterFragment registerFragment = new RegisterFragment();
            registerFragment.setCallBack(this);
            showFragment(registerFragment);
        }
    }
}
