package com.xbchips.rk.user;

import androidx.fragment.app.Fragment;

/**
 * Created by york on 2017/8/5.
 */

public interface UserAuthCallBack {

    void action(Fragment fragment);
}
