package com.xbchips.rk.user.entity;

/**
 * Created by york on 2017/8/3.
 */

public class User {
    private String id;
    private String username;
    private String identifier;
    private String api_token;
    private int vip_type;
    private String created_at;
    private String updated_at;
    private String channel;
    private Vip vip;
    private boolean is_register;
    private String phone;
    private String signature;
    private String nickname;
    private int sex;
    private String avatar;
    private String birthday;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getApi_token() {
        if (api_token == null) {
            return "";
        }
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }

    public int getVip_type() {
        return vip_type;
    }

    public void setVip_type(int vip_type) {
        this.vip_type = vip_type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Vip getVip() {
        return vip;
    }

    public void setVip(Vip vip) {
        this.vip = vip;
    }

    public boolean is_register() {
        return is_register;
    }

    public void setIs_register(boolean is_register) {
        this.is_register = is_register;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public int getSex() {
        return sex;
    }

    public String getGender() {
        if (1 == sex) {
            return "男";
        } else if (2 == sex) {
            return "女";
        } else {
            return "未知";
        }
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public static class Vip {
        private boolean is_vip;
        private long expire_at;

        public boolean is_vip() {
            return is_vip;
        }

        public void setIs_vip(boolean is_vip) {
            this.is_vip = is_vip;
        }

        public long getExpire_at() {
            return expire_at * 1000;
        }

        public void setExpire_at(long expire_at) {
            this.expire_at = expire_at;
        }

        public int getLeftDays() {
            long delta = getExpire_at() - System.currentTimeMillis();
            if (delta <= 0) return 0;
            int oneDay = 24 * 60 * 60 * 1000;
            final double ceil = Math.ceil(delta / oneDay);
            return (int) ceil;
        }
    }
}
