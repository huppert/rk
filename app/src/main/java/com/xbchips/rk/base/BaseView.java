package com.xbchips.rk.base;

import com.xbchips.rk.base.response.ObjectResponse;

/**
 * Created by Harry on 2017/7/26.
 */

public interface BaseView<T> {
    void setPresenter(T presenter);

    boolean handleResponse(ObjectResponse response);

    void handleFailure(Throwable throwable);

}
