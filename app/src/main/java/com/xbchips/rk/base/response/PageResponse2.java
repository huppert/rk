package com.xbchips.rk.base.response;

import java.util.List;

public class PageResponse2<T> extends ObjectResponse<List<T>> {
    protected Page page;

    public Page getPage() {
        if (page == null) {
            page = new Page();
        }
        return page;
    }


    public static class Page {
        /**
         * 总页数
         */
        private int total;
        /**
         * 当前页数
         */
        private int current;
        /**
         * 每页显示条数
         */
        private int size;


        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getCurrent() {
            return current;
        }

        public void setCurrent(int current) {
            this.current = current;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }
    }
}
