package com.xbchips.rk.base;


import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;

import com.xbchips.rk.R;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.util.LogTrace;

import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;


/**
 * Created by york on 2016/4/18.
 */
public abstract class BaseFragment extends Fragment implements View.OnClickListener, BaseView2 {
    protected View rootView;
    protected boolean isInit;
    protected Unbinder mUnBinder;
    protected CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    protected String TAG;
    private RespHandler mRespHandler;
    protected TextView mTvTitle;
    protected ImageButton mBtnBack;
    protected FrameLayout mTopBar;
    protected Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = getClass().getSimpleName();
        mRespHandler = new RespHandler(getContext());
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (rootView == null || isInit) return;
            init();
            isInit = true;
        }
    }

    protected abstract void init();

    protected void initTopBar() {
        mBtnBack = rootView.findViewById(R.id.btn_back);
        mTvTitle = rootView.findViewById(R.id.bar_title);
        mTopBar = rootView.findViewById(R.id.top_bar);
        if (mBtnBack != null) {
            mBtnBack.setOnClickListener(this);
            mBtnBack.setImageResource(R.drawable.icon_toolbar_back);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_back) {
            getActivity().onBackPressed();
        }
    }

    public void showToast(CharSequence text) {
        mRespHandler.showToast(text);
    }

    public void showToast(@StringRes int resId) {
        mRespHandler.showToast(resId);
    }

    public void showProgress(String msg) {
        mRespHandler.showProgress(msg);
    }

    public void dismissDialog() {
        mRespHandler.dismissDialog();
    }

    @Override
    public boolean handleResponse(ObjectResponse response) {
        LogTrace.d(getClass().getName(), "handleResponse:" + response.toString());
        return mRespHandler.handleResponse(response);
    }

    @Override
    public void handleFailure(Throwable e) {
        mRespHandler.handleFailure(e);
    }


    @Override
    public void onDestroyView() {
        if (mUnBinder != null) {
            mUnBinder.unbind();
        }
        mCompositeDisposable.clear();
        super.onDestroyView();
    }


}
