package com.xbchips.rk.base;

import android.accounts.NetworkErrorException;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;

import com.xbchips.rk.R;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.user.activities.UserAuthActivity;
import com.xbchips.rk.user.activities.WapPayActivity;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;

/**
 * Created by york on 2017/2/16.
 */

public class RespHandler {
    public static final int SUCCESS_CODE = 0;
    public static final int SUCCESS_ONE = 1;
    public static final int NO_PERMISSION = 403;
    private static final int NO_AUTHORIZATION = 401;
    public static final int NOT_FOUND = 404;
    private static final int BUSINESS_FAILED = 405;
    protected ProgressDialog bar;
    protected Toast mToast;
    private Context mContext;
    private AlertDialog mAuthDialog;
    private AlertDialog vipDialog;

    public RespHandler(Context context) {
        mContext = context;
    }

    protected void showProgress(String msg) {
        if (bar == null) {
            bar = new ProgressDialog(mContext);
            bar.setMessage(msg);
            bar.setIndeterminate(true);
            bar.setCancelable(false);
        }
        if (bar.isShowing()) {
            bar.setMessage(msg);
        }
        bar.show();

    }

    public void dismissDialog() {
        if (bar != null && bar.isShowing()) {
            bar.dismiss();
        }
    }

    public void showToast(CharSequence text) {
        if (mToast == null) {
            mToast = Toast.makeText(mContext, text, Toast.LENGTH_SHORT);
        }
        mToast.setText(text);
        mToast.show();
    }

    public void showToast(@StringRes int resId) {
        showToast(mContext.getResources().getText(resId));
    }

    public boolean handleResponse(ObjectResponse response) {
        int statusCode = response.getStatus();
        if (statusCode != SUCCESS_CODE) {
            dismissDialog();
            switch (statusCode) {
                case NO_PERMISSION://not vip
                    if (vipDialog != null && vipDialog.isShowing()) {
                        return true;
                    }
                    vipDialog = new AlertDialog.Builder(mContext)
                            .setTitle("提示")
                            .setMessage("充值VIP，享受更多免费福利")
                            .setPositiveButton("确定", (dialog, which) -> mContext.startActivity(new Intent(mContext, WapPayActivity.class)))
                            .show();
                    break;
                case NO_AUTHORIZATION://not login
                    alertLoginDialog();
                    break;
                default:
                    showToast(response.getMessage());
                    break;
            }
            return true;
        }
        return false;
    }

    private void alertLoginDialog() {
        if (mAuthDialog != null && mAuthDialog.isShowing()) {
            return;
        }
        mAuthDialog = new AlertDialog.Builder(mContext)
                .setMessage(mContext.getString(R.string.login_first_need))
                .setPositiveButton(mContext.getString(R.string.ensure), (dialog, which) -> {
                            Intent intent = new Intent(mContext, UserAuthActivity.class);
                            intent.putExtra(UserAuthActivity.ARG_IS_LOGIN, true);
                            mContext.startActivity(intent);
                        }
                )
                .show();
    }

    /**
     * 处理异常情况
     */
    public void handleFailure(Throwable e) {
        dismissDialog();
        showToast(getErrorMsg(e));
    }

    public String getErrorMsg(Throwable throwable) {
        throwable.printStackTrace();
        String resultMsg = "未知错误";
        if (throwable instanceof retrofit2.HttpException) {
            resultMsg = "服务器异常";
        } else if (throwable instanceof ConnectException) {
            resultMsg = "网络连接失败";
        } else if (throwable instanceof NetworkErrorException) {
            resultMsg = "网络错误";
        } else if (throwable instanceof TimeoutException || throwable instanceof SocketTimeoutException) {
            resultMsg = "连接超时";
        } else if (throwable instanceof java.net.UnknownHostException) {
            resultMsg = "无法连接网络,请检查网络设置";
        }
        return resultMsg;
    }
}
