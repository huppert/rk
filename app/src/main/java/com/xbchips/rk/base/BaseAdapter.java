package com.xbchips.rk.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;

import java.util.List;

public abstract class BaseAdapter<T> extends RecyclerView.Adapter {
    private static final String TAG = "BaseAdapter";
    /**
     * normal item
     */
    protected static final int TYPE_ITEM = 0;
    /**
     * footer item
     */
    protected static final int TYPE_FOOTER = 1;
    protected static final int TYPE_HEADER = 2;

    protected LayoutInflater mInflater;
    protected Context mContext;
    protected List<T> mList;
    protected int mHeaderSize;
    private int mFooterSize;
    protected boolean isMoreFooter;
    protected View.OnClickListener mOnClickListener;
    protected View mHeaderView;

    public BaseAdapter(List<T> list, Context context) {
        mList = list;
        mInflater = LayoutInflater.from(context);
        mContext = context;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }


    public void setHeaderView(View headerView) {
        mHeaderView = headerView;
        mHeaderSize++;
        notifyItemInserted(0);
    }

    public void removeHeader() {
        if (mHeaderSize > 0) {
            mHeaderSize--;
            notifyItemRemoved(0);
        }
    }

    public void addNoMoreView() {
        isMoreFooter = true;
        addFooter();
    }

    public void addFooter() {
        mFooterSize++;
        notifyItemRangeChanged(0, getItemCount() - 1);
    }

    public void removeFooter() {
        isMoreFooter = false;
        if (mFooterSize > 0) {
            mFooterSize--;
            notifyItemRemoved(getItemCount());
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int itemViewType = getItemViewType(position);
        if (itemViewType == TYPE_ITEM) {
            bindItemViewHolder(holder, position);
        } else if (itemViewType == TYPE_FOOTER) {
            FooterViewHolder viewHolder = (FooterViewHolder) holder;
            viewHolder.mProgressBar.setVisibility(isMoreFooter ? View.GONE : View.VISIBLE);
            viewHolder.mTvNoMore.setVisibility(isMoreFooter ? View.VISIBLE : View.GONE);
        }
    }


    /**
     * 由子类实现具体操作
     */
    protected void bindItemViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemViewType(int position) {
        if (mHeaderSize > 0 && position == 0) {
            return TYPE_HEADER;
        }
        if (mFooterSize > 0 && position == getItemCount() - 1) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }


    public int getRealPosition(int position) {
        return position - mHeaderSize;
    }

    @Override
    public int getItemCount() {
        return mList.size() + mHeaderSize + mFooterSize;
    }

    public int getHeaderSize() {
        return mHeaderSize;
    }

    public int getFooterSize() {
        return mFooterSize;
    }


    @NonNull
    protected FooterViewHolder getFooterViewHolder(ViewGroup parent) {
        return new FooterViewHolder(mInflater.inflate(R.layout.item_footer, parent, false));
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar mProgressBar;
        public TextView mTvNoMore;

        FooterViewHolder(View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.progress);
            mTvNoMore = itemView.findViewById(R.id.tv_no_more);
        }
    }

    protected static class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }
}
