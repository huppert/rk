package com.xbchips.rk.base;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.IBinder;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import com.umeng.analytics.MobclickAgent;
import com.xbchips.rk.BuildConfig;
import com.xbchips.rk.R;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.common.ActivityStackManager;
import com.xbchips.rk.util.LogTrace;
import com.xbchips.rk.util.StatusBarUtil;

import java.lang.ref.WeakReference;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by york on 2017/7/28.
 */

public class BaseActivity extends AppCompatActivity implements BaseView2 {
    protected TextView mTvTitle;
    protected ImageButton mBtnBack;
    protected ImageButton mBtnChan;
    protected CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    protected RespHandler mRespHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //dark 模式
        if (BuildConfig.SKIN_MODE) {
            StatusBarUtil.setImmersiveStatusBar(this, true);
        }
        ActivityStackManager.add(new WeakReference<>(this).get());
        mRespHandler = new RespHandler(this);
    }

    protected void initTopBar() {
        mBtnBack = findViewById(R.id.btn_back);
        mBtnChan = findViewById(R.id.btn_chuan);
        mTvTitle = findViewById(R.id.bar_title);
        if (mBtnBack != null) {
            mBtnBack.setVisibility(View.VISIBLE);
            mBtnBack.setOnClickListener(v -> finish());
            mBtnBack.setImageResource(R.drawable.icon_toolbar_back);

        }
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {
                hideSoftInput(v.getWindowToken());
            }
        }
        return super.dispatchTouchEvent(ev);
    }


    private boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] ints = new int[]{0, 0};
            v.getLocationInWindow(ints);
            Rect rect = new Rect(ints[0], ints[1], ints[0] + v.getWidth(), ints[1] + v.getHeight());
            return !rect.contains(((int) event.getRawX()), ((int) event.getRawY()));
        }

        return false;
    }


    private void hideSoftInput(IBinder token) {
        if (token != null) {
            InputMethodManager im = ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE));
            im.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    public void showToast(CharSequence text) {
        mRespHandler.showToast(text);
    }

    public void showToast(@StringRes int resId) {
        mRespHandler.showToast(resId);
    }

    protected void showProgress() {
        mRespHandler.showProgress(null);
    }

    @Override
    public void showProgress(String msg) {
        mRespHandler.showProgress(msg);
    }

    public void dismissDialog() {
        mRespHandler.dismissDialog();
    }

    public boolean handleResponse(ObjectResponse response) {
        LogTrace.d(getClass().getName(), "handleResponse:" + response.toString());
        return mRespHandler.handleResponse(response);
    }

    public void handleFailure(Throwable e) {
        mRespHandler.handleFailure(e);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityStackManager.remove(this);
        mCompositeDisposable.clear();
    }

    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
