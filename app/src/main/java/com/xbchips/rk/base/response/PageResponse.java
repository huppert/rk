package com.xbchips.rk.base.response;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by york on 2016/8/3.
 * 分页response
 */

public class PageResponse<T> {
    protected List<T> list;

    protected Page page;


    @NonNull
    public List<T> getList() {
        if (list == null) {
            list = new ArrayList<>();
        }
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public Page getPage() {
        if (page == null) {
            page = new Page();
        }
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public static class Page {
        /**
         * 总页数
         */
        private int total;
        /**
         * 当前页数
         */
        private int current;
        /**
         * 每页显示条数
         */
        private int size;


        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getCurrent() {
            return current;
        }

        public void setCurrent(int current) {
            this.current = current;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }
    }
}
