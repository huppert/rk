package com.xbchips.rk.base.response;

import com.xbchips.rk.base.RespHandler;

/**
 * Created by york on 2017/8/3.
 * 普通对象response
 */

public class ObjectResponse<T> {
    protected int status = -1;
    protected String message;
    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return status == RespHandler.SUCCESS_CODE;
    }
}
