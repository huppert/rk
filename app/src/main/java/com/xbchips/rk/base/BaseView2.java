package com.xbchips.rk.base;


import androidx.annotation.StringRes;

import com.xbchips.rk.base.response.ObjectResponse;

/**
 * baseActivity 和 baseFragment 公用方法
 */

public interface BaseView2 {
    boolean handleResponse(ObjectResponse response);

    void handleFailure(Throwable throwable);

    void showToast(CharSequence text);

    void showToast(@StringRes int resId);

    void showProgress(String msg);

    void dismissDialog();
}
