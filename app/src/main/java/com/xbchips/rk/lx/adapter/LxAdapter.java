package com.xbchips.rk.lx.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.util.DisplayUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LxAdapter extends BaseAdapter<VideoItem> {


    private int mSpace;
    private RequestOptions mOptions;

    public LxAdapter(List<VideoItem> list, Context context) {
        super(list, context);
        mSpace = DisplayUtils.dip2px(context, 5);
        mOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .placeholder(R.drawable.place_holder2)
                .dontAnimate();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return viewType == TYPE_ITEM ?
                new LxItemViewHolder(mInflater.inflate(R.layout.item_lx, parent, false)) :
                getFooterViewHolder(parent);
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        LxItemViewHolder viewHolder = (LxItemViewHolder) holder;
        VideoItem video = mList.get(position);
        viewHolder.ivCover.setTag(viewHolder.ivCover.getId(), video);
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) viewHolder.ivCover.getLayoutParams();
        if (video.getIs_advert() == 1) {
            layoutParams.leftMargin = layoutParams.rightMargin = mSpace;
            layoutParams.dimensionRatio = "11:3";
            viewHolder.ivCover.setLayoutParams(layoutParams);
            viewHolder.ivCover.setScaleType(ImageView.ScaleType.FIT_XY);
            ImageLoader.load(mContext, video.getImg_url(), viewHolder.ivCover, mOptions);
            viewHolder.flTime.setVisibility(View.GONE);
            viewHolder.mTvTitle.setVisibility(View.GONE);
        } else {
            layoutParams.dimensionRatio = "4:3";
            boolean lastInSingleLine = isLastInSingleLine(position);
            layoutParams.leftMargin = lastInSingleLine ? mSpace / 2 : mSpace;
            layoutParams.rightMargin = lastInSingleLine ? mSpace : mSpace / 2;
            viewHolder.ivCover.setLayoutParams(layoutParams);
            viewHolder.mTvTitle.setText(video.getTitle());
            ImageLoader.load(mContext, video.getThumb_img_url(), viewHolder.ivCover);
            viewHolder.flTime.setVisibility(View.VISIBLE);
            viewHolder.mTvTitle.setVisibility(View.VISIBLE);
        }

        viewHolder.ivCover.setOnClickListener(mOnClickListener);
        viewHolder.mTvPlayCount.setText(mContext.getString(R.string.watch_times, video.getViews()));
        viewHolder.tvMovieTime.setText(video.getUpdated_at());
    }

    /**
     * 是否为一行中最后一个item
     */
    private boolean isLastInSingleLine(int position) {
        int adCount = 0;
        for (int i = 0; i < position; i++) {
            if (mList.get(i).getIs_advert() == 1) {
                adCount++;
            }
        }

        return (position - adCount) % 2 == 1;

    }

    static class LxItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_cover)
        ImageView ivCover;
        @BindView(R.id.tv_title)
        TextView mTvTitle;
        @BindView(R.id.tv_movie_time)
        TextView tvMovieTime;
        @BindView(R.id.tv_play_count)
        TextView mTvPlayCount;
        @BindView(R.id.fl_time)
        FrameLayout flTime;

        public LxItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
