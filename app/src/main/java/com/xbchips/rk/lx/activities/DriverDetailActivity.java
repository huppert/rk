package com.xbchips.rk.lx.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;

import com.xbchips.rk.BuildConfig;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.common.DataHandler;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.lx.JudgeLxPresenter;
import com.xbchips.rk.lx.adapter.OthersFavorAdapter;
import com.xbchips.rk.lx.api.LxService;
import com.xbchips.rk.lx.entity.DriverDetail;
import com.xbchips.rk.lx.widgets.NotVipFragment;
import com.xbchips.rk.user.activities.UserAuthActivity;
import com.xbchips.rk.util.DisplayUtils;
import com.xbchips.rk.util.StatusBarUtil;
import com.xbchips.rk.video.VideoDetailActivity;
import com.xbchips.rk.widgets.EndlessRecyclerView;
import com.xbchips.rk.widgets.ItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.Disposable;

public class DriverDetailActivity extends BaseActivity {
    public static final String ARG_ID = "arg_id";
    public static final String ARG_NAME = "arg_name";
    public static final String ARG_PREVIEW = "arg_preview";
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_favor_count)
    TextView tvFavorCount;
    @BindView(R.id.recycler_view)
    EndlessRecyclerView recyclerView;
    private LxService mLxService = HttpRequest.createService(LxService.class);
    private final List<DriverDetail.ListBean> mList = new ArrayList<>();
    private DataHandler<DriverDetail.ListBean> mDataHandler;
    private OthersFavorAdapter mAdapter;
    //是否是老司机预览界面
    private boolean isPreview;
    private JudgeLxPresenter mLxPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_detail);
        ButterKnife.bind(this);
        mLxPresenter = new JudgeLxPresenter(this, this);
        isPreview = getIntent().getBooleanExtra(ARG_PREVIEW, true);
        initRecyclerView();
        loadData();
    }


    private void initRecyclerView() {
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int i) {
                if (i == mList.size() || mList.get(i) == null) {
                    return 2;
                }
                return 1;
            }
        });
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new ItemDecoration(DisplayUtils.dip2px(this, 5), 2));
        mAdapter = new OthersFavorAdapter(mList, this);
        mAdapter.setOnClickListener(v -> {
            //查看更多
            if (v.getId() == R.id.tv_look_more) {
                if (isVip()) {
                    Intent intent = new Intent(this, DriverDetailActivity.class);
                    intent.putExtra(DriverDetailActivity.ARG_ID, getIntent().getIntExtra(ARG_ID, 0));
                    intent.putExtra(DriverDetailActivity.ARG_NAME, getIntent().getStringExtra(ARG_NAME));
                    intent.putExtra(DriverDetailActivity.ARG_PREVIEW, false);
                    startActivity(intent);
                }
            } else {
                if (isVip()) {
                    DriverDetail.ListBean bean = mList.get(((int) v.getTag()));
                    if (bean.isIs_liuxiang()) {
                        mLxPresenter.judge(bean);
                    } else {
                        Intent intent = new Intent(this, VideoDetailActivity.class);
                        intent.putExtra(VideoDetailActivity.ARG_VIDEO_ID, bean.getId());
                        intent.putExtra(VideoDetailActivity.ARG_VIDEO_TITLE, bean.getTitle());
                        startActivity(intent);
                    }

                }
            }

        });
        recyclerView.setAdapter(mAdapter);
        if (!isPreview) {
            recyclerView.setOnLoadMoreListener(() -> {
                mAdapter.addFooter();
                loadData();
            });
        }

        mDataHandler = new DataHandler<>(mList, recyclerView, this);

    }


    private boolean isVip() {
        if (UserHelper.getLocalUser() == null) {
            Intent intent = new Intent(this, UserAuthActivity.class);
            intent.putExtra(UserAuthActivity.ARG_IS_LOGIN, false);
            startActivity(intent);
            return false;
        }
        if (!UserHelper.getLocalUser().getVip().is_vip()) {
            NotVipFragment notVipFragment = new NotVipFragment();
            notVipFragment.show(getSupportFragmentManager(), "");
            return false;
        }
        return true;
    }

    private void initData(DriverDetail detail) {
        final int count = detail.getCollect_amount();
        tvFavorCount.setText(getString(R.string.total_favor_of_ta, count));
        tvTitle.setText(getIntent().getStringExtra(ARG_NAME));
    }

    private void loadData() {
        int userId = getIntent().getIntExtra(ARG_ID, 0);
        Disposable disposable = mLxService.getDriverFavors(userId, recyclerView.getCurPage(), isPreview ? 10 : 20)
                .compose(RxUtils.applySchedulers())
                .filter(resp -> !handleResponse(resp))
                .subscribe(response -> {
                    if (recyclerView.getCurPage() == 1) {
                        initData(response.getData());
                    }

                    if (isPreview) {
                        //预览界面
                        DriverDetail data = response.getData();
                        if (data.getCollect_amount() > 10) {
                            data.getList().add(null);
                        }
                    }
                    mDataHandler.handResp(response);
                }, t -> mDataHandler.processError(t));
        mCompositeDisposable.add(disposable);
    }

    @OnClick(R.id.iv_back)
    public void onViewClicked() {
        finish();
    }
}
