package com.xbchips.rk.lx.activities;

import android.os.Bundle;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;

public class TutorialsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorials);
        initTopBar();
    }

    @Override
    protected void initTopBar() {
        super.initTopBar();
        mTvTitle.setText(R.string.tutorials);
    }
}
