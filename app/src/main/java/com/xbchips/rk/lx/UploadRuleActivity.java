package com.xbchips.rk.lx;

import android.os.Bundle;
import android.view.View;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;


public class UploadRuleActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uploadrule);
        initTopBar();
    }


    @Override
    protected void initTopBar() {
        mBtnBack = findViewById(R.id.btn_back);
        mTvTitle = findViewById(R.id.bar_title);
        if (mBtnBack != null) {
            mBtnBack.setVisibility(View.VISIBLE);
            mBtnBack.setOnClickListener(v -> finish());
            mBtnBack.setImageResource(R.drawable.icon_toolbar_back);

        }
        mTvTitle.setText(R.string.uploadvidoe);
    }


}
