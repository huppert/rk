package com.xbchips.rk.lx.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.chrisbanes.photoview.PhotoView;
import com.xbchips.rk.R;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.lx.entity.Picture;
import com.xbchips.rk.user.entity.User;
import com.xbchips.rk.util.transform.BlurTransformation;
import com.xbchips.rk.widgets.RoundButton;

import java.util.List;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

/**
 * Created by york on 2017/8/4.
 */

public class PhotoPagerAdapter extends PagerAdapter {
    private static final String TAG = "PhotoPagerAdapter";
    private final RequestOptions mBlurOptions = new RequestOptions()
            .apply(bitmapTransform(new BlurTransformation(80)));
    private List<String> mList;
    private Picture.Advert mAdvert;
    private Fragment mFragment;
    private LayoutInflater mInflater;
    private View.OnClickListener mOnClickListener;
    private int mPreviewSize;
    private boolean isVip;
    private final RequestOptions mRequestOptions = new RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .placeholder(R.drawable.place_holder)
            .dontAnimate();

    public PhotoPagerAdapter(Picture picture, Fragment fragment, int previewSize) {
        mList = picture.getContent();
        mAdvert = picture.getAdvert();
        mFragment = fragment;
        mInflater = LayoutInflater.from(fragment.getContext());
        mPreviewSize = previewSize;
        initVip();
    }

    private void initVip() {
        User localUser = UserHelper.getLocalUser();
        if (!UserHelper.isLoginState()) {
            isVip = false;
        } else {
            boolean vip = localUser.getVip().is_vip();
            if (vip) {
                isVip = localUser.getVip().getExpire_at() > System.currentTimeMillis();
            } else {
                isVip = false;
            }
        }
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View rootView = mInflater.inflate(R.layout.item_photoview, container, false);
        container.addView(rootView);
        PhotoView photoView = (PhotoView) rootView.findViewById(R.id.photo_view);
        RoundButton btn_vip = (RoundButton) rootView.findViewById(R.id.btn_vip);
        ImageView adImage = (ImageView) rootView.findViewById(R.id.riv_ad);
        btn_vip.setOnClickListener(mOnClickListener);
        if (position > mPreviewSize - 1 && !isVip) {
            Glide.with(mFragment)
                    .load(mList.get(position))
                    .apply(mBlurOptions)
                    .into(photoView);
            btn_vip.setVisibility(View.VISIBLE);
            //显示广告
            if (mAdvert != null) {
                adImage.setVisibility(View.VISIBLE);
                ImageLoader.load(mFragment.getContext(), mAdvert.getThumb_img_url(), adImage);
                adImage.setTag(adImage.getId(), mAdvert.getUrl());
                adImage.setOnClickListener(mOnClickListener);
            }

        } else {
            Glide.with(mFragment)
                    .load(mList.get(position))
                    .apply(mRequestOptions)
                    .into(photoView);
            btn_vip.setVisibility(View.GONE);
            adImage.setVisibility(View.GONE);
        }
        return rootView;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }
}