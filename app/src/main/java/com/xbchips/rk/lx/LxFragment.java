package com.xbchips.rk.lx;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.xbchips.rk.R;
import com.xbchips.rk.activities.HtmlActivity;
import com.xbchips.rk.base.BaseFragment;
import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.base.response.PageResponse;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.lx.activities.OldDriverActivity;
import com.xbchips.rk.lx.activities.UploadActivity;
import com.xbchips.rk.lx.adapter.LxAdapter;
import com.xbchips.rk.user.activities.UserAuthActivity;
import com.xbchips.rk.vip.entity.Category;
import com.xbchips.rk.vip.service.VipService;
import com.xbchips.rk.widgets.EndlessRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static com.xbchips.rk.activities.HtmlActivity.ARG_TITLE;
import static com.xbchips.rk.activities.HtmlActivity._URL;

/**
 * Created by york on 2017/7/26.
 */

public class LxFragment extends BaseFragment {
    @BindView(R.id.recycler_view)
    EndlessRecyclerView mRecyclerView;
    @BindView(R.id.fab_add)
    FloatingActionButton fabAdd;
    @BindView(R.id.iv_to_ranking)
    ImageView ivToRanking;

    private LxAdapter mAdapter;
    private final List<VideoItem> mList = new ArrayList<>();
    private VipService mVipService = HttpRequest.createService(VipService.class);
    /**
     * 港友分类ID
     */
    private String mLxId;
    private JudgeLxPresenter mPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_lx, container, false);
        mUnBinder = ButterKnife.bind(this, rootView);
        mPresenter = new JudgeLxPresenter(getContext(), this);
        return rootView;
    }


    @Override
    protected void init() {
        fabAdd.setOnClickListener(v -> startActivity(new Intent(getContext(), UploadActivity.class)));
        ivToRanking.setOnClickListener(v -> startActivity(new Intent(getContext(), OldDriverActivity.class)));
        initRecyclerView();
        loadData();
    }


    public void getCategoryList() {
        Disposable disposable = mVipService.getCategoryList()
                .compose(RxUtils.applySchedulers())
                .filter(response -> !LxFragment.this.handleResponse(response))
                .flatMap((Function<ObjectResponse<List<Category>>, ObservableSource<Category>>) response ->
                        Observable.fromIterable(response.getData()))
                .filter(category -> "巷友".equals(category.getName()))
                .subscribe(category -> {
                    mLxId = category.getId();
                    loadData();
                }, this::handleFailure);
        mCompositeDisposable.add(disposable);
    }


    private void loadData() {
        if (mLxId == null) {
            getCategoryList();
            return;
        }

        Disposable disposable = mVipService.getVideoList(mRecyclerView.getCurPage(), Constants.DEFAULT_PAGESIZE, mLxId, "", null)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if (mRecyclerView.getCurPage() == 1) {
                        mList.clear();
                    }
                    mAdapter.removeFooter();
                    if (!handleResponse(response)) {
                        PageResponse<VideoItem> data = response.getData();
                        List<VideoItem> list = data.getList();
                        mRecyclerView.setPageCount(data.getPage().getTotal());
                        mList.addAll(list);
                        int size = list.size();
                        if (mRecyclerView.getCurPage() == 1) {
                            mAdapter.notifyDataSetChanged();
                        } else {
                            mAdapter.notifyItemRangeInserted(mList.size() - size, size);
                        }
                        if (mRecyclerView.isLastPage()) {
                            mAdapter.addNoMoreView();
                        }
                    }
                    mRecyclerView.setLoading(false);
                }, this::processError);
        mCompositeDisposable.add(disposable);
    }

    private void processError(@NonNull Throwable throwable) {
        handleFailure(throwable);
        mRecyclerView.setLoading(false);
        if (mRecyclerView.getCurPage() > 1) {
            mRecyclerView.pageDecrement();
            mAdapter.removeFooter();
        }
    }

    private void initRecyclerView() {
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int i) {
                if (i == mList.size() || mList.get(i).getIs_advert() == 1) {
                    return 2;
                }
                return 1;
            }
        });
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new LxAdapter(mList, getContext());
        mAdapter.setOnClickListener(v -> {
            VideoItem tag = (VideoItem) v.getTag(v.getId());
            //广告
            if (tag.getIs_advert() == 1) {
                Intent intent = new Intent(getContext(), HtmlActivity.class);
                intent.putExtra(_URL, tag.getUrl());
                intent.putExtra(ARG_TITLE, tag.getTitle());
                startActivity(intent);
            } else {
                //未登录
                if (!UserHelper.isLoginState()) {
                    new AlertDialog.Builder(getContext())
                            .setMessage(R.string.login_first_need)
                            .setPositiveButton(R.string.ensure, (dialog, which) -> {
                                Intent intent = new Intent(getContext(), UserAuthActivity.class);
                                intent.putExtra(UserAuthActivity.ARG_IS_LOGIN, true);
                                startActivity(intent);
                            })
                            .show();
                } else {
                    mPresenter.judge(tag);
                }

            }
        });
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setOnLoadMoreListener(() -> {
            mAdapter.addFooter();
            loadData();
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.clear();
    }
}
