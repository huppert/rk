package com.xbchips.rk.lx.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.xbchips.rk.util.DisplayUtils;

/**
 * Created by york on 2017/7/31.
 */

public class IndexView extends View {

    private Paint mPaint = new Paint();
    private Paint mTextPaint = new Paint();
    private Path path = new Path();
    private String mText;
    private int mWidth;
    private int mHeight;

    public IndexView(Context context) {
        super(context);
    }

    public IndexView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mPaint.setColor(0xffe20055);
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setAntiAlias(true);
        mTextPaint.setColor(Color.WHITE);
        mTextPaint.setTextSize(DisplayUtils.sp2px(context, 16));
        mTextPaint.setAntiAlias(true);
    }

    public void setText(String text) {
        mText = text;
        requestLayout();
        invalidate();
    }

    private Rect rect = new Rect();

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (!TextUtils.isEmpty(mText)) {
            mTextPaint.getTextBounds(mText, 0, mText.length() - 1, rect);
            mHeight = rect.height() + getPaddingTop() + getPaddingBottom();
            mWidth = rect.width() + getPaddingEnd() + getPaddingStart() + rect.width() / 5;
        }
        setMeasuredDimension(mWidth, mHeight);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        path.moveTo(mWidth / 5, 0);
        path.lineTo(mWidth, 0);
        path.lineTo(mWidth, mHeight);
        path.lineTo(mWidth / 5, mHeight);
        path.lineTo(0, mHeight / 2);
        path.close();
        canvas.drawPath(path, mPaint);
        if (TextUtils.isEmpty(mText)) {
            mText = "";
        }
        canvas.drawText(mText, mWidth / 5, (mHeight + rect.height()) / 2, mTextPaint);
    }
}
