package com.xbchips.rk.lx.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.util.DisplayUtils;
import com.xbchips.rk.video.entity.MyVideo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyVideoPreviewAdapter extends BaseAdapter<MyVideo> {

    private int mItemWidth;

    public MyVideoPreviewAdapter(List<MyVideo> list, Context context) {
        super(list, context);
        int screenW = DisplayUtils.getScreenW(context);
        int padding = DisplayUtils.dip2px(context, 10);
        mItemWidth = (screenW - padding * 3) / 2;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.item_my_video_preview, viewGroup, false);
        RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(mItemWidth, mItemWidth);
        params.bottomMargin = DisplayUtils.dip2px(mContext, 5);
        view.setLayoutParams(params);
        return new MyVideoPreviewViewHolder(view);
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        MyVideoPreviewViewHolder viewHolder = (MyVideoPreviewViewHolder) holder;
        MyVideo myVideo = mList.get(position);

        if (mList.size() > 1 && position == mList.size() - 1) {
            viewHolder.tvStatus.setVisibility(View.VISIBLE);
        } else {
            viewHolder.tvStatus.setVisibility(View.INVISIBLE);
        }

        ImageLoader.load(mContext, myVideo.getStatus() == 1 ? myVideo.getThumb_img_url() : R.drawable.reviewing, viewHolder.ivCover);

        viewHolder.tvName.setText(myVideo.getTitle());
        viewHolder.tvNum.setText(String.valueOf(myVideo.getCount()));
        viewHolder.itemView.setTag(position);
        viewHolder.itemView.setOnClickListener(mOnClickListener);

    }


    static class MyVideoPreviewViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_cover)
        ImageView ivCover;
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindView(R.id.tv_num)
        TextView tvNum;
        @BindView(R.id.tv_name)
        TextView tvName;

        public MyVideoPreviewViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
