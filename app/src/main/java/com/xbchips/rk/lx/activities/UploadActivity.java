package com.xbchips.rk.lx.activities;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.common.UploadRequest;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.lx.UploadRuleActivity;
import com.xbchips.rk.lx.adapter.MyVideoPreviewAdapter;
import com.xbchips.rk.lx.widgets.UploadProgressDialog;
import com.xbchips.rk.profile.activity.MyVideoActivity;
import com.xbchips.rk.system.entity.Configs;
import com.xbchips.rk.system.entity.DynamicConfig;
import com.xbchips.rk.upload.ProgressRequestBody;
import com.xbchips.rk.util.AtomicDouble;
import com.xbchips.rk.util.FileUtil;
import com.xbchips.rk.util.LogTrace;
import com.xbchips.rk.util.PreferenceUtils;
import com.xbchips.rk.video.VideoDetailActivity;
import com.xbchips.rk.video.VideoService;
import com.xbchips.rk.video.entity.MyVideo;
import com.xbchips.rk.video.entity.VideoParam;
import com.xbchips.rk.video.widgets.ConfirmWindow;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import okhttp3.MultipartBody;

public class UploadActivity extends BaseActivity {
    private static final int REQUEST_TAKE_GALLERY_VIDEO = 200;
    private static final String TAG = "UploadActivity";
    @BindView(R.id.bar_title)
    TextView barTitle;
    @BindView(R.id.top_bar)
    FrameLayout topBar;
    @BindView(R.id.tv_upload)
    TextView tvUpload;
    @BindView(R.id.et_title)
    EditText etTitle;
    @BindView(R.id.et_desc)
    EditText etDesc;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.thumbnail)
    ImageView thumbnail;
    @BindView(R.id.et_price)
    EditText etPrice;
    @BindView(R.id.root_view)
    ScrollView rootView;
    @BindView(R.id.fl_upload)
    FrameLayout flUpload;
    //    @BindView(R.id.dash_line)
    //    View dashLine;
    private VideoService mUploadService;
    private VideoService mService;
    private Uri mVideoUri;
    private UploadProgressDialog mProgressDialog;
    /**
     * 上传进度
     */
    private final AtomicDouble mProgress = new AtomicDouble();
    private UploadRequest uploadRequest = new UploadRequest((bytesWritten, contentLength) -> {
        double progress = bytesWritten / ((double) contentLength);
        mProgress.set(progress);
        LogTrace.d(TAG, Thread.currentThread().getName() + "进度= " + progress);
    });


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        ButterKnife.bind(this);
        initUpLoadUrl();
        initTopBar();
        getMyVideos();


    }

    private void initUpLoadUrl() {
        mService = HttpRequest.createService(VideoService.class);
        String json = PreferenceUtils.getString(UploadActivity.this, Constants.PREF_CONFIG);
        if (!TextUtils.isEmpty(json)) {
            Configs configs = new Gson().fromJson(json, Configs.class);
            DynamicConfig dynamicConfig = configs.getUpload_url();
            String upload_url = dynamicConfig.getDescription();
            //链接地址后边拼接上api
            mUploadService = uploadRequest.createService(VideoService.class, upload_url + "/api/");
        }


    }

    @Override
    protected void initTopBar() {
        mBtnBack = findViewById(R.id.btn_back);
        mBtnChan = findViewById(R.id.btn_chuan);
        mTvTitle = findViewById(R.id.bar_title);
        if (mBtnChan != null) {
            mBtnChan.setVisibility(View.VISIBLE);
            mBtnChan.setOnClickListener(v ->
                    showInetntRule()
            );
            mBtnChan.setImageResource(R.drawable.icon_rule);
        }
        if (mBtnBack != null) {
            mBtnBack.setVisibility(View.VISIBLE);
            mBtnBack.setOnClickListener(v -> finish());
            mBtnBack.setImageResource(R.drawable.icon_toolbar_back);

        }
        mTvTitle.setText(R.string.uploadvidoe);
    }

    @OnClick({R.id.fl_upload, R.id.btn_submit, R.id.thumbnail})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.fl_upload:
                selectVideo();
                break;
            case R.id.btn_submit:
                if (validate()) {
                    showConfirmWindow();
                }
                break;
            case R.id.thumbnail:
                selectVideo();
                break;
        }
    }

    private void showInetntRule() {
        Intent intent = new Intent(new Intent(this, UploadRuleActivity.class));
        startActivity(intent);
    }

    private void showConfirmWindow() {
        VideoParam video = new VideoParam();
        video.setTitle(getVideoTitle());
        video.setAuthor(UserHelper.getLocalUser().getUsername());
        video.setDesc(getDesc());
        video.setPrice(getPrice());
        video.setImgUrl(mVideoUri);


        ConfirmWindow window = new ConfirmWindow(this);
        window.setListener(v1 -> {
            switch (v1.getId()) {
                case R.id.btn_cancel:
                    window.dismiss();
                    break;
                case R.id.btn_submit:
                    window.dismiss();
                    upload(mVideoUri);
                    break;
            }
        });
        window.show(video);
    }

    private void getMyVideos() {
        Disposable disposable = mService.getMyVideos(1, 1, 6)
                .compose(RxUtils.applySchedulers())
                .filter(response -> !handleResponse(response))
                .subscribe(response -> {
                    List<MyVideo> list = response.getData().getList();
                    initRecyclerView(list);
                }, this::handleFailure);
        mCompositeDisposable.add(disposable);
    }

    private void initRecyclerView(List<MyVideo> list) {
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        MyVideoPreviewAdapter adapter = new MyVideoPreviewAdapter(list, this);
        adapter.setOnClickListener(v -> {
            int position = ((int) v.getTag());
            if (position >= 1) {
                UploadActivity.this.startActivity(new Intent(UploadActivity.this, MyVideoActivity.class));
            } else {
                MyVideo myVideo = list.get(position);
                Intent intent = new Intent(UploadActivity.this, VideoDetailActivity.class);
                intent.putExtra(VideoDetailActivity.ARG_VIDEO_ID, myVideo.getId());
                intent.putExtra(VideoDetailActivity.ARG_VIDEO_TITLE, myVideo.getTitle());
                intent.putExtra(VideoDetailActivity.ARG_VIDEO_ISXIANGYOU, "xiangyou");
                UploadActivity.this.startActivity(intent);
            }
        });
        recyclerView.setAdapter(adapter);
    }


    private void selectVideo() {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        Intent chooser = Intent.createChooser(intent, "选择视频");
        startActivityForResult(chooser, REQUEST_TAKE_GALLERY_VIDEO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TAKE_GALLERY_VIDEO && resultCode == RESULT_OK) {
            flUpload.setClickable(false);
            LogTrace.d(TAG, "onActivityResult: " + mVideoUri);
            mVideoUri = data.getData();
            ImageLoader.load(this, mVideoUri, thumbnail);
        }
    }


    /**
     * 上传视频
     */
    private void upload(Uri uri) {
        //重置进度
        mProgress.set(0);
        observerProgress();
        String path = FileUtil.getRealPathFromURI(this, uri);
        File file = new File(path);
        //        RequestBody requestBody = RequestBody.create(MediaType.parse(getContentResolver().getType(uri)), file);
        //        MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), requestBody);

        //是否需要压缩
        //实现上传进度监听
        ProgressRequestBody requestFile = new ProgressRequestBody(file, "image/*", new ProgressRequestBody.UploadCallbacks() {
            @Override
            public void onProgressUpdate(int percentage) {
                //  Log.e(TAG, "onProgressUpdate: " + percentage);
                //  mCircleProgress.setProgress(percentage);
                //updateProgressy(percentage);
                updateProgressy(percentage);
            }

            @Override
            public void onError() {
                if (mProgressDialog != null) {
                    mProgressDialog.setTvResult(R.string.upload_failed);
                }
                mProgressDialogDismiss();
            }

            @Override
            public void onFinish() {
                if (mProgressDialog != null) {
                    mProgressDialog.setTvResult(R.string.upload_failed);
                }
                mProgressDialogDismiss();
            }
        });

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        Disposable disposable = mUploadService.uploadVideo(body)
                .compose(RxUtils.applySchedulers())
                .subscribe(response -> {
                    if (!handleResponse(response)) {
                        String video_url = response.getData().get("video_url");
                        LogTrace.i(TAG, "upload: successfully" + video_url);
                        addVideo(video_url);
                    } else {
                        if (mProgressDialog != null) {
                            mProgressDialog.setTvResult(R.string.upload_failed);
                        }
                    }
                }, throwable -> {
                    handleFailure(throwable);
                    if (mProgressDialog != null) {
                        mProgressDialog.setTvResult(R.string.upload_failed);
                    }
                    // dispose();

                });

        mCompositeDisposable.add(disposable);
    }


    //  private Disposable mProgressDisposable;


    /**
     * 监听进度
     */
    private void observerProgress() {
        mProgressDialog = new UploadProgressDialog();
        //        mProgressDialog.setOnDismissListener(new UploadProgressDialog.OnDismissListener() {
        //            @Override
        //            public void onDismiss() {
        //                dispose();
        //            }
        //        });
        mProgressDialog.show(getSupportFragmentManager(), "");
        //        mProgressDisposable = Flowable.interval(1, TimeUnit.SECONDS)
        //                .map(new Function<Long, Double>() {
        //                    @Override
        //                    public Double apply(Long aLong) throws Exception {
        //                        return mProgress.get();
        //                    }
        //                })
        //                .filter(new Predicate<Double>() {
        //                    @Override
        //                    public boolean test(Double aDouble) throws Exception {
        //                        return mProgressDialog != null && mProgressDialog.isVisible();
        //                    }
        //                })
        //                .observeOn(AndroidSchedulers.mainThread())
        //                .subscribe(new Consumer<Double>() {
        //                    @Override
        //                    public void accept(Double aDouble) throws Exception {
        //                       // updateProgress((aDouble));
        //                    }
        //                });
        //        mCompositeDisposable.add(mProgressDisposable);

    }

    //    private void updateProgress(double progress) {
    //        if (progress == 1) {
    //            dispose();
    //        }
    //        mProgressDialog.setProgress((int) (progress * 100));
    //     }

    private void updateProgressy(int progress) {
        //        if (progress == 1) {
        //            dispose();
        //        }
        if (progress >= 1 && progress < 101) {
            mProgressDialog.setProgress(progress);
        } else {
            mProgressDialog.setProgress(0);
        }
    }

    //    private void dispose() {
    //        if (mProgressDisposable != null && !mProgressDisposable.isDisposed()) {
    //            mProgressDisposable.dispose();
    //        }
    //    }

    private void addVideo(String videoUrl) {
        HashMap<String, String> map = new HashMap<>();
        map.put("title", getVideoTitle());
        map.put("intro", getDesc());
        map.put("video_url", videoUrl);
        map.put("price", getPrice());
        Disposable disposable = mService.addVideo(map)
                .compose(RxUtils.applySchedulers())
                .subscribe(new Consumer<ObjectResponse<String>>() {
                    @Override
                    public void accept(ObjectResponse<String> response) throws Exception {
                        if (!UploadActivity.this.handleResponse(response)) {
                            mProgressDialog.setTvResult(R.string.upload_successfully);
                            CleanUpInterface();
                        } else {
                            mProgressDialog.setTvResult(R.string.upload_failed);
                        }
                        mProgressDialogDismiss();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        UploadActivity.this.handleFailure(throwable);
                        if (mProgressDialog != null) {
                            mProgressDialog.setTvResult(R.string.upload_failed);
                        }
                        mProgressDialogDismiss();
                    }
                });
        mCompositeDisposable.add(disposable);
    }

    /**
     * 上传成功重置界面
     */
    private void CleanUpInterface() {
        thumbnail.setImageDrawable(null);
        etPrice.setText("");
        etTitle.setText("");
        etDesc.setText("");
    }

    //1.5秒后关闭弹窗
    private void mProgressDialogDismiss() {
        CountDownTimer timer = new CountDownTimer(1500, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                }

            }
        }.start();
    }

    private String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Video.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            LogTrace.d(TAG, "getRealPathFromURI Exception : " + e.toString());
            return "";
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    private boolean validate() {
        if (mVideoUri == null) {
            showToast("请选择要上传的视频");
            return false;
        }
        if (TextUtils.isEmpty(getVideoTitle())) {
            showToast("视频标题不能为空");
            return false;
        }
        if (TextUtils.isEmpty(getPrice())) {
            showToast("视频价格不能为空");
            return false;
        }
        return true;
    }

    private String getPrice() {
        return etPrice.getText().toString();
    }


    public String getVideoTitle() {
        return etTitle.getText().toString();
    }

    public String getDesc() {
        return etDesc.getText().toString();
    }


}
