package com.xbchips.rk.lx.api;

import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.lx.entity.Driver;
import com.xbchips.rk.lx.entity.DriverDetail;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LxService {
    /**
     * 老司机列表
     */
    @GET("video/getDriverDatas")
    Observable<ObjectResponse<List<Driver>>> getDrivers();


    /**
     * 老司机用户收藏的视频列表
     */
    @GET("video/getDriverCollects")
    Observable<ObjectResponse<DriverDetail>> getDriverFavors(@Query("user_id") int userId,
                                                             @Query("page") int page,
                                                             @Query("page_size") int pageSize);
}
