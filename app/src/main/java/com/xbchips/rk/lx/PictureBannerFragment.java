package com.xbchips.rk.lx;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.xbchips.rk.R;
import com.xbchips.rk.activities.HtmlActivity;
import com.xbchips.rk.base.BaseFragment;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.lx.adapter.PhotoPagerAdapter;
import com.xbchips.rk.lx.entity.Picture;
import com.xbchips.rk.lx.widgets.IndexView;
import com.xbchips.rk.lx.widgets.PhotoViewPager;
import com.xbchips.rk.user.activities.UserAuthActivity;
import com.xbchips.rk.user.activities.WapPayActivity;
import com.xbchips.rk.util.LogTrace;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by york on 2017/7/26.
 */

public class PictureBannerFragment extends BaseFragment {
    public static final String ARGS_TITLE = "ARGS_TITLE";
    public static final String ARGS_ID = "ARGS_ID";
    @BindView(R.id.photo_viewPager)
    PhotoViewPager mPhotoViewPager;
    @BindView(R.id.tv_page)
    IndexView mTvPage;
    private int mTotalNum;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_porn_banner, container, false);
        mUnBinder = ButterKnife.bind(this, rootView);
        init();
        return rootView;
    }

    @Override
    protected void init() {
        initTopBar();
        loadData();
    }

    private void loadData() {
        PictureService service = HttpRequest.createService(PictureService.class);
        mCompositeDisposable.add(service.load(getArguments().getString(ARGS_ID))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(response -> !handleResponse(response))
                .subscribe(response -> {
                    Picture data = response.getData();
                    initViewPager(data);
                }, throwable -> {
                    throwable.printStackTrace();
                    handleFailure(throwable);
                })
        );
    }

    private void initViewPager(Picture data) {
        List<String> content = data.getContent();
        if (content == null || content.isEmpty()) return;
        mTotalNum = data.getTotal_num();
        int previewNum = data.getPreview_num();
        PhotoPagerAdapter adapter = new PhotoPagerAdapter(data, this, previewNum);
        adapter.setOnClickListener(mOnClickListener);
        mPhotoViewPager.setAdapter(adapter);
        mPhotoViewPager.addOnPageChangeListener(mOnPageChangeListener);
        mTvPage.setText(String.format(Locale.getDefault(), "1/%d", mTotalNum));
    }

    @Override
    protected void initTopBar() {
        super.initTopBar();
        mBtnBack.setVisibility(View.VISIBLE);
        String title = getArguments().getString(ARGS_TITLE);
        if (title.length() >= 10) {
            title = title.substring(0, 9) + "...";
        }
        mTvTitle.setText(title);
    }

    private View.OnClickListener mOnClickListener = v -> {
        switch (v.getId()) {
            //购买vip
            case R.id.btn_vip:
                payForVip();
                break;
            //广告链接
            case R.id.riv_ad:
                String url = v.getTag(v.getId()).toString();
                Intent intent = new Intent(getContext(), HtmlActivity.class);
                intent.putExtra(HtmlActivity._URL, url);
                startActivity(intent);
                break;
        }

    };

    private void payForVip() {
        if (UserHelper.isLoginState()) {
            startActivity(new Intent(getContext(), WapPayActivity.class));
        } else {
            new AlertDialog.Builder(getActivity())
                    .setMessage(getContext().getString(R.string.login_first_need))
                    .setPositiveButton(getActivity().getString(R.string.ensure), (dialog, which) -> {
                                Intent intent = new Intent(getActivity(), UserAuthActivity.class);
                                intent.putExtra(UserAuthActivity.ARG_IS_LOGIN, true);
                                startActivity(intent);
                            }
                    )
                    .setNegativeButton(getActivity().getString(R.string.cancel), null)
                    .show();
        }
    }

    private ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            LogTrace.d(getClass().getName(), "pos=" + position);
            mTvPage.setText(String.format(Locale.getDefault(), "%d/%d", position + 1, mTotalNum));
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

}
