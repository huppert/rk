package com.xbchips.rk.lx.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.lx.entity.Driver;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DriverRankAdapter extends BaseAdapter<Driver> {


    public DriverRankAdapter(List<Driver> list, Context context) {
        super(list, context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DriverViewHolder(mInflater.inflate(R.layout.item_driver, parent, false));
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        DriverViewHolder holder = (DriverViewHolder) viewHolder;
        Driver driver = mList.get(position);
        holder.tvSeq.setText(String.valueOf(position + 1));
        holder.tvSeq.setTextColor(ContextCompat.getColor(mContext, position < 3 ? R.color.od_item_seq_top : R.color.od_item_seq));
        holder.tvNickname.setText(driver.getNickname());
        holder.tvNickname.setTextColor(ContextCompat.getColor(mContext, position < 3 ? R.color.od_item_nickname_top : R.color.od_item_nickname));
        holder.tvVideoCount.setText(mContext.getString(R.string.star_video_format, driver.getCollect_amount()));
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(mOnClickListener);
        holder.divider.setVisibility(position == getItemCount() - 1 ? View.INVISIBLE : View.VISIBLE);
        ImageLoader.loadAvatar(mContext, driver.getAvatar(), holder.ivAvatar);

    }

    static class DriverViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_seq)
        TextView tvSeq;
        @BindView(R.id.iv_avatar)
        ImageView ivAvatar;
        @BindView(R.id.tv_nickname)
        TextView tvNickname;
        @BindView(R.id.tv_video_count)
        TextView tvVideoCount;
        @BindView(R.id.divider)
        View divider;

        public DriverViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
