package com.xbchips.rk.lx;

import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.base.response.PageResponse;
import com.xbchips.rk.lx.entity.Picture;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by york on 2017/8/4.
 */

public interface PictureService {

    @GET("picture/list")
    Observable<ObjectResponse<PageResponse<Picture>>> getPictures(@Query("page") int page, @Query("page_size") int pageSize);


    @GET("picture/detail/{picture_id}")
    Observable<ObjectResponse<Picture>> load(@Path("picture_id") String pId);
}
