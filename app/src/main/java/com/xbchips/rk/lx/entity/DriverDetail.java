package com.xbchips.rk.lx.entity;

import com.xbchips.rk.base.response.PageResponse;
import com.xbchips.rk.free.entity.VideoItem;

public class DriverDetail extends PageResponse<DriverDetail.ListBean> {
    private int is_vip;
    private int collect_amount;

    public int getIs_vip() {
        return is_vip;
    }

    public void setIs_vip(int is_vip) {
        this.is_vip = is_vip;
    }

    public int getCollect_amount() {
        return collect_amount;
    }

    public void setCollect_amount(int collect_amount) {
        this.collect_amount = collect_amount;
    }

    public static class ListBean extends VideoItem {

        private int collect_count;


        public int getCollect_count() {
            return collect_count;
        }
    }
}
