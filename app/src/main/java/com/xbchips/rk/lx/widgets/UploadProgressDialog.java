package com.xbchips.rk.lx.widgets;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.DialogFragment;

import com.xbchips.rk.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class UploadProgressDialog extends DialogFragment {
    @BindView(R.id.progress)
    CircularProgressBar mProgressBar;
    @BindView(R.id.tv_progress)
    TextView tvProgress;
    Unbinder unbinder;
    @BindView(R.id.tv_result)
    TextView tvResult;
    private OnDismissListener mOnDismissListener;

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        dialog.setCancelable(false);
        Window win = dialog.getWindow();
        win.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent_90)));
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

        WindowManager.LayoutParams params = win.getAttributes();
        params.dimAmount = 0.0f;
        params.gravity = Gravity.CENTER;
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        win.setAttributes(params);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_upload_progress, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    public void setProgress(int progress) {
        mProgressBar.setProgressValue(progress);
        tvProgress.setText(String.valueOf(progress));
    }

    public void setTvResult(@StringRes int result) {
        getDialog().setCancelable(true);
        tvProgress.setVisibility(View.INVISIBLE);
        mProgressBar.setVisibility(View.INVISIBLE);

        tvResult.setText(result);
        tvResult.setVisibility(View.VISIBLE);
        if (mProgressBar != null) {
            mProgressBar.setKeepScreenOn(false);
        }
    }


    @Override
    public void onDestroyView() {
        if (mProgressBar != null) {
            mProgressBar.setKeepScreenOn(false);
        }
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroyView();
    }

    @OnClick({R.id.card_view, R.id.root_view})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.card_view:
                break;
            case R.id.root_view:
                //上传成功或者失败
                if (tvResult.getVisibility() == View.VISIBLE) {
                    dismiss();
                }
                break;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss();
        }
    }


    public interface OnDismissListener {
        void onDismiss();
    }


    public void setOnDismissListener(OnDismissListener onDismissListener) {
        mOnDismissListener = onDismissListener;
    }
}
