package com.xbchips.rk.lx.widgets;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.xbchips.rk.R;
import com.xbchips.rk.user.activities.WapPayActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class NotVipFragment extends DialogFragment {


    private Unbinder mUnbinder;

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        Window win = dialog.getWindow();
        win.setBackgroundDrawable(new ColorDrawable(0x99000000));
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

        WindowManager.LayoutParams params = win.getAttributes();
        params.dimAmount = 0.0f;
        params.gravity = Gravity.BOTTOM;
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        win.setAttributes(params);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_not_vip, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }


    @OnClick({R.id.rb_be_vip, R.id.iv_close})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rb_be_vip:
                startActivity(new Intent(getContext(), WapPayActivity.class));
                break;
            case R.id.iv_close:
                dismiss();
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }
}
