package com.xbchips.rk.lx.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.lx.entity.DriverDetail;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OthersFavorAdapter extends BaseAdapter<DriverDetail.ListBean> {

    private static final int ITEM_LK_MORE = 10;


    public OthersFavorAdapter(List<DriverDetail.ListBean> list, Context context) {
        super(list, context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        switch (viewType) {
            case ITEM_LK_MORE:
                return new MoreViewHolder(mInflater.inflate(R.layout.item_lk_more, parent, false));
            case TYPE_ITEM:
                return new OthersFavorViewHolder(mInflater.inflate(R.layout.item_others_favor, parent, false));
            case TYPE_FOOTER:
                return getFooterViewHolder(parent);
        }
        throw new IllegalArgumentException("无效的viewType");
    }


    @Override
    public int getItemViewType(int position) {
        if (position == mList.size() - 1 && mList.get(position) == null) {
            return ITEM_LK_MORE;
        }
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        int itemViewType = getItemViewType(position);
        if (itemViewType == TYPE_ITEM) {
            OthersFavorViewHolder holder = (OthersFavorViewHolder) viewHolder;
            DriverDetail.ListBean model = mList.get(position);
            ImageLoader.load(mContext, model.getThumb_img_url(), holder.ivCover);
            holder.tvTitle.setText(model.getTitle());
            holder.tvPlayCount.setText(mContext.getString(R.string.times_format, model.getCollect_count()));
            holder.itemView.setTag(position);
            holder.itemView.setOnClickListener(mOnClickListener);
        } else if (itemViewType == ITEM_LK_MORE) {
            MoreViewHolder holder = (MoreViewHolder) viewHolder;
            holder.tvLookMore.setOnClickListener(mOnClickListener);
        } else if (itemViewType == TYPE_FOOTER) {
            FooterViewHolder holder = (FooterViewHolder) viewHolder;
            holder.mProgressBar.setVisibility(isMoreFooter ? View.GONE : View.VISIBLE);
            holder.mTvNoMore.setVisibility(isMoreFooter ? View.VISIBLE : View.GONE);
        }
    }


    static class OthersFavorViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_cover)
        ImageView ivCover;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_play_count)
        TextView tvPlayCount;

        public OthersFavorViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static class MoreViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_look_more)
        TextView tvLookMore;

        public MoreViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

