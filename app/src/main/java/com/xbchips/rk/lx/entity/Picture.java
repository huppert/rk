package com.xbchips.rk.lx.entity;

import java.util.List;

/**
 * Created by york on 2017/8/3.
 */

public class Picture {
    private String id;
    private String title;
    private String thumb;
    private List<String> content;
    private int preview_num;
    private int total_num;
    private String url;
    private String desc;
    private String rank;
    private String img_url;
    private int is_advert;
    private Advert advert;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public int getIs_advert() {
        return is_advert;
    }

    public void setIs_advert(int is_advert) {
        this.is_advert = is_advert;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public List<String> getContent() {
        return content;
    }

    public void setContent(List<String> content) {
        this.content = content;
    }

    public int getPreview_num() {
        return preview_num;
    }

    public void setPreview_num(int preview_num) {
        this.preview_num = preview_num;
    }

    public int getTotal_num() {
        return total_num;
    }

    public void setTotal_num(int total_num) {
        this.total_num = total_num;
    }

    public Advert getAdvert() {
        return advert;
    }

    public void setAdvert(Advert advert) {
        this.advert = advert;
    }

    public static class Advert {
        private String thumb_img_url;
        private int url_type;
        private String url;

        public String getThumb_img_url() {
            return thumb_img_url;
        }

        public void setThumb_img_url(String thumb_img_url) {
            this.thumb_img_url = thumb_img_url;
        }

        public int getUrl_type() {
            return url_type;
        }

        public void setUrl_type(int url_type) {
            this.url_type = url_type;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
