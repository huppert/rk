package com.xbchips.rk.lx.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.lx.entity.Picture;
import com.xbchips.rk.util.DisplayUtils;
import com.xbchips.rk.widgets.ImageLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by york on 2017/7/26.
 */

public class PictureListAdapter extends BaseAdapter<Picture> {

    private int width, height, height_ads;

    public PictureListAdapter(List<Picture> list, Context context) {
        super(list, context);
        width = DisplayUtils.getScreenW(context);
        height = width / 4 * 3;
        height_ads = height / 2;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return viewType == TYPE_ITEM ?
                new LxItemViewHolder(mInflater.inflate(R.layout.item_lx, parent, false)) :
                getFooterViewHolder(parent);
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        LxItemViewHolder viewHolder = (LxItemViewHolder) holder;
        Picture picture = mList.get(position);
        viewHolder.mImageLayout.setTag(picture);
        viewHolder.mImageLayout.setOnClickListener(mOnClickListener);
        if (picture.getIs_advert() == 1) {
            viewHolder.mImageLayout.setCoverWidthAndHeight(width, height_ads);
            Glide.with(mContext).load(picture.getImg_url()).into(viewHolder.mImageLayout.getCoverImage());
            viewHolder.mTvTitle.setText(picture.getDesc());
        } else {
            viewHolder.mTvTitle.setText(picture.getTitle());
            viewHolder.mImageLayout.setCoverWidthAndHeight(width, height);
            Glide.with(mContext).load(picture.getThumb()).into(viewHolder.mImageLayout.getCoverImage());
        }
    }

    static class LxItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_layout)
        ImageLayout mImageLayout;
        @BindView(R.id.tv_title)
        TextView mTvTitle;

        public LxItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
