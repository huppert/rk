package com.xbchips.rk.lx.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.lx.adapter.DriverRankAdapter;
import com.xbchips.rk.lx.api.LxService;
import com.xbchips.rk.lx.entity.Driver;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;

public class OldDriverActivity extends BaseActivity {

    @BindView(R.id.ranking_recycler_view)
    RecyclerView rankingRecyclerView;
    @BindView(R.id.advertise_recycler_view)
    RecyclerView advertiseRecyclerView;
    private final LxService mLxService = HttpRequest.createService(LxService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_old_driver);
        ButterKnife.bind(this);
        initTopBar();
        loadData();
    }


    @Override
    protected void initTopBar() {
        super.initTopBar();
        mTvTitle.setText(R.string.old_driver_ranking);
    }

    private void initRankRecyclerView(List<Driver> list) {
        rankingRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        DriverRankAdapter adapter = new DriverRankAdapter(list, this);
        adapter.setOnClickListener(v -> {
            int position = (int) v.getTag();
            Driver driver = list.get(position);
            Intent intent = new Intent(this, DriverDetailActivity.class);
            intent.putExtra(DriverDetailActivity.ARG_ID, driver.getUser_id());
            intent.putExtra(DriverDetailActivity.ARG_NAME, driver.getNickname());
            startActivity(intent);
        });
        rankingRecyclerView.setAdapter(adapter);
    }


    private void loadData() {
        Disposable disposable = mLxService.getDrivers()
                .compose(RxUtils.applySchedulers())
                .filter(resp -> !handleResponse(resp))
                .subscribe(response -> {
                    initRankRecyclerView(response.getData());
                }, this::handleFailure);
        mCompositeDisposable.add(disposable);
    }
}
