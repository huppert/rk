package com.xbchips.rk.lx;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AlertDialog;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseView2;
import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.lx.activities.TutorialsActivity;
import com.xbchips.rk.user.activities.WapPayActivity;
import com.xbchips.rk.video.VideoDetailActivity;
import com.xbchips.rk.video.VideoService;
import com.xbchips.rk.video.widgets.PurchaseWindow;

import java.util.HashMap;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class JudgeLxPresenter {

    private Context mContext;
    private BaseView2 mBaseView;

    public JudgeLxPresenter(Context context, BaseView2 baseView) {
        mContext = context;
        mBaseView = baseView;
    }

    private VideoService mVideoService = HttpRequest.createService(VideoService.class);
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    private void buyVideo(VideoItem video) {
        HashMap<String, String> map = new HashMap<>();
        map.put("video_id", video.getId());
        Disposable disposable = mVideoService.buyVideo(map)
                .compose(RxUtils.applySchedulers())
                .filter(response -> !mBaseView.handleResponse(response))
                .subscribe(response -> {
                    toDetailActivity(video);
                    mBaseView.showToast("购买成功");
                }, t -> mBaseView.handleFailure(t));
        mCompositeDisposable.add(disposable);
    }

    /**
     * 判断是否购买过
     */
    public void judge(VideoItem video) {
        mBaseView.showProgress(null);
        Disposable disposable = mVideoService.judge(video.getId())
                .compose(RxUtils.applySchedulers())
                .subscribe(response -> {
                    mBaseView.dismissDialog();
                    if (response.getStatus() == 0) {
                        toDetailActivity(video);
                    } else {
                        if (UserHelper.getLocalUser().getVip().is_vip()) {
                            showPurChaseWindow(video);
                        } else {
                            new AlertDialog.Builder(mContext)
                                    .setTitle("提示")
                                    .setMessage("充值VIP，享受更多免费福利")
                                    .setPositiveButton("确定", (dialog, which) -> mContext.startActivity(new Intent(mContext, WapPayActivity.class)))
                                    .show();
                        }
                    }
                }, t -> mBaseView.handleFailure(t));
        mCompositeDisposable.add(disposable);
    }


    private void toDetailActivity(VideoItem video) {
        Intent intent = new Intent(mContext, VideoDetailActivity.class);
        intent.putExtra(VideoDetailActivity.ARG_VIDEO_ID, video.getId());
        intent.putExtra(VideoDetailActivity.ARG_VIDEO_TITLE, video.getTitle());
        intent.putExtra(VideoDetailActivity.ARG_VIDEO_ISXIANGYOU, "xiangyou");
        mContext.startActivity(intent);
    }

    private void showPurChaseWindow(VideoItem video) {
        PurchaseWindow window = new PurchaseWindow(mContext);
        window.setListener(v1 -> {
            switch (v1.getId()) {
                case R.id.rb_confirm:
                    window.dismiss();
                    buyVideo(video);
                    break;
                case R.id.tv_tutorials:
                    window.dismiss();
                    mContext.startActivity(new Intent(mContext, TutorialsActivity.class));
                    break;
                case R.id.cl_content:
                    break;
                case R.id.root:
                    window.dismiss();
                    break;
            }
        });
        window.show(video);
    }

    public void clear() {
        mCompositeDisposable.clear();
    }
}
