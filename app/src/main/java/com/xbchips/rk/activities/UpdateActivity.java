package com.xbchips.rk.activities;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.base.RespHandler;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.system.SystemService;
import com.xbchips.rk.system.UpGradeDialog;
import com.xbchips.rk.system.entity.VersionInfo;
import com.xbchips.rk.util.PopUtil;
import com.xbchips.rk.util.UuidUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class UpdateActivity extends BaseActivity {
    @BindView(R.id.tv_version)
    TextView tvVersion;
    private RelativeLayout rlSystemsettings;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        ButterKnife.bind(this);
        initTopBar();
        init();
    }

    @Override
    protected void initTopBar() {
        mBtnBack = findViewById(R.id.btn_back);
        mTvTitle = findViewById(R.id.bar_title);
        rlSystemsettings = findViewById(R.id.rl_systemsettings);
        if (mBtnBack != null) {
            mBtnBack.setVisibility(View.VISIBLE);
            mBtnBack.setOnClickListener(v -> finish());
            mBtnBack.setImageResource(R.drawable.icon_toolbar_back);

        }
        mTvTitle.setText("版本更新");
    }

    private void init() {
        try {
            String versionName = UuidUtil.getVersionName(this);
            tvVersion.setText("当前版本:" + versionName);
            rlSystemsettings.setOnClickListener(v -> checkVersion());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * APP升级
     */
    private void checkVersion() {
        Disposable disposable = HttpRequest.createService(SystemService.class).checkVersion()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    VersionInfo mVersion = response.getData();

                    if (response.getStatus() == RespHandler.SUCCESS_ONE && mVersion.isUpdate()) {
                        showToast("请求失败");
                    } else if (response.getStatus() == RespHandler.SUCCESS_CODE && mVersion.isUpdate()) {
                        if (mVersion.isUpdate() == true) {
                            //非强制升级
                            PopUtil popUtil = new PopUtil(this, R.layout.version_update, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, rlSystemsettings, Gravity.CENTER, 0, 0, 1
                                    , builder -> {
                                TextView mVersionnumber = builder.getView(R.id.versionnumber);
                                TextView mUpdateimmed = builder.getView(R.id.tv_updateimmediately);
                                ImageView mUpdateCancel = builder.getView(R.id.tv_update_info_cancel);
                                mVersionnumber.setText("新版本" + mVersion.getInfo().getVersion_code());
                                mUpdateimmed.setOnClickListener(v -> {
                                    UpGradeDialog dialogl = new UpGradeDialog(UpdateActivity.this);
                                    dialogl.update(mVersion.getInfo());
                                    builder.dismiss();

                                });
                                mUpdateCancel.setOnClickListener(v -> builder.dismiss());
                            });
                        }


                    } else {
                        showToast("目前为最新版本...");
                    }
                });
        mCompositeDisposable.add(disposable);

    }
}
