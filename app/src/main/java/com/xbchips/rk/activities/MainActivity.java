package com.xbchips.rk.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.xbchips.rk.BuildConfig;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.base.RespHandler;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.common.FragmentAdapter;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.common.UserHelper;
import com.xbchips.rk.free.FreeFragment;
import com.xbchips.rk.lx.LxFragment;
import com.xbchips.rk.profile.ProfileFragment;
import com.xbchips.rk.system.NoticeActivity;
import com.xbchips.rk.system.SystemService;
import com.xbchips.rk.system.entity.AdvertList;
import com.xbchips.rk.system.entity.AnnounNotice;
import com.xbchips.rk.system.entity.Notice;
import com.xbchips.rk.system.entity.VersionInfo;
import com.xbchips.rk.user.entity.User;
import com.xbchips.rk.util.LogTrace;
import com.xbchips.rk.util.PopUtil;
import com.xbchips.rk.util.PopUtilto;
import com.xbchips.rk.util.PreferenceUtils;
import com.xbchips.rk.util.StatusBarUtil;
import com.xbchips.rk.vip.view.VipFragment;
import com.xbchips.rk.widgets.NewVersionDialogFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.xbchips.rk.activities.HtmlActivity.ARG_TITLE;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";
    @BindView(R.id.bottom_tabs)
    TabLayout mBottomTabs;
    private static final int CODE = 200;
    private SystemService appService;
    private ImageView iv_footer_advert;
    private ImageView iv_close;
    private RelativeLayout rl_footer_advert;
    private AnnounNotice annpunData;
    PopUtilto utilto;
    PopUtil util;
    PopUtilto.PopBuilderto popBuilderto;
    public static final String _URL = "url";
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            mBottomTabs.post(() -> performTab());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
        StatusBarUtil.setImmersiveStatusBar(MainActivity.this, BuildConfig.SKIN_MODE);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        IntentFilter intentFilter = new IntentFilter(Constants.ACTION_PERFORM_TAB);
        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, intentFilter);
        appService = HttpRequest.createService(SystemService.class);
        initTab();
        initFragments();

        initView();
        showNotice();
        getConfigs();
        getFooterAdvert();
        checkVersion();

        if (UserHelper.getLocalUser() != null) {
            mBottomTabs.post(() -> performTab());
        }
    }

    private void initView() {
        iv_close = findViewById(R.id.iv_close);
        iv_close.setOnClickListener(this);
        rl_footer_advert = findViewById(R.id.rl_footer_advert);
        iv_footer_advert = findViewById(R.id.iv_footer_advert);
        iv_footer_advert.setOnClickListener(this);
        rl_footer_advert.post(this::popu);
    }

    List<AdvertList> data;

    /**
     * ty  asd  asdasdasd
     * APP底部全站广告
     */
    private void getFooterAdvert() {
        Disposable disposable = HttpRequest.createService(SystemService.class).getAdvertList(Constants.APP_FOOTER)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(response -> response.getStatus() == 0)
                .subscribe(response -> {
                    data = response.getData();
                    setFooterAdvert();
                }, throwable -> LogTrace.i(TAG, "获取APP底部全站广告失败=" + throwable.getMessage()));
        mCompositeDisposable.add(disposable);
    }

    private void setFooterAdvert() {
        LogTrace.i(TAG, "img_url=" + data.get(0).getImg_url());
        rl_footer_advert.setVisibility(View.VISIBLE);
        if (data.size() != 0) {
            ImageLoader.load(this, data.get(0).getImg_url(), iv_footer_advert, new RequestOptions()
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .dontAnimate());
        }
    }


    private void initFragments() {
        Fragment[] fragments = {
                new FreeFragment(),
                new VipFragment(),
                new LxFragment(),
                new ProfileFragment()
        };
        final FragmentAdapter fragmentAdapter = new FragmentAdapter(getSupportFragmentManager(), fragments, R.id.fl_content);
        mBottomTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                fragmentAdapter.showFragment(position);
                if (BuildConfig.SKIN_MODE) {
                    if (position == 2) {
                        getWindow().setStatusBarColor(ContextCompat.getColor(MainActivity.this, R.color.lx_tool_layout_bg));
                    } else if (position == 3) {
                        getWindow().setStatusBarColor(ContextCompat.getColor(MainActivity.this, R.color.pf_top_color));
                    } else {
                        getWindow().setStatusBarColor(ContextCompat.getColor(MainActivity.this, R.color.main_status_bar));
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void initTab() {
        int[] icons = {R.drawable.selector_tab_01,
                R.drawable.selector_tab_02,
                R.drawable.selector_tab_03,
                R.drawable.selector_tab_04
        };
        int[] titles = {
                R.string.free_trial,
                R.string.vip_area,
                R.string.liuxiang,
                R.string.mine
        };
        for (int i = 0; i < titles.length; i++) {
            TabLayout.Tab tab = mBottomTabs.newTab()
                    .setCustomView(R.layout.custom_tab)
                    .setText(titles[i])
                    .setIcon(icons[i]);
            mBottomTabs.addTab(tab);
        }
    }

    public void performTab() {
        User localUser = UserHelper.getLocalUser();
        if (localUser != null) {
            mBottomTabs.getTabAt(1).select();
        } else {
            mBottomTabs.getTabAt(0).select();
        }

    }


    private void showNotice() {
        mCompositeDisposable.add(appService.notice()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(response -> response.getStatus() == RespHandler.SUCCESS_CODE)
                .subscribe(response -> {
                    Notice data = response.getData();
                    Intent intent = new Intent(MainActivity.this, NoticeActivity.class);
                    intent.putExtra(NoticeActivity._URL, data.getUrl());
                    intent.putExtra(NoticeActivity._CLOSEABLE, !data.isForce());
                    startActivity(intent);
                }, throwable -> LogTrace.d(TAG, "accept: " + throwable.getMessage()))
        );
    }

    private void getConfigs() {
        mCompositeDisposable.add(appService.getConfigs()
                .subscribeOn(Schedulers.io())
                .filter(response -> response.getStatus() == RespHandler.SUCCESS_CODE)
                .subscribe(response -> PreferenceUtils.setString(
                        MainActivity.this.getApplicationContext(),
                        Constants.PREF_CONFIG,
                        new Gson().toJson(response.getData())), throwable -> {
                }));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        Glide.get(this).clearMemory();
    }

    @Override
    public void onBackPressed() {
        //如果当前显示的fragment有回退栈，由回退栈消费此事件
        int selectedTabPosition = mBottomTabs.getSelectedTabPosition();
        Fragment currentFragment = getSupportFragmentManager().getFragments().get(selectedTabPosition);
        FragmentManager childFragmentManager = currentFragment.getChildFragmentManager();
        if (childFragmentManager.getBackStackEntryCount() > 0) {
            childFragmentManager.popBackStack();
            return;
        }
        super.onBackPressed();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_close:
                rl_footer_advert.setVisibility(View.GONE);
                break;
            case R.id.iv_footer_advert:
                Intent intent = new Intent(this, HtmlActivity.class);
                AdvertList advertList = data.get(0);
                intent.putExtra(HtmlActivity._URL, advertList.getUrl());
                intent.putExtra(ARG_TITLE, advertList.getTitle());
                startActivity(intent);
                break;
        }
    }

    private void popu() {
        mCompositeDisposable.add(appService.Annnotice()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(response -> response.getStatus() == RespHandler.SUCCESS_CODE)
                .subscribe(response -> {
                    annpunData = response.getData();
                    if (annpunData != null) {
                        utilto = new PopUtilto(this, R.layout.item_announcement, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, rl_footer_advert, Gravity.CENTER, 0, 0
                                , builder -> {
                            TextView content = builder.getView(R.id.tv_content);
                            ImageView mUpdateInfo = builder.getView(R.id.tv_update_info_cancel);
                            ImageView url = builder.getView(R.id.iv_url);
                            TextView determine = builder.getView(R.id.tv_determine);
                            Glide.with(MainActivity.this).load(annpunData.getImg_url()).into(url);
                            content.setText(annpunData.getContent());
                            popBuilderto = popBuilderto(builder);
                            mUpdateInfo.setOnClickListener(v -> {
                                builder.dismiss();
                                utilto = null;
                            });
                            determine.setOnClickListener(v -> {
                                Intent intent = new Intent(MainActivity.this, HtmlActivity.class);
                                intent.putExtra(_URL, annpunData.getJump_link());
                                startActivity(intent);
                                builder.dismiss();
                                utilto = null;
                            });
                        });

                    }
                }, throwable -> LogTrace.d(TAG, "accept: " + throwable.getMessage()))
        );
    }


    /**
     * APP升级
     */
    private void checkVersion() {
        Disposable disposable = HttpRequest.createService(SystemService.class).checkVersion()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    VersionInfo mVersion = response.getData();
                    if (response.getStatus() == RespHandler.SUCCESS_ONE && mVersion.isUpdate()) {
                        showToast("请求失败");
                    } else if (response.getStatus() == RespHandler.SUCCESS_CODE && mVersion.isUpdate()) {
                        NewVersionDialogFragment fragment = new NewVersionDialogFragment();
                        fragment.setVersionInfo(mVersion);
                        fragment.show(getSupportFragmentManager(), "");

                    }
                });
        mCompositeDisposable.add(disposable);

    }

    public PopUtilto.PopBuilderto popBuilderto(PopUtilto.PopBuilderto popBuilderto) {
        return popBuilderto;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            if (util != null || utilto != null) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return super.dispatchTouchEvent(ev);
    }
}
