package com.xbchips.rk.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.Group;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.base.RespHandler;
import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.base.response.PageResponse;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.free.entity.HotSearch;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.lx.JudgeLxPresenter;
import com.xbchips.rk.util.DisplayUtils;
import com.xbchips.rk.util.PreferenceUtils;
import com.xbchips.rk.video.VideoDetailActivity;
import com.xbchips.rk.vip.adapter.HotSearchAdapter;
import com.xbchips.rk.vip.adapter.SearchHistoryAdapter;
import com.xbchips.rk.vip.adapter.SearchResultAdapter;
import com.xbchips.rk.vip.service.VipService;
import com.xbchips.rk.widgets.EndlessRecyclerView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

import static com.xbchips.rk.video.VideoDetailActivity.ARG_VIDEO_ID;
import static com.xbchips.rk.video.VideoDetailActivity.ARG_VIDEO_TITLE;

public class SearchActivity extends BaseActivity {

    private EditText mContentEditText;
    private TextView mCancelTextView;
    private ImageView mDeleteHistoryImageView;
    private RecyclerView mHistoryRecyclerView;
    private RecyclerView mHotRecyclerView;
    private EndlessRecyclerView mResultRecyclerView;
    private SearchHistoryAdapter searchHistoryAdapter;
    private HotSearchAdapter hotSearchAdapter;
    private RespHandler mRespHandler;
    private Group historyGroup;
    private VipService vipService;
    private SearchResultAdapter searchResultAdapter;
    private ImageView clearSearchImageView;
    private List<VideoItem> mList;
    private TextView recommendTextView;
    private List<VideoItem> listBeans;
    private JudgeLxPresenter mLxPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.se_bg));
        mLxPresenter = new JudgeLxPresenter(this, this);
        mRespHandler = new RespHandler(this);
        listBeans = new ArrayList<>();
        mList = new ArrayList<>();
        recommendTextView = new TextView(this);
        recommendTextView.setTextColor(Color.WHITE);
        int dp_15 = DisplayUtils.dip2px(this, 15);
        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        marginLayoutParams.leftMargin = dp_15;
        marginLayoutParams.topMargin = dp_15;
        recommendTextView.setLayoutParams(marginLayoutParams);
        historyGroup = findViewById(R.id.group_search);
        mContentEditText = findViewById(R.id.et_search_content);
        mCancelTextView = findViewById(R.id.tv_search_cancel);
        mDeleteHistoryImageView = findViewById(R.id.iv_search_delete_history);
        mHistoryRecyclerView = findViewById(R.id.recycler_search_history);
        mHotRecyclerView = findViewById(R.id.recycler_hot);
        mResultRecyclerView = findViewById(R.id.recycler_result);
        searchHistoryAdapter = new SearchHistoryAdapter(this);
        clearSearchImageView = findViewById(R.id.iv_clear_search);
        mHistoryRecyclerView.setLayoutManager(new NoScrollLayoutManager(this));
        mHotRecyclerView.setLayoutManager(new NoScrollLayoutManager(this));
        searchResultAdapter = new SearchResultAdapter(mList, this);
        searchResultAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getTag() == null) return;
                VideoItem videoItem = (VideoItem) v.getTag();
                if (videoItem.isIs_liuxiang()) {
                    mLxPresenter.judge(videoItem);
                } else {
                    Intent intent = new Intent(SearchActivity.this, VideoDetailActivity.class);
                    intent.putExtra(ARG_VIDEO_TITLE, videoItem.getTitle());
                    intent.putExtra(ARG_VIDEO_ID, videoItem.getId());
                    intent.putExtra("isFromSearch", "1");
                    startActivity(intent);
                }
            }
        });
        mResultRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mResultRecyclerView.setAdapter(searchResultAdapter);
        mResultRecyclerView.setOnLoadMoreListener(() -> {
            searchResultAdapter.addFooter();
            searchData(mResultRecyclerView.getCurPage(), searchHistoryAdapter.getContentList().get(0), false);
        });
        mContentEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
                    search(mContentEditText.getText().toString());
                    return true;
                }
                return false;
            }
        });
        clearSearchImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContentEditText.setText("");
                searchResultAdapter.removeHeader();
                searchResultAdapter.clearData();
                mResultRecyclerView.setVisibility(View.GONE);
            }
        });
        mContentEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    clearSearchImageView.setVisibility(View.VISIBLE);
                } else {
                    clearSearchImageView.setVisibility(View.GONE);
                }
            }
        });
        mCancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mDeleteHistoryImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchHistoryAdapter.clearData();
                PreferenceUtils.setString(SearchActivity.this, Constants.SEARCH_HISTORY, "");
                historyGroup.setVisibility(View.GONE);
            }
        });

        String historyJson = PreferenceUtils.getString(this, Constants.SEARCH_HISTORY);
        List<String> searchList = new Gson().fromJson(historyJson, new TypeToken<List<String>>() {
        }.getType());
        if (searchList == null || searchList.size() == 0) {
            historyGroup.setVisibility(View.GONE);
        }
        searchHistoryAdapter = new SearchHistoryAdapter(this);
        searchHistoryAdapter.addDataList(searchList);
        searchHistoryAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data = v.getTag().toString();
                mContentEditText.setText(data);
                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
                search(data);
            }
        });
        mHistoryRecyclerView.setAdapter(searchHistoryAdapter);
        hotSearchAdapter = new HotSearchAdapter(this);
        hotSearchAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VideoItem bean = (VideoItem) v.getTag();
                if (bean.isIs_liuxiang()) {
                    mLxPresenter.judge(bean);
                } else {
                    Intent intent = new Intent(SearchActivity.this, VideoDetailActivity.class);
                    intent.putExtra(ARG_VIDEO_TITLE, bean.getTitle());
                    intent.putExtra(ARG_VIDEO_ID, bean.getId());
                    startActivity(intent);
                }

            }
        });
        mHotRecyclerView.setAdapter(hotSearchAdapter);
        vipService = HttpRequest.createService(VipService.class);
        Disposable disposable = vipService.getHotSearch().compose(RxUtils.applySchedulers()).subscribe(new Consumer<ObjectResponse<HotSearch>>() {
            @Override
            public void accept(ObjectResponse<HotSearch> hotSearchObjectResponse) throws Exception {
                if (!mRespHandler.handleResponse(hotSearchObjectResponse)) {
                    listBeans.clear();
                    hotSearchAdapter.addDataList(hotSearchObjectResponse.getData().getList());
                    listBeans.addAll(hotSearchObjectResponse.getData().getList());
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                Toast.makeText(SearchActivity.this, "获取热门搜索失败", Toast.LENGTH_SHORT).show();
            }
        });
        mCompositeDisposable.add(disposable);
    }

    private void search(String content) {
        historyGroup.setVisibility(View.VISIBLE);
        searchHistoryAdapter.addData(content);
        mResultRecyclerView.setVisibility(View.VISIBLE);
        searchData(1, content, true);
    }

    private void searchData(int pageIndex, String content, boolean clearData) {
        mCompositeDisposable.add(vipService.searchData(pageIndex, 10, content).compose(RxUtils.applySchedulers()).subscribe(new Consumer<ObjectResponse<PageResponse<VideoItem>>>() {
            @Override
            public void accept(ObjectResponse<PageResponse<VideoItem>> response) throws Exception {
                mResultRecyclerView.setLoading(false);
                if (!mRespHandler.handleResponse(response)) {
                    PageResponse<VideoItem> data = response.getData();
                    List<VideoItem> list = data.getList();
                    mResultRecyclerView.setPageCount(data.getPage().getTotal());
                    if (clearData) {
                        mList.clear();
                        if (list.size() == 0) {
                            for (VideoItem h : listBeans) {
                                VideoItem videoItem = new VideoItem();
                                videoItem.setTitle(h.getTitle());
                                videoItem.setId(h.getId());
                                videoItem.setThumb_img_url(h.getThumb_img_url());
                                list.add(videoItem);
                            }
                            recommendTextView.setText("抱歉没有找到\"" + searchHistoryAdapter.getContentList().get(0) + "\"相关视频\n\n为您推荐：");
                            searchResultAdapter.setHeaderView(recommendTextView);
                        }
                    }
                    mList.addAll(list);
                    searchResultAdapter.removeFooter();
                    if (mResultRecyclerView.isLastPage()) {
                        searchResultAdapter.addNoMoreView();
                    }
                    searchResultAdapter.notifyItemRangeChanged(0, searchResultAdapter.getItemCount() - 1);
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                Toast.makeText(SearchActivity.this, "加载失败", Toast.LENGTH_SHORT).show();
                mResultRecyclerView.setLoading(false);
            }
        }));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        List<String> list = searchHistoryAdapter.getContentList();
        String json = new Gson().toJson(list);
        PreferenceUtils.setString(this, Constants.SEARCH_HISTORY, json);
    }

    private static class NoScrollLayoutManager extends LinearLayoutManager {


        private NoScrollLayoutManager(Context context) {
            super(context);
        }

        @Override
        public boolean canScrollVertically() {
            return false;
        }
    }
}
