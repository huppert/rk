package com.xbchips.rk.activities;

import android.Manifest;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseActivity;
import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.common.SystemDomain;
import com.xbchips.rk.system.SystemService;
import com.xbchips.rk.system.entity.AdvertList;
import com.xbchips.rk.system.entity.CloudDomain;
import com.xbchips.rk.user.UserPresenter;
import com.xbchips.rk.user.UserView;
import com.xbchips.rk.util.EncryptUtils;
import com.xbchips.rk.util.LogTrace;
import com.xbchips.rk.util.PreferenceUtils;
import com.xbchips.rk.widgets.AppSettingsDialog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

import static com.xbchips.rk.activities.HtmlActivity.ARG_TITLE;

@RuntimePermissions
public class SplashActivity extends BaseActivity implements UserView, View.OnClickListener {
    private static final String TAG = "SplashActivity";
    private static final int CODE = 200;
    private UserPresenter userPresenter;
    /**
     * 是否自动跳到首页，点击后不跳转
     */
    private boolean autoToMain = true;
    private ImageView ivSplash;
    private LinearLayout ll_skip;
    private TextView tv_ss;


    /**
     * 确认年满18周岁
     */
    private boolean isConsent;


    /**
     * 云端域名列表
     */
    private static final String[] CLOULD_DOMAINS = {
//            "https://url.nos-eastchina1.126.net/rrrpqdykmesztljocfb",
//            "https://yum-app.oss-cn-zhangjiakou.aliyuncs.com/rrrpqdykmesztljocfb"
            "https://url.nos-eastchina1.126.net/lllpqdykmesztljodfc",
            "https://yum-app.oss-cn-zhangjiakou.aliyuncs.com/lllpqdykmesztljodfc"

    };

    private static final String[] DEBUG_CLOULD_DOMAINS = {
            "http://url.nos-eastchina1.126.net/rrrpqdykmesztljocfb123xxx",
            "http://yum-app.oss-cn-zhangjiakou.aliyuncs.com/rrrpqdykmesztljocfb123xxx"
//            "https://url.nos-eastchina1.126.net/lllpqdykmesztljodfc",
//            "https://yum-app.oss-cn-zhangjiakou.aliyuncs.com/lllpqdykmesztljodfc"
    };
    //    private static final String KEY = "epduczbnfhkqvogt";
//    private static final String IV = "5689120374737560";
    private String KEY = "uhcacrfgeshlkooj";
    private String IV = "9573933390553241";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        initView();
        SplashActivityPermissionsDispatcher.getFromRemoteWithPermissionCheck(this);
    }

    private void initView() {
        ivSplash = (ImageView) findViewById(R.id.iv_splash);
        ivSplash.setOnClickListener(this);
        ivSplash.setEnabled(false);
        ll_skip = (LinearLayout) findViewById(R.id.ll_skip);
        ll_skip.setOnClickListener(this);
        tv_ss = (TextView) findViewById(R.id.tv_ss);
    }


    /**
     * 弹出未成年警告
     */
    private void showJuvenileWarn() {
        boolean aBoolean = PreferenceUtils.getBoolean(this, Constants.IS_ADULT);
        if (aBoolean) {
            isConsent = true;
            //            checkVersion();
            switchAdvert();
        } else {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.juvenile_warn_title))
                    .setMessage(getString(R.string.juvenile_warn))
                    .setPositiveButton(getString(R.string.adult), (dialog, which) -> {
                        isConsent = true;
                        PreferenceUtils.setBoolean(SplashActivity.this, Constants.IS_ADULT, true);
                        //                        checkVersion();
                        switchAdvert();
                    }).setNegativeButton(getString(R.string.leave), null).show();
        }
    }

    List<AdvertList> data;

    private void getAdvertList() {
        Disposable disposable = HttpRequest.createService(SystemService.class).getAdvertList(Constants.APP_START)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    data = response.getData();
                    showJuvenileWarn();
                }, throwable -> {
                    Log.i(TAG, "获取广告列表失败=" + throwable.getMessage());
                    showJuvenileWarn();
                });
        mCompositeDisposable.add(disposable);
    }


    //广告图url
    private List<String> advertList;

    /**
     * 切换广告图
     */
    private void switchAdvert() {
        advertList = new ArrayList<>();
        if (data != null && data.size() > 0) {
            ll_skip.setVisibility(View.VISIBLE);
            for (int i = 0; i < data.size(); i++) {
                advertList.add(data.get(i).getImg_url());
            }
            countDownHandler.postDelayed(countDownRunnable, 0);
            Glide.with(getApplicationContext())
                    .load(advertList.get(0))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            count++;
                            mHandler.postDelayed(mRunnable, 1000);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            count++;
                            mHandler.postDelayed(mRunnable, 1000);

                            return false;
                        }
                    })
                    .into(ivSplash);
        } else {
            next();
        }
    }


    /**
     * 切换图片
     */
    int count;
    Handler mHandler = new Handler();
    Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (count < advertList.size()) {
                Glide.with(SplashActivity.this)
                        .load(advertList.get(count))
                        .into(ivSplash);
            }
        }
    };

    /**
     * 倒计时
     */
    int countDown = 5;
    Handler countDownHandler = new Handler();
    Runnable countDownRunnable = new Runnable() {
        @Override
        public void run() {
            if (countDown > 0) {
                tv_ss.setText(countDown + "S");
                countDown--;
                countDownHandler.postDelayed(countDownRunnable, 1 * 1000);
            } else {
                countDownHandler.removeCallbacksAndMessages(null);
                next();
            }

        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        SplashActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }


    /**
     * 从cdn取域名
     */
    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void getFromRemote() {
        List<String> list = Arrays.asList(CLOULD_DOMAINS);
        Collections.shuffle(list);
        Disposable disposable = Observable.fromIterable(list)
                .map(s -> {
                    Response response = httpGet(s);
                    if (response != null && response.isSuccessful() && response.body() != null) {
                        return decryptDomains(response.body().string());
                    }
                    return "";
                })
                .compose(RxUtils.applySchedulers())
                .filter(s -> s.length() > 0)
                .first("")
                .subscribe(s -> {
                    if (TextUtils.isEmpty(s)) {
                        showErrorMsg(getString(R.string.bad_connection));
                    } else {
                        CloudDomain domain = new Gson().fromJson(s, CloudDomain.class);
                        SystemDomain.server_domain = domain.getService().get(0);
                        LogTrace.d(TAG, "getFromRemote: " + SystemDomain.server_domain);
                        PreferenceUtils.setString(SplashActivity.this, Constants.VIDEO_DOMAINS, s);
                        PreferenceUtils.setBoolean(SplashActivity.this, Constants.isUpdata, true);
                        userPresenter = new UserPresenter(this);
                        userPresenter.checkUser();
                    }

                }, throwable -> showErrorMsg(getString(R.string.bad_connection)));

        mCompositeDisposable.add(disposable);
    }


    private void showErrorMsg(String msg) {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setMessage(msg)
                .setPositiveButton(R.string.reload, (dialog, which) -> ActivityCompat.requestPermissions
                        (SplashActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123))
                .setNegativeButton(R.string.exit, (dialog, which) -> finish())
                .show();
    }


    private String decryptDomains(String origin) {
        byte[] bytes = EncryptUtils.decryptBase64AES(origin.getBytes(), KEY.getBytes(), "AES/CBC/PKCS5Padding", IV.getBytes());
        LogTrace.d(TAG, "decryptDomains: " + new String(bytes));
        return new String(bytes);
    }

    /**
     * 检查用户信息成功
     */
    @Override
    public void checkSuccess() {
        ivSplash.setEnabled(true);
        getAdvertList();
    }

    /**
     * 检查用户信息失败
     */
    @Override
    public void failed(Throwable throwable) {
        showErrorMsg(mRespHandler.getErrorMsg(throwable));
    }

    private void next() {
        if (autoToMain) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
            autoToMain = false;
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_skip:
                next();
                break;
            case R.id.iv_splash:
                if (count == 0) {
                    if (!isConsent) {
                        showJuvenileWarn();
                    }
                } else {
                    autoToMain = false;
                    Intent intent = new Intent(this, HtmlActivity.class);
                    AdvertList advertList = data.get(count - 1);
                    intent.putExtra(HtmlActivity._URL, advertList.getUrl());
                    intent.putExtra(ARG_TITLE, advertList.getTitle());
                    startActivityForResult(intent, CODE);
                }
                break;
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE) {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            finish();
        }
    }


    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showRationaleForStorage(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.request_permission)
                .setMessage(R.string.permission_storage_rationale)
                .setPositiveButton(R.string.allow, (dialog, which) -> request.proceed())
                .setNegativeButton(R.string.deny, (dialog, which) -> request.cancel())
                .show();
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showDeniedForStorage() {
        finish();
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showNeverAskForStorage() {
        new AppSettingsDialog.Builder(this)
                .setRationale(R.string.storage_permission)
                .setNegativeButton(R.string.cancel, (dialog, which) -> finish())
                .build()
                .show();
    }


    private OkHttpClient httpClient = new OkHttpClient.Builder().build();

    private Response httpGet(String url) {
        try {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Call call = httpClient.newCall(request);
            return call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ivSplash.setImageBitmap(null);
        if (userPresenter != null) {
            userPresenter.unSubscribe();
        }
    }


}
