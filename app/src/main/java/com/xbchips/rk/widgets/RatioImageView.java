package com.xbchips.rk.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

import com.xbchips.rk.R;

/**
 * Created by york on 2017/9/25.
 */

public class RatioImageView extends androidx.appcompat.widget.AppCompatImageView {
    private static final String TAG = "RatioImageView";
    private String mRatio;

    public RatioImageView(Context context) {
        this(context, null);
    }

    public RatioImageView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RatioImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RatioImageView);
            mRatio = typedArray.getString(R.styleable.RatioImageView_riv_ratio);
            typedArray.recycle();
        }
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        if (mRatio != null) {
            char mode = mRatio.charAt(0);
            String ratio = mRatio.substring(1);
            String[] splits = ratio.split(":");
            int first = Integer.parseInt(splits[0]);
            int second = Integer.parseInt(splits[1]);
            if (mode == 'h') {
                setMeasuredDimension(height / first * second, height);
            } else {
                setMeasuredDimension(width, width / first * second);
            }
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}
