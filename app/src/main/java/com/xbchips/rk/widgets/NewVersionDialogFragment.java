package com.xbchips.rk.widgets;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.xbchips.rk.R;
import com.xbchips.rk.system.UpGradeDialog;
import com.xbchips.rk.system.entity.VersionInfo;

public class NewVersionDialogFragment extends DialogFragment {
    private VersionInfo versionInfo;


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        dialog.setCanceledOnTouchOutside(false);
        Window win = dialog.getWindow();
        win.setBackgroundDrawable(new ColorDrawable(0x00000000));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.version_update, container, false);
        return inflate;
    }

    public void setVersionInfo(VersionInfo versionInfo) {
        this.versionInfo = versionInfo;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        show();
    }

    private void show() {
        TextView mVersionnumber = getView().findViewById(R.id.versionnumber);
        mVersionnumber.setText("新版本" + versionInfo.getInfo().getVersion_code());


        TextView mUpdateimmed = getView().findViewById(R.id.tv_updateimmediately);
        mUpdateimmed.setOnClickListener(v -> {
            dismiss();
            UpGradeDialog dialogl = new UpGradeDialog(getActivity());
            dialogl.update(versionInfo.getInfo());


        });

        ImageView mUpdateCancel = getView().findViewById(R.id.tv_update_info_cancel);
        getDialog().setCancelable(versionInfo.getIs_force() != 1);
        //强制升级
        if (versionInfo.getIs_force() == 1) {
            mUpdateCancel.setVisibility(View.GONE);
            getDialog().setOnKeyListener((dialog, keyCode, event) -> {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    return true;
                }
                return false;
            });
        } else {
            mUpdateCancel.setVisibility(View.VISIBLE);
            mUpdateCancel.setOnClickListener(v -> dismiss());
        }


    }


}
