package com.xbchips.rk.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by york on 2017/4/1.
 * viewpager auto scroll  per 5 seconds
 */

public class AutoScrollViewPager extends ViewPager {
    private static final String TAG = "AutoScrollViewPager";
    private boolean isPause;
    private Disposable mDisposable;


    public AutoScrollViewPager(Context context) {
        this(context, null);
    }

    public AutoScrollViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void startScroll() {
        if (getAdapter().getCount() == 1) return;
        mDisposable = Observable.interval(5, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        if (!isPause) {
                            setNextItem();
                        }
                    }
                });
    }

    public void setNextItem() {
        setCurrentItem(getCurrentItem() == getAdapter().getCount() - 1 ?
                0 : getCurrentItem() + 1);
    }

    public void setPreItem() {
        setCurrentItem(getCurrentItem() == 0 ?
                getAdapter().getCount() - 1 : getCurrentItem() - 1);
    }

    public void pauseScroll() {
        isPause = true;
    }

    public void resumeScroll() {
        isPause = false;
    }

    private float mLastX;
    private float mLastY;

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mLastX = ev.getX();
                mLastY = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                float dx = ev.getX() - mLastX;
                float dy = ev.getY() - mLastY;
                if (Math.abs(dx) < Math.abs(dy)) {
                    return false;
                }
                break;
        }
        return super.onTouchEvent(ev);
    }


    public void stopScroll() {
        if (mDisposable != null && !mDisposable.isDisposed()) {
            mDisposable.dispose();
        }
    }
}
