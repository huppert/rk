package com.xbchips.rk.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.xbchips.rk.util.DisplayUtils;

public class ToolBarLayout extends LinearLayout {
    public ToolBarLayout(Context context) {
        this(context, null);
    }

    public ToolBarLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, DisplayUtils.getStatusBarHeight(context));
        addView(new View(context), 0, layoutParams);
    }
}

