package com.xbchips.rk.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.util.LogTrace;


public class EndlessRecyclerView extends RecyclerView {
    private static final String TAG = "EndlessRecyclerView";
    private OnLoadMoreListener mOnLoadMoreListener;
    private OnLoadHeaderListener mOnLoadHeaderListener;
    private OnPageChangeListener mPageChangeListener;
    private View emptyView;
    private boolean mLoading;
    /**
     * 底部当前页数
     */
    private int mCurPage = 1;
    /**
     * 头部当前页数
     */
    private int mHeadPage = 1;
    private int mPageCount;

    /**
     * 第一个item在第几页
     */
    private int mFirstPage = 1;

    public EndlessRecyclerView(final Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setOverScrollMode(OVER_SCROLL_NEVER);
        setClipToPadding(false);
        setClipChildren(false);
        setItemAnimator(null);
        this.addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LayoutManager layoutManager = getLayoutManager();
                int visibleCount = layoutManager.getChildCount();
                int count = layoutManager.getItemCount();
                int first = 0;
                //注意顺序，GridLayoutManager继承自LinearLayoutManager
                if (layoutManager instanceof GridLayoutManager) {
                    first = ((GridLayoutManager) layoutManager).findFirstVisibleItemPosition();
                } else if (layoutManager instanceof LinearLayoutManager) {
                    first = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
                }
//                else {//StaggeredGridLayoutManager
//                }
                if (mOnLoadMoreListener != null
                        && first + visibleCount >= count
                        && !mLoading
                        && mCurPage < mPageCount) {
                    mCurPage++;
                    setLoading(true);
                    //https://stackoverflow.com/questions/39445330/cannot-call-notifyiteminserted-method-in-a-scroll-callback-recyclerview-v724-2
                    post(() -> mOnLoadMoreListener.loadMore());
                }
                if (mOnLoadHeaderListener != null && !mLoading && first == 0
                        && mHeadPage > 1) {
                    LogTrace.d(TAG, "onScrolled: " + mHeadPage);
                    mHeadPage--;
                    setLoading(true);
                    post(() -> mOnLoadHeaderListener.loadHeader());
                }

            }
        });

    }


    private void checkIfEmpty() {
        if (emptyView != null) {
            LogTrace.d(TAG, "checkIfEmpty: " + getAdapter().getItemCount());
            if (isEmpty()) {
                emptyView.setVisibility(VISIBLE);
                this.setVisibility(GONE);
            } else {
                emptyView.setVisibility(GONE);
                this.setVisibility(VISIBLE);
            }
        }
    }

    public boolean isEmpty() {
        return getAdapter().getItemCount() == 0;
    }

    private final AdapterDataObserver observer = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            checkIfEmpty();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            super.onItemRangeRemoved(positionStart, itemCount);
            checkIfEmpty();
        }
    };


    @Override
    public void swapAdapter(Adapter adapter, boolean removeAndRecycleExistingViews) {
        final Adapter oldAdapter = getAdapter();
        if (oldAdapter != null) {
            oldAdapter.unregisterAdapterDataObserver(observer);
        }
        LogTrace.d(getClass().getSimpleName(), "swapAdapter: " + adapter);
        if (adapter != null) {
            adapter.registerAdapterDataObserver(observer);
        }
        super.swapAdapter(adapter, removeAndRecycleExistingViews);
    }

    public void setEmptyView(@Nullable View emptyView) {
        this.emptyView = emptyView;
    }


    public interface OnLoadMoreListener {
        void loadMore();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener listener) {
        mOnLoadMoreListener = listener;
    }

    public interface OnLoadHeaderListener {
        void loadHeader();
    }

    public void setOnLoadHeaderListener(OnLoadHeaderListener onLoadHeaderListener) {
        mOnLoadHeaderListener = onLoadHeaderListener;
    }

    public interface OnPageChangeListener {
        void onPageChange(int currentPage);
    }

    public void setPageChangeListener(OnPageChangeListener pageChangeListener) {
        mPageChangeListener = pageChangeListener;
    }

    public boolean isLoading() {
        return mLoading;
    }

    public void setLoading(boolean loading) {
        mLoading = loading;
    }

    public int getCurPage() {
        return mCurPage;
    }

    public void setCurPage(int curPage) {
        mCurPage = curPage;
    }

    public void pageDecrement() {
        mCurPage--;
    }

    public int getPageCount() {
        return mPageCount;
    }

    public int getHeadPage() {
        return mHeadPage;
    }

    public void setHeadPage(int headPage) {
        mHeadPage = headPage;
    }

    public void headPageIncrement() {
        mHeadPage++;
    }

    /**
     * 是否是第一页
     */
    public boolean isFirstPage() {
        return mCurPage == 1;
    }

    public void setPageCount(int pageCount) {
        mPageCount = pageCount;
    }

    public boolean isLastPage() {
        return mCurPage == mPageCount && getPageCount() > 1;
    }
}
