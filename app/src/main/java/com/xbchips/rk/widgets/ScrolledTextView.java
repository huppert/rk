package com.xbchips.rk.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.method.ScrollingMovementMethod;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.appcompat.widget.AppCompatTextView;

import com.xbchips.rk.R;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Harry on 2017/8/17.
 * 可滚动TextView，滚动到底部或者顶部，则事件回传给父视图
 */

public class ScrolledTextView extends AppCompatTextView {

    private int maxLines;
    private ExecutorService fixedThreadPoll;
    private int downY = -1;
    private boolean isFirst = true;

    public ScrolledTextView(Context context) {
        this(context, null);
    }

    public ScrolledTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScrolledTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ScrolledTextView, defStyleAttr, 0);
        maxLines = typedArray.getInt(R.styleable.ScrolledTextView_vertical_max_lines, 8);
        setMaxLines(maxLines);
        typedArray.recycle();
        setMovementMethod(ScrollingMovementMethod.getInstance());
        fixedThreadPoll = Executors.newFixedThreadPool(2);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (maxLines < getLineCount()) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    downY = getScrollY();
                    //不让父控件拦截事件，即保证自己能接受到ACTION_MOVE事件
                    getParent().requestDisallowInterceptTouchEvent(true);
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (isFirst) {
                        //通过子线程休息100ms，来对比当前getScrollY和downY，
                        // 如果相等，说明到顶部或者底部，这时让父亲拦截事件
                        isFirst = false;
                        fixedThreadPoll.execute(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                if (getScrollY() == downY) {
                                    getParent().requestDisallowInterceptTouchEvent(false);
                                }
                            }
                        });
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    isFirst = true;
                    //让父亲拦截事件
                    getParent().requestDisallowInterceptTouchEvent(false);
                    break;
            }
        }

        return super.dispatchTouchEvent(event);
    }
}
