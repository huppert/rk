package com.xbchips.rk.widgets;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.system.entity.AdvertList;
import com.xbchips.rk.util.DisplayUtils;
import com.xbchips.rk.video.adapter.AdvertisingAdapter;

import java.util.List;

public class AdRecyclerView extends RecyclerView {
    public AdRecyclerView(@NonNull Context context) {
        this(context, null);
    }

    public AdRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setNestedScrollingEnabled(false);
    }


    public void initData(List<AdvertList> data) {
        AdvertisingAdapter centerAdapter = new AdvertisingAdapter(data, getContext());
        setLayoutManager(new LinearLayoutManager(getContext()));
        addItemDecoration(new SpaceItemDecoration(DisplayUtils.dip2px(getContext(), 5)));
        setAdapter(centerAdapter);
    }
}
