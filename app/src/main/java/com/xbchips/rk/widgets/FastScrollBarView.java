package com.xbchips.rk.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import com.google.android.exoplayer2.util.Util;
import com.xbchips.rk.R;
import com.xbchips.rk.util.DisplayUtils;
import com.xbchips.rk.util.LogTrace;

/**
 * Created by york on 2018/3/10.
 * 快速滑动条
 */

public class FastScrollBarView extends View {
    private static final String TAG = "FastScrollBarView";
    /**
     * 竖条
     */
    private RectF mRectF = new RectF();
    private Paint mRectPaint = new Paint();
    private Paint mCirclePaint = new Paint();
    private Paint mMarkPaint = new Paint();
    private Paint mTextPaint = new Paint();

    /**
     * 竖条分母
     */
    private int mDuration;

    /**
     * 竖条高度
     */
    private int mBarHeight;
    /**
     * 小圆点直径
     */
    private int mCircleDiameter;

    private OnMoveListener mOnMoveListener;
    /**
     * 小圆点矩形范围
     */
    private Rect mCircleRect = new Rect();

    /**
     * 小圆点Y当前坐标
     */
    private float mCurrentY;
    /**
     * 指示器图标
     */
    private Bitmap mMarkerBitmap;

    /**
     * 小圆点位置比例
     */
    private float mRatio;

    public FastScrollBarView(Context context) {
        this(context, null);
    }

    public FastScrollBarView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FastScrollBarView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mRectPaint.setColor(0xFFA09F9F);
        mRectPaint.setAntiAlias(true);

        mCirclePaint.setColor(0xFFFB4261);
        mCirclePaint.setAntiAlias(true);

        mTextPaint.setColor(Color.WHITE);
        mTextPaint.setTextSize(DisplayUtils.sp2px(context, 12));
        mTextPaint.setAntiAlias(true);
        mMarkerBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.left_marker);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        mCircleDiameter = width - mMarkerBitmap.getWidth();
        mBarHeight = height - mMarkerBitmap.getHeight();
        setMeasuredDimension(width, height);
    }

    /**
     * 设置小圆点位置
     */
    public void setCirclePosition(int duration) {
        mDuration = duration;
    }

    public void setPage(int page) {
//        if (page == 1) {//设置到初始位置
//            mCurrentY = 0;
//            mRatio = 0;
//        } else {
//            float ratio = ((float) page) / mDuration;
//            mCurrentY = Util.constrainValue((mBarHeight * ratio) + mRectF.top, mCircleDiameter / 2 + mRectF.top, mBarHeight - mCircleDiameter / 2);
//            mRatio = Math.max(0, Math.min(ratio, 1));
//        }
//        invalidate();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        drawBar(canvas);
        drawCircle(canvas);
        drawMarker(canvas);
    }

    /**
     * 绘制垂直的进度条
     */
    private void drawBar(Canvas canvas) {
        int rectW = mCircleDiameter - 28;
        mRectF.set(mMarkerBitmap.getWidth() + 14, mMarkerBitmap.getHeight() / 2, getMeasuredWidth() - 12, mBarHeight);
        canvas.drawRoundRect(mRectF, rectW / 2, rectW / 2, mRectPaint);
    }

    /**
     * 绘制小圆点
     */
    private void drawCircle(Canvas canvas) {
        if (mCurrentY == 0) {
            mCurrentY = (mCircleDiameter / 2) + mRectF.top;
        }
        mCircleRect.set(mMarkerBitmap.getWidth(), ((int) mCurrentY) - (mCircleDiameter / 2), getMeasuredWidth(), ((int) mCurrentY) + (mCircleDiameter / 2));
        canvas.drawCircle(mMarkerBitmap.getWidth() + (mCircleDiameter / 2), mCurrentY, mCircleDiameter / 2, mCirclePaint);
    }

    /**
     * 绘制页码指示器
     */
    private void drawMarker(Canvas canvas) {
        canvas.drawBitmap(mMarkerBitmap, 0, mCurrentY - mMarkerBitmap.getHeight() / 2, mMarkPaint);
        int curPage = Math.max(1, (int) (mDuration * mRatio));
        float textSize = mTextPaint.getTextSize();
        canvas.drawText(String.valueOf(curPage), 10, mCurrentY + textSize / 4, mTextPaint);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                int x = (int) event.getX();
                int y = (int) event.getY();
                return mCircleRect.contains(x, y);
            case MotionEvent.ACTION_MOVE:
                calcRatio(event);
                mOnMoveListener.move(mRatio);
                invalidate();
                return true;
            case MotionEvent.ACTION_UP:
                calcRatio(event);
                mOnMoveListener.finish(mRatio);
                return true;
        }
        return super.onTouchEvent(event);
    }

    /**
     * 计算当前比例
     */
    private void calcRatio(MotionEvent event) {
        mCurrentY = Util.constrainValue(event.getY(), mCircleDiameter / 2 + mRectF.top, mBarHeight - mCircleDiameter / 2);
        LogTrace.d(TAG, "calcRatio: " + mCurrentY);
        float ratio = (event.getY() - mRectF.top) / mBarHeight;
        mRatio = Math.max(0, Math.min(ratio, 1));
        LogTrace.d(TAG, "calcRatio: " + ratio + " constrain :" + mRatio);
    }


    /**
     * 滑动回调
     */
    public interface OnMoveListener {
        void move(float ratio);

        void finish(float ratio);
    }

    public void setOnMoveListener(OnMoveListener onMoveListener) {
        mOnMoveListener = onMoveListener;
    }
}
