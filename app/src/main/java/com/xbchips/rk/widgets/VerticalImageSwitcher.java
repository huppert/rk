package com.xbchips.rk.widgets;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageSwitcher;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.xbchips.rk.R;
import com.xbchips.rk.activities.HtmlActivity;
import com.xbchips.rk.free.entity.Banner;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

import static com.xbchips.rk.activities.HtmlActivity.ARG_TITLE;

/**
 * Created by york on 2017/10/19.
 */

public class VerticalImageSwitcher extends ImageSwitcher {
    private boolean mActive = true;
    private Disposable disposable;
    private Banner mCurrentBanner;

    public VerticalImageSwitcher(Context context) {
        this(context, null);
    }

    public VerticalImageSwitcher(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    private void init() {
        setFactory(() -> {
            ImageView imageView = new ImageView(getContext());
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setLayoutParams(new LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            return imageView;
        });
        Animation in = AnimationUtils.loadAnimation(getContext(), R.anim.bottom_in);
        Animation out = AnimationUtils.loadAnimation(getContext(), R.anim.top_out);
        setInAnimation(in);
        setOutAnimation(out);
        setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mCurrentBanner.getUrl())) {
                Intent intent = new Intent(getContext(), HtmlActivity.class);
                intent.putExtra(HtmlActivity._URL, mCurrentBanner.getUrl());
                intent.putExtra(ARG_TITLE, mCurrentBanner.getTitle());
                getContext().startActivity(intent);
            }
        });
    }

    public void start(@NonNull final List<Banner> data) {
        disposable = Flowable.interval(0, 3, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .filter(aLong -> mActive)
                .subscribe(aLong -> switchImage(data.get((int) (aLong % data.size()))));
    }

    public void pause() {
        mActive = false;
    }

    public void resume() {
        mActive = true;
    }

    public void stop() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    private void switchImage(Banner banner) {
        mCurrentBanner = banner;
        Glide.with(getContext())
                .asDrawable()
                .apply(new RequestOptions()
                        .placeholder(R.drawable.drawable_login)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                )
                .load(banner.getThumb_img_url())
                .into(new CustomTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        setImageDrawable(resource);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {

                    }
                });
    }


}
