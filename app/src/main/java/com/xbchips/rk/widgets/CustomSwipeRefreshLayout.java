package com.xbchips.rk.widgets;

import android.content.Context;
import android.util.AttributeSet;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.xbchips.rk.R;


/**
 * Created by P.Y on 2016/7/28.
 */

public class CustomSwipeRefreshLayout extends SwipeRefreshLayout {
    public CustomSwipeRefreshLayout(Context context) {
        this(context, null);
    }

    public CustomSwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        setColorSchemeResources(R.color.colorPrimary);
    }

//    private float mDownPosX = 0;
//    private float mDownPosY = 0;

//    @Override
//    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        final float x = ev.getX();
//        final float y = ev.getY();
//
//        final int action = ev.getAction();
//        switch (action) {
//            case MotionEvent.ACTION_DOWN:
//                mDownPosX = x;
//                mDownPosY = y;
//
//                break;
//            case MotionEvent.ACTION_MOVE:
//                final float deltaX = Math.abs(x - mDownPosX);
//                final float deltaY = Math.abs(y - mDownPosY);
//                if (deltaX > deltaY) {
//                    //如果左右移动距离大于上下距离，则该SwipeRefreshLayout不响应事件，由子控件响应
//                    return false;
//                }
//        }
//
//        return super.onInterceptTouchEvent(ev);
//    }
}
