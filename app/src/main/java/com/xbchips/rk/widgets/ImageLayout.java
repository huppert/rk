package com.xbchips.rk.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.xbchips.rk.R;
import com.xbchips.rk.util.DisplayUtils;

/**
 * Created by york on 2017/8/2.
 * 封面图
 */

public class ImageLayout extends FrameLayout {
    private ImageView coverImage;
    private int gridWidth;
    private boolean isShowHDLabel = true;
    private int imageWidth;

    public ImageLayout(@NonNull Context context) {
        this(context, null);
    }

    public ImageLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ImageLayout(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ImageLayout, defStyleAttr, 0);
        isShowHDLabel = typedArray.getBoolean(R.styleable.ImageLayout_is_show_hd, true);
        imageWidth = typedArray.getDimensionPixelSize(R.styleable.ImageLayout_image_width, 6);
        typedArray.recycle();
        init(context);
    }

    private void init(@NonNull Context context) {
        coverImage = new ImageView(context);
        coverImage.setScaleType(ImageView.ScaleType.FIT_XY);
        int screenW = DisplayUtils.getScreenW(context);
        int width = screenW - imageWidth * 2;
        gridWidth = (screenW - DisplayUtils.dip2px(context, 18)) / 2;
        setCoverWidth(width);

        if (isShowHDLabel) {
            showHDLabel();
        }
    }


    /**
     * 设置封面图宽度
     */
    public void setCoverWidth(int width) {
        int height = width / 4 * 3;
        coverImage.setLayoutParams(new ViewGroup.LayoutParams(width, height));
        addView(coverImage);
    }

    public void setCoverWidthAndHeight(int width, int height) {
        coverImage.setLayoutParams(new FrameLayout.LayoutParams(width, height));
    }


    public int getGridWidth() {
        return gridWidth;
    }


    /**
     * 设置表格封面图宽度
     */
    public void setCoverWidthGrid() {
        removeAllViews();
        int height = gridWidth / 4 * 3;
        coverImage.setLayoutParams(new ViewGroup.LayoutParams(gridWidth, height));
        addView(coverImage);
    }


    public ImageView getCoverImage() {
        return coverImage;
    }


    public void showHDLabel() {
        LayoutParams layoutParams = new LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.END;
        layoutParams.topMargin = layoutParams.rightMargin = DisplayUtils.dip2px(getContext(), 12);
        TextView textView = new TextView(getContext());
        textView.setBackgroundResource(R.drawable.bg_hd_label);
        int hpadding = DisplayUtils.dip2px(getContext(), 5);
        int vpadding = DisplayUtils.dip2px(getContext(), 3);
        textView.setPadding(hpadding, vpadding, hpadding, vpadding);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        textView.setTypeface(null, Typeface.BOLD);
        textView.setText("HD");
        addView(textView, layoutParams);
    }
}
