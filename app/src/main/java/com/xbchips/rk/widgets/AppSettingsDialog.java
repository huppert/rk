package com.xbchips.rk.widgets;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Parcel;
import android.provider.Settings;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.xbchips.rk.R;

import java.util.Locale;

/**
 * Created by york on 2017/7/28.
 */

public class AppSettingsDialog implements DialogInterface.OnClickListener {
    public static final int DEFAULT_SETTINGS_REQ_CODE = 16061;

    static final String EXTRA_APP_SETTINGS = "extra_app_settings";

    private final String mRationale;
    private final String mTitle;
    private final String mPositiveButtonText;
    private final String mNegativeButtonText;
    private final int mRequestCode;
    private Context mContext;
    private Object mActivityOrFragment;
    private DialogInterface.OnClickListener mNegativeListener;

    private AppSettingsDialog(Parcel in) {
        mRationale = in.readString();
        mTitle = in.readString();
        mPositiveButtonText = in.readString();
        mNegativeButtonText = in.readString();
        mRequestCode = in.readInt();
    }

    private AppSettingsDialog(@NonNull final Object activityOrFragment,
                              @NonNull final Context context,
                              @Nullable String rationale,
                              @Nullable String title,
                              @Nullable String positiveButtonText,
                              @Nullable String negativeButtonText,
                              @Nullable DialogInterface.OnClickListener negativeListener,
                              int requestCode) {
        mActivityOrFragment = activityOrFragment;
        mContext = context;
        mRationale = rationale;
        mTitle = title;
        mPositiveButtonText = positiveButtonText;
        mNegativeButtonText = negativeButtonText;
        mNegativeListener = negativeListener;
        mRequestCode = requestCode;
    }

    void setActivityOrFragment(Object activityOrFragment) {
        mActivityOrFragment = activityOrFragment;
    }

    void setContext(Context context) {
        mContext = context;
    }

    void setNegativeListener(DialogInterface.OnClickListener negativeListener) {
        mNegativeListener = negativeListener;
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    private void startForResult(Intent intent) {
        if (mActivityOrFragment instanceof Activity) {
            ((Activity) mActivityOrFragment).startActivityForResult(intent, mRequestCode);
            ((Activity) mActivityOrFragment).finish();
        } else if (mActivityOrFragment instanceof Fragment) {
            ((Fragment) mActivityOrFragment).startActivityForResult(intent, mRequestCode);
        } else if (mActivityOrFragment instanceof android.app.Fragment) {
            ((android.app.Fragment) mActivityOrFragment).startActivityForResult(intent,
                    mRequestCode);
        }
    }


    /**
     * Show the dialog.
     */
    public AlertDialog show() {
        return new AlertDialog.Builder(mContext)
                .setCancelable(false)
                .setTitle(mTitle)
                .setMessage(mRationale)
                .setPositiveButton(mPositiveButtonText, this)
                .setNegativeButton(mNegativeButtonText, mNegativeListener)
                .show();
    }

    @SuppressWarnings("NewApi")
    @Override
    public void onClick(DialogInterface dialog, int which) {
        // Create app settings intent
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", mContext.getPackageName(), null);
        intent.setData(uri);

        // Start for result
        startForResult(intent);
    }


    /**
     * Builder for an {@link AppSettingsDialog}.
     */
    public static class Builder {

        private Object mActivityOrFragment;
        private Context mContext;
        private String mRationale;
        private String mTitle;
        private String mPositiveButton;
        private String mNegativeButton;
        private DialogInterface.OnClickListener mNegativeListener;
        private int mRequestCode = -1;

        /**
         * Create a new Builder for an {@link AppSettingsDialog}.
         *
         * @param activity  the {@link Activity} in which to display the dialog.
         * @param rationale text explaining why the user should launch the app settings screen.
         * @deprecated Use {@link #Builder(Activity)} with {@link #setRationale(String)} or {@link
         * #setRationale(int)}.
         */
        @Deprecated
        public Builder(@NonNull Activity activity, @NonNull String rationale) {
            mActivityOrFragment = activity;
            mContext = activity;
            mRationale = rationale;
        }

        /**
         * Create a new Builder for an {@link AppSettingsDialog}.
         *
         * @param fragment  the {@link Fragment} in which to display the dialog.
         * @param rationale text explaining why the user should launch the app settings screen.
         * @deprecated Use {@link #Builder(Fragment)} with {@link #setRationale(String)} or {@link
         * #setRationale(int)}.
         */
        @Deprecated
        public Builder(@NonNull Fragment fragment, @NonNull String rationale) {
            mActivityOrFragment = fragment;
            mContext = fragment.getContext();
            mRationale = rationale;
        }

        /**
         * Create a new Builder for an {@link AppSettingsDialog}.
         *
         * @param fragment  the {@link android.app.Fragment} in which to display the dialog.
         * @param rationale text explaining why the user should launch the app settings screen.
         * @deprecated Use {@link #Builder(android.app.Fragment)} with {@link #setRationale(String)}
         * or {@link #setRationale(int)}.
         */
        @Deprecated
        @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
        public Builder(@NonNull android.app.Fragment fragment, @NonNull String rationale) {
            mActivityOrFragment = fragment;
            mContext = fragment.getActivity();
            mRationale = rationale;
        }

        /**
         * Create a new Builder for an {@link AppSettingsDialog}.
         *
         * @param activity the {@link Activity} in which to display the dialog.
         */
        public Builder(@NonNull Activity activity) {
            mActivityOrFragment = activity;
            mContext = activity;
        }

        /**
         * Create a new Builder for an {@link AppSettingsDialog}.
         *
         * @param fragment the {@link Fragment} in which to display the dialog.
         */
        public Builder(@NonNull Fragment fragment) {
            mActivityOrFragment = fragment;
            mContext = fragment.getContext();
        }

        /**
         * Create a new Builder for an {@link AppSettingsDialog}.
         *
         * @param fragment the {@link android.app.Fragment} in which to display the dialog.
         */
        @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
        public Builder(@NonNull android.app.Fragment fragment) {
            mActivityOrFragment = fragment;
            mContext = fragment.getActivity();
        }


        /**
         * Set the title dialog. Default is "Permissions Required".
         */
        public Builder setTitle(String title) {
            mTitle = title;
            return this;
        }

        /**
         * Set the title dialog. Default is "Permissions Required".
         */
        public Builder setTitle(@StringRes int title) {
            mTitle = mContext.getString(title);
            return this;
        }

        /**
         * Set the rationale dialog. Default is
         * "This app may not work correctly without the requested permissions.
         * Open the app settings screen to modify app permissions."
         */
        public Builder setRationale(String rationale) {
            mRationale = rationale;
            return this;
        }

        /**
         * Set the rationale dialog. Default is
         * "This app may not work correctly without the requested permissions.
         * Open the app settings screen to modify app permissions."
         */
        public Builder setRationale(@StringRes int rationale) {
            mRationale = mContext.getString(rationale);
            return this;
        }

        /**
         * Set the positive button text, default is {@link android.R.string#ok}.
         */
        public Builder setPositiveButton(String positiveButton) {
            mPositiveButton = positiveButton;
            return this;
        }

        /**
         * Set the positive button text, default is {@link android.R.string#ok}.
         */
        public Builder setPositiveButton(@StringRes int positiveButton) {
            mPositiveButton = mContext.getString(positiveButton);
            return this;
        }

        /**
         * Set the negative button text and click listener, default text is
         * {@link android.R.string#cancel}.
         */
        @Deprecated
        public Builder setNegativeButton(String negativeButton,
                                         DialogInterface.OnClickListener negativeListener) {
            mNegativeButton = negativeButton;
            mNegativeListener = negativeListener;
            return this;
        }


        /**
         * Set the negative button text, default is {@link android.R.string#cancel}.
         */
        public Builder setNegativeButton(@StringRes int negativeButton, DialogInterface.OnClickListener negativeListener) {
            mNegativeButton = mContext.getString(negativeButton);
            mNegativeListener = negativeListener;
            return this;
        }

        /**
         * Set the request code use when launching the Settings screen for result, can be retrieved
         * in the calling Activity's {@link Activity#onActivityResult(int, int, Intent)} method.
         * Default is {@link #DEFAULT_SETTINGS_REQ_CODE}.
         */
        public Builder setRequestCode(int requestCode) {
            mRequestCode = requestCode;
            return this;
        }

        /**
         * Build the {@link AppSettingsDialog} from the specified options. Generally followed by a
         * call to {@link AppSettingsDialog#show()}.
         */
        public AppSettingsDialog build() {
            mTitle = TextUtils.isEmpty(mTitle) ?
                    mContext.getString(R.string.request_permission) : mTitle;
            mRationale = String.format(Locale.CHINA,
                    mContext.getString(R.string.rationale_ask_again),
                    mContext.getString(R.string.app_name),
                    mRationale);
            mPositiveButton = TextUtils.isEmpty(mPositiveButton) ?
                    mContext.getString(R.string.setting) : mPositiveButton;
            mNegativeButton = TextUtils.isEmpty(mNegativeButton) ?
                    mContext.getString(android.R.string.cancel) : mNegativeButton;
            mRequestCode = mRequestCode > 0 ? mRequestCode : DEFAULT_SETTINGS_REQ_CODE;

            return new AppSettingsDialog(
                    mActivityOrFragment,
                    mContext,
                    mRationale,
                    mTitle,
                    mPositiveButton,
                    mNegativeButton,
                    mNegativeListener,
                    mRequestCode);
        }

    }

}
