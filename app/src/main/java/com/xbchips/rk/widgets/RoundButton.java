package com.xbchips.rk.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;

import com.xbchips.rk.R;


/**
 * Created by york on 2017/7/5.
 */

public class RoundButton extends AppCompatButton {
    private int solidColor;
    private int pressedColor;
    private int strokeColor;
    private int disabledColor;
    private static final int DEFAULT_COLOR = Color.TRANSPARENT;
    private int radius;
    private int strokeWidth;

    public RoundButton(Context context) {
        this(context, null);
    }

    public RoundButton(Context context, AttributeSet attrs) {
        this(context, attrs, androidx.appcompat.R.attr.buttonStyle);
    }

    public RoundButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RoundButton, defStyleAttr, 0);
        solidColor = a.getColor(R.styleable.RoundButton_rtv_solid, DEFAULT_COLOR);
        pressedColor = a.getColor(R.styleable.RoundButton_pressed_color, solidColor);
        disabledColor = a.getColor(R.styleable.RoundButton_disabled_color, solidColor);
        radius = a.getDimensionPixelSize(R.styleable.RoundButton_conner_radius, 0);
        strokeColor = a.getColor(R.styleable.RoundButton_stroke_color, DEFAULT_COLOR);
        strokeWidth = a.getDimensionPixelSize(R.styleable.RoundButton_stroke_width, 0);
        a.recycle();
        init();
    }

    private void init() {
        GradientDrawable drawable = createGradientDrawable(solidColor);
        GradientDrawable pressedDrawable = createGradientDrawable(pressedColor);
        GradientDrawable disabledDrawable = createGradientDrawable(disabledColor);

        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_pressed}, pressedDrawable);
        states.addState(new int[]{android.R.attr.state_selected}, pressedDrawable);
        states.addState(new int[]{android.R.attr.state_enabled}, drawable);
        states.addState(new int[]{}, disabledDrawable);
        setBackground(states);
    }

    @NonNull
    private GradientDrawable createGradientDrawable(@ColorInt int color) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setShape(GradientDrawable.RECTANGLE);
        drawable.setCornerRadius(radius);
        drawable.setColor(color);
        drawable.setStroke(strokeWidth, strokeColor);
        return drawable;
    }


}
