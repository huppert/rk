package com.xbchips.rk.vip.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.common.ImageLoader;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.util.DisplayUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Harry on 2017/8/1.
 */

public class VipCategoryAdapter extends BaseAdapter<VideoItem> {


    private int mSpace;

    public VipCategoryAdapter(List<VideoItem> list, Context context) {
        super(list, context);
        mSpace = DisplayUtils.dip2px(context, 5);
    }

    public void addHeader() {
        mHeaderSize++;
        notifyItemInserted(0);
    }

    public void removeHeader() {
        if (mHeaderSize > 0) {
            mHeaderSize--;
            notifyItemRemoved(0);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_FOOTER:
                return getFooterViewHolder(parent);
            case TYPE_HEADER:
                return new HeadViewHolder(mInflater.inflate(R.layout.item_footer, parent, false));
            case TYPE_ITEM:
                return new VipCategoryViewHolder(mInflater.inflate(R.layout.item_free, parent, false));
        }
        return null;

    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        VipCategoryViewHolder viewHolder = (VipCategoryViewHolder) holder;
        VideoItem videoItem = mList.get(getRealPosition(position));
        viewHolder.mImageLayout.setOnClickListener(mOnClickListener);
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) viewHolder.mImageLayout.getLayoutParams();
        if (videoItem.getIs_advert() == 1) {
            viewHolder.mTvPlayCount.setVisibility(View.GONE);
            viewHolder.mTvMovieName.setVisibility(View.GONE);
            viewHolder.llBgMask.setVisibility(View.GONE);
            layoutParams.leftMargin = layoutParams.rightMargin = mSpace;
            layoutParams.dimensionRatio = "11:3";
            viewHolder.mImageLayout.setLayoutParams(layoutParams);
            viewHolder.mImageLayout.setScaleType(ImageView.ScaleType.FIT_XY);

            ImageLoader.load(mContext, videoItem.getImg_url(), viewHolder.mImageLayout, new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .override(layoutParams.width, layoutParams.height)
                    .placeholder(R.drawable.place_holder2)
                    .dontAnimate());
        } else {
            ImageLoader.load(mContext, videoItem.getThumb_img_url(), viewHolder.mImageLayout);
            viewHolder.mTvPlayCount.setVisibility(View.VISIBLE);
            viewHolder.mTvMovieName.setVisibility(View.VISIBLE);
            viewHolder.mTvMovieName.setText(videoItem.getTitle());
            layoutParams.dimensionRatio = "4:3";
            viewHolder.mImageLayout.setLayoutParams(layoutParams);
            boolean lastInSingleLine = isLastInSingleLine(position);
            layoutParams.leftMargin = lastInSingleLine ? mSpace / 2 : mSpace;
            layoutParams.rightMargin = lastInSingleLine ? mSpace : mSpace / 2;
            viewHolder.llBgMask.setVisibility(View.VISIBLE);
            viewHolder.llBgMask.setBackgroundColor(mContext.getResources().getColor(R.color.color_1A));
        }

        viewHolder.mImageLayout.setTag(R.id.glideId, videoItem);
        viewHolder.mTvPlayCount.setText(mContext.getString(R.string.watch_times, videoItem.getViews()));
        viewHolder.tvMovieTime.setText(videoItem.getUpdated_at());

    }


    /**
     * 是否为一行中最后一个item
     */
    private boolean isLastInSingleLine(int position) {
        int adCount = 0;
        for (int i = 0; i < position; i++) {
            if (mList.get(i).getIs_advert() == 1) {
                adCount++;
            }
        }

        return (position - adCount) % 2 == 1;

    }


    static class HeadViewHolder extends RecyclerView.ViewHolder {

        HeadViewHolder(View itemView) {
            super(itemView);
            TextView mTvNoMore = (TextView) itemView.findViewById(R.id.tv_no_more);
            mTvNoMore.setVisibility(View.GONE);
        }
    }

    static class VipCategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_layout)
        ImageView mImageLayout;
        @BindView(R.id.tv_movie_name)
        TextView mTvMovieName;
        @BindView(R.id.tv_play_count)
        TextView mTvPlayCount;
        @BindView(R.id.ll_item)
        ConstraintLayout rootLayout;
        @BindView(R.id.tv_movie_time)
        TextView tvMovieTime;
        @BindView(R.id.ll_bg_mask)
        RelativeLayout llBgMask;

        public VipCategoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
