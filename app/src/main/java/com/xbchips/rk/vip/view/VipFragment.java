package com.xbchips.rk.vip.view;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.ArrayMap;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.xbchips.rk.R;
import com.xbchips.rk.activities.SearchActivity;
import com.xbchips.rk.base.BaseFragment;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.vip.adapter.ViewPagerAdapter;
import com.xbchips.rk.vip.contract.VipContract;
import com.xbchips.rk.vip.entity.Category;
import com.xbchips.rk.vip.presenter.VipCategoryPresenter;
import com.xbchips.rk.vip.presenter.VipPresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by york on 2017/7/26.
 */

public class VipFragment extends BaseFragment implements VipContract.View {
    @BindView(R.id.category_tabs)
    TabLayout mCategoryTabs;
    @BindView(R.id.rb_filter)
    TextView mRbFilter;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    @BindView(R.id.ll_sort)
    LinearLayout mLlSort;
    @BindView(R.id.fl_vip_search)
    FrameLayout searchFrameLayout;
    //    @BindView(R.id.tv_vip_type)
//    TextView vipSearchTypeTextView;
    private VipPresenter mVipPresenter;
    private List<Category> mCategoryList = new ArrayList<>();
    private List<Fragment> mFragments = new ArrayList<>();
    private boolean isGrid;
    /**
     * 每个fragment排序方式
     */
    private ArrayMap<Integer, String> mSortMap = new ArrayMap<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_vip, container, false);
        mUnBinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    protected void init() {
        SparseArray<String> indexToSortMap = new SparseArray<>();
        indexToSortMap.put(1, Constants.SORT_DEFAULT);
        indexToSortMap.put(2, Constants.SORT_NEW);
        indexToSortMap.put(3, Constants.SORT_HOTTEST);
        indexToSortMap.put(4, Constants.SORT_COLLECT);
        mVipPresenter = new VipPresenter(this);
        mVipPresenter.getCategoryList();
    }


    @Override
    public void setPresenter(VipContract.Presenter presenter) {

    }

    @Override
    public void getCategorySuccess(List<Category> categoryList) {
        if (categoryList != null && categoryList.size() > 0) {
            for (int i = 0; i < categoryList.size(); i++) {
                if ("巷友".equals(categoryList.get(i).getName())) {
                    categoryList.remove(i);
                }
            }
            for (int i = 0; i < categoryList.size(); i++) {
                if ("其他".equals(categoryList.get(i).getName())) {
                    categoryList.remove(i);
                }
            }
        }

        for (int i = 0; i < mLlSort.getChildCount(); i++) {
            mLlSort.getChildAt(i).setVisibility(View.VISIBLE);
        }

        mCategoryList.addAll(categoryList);
        List<String> titles = new ArrayList<>();
        for (int i = 0; i < mCategoryList.size(); i++) {
            Category category = mCategoryList.get(i);
            VipCategoryFragment fragment = VipCategoryFragment.newInstance(category.getId(), i == 0);
            fragment.setPresenter(new VipCategoryPresenter(fragment));
            mFragments.add(fragment);
            titles.add(category.getName());
            mSortMap.put(i, "默认");
        }
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager(), mFragments, titles);
        mViewPager.setAdapter(viewPagerAdapter);
        mViewPager.setOffscreenPageLimit(mCategoryList.size());
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (mCategoryTabs.getSelectedTabPosition() != position) {
                    mCategoryTabs.getTabAt(position).select();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        initTabLayout();
    }

    private void initTabLayout() {
        mCategoryTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View customView = tab.getCustomView();
                TextView text = customView.findViewById(android.R.id.text1);
                text.setTextSize(17);
                text.setTypeface(Typeface.DEFAULT_BOLD);
                int position = tab.getPosition();
                if (mViewPager.getCurrentItem() != position) {
                    mViewPager.setCurrentItem(position);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View customView = tab.getCustomView();
                TextView text = customView.findViewById(android.R.id.text1);
                text.setTextSize(14);
                text.setTypeface(Typeface.DEFAULT);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        for (int i = 0; i < mCategoryList.size(); i++) {
            TabLayout.Tab tab = mCategoryTabs.newTab()
                    .setCustomView(R.layout.custom_category_tab)
                    .setText(mCategoryList.get(i).getName());
            mCategoryTabs.addTab(tab);
        }
        mCategoryTabs.getTabAt(0).select();


    }

    @OnClick({R.id.rb_filter, R.id.fl_vip_search})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rb_filter:
                ((VipCategoryFragment) mFragments.get(mViewPager.getCurrentItem())).setFastBarVisibility();
                break;
            case R.id.fl_vip_search:
                startActivity(new Intent(mContext, SearchActivity.class));
                break;

        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mVipPresenter != null) {
            mVipPresenter.unSubscribe();
        }
    }
}
