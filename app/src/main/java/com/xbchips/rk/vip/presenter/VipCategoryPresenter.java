package com.xbchips.rk.vip.presenter;


import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.vip.contract.VipCategoryContract;
import com.xbchips.rk.vip.service.VipService;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Harry on 2017/7/31.
 */

public class VipCategoryPresenter implements VipCategoryContract.Presenter {
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private VipCategoryContract.View mView;
    private VipService mVipService;

    public VipCategoryPresenter(VipCategoryContract.View view) {
        mView = view;
        mVipService = HttpRequest.createService(VipService.class);
    }

    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }


    @Override
    public void getVideoList(int page, int pageSize, String category_id, List<String> tags, String sort, boolean isReverse) {
        String s1 = Observable.fromIterable(tags)
                .reduce((s, s2) -> s + "," + s2)
                .blockingGet("");
        Disposable disposable = mVipService.getVideoList(page, pageSize, category_id, s1, sort)
                .compose(RxUtils.applySchedulers())
                .subscribe(response -> {
                            if (isReverse) {
                                mView.loadHeaderSuccess(response);
                            } else {
                                mView.loadSuccess(response);
                            }
                        },
                        throwable -> {
                            if (isReverse) {
                                mView.loadHeaderFailed(throwable);
                            } else {
                                mView.loadFailed(throwable);
                            }
                        });
        mCompositeDisposable.add(disposable);
    }
}
