package com.xbchips.rk.vip.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by york on 2017/10/31.
 */

public class FilterAdapter extends BaseAdapter<String> {
    private List<String> mCheckedList = new ArrayList<>();

    public FilterAdapter(List<String> list, Context context, List<String> checkedList) {
        super(list, context);
        mCheckedList.addAll(checkedList);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FilterViewHolder(mInflater.inflate(R.layout.item_filter, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        FilterViewHolder viewHolder = (FilterViewHolder) holder;
        String tag = mList.get(position);
        viewHolder.mCbFilter.setChecked(mCheckedList.contains(tag));
        viewHolder.mCbFilter.setText(tag);
        viewHolder.mCbFilter.setTag(tag);
        viewHolder.mCbFilter.setOnCheckedChangeListener(mOnCheckedChangeListener);
    }


    private CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener = (buttonView, isChecked) -> {
        String tag = buttonView.getTag().toString();
        if (isChecked) {
            mCheckedList.add(tag);
        } else {
            mCheckedList.remove(tag);
        }
    };


    public void reset() {
        mCheckedList.clear();
        notifyDataSetChanged();
    }

    public List<String> getCheckedList() {
        return mCheckedList;
    }

    static class FilterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cb_filter)
        CheckBox mCbFilter;

        public FilterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
