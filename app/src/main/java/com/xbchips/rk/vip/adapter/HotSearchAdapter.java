package com.xbchips.rk.vip.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;
import com.xbchips.rk.free.entity.VideoItem;

import java.util.ArrayList;
import java.util.List;

public class HotSearchAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<VideoItem> videoItemList;
    private LayoutInflater layoutInflater;
    private View.OnClickListener onClickListener;
    private Context mContext;
    private int[] colors;


    public HotSearchAdapter(Context context) {
        mContext = context;
        videoItemList = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        colors = new int[]{
                ContextCompat.getColor(mContext, R.color.se_item_seq1),
                ContextCompat.getColor(mContext, R.color.se_item_seq1_),
                ContextCompat.getColor(mContext, R.color.se_item_seq2),
                ContextCompat.getColor(mContext, R.color.se_item_seq2_),
                ContextCompat.getColor(mContext, R.color.se_item_seq3),
                ContextCompat.getColor(mContext, R.color.se_item_seq3_)
        };

    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void addDataList(List<VideoItem> videoItemList) {
        if (videoItemList == null || videoItemList.size() == 0)
            return;
        this.videoItemList.clear();
        this.videoItemList.addAll(videoItemList);
        notifyItemRangeChanged(0, videoItemList.size());
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new BaseViewHolder(layoutInflater.inflate(R.layout.item_hot_search, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder baseViewHolder, int i) {
        TextView indexTextView = baseViewHolder.getView(R.id.tv_hot_search_index);
        TextView contentTextView = baseViewHolder.getView(R.id.tv_hot_search_content);
        VideoItem bean = videoItemList.get(i);
        int index = i + 1;

        if (i == 0) {
            GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{colors[0], colors[1]});
            gradientDrawable.setShape(GradientDrawable.OVAL);
            indexTextView.setBackground(gradientDrawable);
            indexTextView.setTextColor(ContextCompat.getColor(mContext, R.color.se_top_text));
        } else if (i == 1) {
            GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{colors[2], colors[3]});
            gradientDrawable.setShape(GradientDrawable.OVAL);
            indexTextView.setBackground(gradientDrawable);
            indexTextView.setTextColor(ContextCompat.getColor(mContext, R.color.se_top_text));
        } else if (i == 2) {
            GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{colors[4], colors[5]});
            gradientDrawable.setShape(GradientDrawable.OVAL);
            indexTextView.setBackground(gradientDrawable);
            indexTextView.setTextColor(ContextCompat.getColor(mContext, R.color.se_top_text));
        } else {
            indexTextView.setBackgroundColor(Color.TRANSPARENT);
            indexTextView.setTextColor(ContextCompat.getColor(mContext, R.color.se_item_text));
        }
        indexTextView.setText(String.valueOf(index));
        contentTextView.setText(bean.getTitle());
        baseViewHolder.itemView.setTag(bean);
        baseViewHolder.itemView.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return videoItemList.size();
    }
}
