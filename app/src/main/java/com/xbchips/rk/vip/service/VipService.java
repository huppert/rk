package com.xbchips.rk.vip.service;

import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.base.response.PageResponse;
import com.xbchips.rk.free.entity.HotSearch;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.vip.entity.Category;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Harry on 2017/8/2.
 */

public interface VipService {

    /**
     * 获取视频列表
     */
    @GET("video/list/vip")
    Observable<ObjectResponse<PageResponse<VideoItem>>> getVideoList(@Query("page") int page, @Query("page_size") int pageSize,
                                                                     @Query("category_id") String category_id, @Query("tag") String tag, @Query("sort") String sort);


    /**
     * 获取分类列表
     */
    @GET("category/list")
    Observable<ObjectResponse<List<Category>>> getCategoryList();

    /**
     * 获取热搜
     */
    @GET("video/list/hotSearch")
    Observable<ObjectResponse<HotSearch>> getHotSearch();

    /**
     * 搜索接口
     */
    @GET("video/list/search")
    Observable<ObjectResponse<PageResponse<VideoItem>>> searchData(@Query("page") int page, @Query("page_size") int pageSize, @Query("content") String content);
}
