package com.xbchips.rk.vip.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xbchips.rk.R;

import java.util.ArrayList;
import java.util.List;

public class SearchHistoryAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private List<String> contentList;
    private LayoutInflater layoutInflater;
    private View.OnClickListener onClickListener;

    public SearchHistoryAdapter(Context context) {
        this.contentList = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public List<String> getContentList() {
        return contentList;
    }

    public void addDataList(List<String> beanList) {
        if (beanList == null || beanList.size() == 0)
            return;
        contentList.clear();
        contentList.addAll(beanList);
        notifyItemRangeChanged(0, contentList.size());
    }

    public void addData(String content) {
        if (contentList.contains(content)) {
            contentList.remove(content);
        }
        contentList.add(0, content);
        notifyItemRangeChanged(0, contentList.size());
    }

    public void clearData() {
        notifyItemRangeRemoved(0, contentList.size());
        contentList.clear();
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new BaseViewHolder(layoutInflater.inflate(R.layout.item_search, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder baseViewHolder, int i) {
        TextView textView = baseViewHolder.getView(R.id.tv_search_content);
        int index = i + 1;
        textView.setText(index + "\t\t\t" + contentList.get(i));
        textView.setTag(contentList.get(i));
        textView.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return contentList.size();
    }
}
