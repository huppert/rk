package com.xbchips.rk.vip.contract;

import com.xbchips.rk.base.BasePresenter;
import com.xbchips.rk.base.BaseView;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.base.response.PageResponse;
import com.xbchips.rk.free.entity.VideoItem;

import java.util.List;

/**
 * Created by Harry on 2017/7/31.
 */

public interface VipCategoryContract {

    interface View extends BaseView<Presenter> {
        void loadSuccess(ObjectResponse<PageResponse<VideoItem>> response);

        void loadHeaderSuccess(ObjectResponse<PageResponse<VideoItem>> response);

        void loadFailed(Throwable throwable);

        void loadHeaderFailed(Throwable throwable);
    }

    interface Presenter extends BasePresenter {
        /**
         * 加载视频列表
         *
         * @param page        当前页
         * @param pageSize    每页条数
         * @param category_id 类型id
         * @param tags        标签
         * @param sort        排序方式
         * @param isReverse   加载头部true,否则false
         */
        void getVideoList(int page, int pageSize,
                          String category_id, List<String> tags, String sort, boolean isReverse);

    }
}
