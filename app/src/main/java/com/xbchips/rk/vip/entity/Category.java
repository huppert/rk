package com.xbchips.rk.vip.entity;

import java.util.List;

/**
 * Created by Harry on 2017/8/3.
 */

public class Category {

    private String id;
    private String name;
    private List<String> tags;

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {

        return name;
    }


}
