package com.xbchips.rk.vip.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.xbchips.rk.R;
import com.xbchips.rk.base.BaseAdapter;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.video.widgets.ZQImageViewRoundOval;

import java.util.List;

public class SearchResultAdapter extends BaseAdapter<VideoItem> {
    public SearchResultAdapter(List<VideoItem> list, Context context) {
        super(list, context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case TYPE_HEADER:
                return new HeaderViewHolder(mHeaderView);
            case TYPE_FOOTER:
                return getFooterViewHolder(viewGroup);
            case TYPE_ITEM:
                return new SearchResultViewHolder(mInflater.inflate(R.layout.item_search_result, viewGroup, false));
        }
        return null;
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        SearchResultViewHolder viewHolder = (SearchResultViewHolder) holder;
        VideoItem videoItem = mList.get(getRealPosition(position));
        viewHolder.titleTextView.setText(videoItem.getTitle());
        viewHolder.dateTextView.setText(videoItem.getUpdated_at());
        Glide.with(mContext).load(videoItem.getThumb_img_url()).into(viewHolder.imageViewRoundOval);
        holder.itemView.setTag(videoItem);
        holder.itemView.setOnClickListener(mOnClickListener);
    }

    static class SearchResultViewHolder extends RecyclerView.ViewHolder {
        TextView titleTextView;
        TextView dateTextView;
        ZQImageViewRoundOval imageViewRoundOval;

        private SearchResultViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.tv_search_result_title);
            dateTextView = itemView.findViewById(R.id.tv_search_result_date);
            imageViewRoundOval = itemView.findViewById(R.id.iv_search_result_cover);
        }
    }

    public void clearData() {
        notifyItemRangeRemoved(0, mList.size());
        mList.clear();
    }

    public void removeHeader() {
        if (mHeaderSize > 0) {
            mHeaderSize--;
            notifyItemRemoved(0);
        }
    }
}
