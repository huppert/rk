package com.xbchips.rk.vip.adapter;

import android.util.SparseArray;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class BaseViewHolder extends RecyclerView.ViewHolder {
    private View view;
    private SparseArray<View> sparseArray;

    public BaseViewHolder(View itemView) {
        super(itemView);
        view = itemView;
        sparseArray = new SparseArray<>();
    }

    public <T extends View> T getView(int id) {
        T t = (T) sparseArray.get(id);
        if (t == null) {
            t = view.findViewById(id);
            sparseArray.put(id, t);
        }
        return t;
    }

    public void setText(int resId, String text) {
        TextView textView = getView(resId);
        textView.setText(text);
    }
}
