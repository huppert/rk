package com.xbchips.rk.vip.view;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.xbchips.rk.R;
import com.xbchips.rk.activities.HtmlActivity;
import com.xbchips.rk.base.BaseFragment;
import com.xbchips.rk.base.response.ObjectResponse;
import com.xbchips.rk.base.response.PageResponse;
import com.xbchips.rk.common.Constants;
import com.xbchips.rk.free.entity.VideoItem;
import com.xbchips.rk.video.VideoDetailActivity;
import com.xbchips.rk.vip.adapter.VipCategoryAdapter;
import com.xbchips.rk.vip.contract.VipCategoryContract;
import com.xbchips.rk.widgets.CustomSwipeRefreshLayout;
import com.xbchips.rk.widgets.EndlessRecyclerView;
import com.xbchips.rk.widgets.FastScrollBarView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.xbchips.rk.activities.HtmlActivity.ARG_TITLE;
import static com.xbchips.rk.activities.HtmlActivity._URL;

/**
 * Created by Harry on 2017/7/26.
 */

public class VipCategoryFragment extends BaseFragment implements VipCategoryContract.View {
    public static final String VIP_CATEGORY_ID = "vip_category_fragment_ID";
    public static final String IS_DEFAULT_SHOW = "is_default_show";
    @BindView(R.id.recycler_view)
    EndlessRecyclerView mRecyclerView;
    @BindView(R.id.refresh_layout)
    CustomSwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.fab_back_top)
    FloatingActionButton mFabBackTop;
    @BindView(R.id.fast_bar)
    FastScrollBarView mFastScrollBarView;

    private VipCategoryContract.Presenter mPresenter;
    private GridLayoutManager mGridLayoutManager;
    private VipCategoryAdapter mAdapter;
    private List<VideoItem> mList = new ArrayList<>();
    private String mCategoryId;
    private List<String> mTags = new ArrayList<>();
    private String mSort;
    /**
     * 是否清除列表所有数据
     */
    private boolean mIsClean;

    public static VipCategoryFragment newInstance(String category_id, boolean isDefaultShow) {
        Bundle args = new Bundle();
        args.putString(VIP_CATEGORY_ID, category_id);
        args.putBoolean(IS_DEFAULT_SHOW, isDefaultShow);
        VipCategoryFragment fragment = new VipCategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_category_vip, container, false);
        mUnBinder = ButterKnife.bind(this, rootView);
        mCategoryId = getArguments().getString(VIP_CATEGORY_ID, "");
        if (getArguments().getBoolean(IS_DEFAULT_SHOW)) {
            init();
        }
        return rootView;

    }

    @Override
    protected void init() {
        isInit = true;
        mGridLayoutManager = new GridLayoutManager(getActivity(), 2);
        mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                int realPosition = mAdapter.getRealPosition(position);
                if (position == mList.size() || (realPosition >= 0 && mList.get(realPosition).getIs_advert() == 1)) {
                    return 2;
                }
                return 1;
            }
        });
        mRecyclerView.setLayoutManager(mGridLayoutManager);
        mAdapter = new VipCategoryAdapter(mList, getActivity());
        mAdapter.setOnClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setOnLoadMoreListener(() -> {
            mAdapter.addFooter();
            loadData(false);
        });
        mRecyclerView.setOnLoadHeaderListener(() -> {
            mAdapter.addHeader();
            mPresenter.getVideoList(mRecyclerView.getHeadPage(), Constants.DEFAULT_PAGESIZE, mCategoryId, mTags, mSort, true);

        });

        mRecyclerView.setPageChangeListener(currentPage -> mFastScrollBarView.setPage(currentPage));

        mFabBackTop.setOnClickListener(this);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!mRecyclerView.isLoading()) {
                    mFastScrollBarView.setVisibility(View.GONE);
                }
                LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
                int position = layoutManager.findFirstCompletelyVisibleItemPosition();
                if (position == 0) {
                    mFabBackTop.setVisibility(View.INVISIBLE);
                }
            }
        });
        mRefreshLayout.post(() -> mRefreshLayout.setRefreshing(true));
        mRefreshLayout.setOnRefreshListener(this::refreshData);

        mFastScrollBarView.setOnMoveListener(new FastScrollBarView.OnMoveListener() {
            @Override
            public void move(float ratio) {
            }

            @Override
            public void finish(float ratio) {
                int curPage = (int) (mRecyclerView.getPageCount() * ratio);
                int max = Math.max(1, curPage);
                //非当前页才加载
                if (mRecyclerView.getCurPage() != max) {
                    mRecyclerView.setCurPage(max);
                    loadData(true);
                }
            }
        });

        loadData(false);
    }


    private void refreshData() {
        mRecyclerView.scrollToPosition(0);
        mRefreshLayout.setRefreshing(true);
        mRecyclerView.setLoading(true);
        mRecyclerView.setCurPage(1);
        loadData(false);
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.image_layout:
                VideoItem videoItem = (VideoItem) v.getTag(R.id.glideId);
                if (videoItem.getIs_advert() == 1) {
                    Intent intent = new Intent(getContext(), HtmlActivity.class);
                    intent.putExtra(ARG_TITLE, videoItem.getTitle());
                    intent.putExtra(_URL, videoItem.getUrl());
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getContext(), VideoDetailActivity.class);
                    intent.putExtra(VideoDetailActivity.ARG_VIDEO_ID, videoItem.getId());
                    intent.putExtra(VideoDetailActivity.ARG_VIDEO_TITLE, videoItem.getTitle());
                    startActivity(intent);
                }
                break;
            case R.id.fab_back_top:
                mRecyclerView.scrollToPosition(0);
                break;
        }
    }

    private void loadData(boolean isClean) {
        mIsClean = isClean;
        mPresenter.getVideoList(mRecyclerView.getCurPage(), Constants.DEFAULT_PAGESIZE, mCategoryId, mTags, mSort, false);
    }


    public void setFastBarVisibility() {
        int visibility = mFastScrollBarView.getVisibility();
        mFastScrollBarView.setVisibility(visibility == View.VISIBLE ? View.GONE : View.VISIBLE);
    }

    public void setSort(String sort) {
        mSort = sort;
        if (mAdapter != null)
            refreshData();
    }


    @Override
    public void setPresenter(VipCategoryContract.Presenter presenter) {
        mPresenter = presenter;

    }

    @Override
    public void loadSuccess(ObjectResponse<PageResponse<VideoItem>> response) {


        if (mIsClean && !handleResponse(response)) {
            mList.clear();
            PageResponse<VideoItem> data = response.getData();
            List<VideoItem> list = data.getList();
            mRecyclerView.setPageCount(data.getPage().getTotal());
            mRecyclerView.setHeadPage(mRecyclerView.getCurPage());
            mFastScrollBarView.setPage(mRecyclerView.getCurPage());
            mList.addAll(list);
            mAdapter.notifyDataSetChanged();
            //快速定位后，向下偏移10像素，便于向上滑动时自动加载上一页
            mRecyclerView.setLoading(true);
            mRecyclerView.scrollBy(0, 10);
            mRecyclerView.setLoading(false);
        } else {
            if (mRecyclerView.getCurPage() == 1) {
                mList.clear();
            }
            mAdapter.removeFooter();
            if (!handleResponse(response)) {
                PageResponse<VideoItem> data = response.getData();
                List<VideoItem> list = data.getList();
                mRecyclerView.setPageCount(data.getPage().getTotal());
                mList.addAll(list);
                int size = list.size();
                if (mRecyclerView.getCurPage() == 1) {
                    mFastScrollBarView.setCirclePosition(mRecyclerView.getPageCount());
                    mFastScrollBarView.setPage(1);
                    mRefreshLayout.setRefreshing(false);
                    mAdapter.notifyDataSetChanged();
                } else {
                    mAdapter.notifyItemRangeInserted(mList.size() - size, size);
                }
                if (mRecyclerView.isLastPage()) {
                    mAdapter.addNoMoreView();
                }
            }
        }
        mRecyclerView.setLoading(false);
    }

    @Override
    public void loadHeaderSuccess(ObjectResponse<PageResponse<VideoItem>> response) {
        mAdapter.removeHeader();
        if (!handleResponse(response)) {
            PageResponse<VideoItem> data = response.getData();
            List<VideoItem> list = data.getList();
            mList.addAll(0, list);
            mAdapter.notifyItemRangeInserted(0, list.size());
        }
        mRecyclerView.setLoading(false);
    }

    @Override
    public void loadFailed(Throwable throwable) {
        handleFailure(throwable);
        mRecyclerView.setLoading(false);
        mRefreshLayout.setRefreshing(false);
        if (mRecyclerView.getCurPage() > 1 && !mIsClean) {
            mRecyclerView.pageDecrement();
            mAdapter.removeFooter();
        }
    }

    @Override
    public void loadHeaderFailed(Throwable throwable) {
        handleFailure(throwable);
        mRecyclerView.setLoading(false);
        mRecyclerView.headPageIncrement();
        mAdapter.removeHeader();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.unSubscribe();
    }

}
