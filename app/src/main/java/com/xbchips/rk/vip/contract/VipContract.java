package com.xbchips.rk.vip.contract;

import com.xbchips.rk.base.BasePresenter;
import com.xbchips.rk.base.BaseView;
import com.xbchips.rk.vip.entity.Category;

import java.util.List;

/**
 * Created by Harry on 2017/7/26.
 */

public interface VipContract {
    interface View extends BaseView<Presenter> {

        void getCategorySuccess(List<Category> categoryList);

    }

    interface Presenter extends BasePresenter {

        void getCategoryList();


    }

}
