package com.xbchips.rk.vip.presenter;

import com.xbchips.rk.base.RxUtils;
import com.xbchips.rk.common.HttpRequest;
import com.xbchips.rk.vip.contract.VipContract;
import com.xbchips.rk.vip.service.VipService;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Harry on 2017/7/26.
 */

public class VipPresenter implements VipContract.Presenter {

    private final VipService mVipService;
    private VipContract.View mVipView;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    public VipPresenter(VipContract.View view) {
        mVipView = view;
        mVipService = HttpRequest.createService(VipService.class);
    }

    public void getCategoryList() {
        Disposable disposable = mVipService.getCategoryList()
                .compose(RxUtils.applySchedulers())
                .filter(response -> !mVipView.handleResponse(response))
                .subscribe(response -> mVipView.getCategorySuccess(response.getData()),
                        throwable -> mVipView.handleFailure(throwable));
        mCompositeDisposable.add(disposable);

    }

    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }


}
